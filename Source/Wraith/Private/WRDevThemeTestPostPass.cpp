#include "Wraith.h"
#include "LGGeneratingLevel.h"
#include "LGGeneratedLevel.h"
#include "LGSpawnablePrefabArea.h"
#include "LGPrefabSlotComponentAreaPoint.h"
#include "WRDevThemeTestPostPass.h"

void UWRDevThemeTestPostPass::PostPassEvent(class ALGGeneratingLevel * GeneratingLevel,
	const TArray<FName>& PassNames,
	FRandomStream& RandomStream) const
{
	//only spawn things on the server
	if (GeneratingLevel->GetNetMode() != NM_Client)
	{
		//spawn player start locations in the first room
		ALGAreaInfo * StartRoom = GeneratingLevel->GeneratedLevel->AreaInfos[0];

		for (auto Iter = StartRoom->AreaPoints.CreateConstKeyIterator(FName(TEXT("PlayerStart"))); Iter; ++Iter)
		{
			auto Component = Iter.Value();

			APlayerStart * PlayerStart = GeneratingLevel->GetWorld()->SpawnActor<APlayerStart>(APlayerStart::StaticClass(), Component->GetComponentToWorld().GetLocation(), Component->GetComponentToWorld().Rotator());
		}
	}
}
#include "Wraith.h"
#include "WRInventoryItemWidget.h"
//#include "WRInventoryItem.h"
#include "WRDamageType.h"
#include "WRImpactType.h"
#include "WRProjectile.h"
#include "WRGameplayStatics.h"

////////////////////////////
//Team

FGenericTeamId UWRGameplayStatics::GetTeam(TEnumAsByte<WRTeam::Type> Team)
{
	switch (Team)
	{
	case WRTeam::HUMANS:
		return WRHumansTeam;

	case WRTeam::MONSTERS:
		return WRMonstersTeam;

	default:
		return FGenericTeamId::NoTeam;
	}
}

FGenericTeamId UWRGameplayStatics::GetTeamForId(uint8 TeamId)
{
	switch (TeamId)
	{
	case WRTeam::HUMANS:
		return WRHumansTeam;

	case WRTeam::MONSTERS:
		return WRMonstersTeam;

	case WRTeam::INVALID:
		return FGenericTeamId::NoTeam;

	default:
		return FGenericTeamId(TeamId);
	}
}

void UWRGameplayStatics::SetObjectTeam(UObject * Object, TEnumAsByte<WRTeam::Type> Team)
{
	IGenericTeamAgentInterface * ObjectTeam = Cast<IGenericTeamAgentInterface>(Object);

	if (ObjectTeam)
	{
		ObjectTeam->SetGenericTeamId(GetTeam(Team));
	}
}

void UWRGameplayStatics::SetObjectTeamForId(UObject * Object, uint8 TeamId)
{
	IGenericTeamAgentInterface * ObjectTeam = Cast<IGenericTeamAgentInterface>(Object);

	if (ObjectTeam)
	{
		ObjectTeam->SetGenericTeamId(GetTeamForId(TeamId));
	}
}

////////////////////////////
//AI

void UWRGameplayStatics::DefaultUpdatePotentialTargetInfo(FWRAIPotentialTargetInfo& PotentialTargetInfo, float CurrentWorldTime)
{
	PotentialTargetInfo.DefaultUpdatePotentialTargetInfo(CurrentWorldTime);
}

////////////////////////////
//Inventory

struct InventoryByTypeSortPredicate
{
	bool operator()(const UWRInventoryItemWidget& Value, const UWRInventoryItemWidget& OtherValue) const
	{
		//TODO: for now sorts by pointer
		return Value < OtherValue;
	}
};

//TODO: make this a function that's part of TArray itself and do a pull request into the engine
template<typename ElementType, class PREDICATE_CLASS>
int32 InsertIntoArraySorted(const ElementType& Element, TArray<ElementType>& Array, const PREDICATE_CLASS & Predicate)
{
	int32 Ind = 0;
	
	for (; Ind < Array.Num(); ++Ind)
	{
		if (Predicate(*Element, *Array[Ind]))
		{
			break;
		}
	}

	return Array.Insert(Element, Ind);
}

//void UWRGameplayStatics::SortInventoryWidgetsByType(TArray<UWRInventoryItemWidget *>& InventoryWidgetArray)
//{
//	InventoryWidgetArray.Sort(InventoryByTypeSortPredicate());
//}
//
//int32 UWRGameplayStatics::InsertIntoWidgetsByType(UWRInventoryItemWidget * const Widget, TArray<UWRInventoryItemWidget *>& InventoryWidgetArray)
//{
//	return InsertIntoArraySorted<UWRInventoryItemWidget *>(Widget, InventoryWidgetArray, InventoryByTypeSortPredicate());
//}

/////////////////////////////
//Projectiles Shooting

AWRProjectile * UWRGameplayStatics::ShootProjectile(TSubclassOf<AWRProjectile> ProjectileClass, const FTransform& SpawnTransform, AActor * Creator, UWorld * World)
{
	//TODO: handle all the network lag related stuff similar to how UT does it
	AWRProjectile * Projectile = NULL;

	if (!World && Creator)
	{
		World = Creator->GetWorld();
	}

	check(World); 

	{
		Projectile = World->SpawnActorDeferred<AWRProjectile>(ProjectileClass, SpawnTransform, Creator, Cast<APawn>(Creator));

		if (Projectile)
		{
			Projectile->Creator = Creator;
			Projectile->FinishSpawning(SpawnTransform);
		}
	}

	return Projectile;
}

bool UWRGameplayStatics::PerformSingleHitscanTrace(struct FHitResult& OutHit,
	const FTransform& OriginTransform,
	bool bTraceComplex,
	ECollisionChannel TraceChannel,	
	const AActor * Creator,
	float MaxDistance,
	UWorld * World,
	bool bDebugDraw)
{
	if (!World && Creator)
	{
		World = Creator->GetWorld();
	}

	check(World);

	FVector Direction = OriginTransform.GetRotation().Vector();

	FVector EndLocation = OriginTransform.GetLocation() + Direction * MaxDistance;

	FCollisionQueryParams QueryParams;

	QueryParams.bTraceComplex = bTraceComplex;

	if (Creator)
	{
		QueryParams.OwnerTag = FName(*Creator->GetName());
		QueryParams.TraceTag = QueryParams.OwnerTag;
		QueryParams.AddIgnoredActor(Creator);
	}

	bool bDidHit = World->LineTraceSingleByChannel(OutHit, OriginTransform.GetLocation(), EndLocation, TraceChannel, QueryParams, FCollisionResponseParams::DefaultResponseParam);
	
	if (!bDidHit)
	{
		OutHit.Location = EndLocation;
		OutHit.ImpactPoint = EndLocation;
		OutHit.ImpactNormal = -Direction;
	}

	//Draw the straight line
	if (bDebugDraw)
	{
		DrawDebugLine(
			World,
			OriginTransform.GetLocation(),
			OutHit.Location,
			FColor::White,
			false, 1.f, 0,
			1);

		DrawDebugPoint(
			World,
			OriginTransform.GetLocation(),
			10,  					//size
			FColor::Green,
			false, 1.f);

		DrawDebugPoint(
			World,
			OutHit.Location,
			10,  					//size
			FColor::Red,
			false, 1.f);
	}

	return bDidHit;
}

bool UWRGameplayStatics::ShootSingleHitscanTrace(struct FHitResult& OutHit,
	const FTransform& OriginTransform,
	TSubclassOf<UWRDamageType> DamageType,
	TSubclassOf<UWRImpactType> ImpactType,
	AActor * Creator,
	float DamageMultiplier,
	float ExplosionRadiusMultiplier,
	float MaxDistance,
	UWorld * World,
	bool bDebugDraw)
{
	bool bDidHit = PerformSingleHitscanTrace(OutHit, 
		OriginTransform,
		true,
		COLLISION_TRACE_WEAPON,
		Creator,
		MaxDistance,
		World,
		bDebugDraw);

	if (bDidHit)
	{		
		if (DamageType)
		{
			FVector ImpactDirection = OriginTransform.GetLocation();
			ImpactDirection.Normalize();

			AActor * HitActor = OutHit.Actor.Get();
			
			AController * EventInstigator = NULL;
			const APawn * InstigatorPawn = Cast<APawn>(Creator);

			if (InstigatorPawn)
			{
				EventInstigator = InstigatorPawn->GetController();
			}

			DamageType->GetDefaultObject<UWRDamageType>()->ApplyDamage(Creator,
				HitActor,
				DamageMultiplier,
				ExplosionRadiusMultiplier,
				ImpactDirection,
				OutHit,
				EventInstigator,
				Creator);
		}

		if (ImpactType)
		{
			ImpactType.GetDefaultObject()->SpawnImpactEffects(World,
				OutHit,
				OriginTransform.GetRotation().Vector() * 1000.f);	//TODO: (ilselets) Allow passing speed for passing impact velocity to effect spawning
		}
	}

	return bDidHit;
}
#include "Wraith.h"
#include "LGGameMode.h"
#include "LGGameSession.h"
#include "LGGameState.h"
#include "LGPlayerController.h"
#include "LGPlayerState.h"
#include "LGGeneratingLevel.h"

namespace MatchState
{
	const FName GeneratingMap = FName(TEXT("GeneratingMap"));
	const FName GeneratedMapWaitingToStart = FName(TEXT("GeneratedMapWaitingToStart"));
}

ALGGameMode::ALGGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	GameStateClass = ALGGameState::StaticClass();
	PlayerStateClass = ALGPlayerState::StaticClass();
	PlayerControllerClass = ALGPlayerController::StaticClass();
}

void ALGGameMode::Tick(float DeltaSeconds)
{
	Super::Super::Tick(DeltaSeconds);

	if (GetMatchState() == MatchState::GeneratedMapWaitingToStart)
	{
		// Check to see if we should start the match
		if (ReadyToStartMatch())
		{
			StartMatch();
		}
	}
	if (GetMatchState() == MatchState::InProgress)
	{
		// Check to see if we should start the match
		if (ReadyToEndMatch())
		{
			EndMatch();
		}
	}
}

TSubclassOf<class AGameSession> ALGGameMode::GetGameSessionClass() const
{
	return ALGGameSession::StaticClass();
}

void ALGGameMode::GetSeamlessTravelActorList(bool bToEntry, TArray<AActor*>& ActorList)
{
	Super::GetSeamlessTravelActorList(bToEntry, ActorList);
}

void ALGGameMode::PostSeamlessTravel()
{
	Super::PostSeamlessTravel();

	//instantiate campaign state if one doesn't exist
}

void ALGGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	//tell new player the seed to generate
	if (MatchState == MatchState::EnteringMap ||
		MatchState == MatchState::WaitingToStart)
	{
		return;
	}

	ALGPlayerController * LGPlayerController = Cast<ALGPlayerController>(NewPlayer);
	ALGGameState * LGGameState = Cast<ALGGameState>(GameState);

	if (LGGameState && LGPlayerController)
	{
		LGPlayerController->NotifyMapGenerateStart(LGGameState->MapSeed);
	}
}

void ALGGameMode::RemotePlayerControllerMapReady(APlayerController * PlayerController)
{
	GeneratedMapPlayerControllers.Add(PlayerController);

	//reset start position to not be some far away location beyond netCullDistance
	if (HasServerMapLoaded())
	{
		ResetPlayerControllerStart(PlayerController);
	}	
}

void ALGGameMode::ResetPlayerControllerStart(APlayerController * PlayerController)
{
	AActor * Start = FindPlayerStart(PlayerController);
	ALGPlayerController * LGPlayerController = Cast<ALGPlayerController>(PlayerController);

	if (Start)
	{
		//TODO: there was a crash here at one point because GetPawnOrSpectator was NULL
		PlayerController->GetPawnOrSpectator()->TeleportTo(Start->GetTransform().GetLocation(), Start->GetTransform().Rotator());

		if (LGPlayerController)
		{
			LGPlayerController->SetSpawnLocation(Start->GetTransform().GetLocation());
		}
	}
	else
	{
		PlayerController->GetPawnOrSpectator()->TeleportTo(FVector(0.f), FRotator(0.f));

		if (LGPlayerController)
		{
			LGPlayerController->SetSpawnLocation(FVector(0.f));
		}
	}
}

void ALGGameMode::LocalMapReady()
{
	check(MatchState == MatchState::GeneratingMap);

	SetMatchState(MatchState::GeneratedMapWaitingToStart);
}

void ALGGameMode::StartPlay()
{
	if (MatchState == MatchState::EnteringMap)
	{
		SetMatchState(MatchState::WaitingToStart);
	}

	// Check to see if we should immediately transfer to match start
	if (MatchState == MatchState::GeneratedMapWaitingToStart && ReadyToStartMatch())
	{
		StartMatch();
	}
}

bool ALGGameMode::HasMatchStarted() const
{
	if (GetMatchState() == MatchState::EnteringMap 
		|| GetMatchState() == MatchState::WaitingToStart 
		|| GetMatchState() == MatchState::GeneratingMap
		|| GetMatchState() == MatchState::GeneratedMapWaitingToStart)
	{
		return false;
	}

	return true;
}

bool ALGGameMode::HasServerMapLoaded() const
{
	if (GetMatchState() == MatchState::EnteringMap
		|| GetMatchState() == MatchState::WaitingToStart
		|| GetMatchState() == MatchState::GeneratingMap)
	{
		return false;
	}

	return true;
}

void ALGGameMode::SetMatchState(FName NewState)
{
	if (MatchState == NewState)
	{
		return;
	}

	MatchState = NewState;

	// Call change callbacks

	if (MatchState == MatchState::WaitingToStart)
	{
		HandleMatchIsWaitingToStart();
	}
	else if (MatchState == MatchState::GeneratingMap)
	{
		HandleMatchGeneratingMap();
	}
	else if (MatchState == MatchState::GeneratedMapWaitingToStart)
	{
		HandleMatchGeneratedMapIsWaitingToStart();
	}
	else if (MatchState == MatchState::InProgress)
	{
		HandleMatchHasStarted();
	}
	else if (MatchState == MatchState::WaitingPostMatch)
	{
		HandleMatchHasEnded();
	}
	else if (MatchState == MatchState::LeavingMap)
	{
		HandleLeavingMap();
	}
	else if (MatchState == MatchState::Aborted)
	{
		HandleMatchAborted();
	}

	if (GameState)
	{
		AGameState * GS = Cast<AGameState>(GameState);

		if (GS)
		{
			GS->SetMatchState(NewState);
		}
	}
}

void ALGGameMode::HandleMatchIsWaitingToStart()
{
	//just call begin play on the world, I'm not sure what reprecussions I'll run into if I force call begin play only on Generating Level
	//world should be empty anyway besides that actor
	GetWorldSettings()->NotifyBeginPlay();

	SetMatchState(MatchState::GeneratingMap);
}

bool ALGGameMode::ReadyToStartMatch()
{
	// If bDelayed Start is set, wait for a manual match start
	if (bDelayedStart)
	{
		return false;
	}

	// By default start when we have > 0 players
	if (GetMatchState() == MatchState::GeneratedMapWaitingToStart)
	{
		if (NumPlayers + NumBots > 0)
		{
			return true;
		}
	}
	return false;
}

void ALGGameMode::HandleMatchGeneratingMap()
{
	ALGGameState * LGGameState = Cast<ALGGameState>(GameState);
	
	//TODO: choose seed from the campaign state when I figure that stuff out
	//TODO: handle clients
	LGGameState->MapSeed = 0;
		
	bool bFoundGeneratingLevel = false;
	for (TActorIterator<ALGGeneratingLevel> Iter(GetWorld()); Iter; ++Iter)
	{
		if (bFoundGeneratingLevel)
		{
			UE_LOG(LG, Error, TEXT("More than one instace of ALGGeneratingLevel found in the map"));
			continue;
		}

		Iter->ReceiveSeed(LGGameState->MapSeed);

		bFoundGeneratingLevel = true;
	}

	if (bFoundGeneratingLevel)
	{
		//notify player controllers
		for (auto Iter = GetWorld()->GetPlayerControllerIterator(); Iter; ++Iter)
		{
			ALGPlayerController * LGPlayerController = Cast<ALGPlayerController>(*Iter);

			if (LGPlayerController)
			{
				LGPlayerController->NotifyMapGenerateStart(LGGameState->MapSeed);
			}
		}
	}
	else
	{
		LocalMapReady();
	}
}

void ALGGameMode::HandleMatchGeneratedMapIsWaitingToStart()
{
	//tell all player controllers who already had their maps generated to start at a valid location
	for (auto Iter = GeneratedMapPlayerControllers.CreateConstIterator(); Iter; ++Iter)
	{
		ResetPlayerControllerStart(*Iter);
	}

	Super::HandleMatchIsWaitingToStart();
}
#include "Wraith.h"
#include "LGPlayerState.h"
#include "UnrealNetwork.h"

void ALGPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(ALGPlayerState, MapGeneratedPercentage);
}


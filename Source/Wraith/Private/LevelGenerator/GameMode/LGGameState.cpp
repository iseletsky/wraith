#include "Wraith.h"
#include "LGGameState.h"
#include "LGGameMode.h"
#include "UnrealNetwork.h"

void ALGGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(ALGGameState, MapSeed);
}

bool ALGGameState::HasMatchStarted() const
{
	if (GetMatchState() == MatchState::EnteringMap
		|| GetMatchState() == MatchState::WaitingToStart
		|| GetMatchState() == MatchState::GeneratingMap
		|| GetMatchState() == MatchState::GeneratedMapWaitingToStart)
	{
		return false;
	}

	return true;
}

void ALGGameState::OnRep_MatchState()
{
	if (MatchState == MatchState::WaitingToStart 
		|| MatchState == MatchState::GeneratingMap
		|| MatchState == MatchState::GeneratedMapWaitingToStart
		|| PreviousMatchState == MatchState::EnteringMap)
	{
		// Call MatchIsWaiting to start even if you join in progress at a later state
		HandleMatchIsWaitingToStart();
	}

	if (MatchState == MatchState::InProgress)
	{
		HandleMatchHasStarted();
	}
	else if (MatchState == MatchState::WaitingPostMatch)
	{
		HandleMatchHasEnded();
	}
	else if (MatchState == MatchState::LeavingMap)
	{
		HandleLeavingMap();
	}

	PreviousMatchState = MatchState;
}
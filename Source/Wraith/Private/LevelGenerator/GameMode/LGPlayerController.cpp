#include "Wraith.h"
#include "LGPlayerController.h"
#include "LGPlayerState.h"
#include "LGGameMode.h"
#include "LGGeneratingLevel.h"
#include "UnrealNetwork.h"

ALGPlayerController::ALGPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	ClientMapState = EClientMapState::AWAITING_SEED;

	//kindof a dirty hack for now so the actor spawns somewhere really far away from the action and won't have things replicated to it until random map is loaded
	SetSpawnLocation(FVector(WORLD_MAX));
}

void ALGPlayerController::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(ALGPlayerController, ClientMapState);
}

void ALGPlayerController::NotifyMapGenerateStart(int32 MapSeed)
{
	check(GetNetMode() != NM_Client);

	ClientMapState = EClientMapState::GENERATING;
	ClientNotifyMapGenerateStart(MapSeed);
}

void ALGPlayerController::ClientNotifyMapGenerateStart_Implementation(int32 MapSeed)
{
	if (GetNetMode() == NM_Client)
	{
		bool bFoundGeneratingLevel = false;
		for (TActorIterator<ALGGeneratingLevel> Iter(GetWorld()); Iter; ++Iter)
		{
			if (bFoundGeneratingLevel)
			{
				UE_LOG(LG, Error, TEXT("More than one instace of ALGGeneratingLevel found in the map"));
				continue;
			}

			Iter->ReceiveSeed(MapSeed);

			bFoundGeneratingLevel = true;
		}

		//If didn't find generating level go to the done state
		if (!bFoundGeneratingLevel)
		{
			ServerNotifyMapGenerated();
		}
	}
}

bool ALGPlayerController::ServerNotifyMapGeneratedPercentage_Validate(float InMapGeneratedPercentage)
{
	return ClientMapState == EClientMapState::GENERATING;
}

void ALGPlayerController::ServerNotifyMapGeneratedPercentage_Implementation(float InMapGeneratedPercentage)
{
	ALGPlayerState * LGPlayerState = Cast<ALGPlayerState>(PlayerState);

	if (LGPlayerState)
	{
		LGPlayerState->MapGeneratedPercentage = InMapGeneratedPercentage;
	}
}

bool ALGPlayerController::ServerNotifyMapGenerated_Validate()
{
	return ClientMapState == EClientMapState::GENERATING;
}

void ALGPlayerController::ServerNotifyMapGenerated_Implementation()
{
	ALGPlayerState * LGPlayerState = Cast<ALGPlayerState>(PlayerState);

	if (LGPlayerState)
	{
		LGPlayerState->MapGeneratedPercentage = 100.f;
	}

	ClientMapState = EClientMapState::GENERATED;
	
	ALGGameMode * GameMode = Cast<ALGGameMode>(GetWorld()->GetAuthGameMode());

	if (GameMode)
	{
		GameMode->RemotePlayerControllerMapReady(this);
	}
}

void ALGPlayerController::SetSpawnLocation(const FVector& NewLocation)
{
	Super::SetSpawnLocation(NewLocation);
}

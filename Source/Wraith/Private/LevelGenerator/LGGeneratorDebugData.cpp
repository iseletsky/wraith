#include "Wraith.h"
#include "LGGeneratingLevel.h"
#include "LGSpawnablePrefab.h"
#include "LGGeneratorDebugData.h"

ALGGeneratorDebugData::ALGGeneratorDebugData(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	CurrentStep(0)
{
	RootComponent = ObjectInitializer.CreateDefaultSubobject<USceneComponent, USceneComponent>(this, TEXT("DummyRoot"));
	
	GeneratorSteps.Emplace();

	SetActorEnableCollision(false);
}

void ALGGeneratorDebugData::BeginPlay()
{
	Super::BeginPlay();

	InputComponent = NewObject<UInputComponent>(this, UInputComponent::StaticClass());
	
	//set up the bindings
	InputComponent->BindAction("LG_RestartVis", IE_Released, this, &ALGGeneratorDebugData::RestartVis);
	InputComponent->BindAction("LG_NextVisStep", IE_Released, this, &ALGGeneratorDebugData::NextVisStep);
	InputComponent->BindAction("LG_PrevVisStep", IE_Released, this, &ALGGeneratorDebugData::PrevVisStep);
	InputComponent->BindAction("LG_FinalVisStep", IE_Released, this, &ALGGeneratorDebugData::FinalVisStep);

	InputComponent->RegisterComponent();

	EnableInput(GetWorld()->GetFirstPlayerController());
}

void ALGGeneratorDebugData::RestartVis()
{
	GoToStep(0);
}

void ALGGeneratorDebugData::NextVisStep()
{
	GoToStep(GetCurrentStep() + 1);
}

void ALGGeneratorDebugData::PrevVisStep()
{
	GoToStep(GetCurrentStep() - 1);
}

void ALGGeneratorDebugData::FinalVisStep()
{
	GoToStep(GetNumSteps() - 1);
}

void ALGGeneratorDebugData::AddObjectToStep(TSubclassOf<ALGSpawnablePrefab> InSpawnablePrefabClass, FTransform InTransform, ALGGeneratingLevel * GeneratingLevel)
{
	ALGSpawnablePrefab * Spawnable = GeneratingLevel->GetSpawnableInstance<ALGSpawnablePrefab>(InSpawnablePrefabClass);
	
	//add the area collision geometry
	for (int32 AreaCollisionIndex = 0; AreaCollisionIndex < Spawnable->AreaCollision.Num(); ++AreaCollisionIndex)
	{
		auto AreaCollisionComp = Spawnable->AreaCollision[AreaCollisionIndex];
		
		UPrimitiveComponent* NewComp = NewObject<UPrimitiveComponent>(this, AreaCollisionComp->GetClass(), NAME_None, RF_NoFlags, AreaCollisionComp);
		NewComp->SetupAttachment(GetRootComponent());
		//NewComp->AttachChildren.Empty();
		NewComp->SetRelativeTransform(AreaCollisionComp->GetComponentToWorld() * InTransform);
		NewComp->bHiddenInGame = false;

		NewComp->RegisterComponent();		

		GeneratorSteps[CurrentStep].Components.Add(NewComp);

		//try to color the object if it's a shape
		UShapeComponent * ShapeComp = Cast<UShapeComponent>(NewComp);

		if (ShapeComp)
		{
			if (ShapeComp->ComponentHasTag(COMPONENT_AREA_OMIT_SPAWN_COLLISION_TAG))
			{
				ShapeComp->ShapeColor = FColor::Yellow;
			}
			else if (NewComp->ComponentHasTag(COMPONENT_AREA_SPAWN_COLLISION_TAG))
			{
				ShapeComp->ShapeColor = FColor::Green;
			}
			else
			{
				ShapeComp->ShapeColor = FColor::Red;
			}
		}
	}

	//add all the components for the object being added

	TArray<USceneComponent *> SceneComponents;
	Spawnable->GetRootComponent()->GetChildrenComponents(true, SceneComponents);
    SceneComponents.Add(Spawnable->GetRootComponent());

	for (int32 CurrComp = 0; CurrComp < SceneComponents.Num(); ++CurrComp)
	{
		auto BaseComp = SceneComponents[CurrComp];

		USceneComponent* NewComp = NewObject<USceneComponent>(this, BaseComp->GetClass(), NAME_None, RF_NoFlags, BaseComp);
		NewComp->SetupAttachment(GetRootComponent());
		//NewComp->AttachChildren.Empty();
		NewComp->SetRelativeTransform(BaseComp->GetComponentToWorld() * InTransform);
		NewComp->bHiddenInGame = false;

		NewComp->RegisterComponent();

		GeneratorSteps[CurrentStep].Components.Add(NewComp);
	}
}

void ALGGeneratorDebugData::AddAreaIndexToStep(int32 AreaIndex)
{
	GeneratorSteps[CurrentStep].AreaIndices.Add(AreaIndex);
}

void ALGGeneratorDebugData::AddLogMessage(const FString& LogMessage)
{
	GeneratorSteps[CurrentStep].LogMessages.Add(LogMessage);
}

void ALGGeneratorDebugData::EndAddingToStep(bool bSuccessfulStep)
{
	GeneratorSteps[CurrentStep].bIsSuccessful = bSuccessfulStep;
	GeneratorSteps.Emplace();

	++CurrentStep;
}

int32 ALGGeneratorDebugData::GetCurrentStep()
{
	return CurrentStep;
}

int32 ALGGeneratorDebugData::GetNumSteps()
{
	return GeneratorSteps.Num();
}

void ALGGeneratorDebugData::GoToStep(int32 Step)
{
	if (Step < 0)
	{
		Step = 0;
	}
	else if (Step > GeneratorSteps.Num() - 1)
	{
		Step = GeneratorSteps.Num() - 1;
	}

	if (CurrentStep == Step)
	{
		return;
	}

	//make current step components not visible if the step wasn't successful
	if (!GeneratorSteps[CurrentStep].bIsSuccessful)
	{
		for (int32 CurrComp = 0; CurrComp < GeneratorSteps[CurrentStep].Components.Num(); ++CurrComp)
		{
			GeneratorSteps[CurrentStep].Components[CurrComp]->bHiddenInGame = true;
			GeneratorSteps[CurrentStep].Components[CurrComp]->SetVisibility(false);
		}
	}

	//if going backward, make all components not visible, then set current step components visible if that step wasn't successful
	if (Step < CurrentStep)
	{
		for (int32 CurrStep = CurrentStep; CurrStep > Step; --CurrStep)
		{
			for (int32 CurrComp = 0; CurrComp < GeneratorSteps[CurrStep].Components.Num(); ++CurrComp)
			{
				GeneratorSteps[CurrStep].Components[CurrComp]->bHiddenInGame = true;
				GeneratorSteps[CurrStep].Components[CurrComp]->SetVisibility(false);
			}
		}

		if (!GeneratorSteps[Step].bIsSuccessful)
		{
			for (int32 CurrComp = 0; CurrComp < GeneratorSteps[Step].Components.Num(); ++CurrComp)
			{
				GeneratorSteps[Step].Components[CurrComp]->bHiddenInGame = false;
				GeneratorSteps[Step].Components[CurrComp]->SetVisibility(true);
			}
		}
	}
	else
	{
		//if going forward, reset all created successful step components to visible
		for (int32 CurrStep = CurrentStep; CurrStep <= Step; ++CurrStep)
		{
			if (GeneratorSteps[CurrStep].bIsSuccessful || CurrStep == Step)
			{
				for (int32 CurrComp = 0; CurrComp < GeneratorSteps[CurrStep].Components.Num(); ++CurrComp)
				{
					GeneratorSteps[CurrStep].Components[CurrComp]->bHiddenInGame = false;
					GeneratorSteps[CurrStep].Components[CurrComp]->SetVisibility(true);
				}
			}
		}
	}
	
	CurrentStep = Step;

	//print the log messages
	UE_LOG(LG, Log, TEXT("=== Printing debug messages for playback step: %d"), CurrentStep);
	GEngine->AddOnScreenDebugMessage(-1, 99.f, FColor::Yellow, FString::Printf(TEXT("=== Printing debug messages for playback step: %d"), CurrentStep));

	for (int32 LogMessage = 0; LogMessage < GeneratorSteps[CurrentStep].LogMessages.Num(); ++LogMessage)
	{
		UE_LOG(LG, Log, TEXT("%s"), *(GeneratorSteps[CurrentStep].LogMessages[LogMessage]));
		GEngine->AddOnScreenDebugMessage(-1, 99.f, FColor::White, GeneratorSteps[CurrentStep].LogMessages[LogMessage]);
	}
}
#include "Wraith.h"
#include "LGGeneratedLevel.h"

ALGGeneratedLevel::ALGGeneratedLevel(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	
}

void ALGGeneratedLevel::GetAreaNodePathInfos(int32 SourceAreaIndex, const TSet<FName>& AreaNodeTags, AreaNodePathInfos& Destination)
{
	TSet<int32> UnprocessedNeighborsSet;

	//add the source area index to the unprocessed neighbors set
	if (!AreaNodeTags.Num() || AreaInfos[SourceAreaIndex]->AreaTags.Intersect(AreaNodeTags).Num())
	{
		UnprocessedNeighborsSet.Add(SourceAreaIndex);

		auto& Info = Destination.PathDistances.FindOrAdd(SourceAreaIndex);

		Info.AreaNodeIndex = SourceAreaIndex;
		Info.PathDistance = 0.f;
	}

	//if no edge nodes, we have no graph
	if (!AreaGraph.Num())
	{
		return;
	}
	
	while (UnprocessedNeighborsSet.Num())
	{
		int32 CurrNodeInd = -1;

		//find unprocessed node with shortest link so far
		if (UnprocessedNeighborsSet.Num() == 1)
		{
			CurrNodeInd = *UnprocessedNeighborsSet.CreateConstIterator();
			UnprocessedNeighborsSet.Empty();
		}
		else
		{
			float MinDist = std::numeric_limits<float>::max();

			for (auto Iter = UnprocessedNeighborsSet.CreateIterator(); Iter; ++Iter)
			{
				int32 NodeInd = *Iter;
				float Dist = Destination.PathDistances[NodeInd].PathDistance;

				if (Dist < MinDist)
				{
					MinDist = Dist;
					CurrNodeInd = NodeInd;
				}
			}

			UnprocessedNeighborsSet.Remove(CurrNodeInd);
		}		

		float CurrPathDistance = Destination.PathDistances[CurrNodeInd].PathDistance;

		for (auto Iter = AreaGraph[CurrNodeInd].CreateConstIterator(); Iter; ++Iter)
		{
			//add all neighbors to unprocessedNeightborsSet if they can be added and aren't processed yet
			//also mark their distances
			int32 NeighborIndex = Iter->OtherLinkInd;

			if (!AreaNodeTags.Num() || AreaInfos[NeighborIndex]->AreaTags.Intersect(AreaNodeTags).Num())
			{
				auto& Info = Destination.PathDistances.FindOrAdd(NeighborIndex);
				bool bUntouched = Info.AreaNodeIndex < 0;
				
				float LinkDistance = AreaGraphEdges[Iter->AreaEdgeInfoInd].Distance;
				
				if (bUntouched
					|| (UnprocessedNeighborsSet.Contains(NeighborIndex) && Info.PathDistance > CurrPathDistance + LinkDistance))
				{
					Info.PathDistance = CurrPathDistance + LinkDistance;
					Info.ParentAreaNodeIndex = CurrNodeInd;
				}

				if (bUntouched)
				{
					UnprocessedNeighborsSet.Add(NeighborIndex);
					Info.AreaNodeIndex = NeighborIndex;
				}
			}
		}
	}
}

void ALGGeneratedLevel::GetAreaNodePathInfosForAstarPath(int32 SourceAreaIndex, int32 DestinationAreaIndex, const TSet<FName>& AreaNodeTags, AreaNodePathInfos& Destination)
{
	//TODO
}
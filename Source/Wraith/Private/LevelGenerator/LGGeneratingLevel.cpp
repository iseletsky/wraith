#include "Wraith.h"
#include "LGGameMode.h"
#include "LGGeneratingLevel.h"
#include "LGGeneratedLevel.h"
#include "LGSpawnablePrefabArea.h"
#include "LGPlayerController.h"
#include "LGGeneratorDebugData.h"
#include "AEBlankActor.h"

void FLGAreaLayoutInfo::RemoveConnectorIndex(int32 ConnectorIndex)
{
	if (ConnectorIndexBuckets.RemoveResult(ConnectorIndex) && !ConnectorIndexBuckets.GetBuckets().Num())
	{
		bConnectorIndexBucketsJustEmptied = true;
	}

	if (PriorityConnectorIndexBuckets.RemoveResult(ConnectorIndex) && !PriorityConnectorIndexBuckets.GetBuckets().Num())
	{
		bPriorityConnectorIndexBucketsJustEmptied = true;
	}
}

void FLGConnectorLayoutInfo::SetupWallClass()
{
	check(SourceComponent);

	int32 * ConnectorTypeIndex = SourceComponent->ConnectorClassToConnectorTypeReverseLookup.Find(ConnectorClass);

	if (ConnectorTypeIndex)
	{
		WallClass = SourceComponent->ConnectorTypes[*ConnectorTypeIndex].WallPrefabClass;
	}
	else
	{
		WallClass = NULL;
	}
}

void FLGConnectorLayoutInfo::SetupAreaCollisionComponents(ALGConnectorInfo * ConnectorInfo, const TArray<UPrimitiveComponent *>& TemplateCollisionAreas, bool bIsWall)
{
	//Spawn the AreaCollision components in the ALGAreaLayoutInfoSpawnablePrefab
	for (int32 AreaCollisionIndex = 0; AreaCollisionIndex < TemplateCollisionAreas.Num(); ++AreaCollisionIndex)
	{				
		auto AreaCollisionComp = TemplateCollisionAreas[AreaCollisionIndex];

		UPrimitiveComponent* NewComp = NewObject<UPrimitiveComponent>(ConnectorInfo, AreaCollisionComp->GetClass(), NAME_None, RF_NoFlags, AreaCollisionComp);
		NewComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		NewComp->SetupAttachment(ConnectorInfo->GetRootComponent());
		//NewComp->AttachChildren.Empty();

		//if wall geometry, it must be flipped 180 since connectors face outward
		if (bIsWall)
		{
			NewComp->SetRelativeTransform(AreaCollisionComp->GetRelativeTransform() * FTransform(FRotator(0.f, 180.f, 0.f)));
		}
		else
		{
			NewComp->SetRelativeTransform(AreaCollisionComp->GetRelativeTransform());
		}

		NewComp->RegisterComponent();
	}
}

void FLGConnectorLayoutInfo::SetupAreaCollision(class ALGGeneratingLevel * GeneratingLevel)
{
	SetupAreaCollision(GeneratingLevel, GeneratingLevel->GeneratedLevel->ConnectorInfos[ConnectorIndex]);
}

void FLGConnectorLayoutInfo::SetupAreaCollision(class ALGGeneratingLevel * GeneratingLevel, class ALGConnectorInfo * ConnectorInfo)
{
	ConnectorInfo->ClearAreaCollision();

	//spawn the wall area collision	
	if (WallClass)
	{
		SetupAreaCollisionComponents(ConnectorInfo, GeneratingLevel->GetSpawnableInstance<ALGSpawnablePrefab>(WallClass)->AreaCollision, true);
	}

	//spawn the connector area collision
	if (bIsPrimaryEnd && ConnectorClass)
	{
		SetupAreaCollisionComponents(ConnectorInfo, GeneratingLevel->GetSpawnableInstance<ALGSpawnablePrefab>(ConnectorClass)->AreaCollision, false);
	}
}

ALGGeneratingLevel::ALGGeneratingLevel(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	GeneratorState(State::AWAIT_SEED)
{
	PrimaryActorTick.bCanEverTick = true;
}

void ALGGeneratingLevel::BeginPlay()
{
	Super::BeginPlay();

	if (bDebugPlayback)
	{
		DebugData = GetWorld()->SpawnActor<ALGGeneratorDebugData>(ALGGeneratorDebugData::StaticClass());
	}

	GeneratedLevel = GetWorld()->SpawnActor<ALGGeneratedLevel>(ALGGeneratedLevel::StaticClass());

	BeginLoadingScreen();
}

void ALGGeneratingLevel::ReceiveSeed(int32 Seed)
{
	if (GeneratorState > State::AWAIT_SEED)
	{
		return;
	}

	RandomSeed = Seed;
	
	GeneratorState = State::RECEIVED_SEED;
}

void ALGGeneratingLevel::Tick(float DeltaTime)
{
	switch (GeneratorState)
	{
	case State::AWAIT_SEED:	//do nothing for now
		SetLoadingScreenMessage(TEXT("Awaiting Seed From Server."));
		CurrentPercentDone = 0.f;
		break;

	case State::RECEIVED_SEED:
	{
		RandomStream.Initialize(RandomSeed);
		CurrentPercentDone = 0.f;

		//TODO: reset generating level

		auto RootArea = GetSpawnableInstance<ALGGenerateableAreaDrunkenWalk>(RootGenerateableArea);

		if (RootArea)
		{
			RootLayoutContext = RootArea->BeginLayoutArea(0.f, 100.f, this, RandomStream, GetTransform(), NULL, -1, TSet<FName>(), TArray<FName>());

			SetLoadingScreenMessage(TEXT("Laying Out Area."));
			GeneratorState = State::LAYOUT;
		}
		else
		{
			SetLoadingScreenMessage(TEXT("No Level To Generate"));
			GeneratorState = State::POST;
		}		

		break;
	}		
		
	case State::LAYOUT:
	{
		AddLogMessage(TEXT("=========Continuing layout.==========="));
		EndAddingToStep(false);

		ELayoutContextStatus Result = RootLayoutContext->ContinueLayout(std::chrono::high_resolution_clock::now());

		if (Result == ELayoutContextStatus::SUCCESS)
		{
			AddLogMessage(TEXT("=========Done with layout.==========="));
			EndAddingToStep(false);

			SetLoadingScreenMessage(TEXT("Creating Level File"));			
			GeneratorState = State::CREATE_FILE;
		}
		else if (Result == ELayoutContextStatus::FAIL)
		{
			AddLogMessage(TEXT("=========Failed and restarting.==========="));
			EndAddingToStep(false);

			GeneratorState = State::AWAIT_SEED;
			ReceiveSeed((int32)RandomStream.GetUnsignedInt());
		}

		break;
	}

	case State::CREATE_FILE:

		SetLoadingScreenMessage(TEXT("Spawning Objects."));
		GeneratorState = State::SPAWN;

		break;

	case State::SPAWN:
	{
		if (!bDebugPlayback)
		{
			//now spawn everything! 
			for (int32 SpawnInd = 0; SpawnInd < AreaSpawnInfos.Num(); ++SpawnInd)
			{
				GetSpawnableInstance<ALGSpawnablePrefabArea>(AreaSpawnInfos[SpawnInd].SpawnablePrefabAreaClass)->Spawn(this, RandomStream, AreaSpawnInfos[SpawnInd].Transform);
			}

			for (int32 SpawnInd = 0; SpawnInd < ConnectorLayoutInfos.Num(); ++SpawnInd)
			{
				auto& ConnectorLayoutInfo = ConnectorLayoutInfos[SpawnInd];
				auto ConnectorInfo = GeneratedLevel->ConnectorInfos[SpawnInd];

				if (ConnectorLayoutInfo.bIsPrimaryEnd && ConnectorLayoutInfo.ConnectorClass)
				{
					GetSpawnableInstance<ALGSpawnablePrefab>(ConnectorLayoutInfo.ConnectorClass)->Spawn(this, RandomStream, ConnectorInfo->GetTransform());
				}

				if (ConnectorLayoutInfo.WallClass)
				{
					//walls need to be flipped 180 since connector slot transforms face outward
					GetSpawnableInstance<ALGSpawnablePrefab>(ConnectorLayoutInfo.WallClass)->Spawn(this, RandomStream, FTransform(FRotator(0.f, 180.f, 0.f)) * ConnectorInfo->GetTransform());
				}
			}
		}		

		//if (bDebugAreaGraph)
		//{
		//	//area nodes
		//	for (int32 AreaInfo = 0; AreaInfo < AreaInfos.Num(); ++AreaInfo)
		//	{
		//		DrawDebugSphere(GetWorld(), AreaInfos[AreaInfo].CenterPosition, 10.f, 8, FColor::Blue, true);
		//		DrawDebugString(GetWorld(), AreaInfos[AreaInfo].CenterPosition + FVector(10.f), FString::Printf(TEXT("%d"), AreaInfo), NULL, FColor::Blue);
		//	}

		//	//links
		//	for (auto Iter = AreaGraph.CreateConstIterator(); Iter; ++Iter)
		//	{
		//		for (auto SetIter = Iter->Value.CreateConstIterator(); SetIter; ++SetIter)
		//		{
		//			DrawDebugLine(GetWorld(), AreaInfos[Iter->Key].CenterPosition, AreaInfos[SetIter->OtherLinkInd].CenterPosition, AreaGraphEdges[SetIter->AreaEdgeInfoInd].FloodFill ? FColor::Green : FColor::Red, true);
		//		}
		//	}

		//	//dijkstra path data just to see if the algorithm worked
		//	{
		//		AreaNodePathInfos Infos;

		//		GetAreaNodePathInfos(0, TSet<FName>(), Infos);

		//		for (auto Iter = Infos.PathDistances.CreateConstIterator(); Iter; ++Iter)
		//		{
		//			DrawDebugString(GetWorld(), AreaInfos[Iter->Key].CenterPosition + FVector(-10.f, 10.f, 10.f), FString::Printf(TEXT("%f"), Iter->Value.PathDistance), NULL, FColor::Magenta);

		//			if (Iter->Value.ParentAreaNodeIndex >= 0)
		//			{
		//				DrawDebugDirectionalArrow(GetWorld(),
		//					AreaInfos[Iter->Value.ParentAreaNodeIndex].CenterPosition + FVector(30.f),
		//					AreaInfos[Iter->Key].CenterPosition + FVector(30.f),
		//					20.f, FColor::Magenta, true);
		//			}
		//		}
		//	}
		//}

		GeneratorState = State::POST;

		break;
	}

	case State::POST:
				
		//destroy all instantiated spawnables
		for (auto Iter = InstantiatedSpawnables.CreateIterator(); Iter; ++Iter)
		{
			bool Destroyed = Iter->Value->Destroy();
			check(Destroyed);
		}

		//allow navigation mesh building to function again
		UAEGameplayStatics::NavReleaseInitialBuildingLock(GetWorld());		
		
		{
			AGameModeBase * GameMode = GetWorld()->GetAuthGameMode();

			if (GameMode)
			{
				//tell local server that the map is generated
				ALGGameMode * LGGameMode = Cast<ALGGameMode>(GameMode);

				if (LGGameMode)
				{
					LGGameMode->LocalMapReady();
				}
			}
			else
			{
				//tell local clients to notify the server that their map is generated		
				for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
				{
					ALGPlayerController* PlayerController = Cast<ALGPlayerController>(*Iterator);
					if (PlayerController)
					{
						ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(PlayerController->Player);
						if (LocalPlayer)
						{
							PlayerController->ServerNotifyMapGenerated();
						}
					}
				}
			}
		}
		
		EndLoadingScreen();

		//now we can destroy ourselves and let the game run
		{
			//calling netForce here since GeneratingLevel Authority is NONE since the object was placed in the map
			//TODO: Find a way to make this object have AUTHORITY on client since it's a local object
			bool Destroyed = Destroy(true);
			check(Destroyed);
		}
		
		//if we're the client we don't need generated level either since server handles everything
		if (GetNetMode() == NM_Client)
		{
			bool Destroyed = GeneratedLevel->Destroy();
			check(Destroyed);
		}

		return;
	}

	//update local clients with current percent done
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		ALGPlayerController* PlayerController = Cast<ALGPlayerController>(*Iterator);
		if (PlayerController)
		{
			ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(PlayerController->Player);
			if (LocalPlayer)
			{
				PlayerController->ServerNotifyMapGeneratedPercentage(CurrentPercentDone);
			}
		}
	}

	SetLoadingScreenPercentage(CurrentPercentDone);
}

void ALGGeneratingLevel::AddObjectToStep(TSubclassOf<class ALGSpawnablePrefab> InSpawnablePrefabClass, FTransform InTransform)
{
	if (DebugData)
	{
		DebugData->AddObjectToStep(InSpawnablePrefabClass, InTransform, this);
	}
}

void ALGGeneratingLevel::AddLogMessage(const FString& LogMessage)
{
	if (DebugData)
	{
		DebugData->AddLogMessage(LogMessage);
	}
}

void ALGGeneratingLevel::AddAreaIndexToStep(int32 AreaIndex)
{
	if (DebugData)
	{
		DebugData->AddAreaIndexToStep(AreaIndex);
	}
}

void ALGGeneratingLevel::EndAddingToStep(bool bSuccessfulStep)
{
	if (DebugData)
	{
		DebugData->EndAddingToStep(bSuccessfulStep);
	}
}

void ALGGeneratingLevel::CheckAreaFreeConnectors(int32 AreaIndex)
{
	bool bNoRegularConnectors = AreaLayoutInfos[AreaIndex].bConnectorIndexBucketsJustEmptied;
	bool bNoPriorityConnectors = AreaLayoutInfos[AreaIndex].bPriorityConnectorIndexBucketsJustEmptied;

	if (bNoRegularConnectors || bNoPriorityConnectors)
	{
		for (auto Iter = AreaIndexToPassNames.CreateConstKeyIterator(AreaIndex); Iter; ++Iter)
		{
			if (bNoRegularConnectors)
			{
				AddLogMessage(FString::Printf(TEXT("Removing area %d from regular list of Pass %s because it had no more free connectors"),
					AreaIndex,
					*(Iter.Value().ToString()),
					AreaLayoutInfos[AreaIndex].PriorityConnectorIndexBuckets.GetBuckets().Num()));

				PassNameToAreaIndexList[Iter.Value()].List.Remove(AreaIndex);

				AreaLayoutInfos[AreaIndex].bConnectorIndexBucketsJustEmptied = false;
			}

			if (bNoPriorityConnectors)
			{
				AddLogMessage(FString::Printf(TEXT("Removing area %d from priority list of Pass %s because it had no more free connectors"),
					AreaIndex,
					*(Iter.Value().ToString()),
					AreaLayoutInfos[AreaIndex].PriorityConnectorIndexBuckets.GetBuckets().Num()));

				PassNameToPriorityAreaIndexList[Iter.Value()].List.Remove(AreaIndex);

				AreaLayoutInfos[AreaIndex].bPriorityConnectorIndexBucketsJustEmptied = false;
			}
		}
	}

	EndAddingToStep(true);
}

void ALGGeneratingLevel::SetupConnectorLink(int32 FromConnectorIndex, int32 ToConnectorIndex, TSubclassOf<ALGSpawnablePrefabConnector> ConnectorClass)
{
	//set up connections between connectors
	GeneratedLevel->ConnectorInfos[FromConnectorIndex]->OtherConnectorIndex = ToConnectorIndex;
	ConnectorLayoutInfos[FromConnectorIndex].bIsPrimaryEnd = true;
	ConnectorLayoutInfos[FromConnectorIndex].ConnectorClass = ConnectorClass;

	ConnectorLayoutInfos[FromConnectorIndex].SetupWallClass();
	ConnectorLayoutInfos[FromConnectorIndex].SetupAreaCollision(this);

	GeneratedLevel->ConnectorInfos[ToConnectorIndex]->OtherConnectorIndex = FromConnectorIndex;
	ConnectorLayoutInfos[ToConnectorIndex].bIsPrimaryEnd = false;
	ConnectorLayoutInfos[ToConnectorIndex].ConnectorClass = ConnectorClass;

	ConnectorLayoutInfos[ToConnectorIndex].SetupWallClass();
	ConnectorLayoutInfos[ToConnectorIndex].SetupAreaCollision(this);

	auto& FromAreaInfo = AreaLayoutInfos[GeneratedLevel->ConnectorInfos[FromConnectorIndex]->AreaIndex];
	auto& ToAreaInfo = AreaLayoutInfos[GeneratedLevel->ConnectorInfos[ToConnectorIndex]->AreaIndex];

	//remove connector indices from the area connector buckets
	FromAreaInfo.RemoveConnectorIndex(FromConnectorIndex);
	ToAreaInfo.RemoveConnectorIndex(ToConnectorIndex);
	
	LinkAreas(GeneratedLevel->ConnectorInfos[FromConnectorIndex]->AreaIndex, GeneratedLevel->ConnectorInfos[ToConnectorIndex]->AreaIndex, FromConnectorIndex);
}

bool ALGGeneratingLevel::CheckMaxTimesClassSpawnedConstraint(UClass * AreaClass, int32 Maximum) const
{
	const int32 * CurrentTimes = NumTimesClassSpawned.Find(AreaClass);

	if (CurrentTimes)
	{
		return *CurrentTimes < Maximum;
	}

	return true;
}

void ALGGeneratingLevel::IncrementTimesClassSpawned(UClass * AreaClass)
{
	int32& NumTimesSpawned = NumTimesClassSpawned.FindOrAdd(AreaClass);
	++NumTimesSpawned;
}

void ALGGeneratingLevel::LinkAreas(int32 AreaA, int32 AreaB, int32 FromConnector)
{
	auto& AreaALinks = GeneratedLevel->AreaGraph.FindOrAdd(AreaA);

	AreaEdge AreaAEdge(AreaB, GeneratedLevel->AreaGraphEdges.Num());
	auto AreaBLink = AreaALinks.Find(AreaAEdge);

	//see if the link to AreaB Exists
	if (!AreaBLink)
	{
		AreaALinks.Emplace(AreaAEdge);
		GeneratedLevel->AreaGraph.FindOrAdd(AreaB).Emplace(AreaEdge(AreaA, GeneratedLevel->AreaGraphEdges.Num()));

		GeneratedLevel->AreaGraphEdges.Emplace();

		auto& LinkInfo = GeneratedLevel->AreaGraphEdges[GeneratedLevel->AreaGraphEdges.Num() - 1];

		LinkInfo.Distance = FVector::Dist(GeneratedLevel->AreaInfos[AreaB]->GetTransform().GetLocation(), GeneratedLevel->AreaInfos[AreaA]->GetTransform().GetLocation());

		if (FromConnector >= 0)
		{
			LinkInfo.FromConnectorInd.Add(FromConnector);
		}
	}
	else
	{
		//if exists, then add the FromConnector
		auto& LinkInfo = GeneratedLevel->AreaGraphEdges[AreaBLink->AreaEdgeInfoInd];

		//it doesn't make sense to call this more than once unless multiple connectors join two areas
		check(FromConnector >= 0 && LinkInfo.FromConnectorInd.Num());

		LinkInfo.FromConnectorInd.Add(FromConnector);
	}
}

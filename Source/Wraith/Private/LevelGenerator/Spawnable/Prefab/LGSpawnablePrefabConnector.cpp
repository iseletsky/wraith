#include "Wraith.h"
#include "LGSpawnablePrefabConnector.h"

ALGSpawnablePrefabConnector::ALGSpawnablePrefabConnector(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	EntryPoint = ObjectInitializer.CreateDefaultSubobject<ULGPrefabSlotComponentPoint>(this, TEXT("EntryPoint"));
	EntryPoint->SetupAttachment(RootComponent);

	ExitPoint = ObjectInitializer.CreateDefaultSubobject<ULGPrefabSlotComponentPoint>(this, TEXT("ExitPoint"));
	ExitPoint->SetupAttachment(RootComponent);
}

bool ALGSpawnablePrefabConnector::Spawn(class ALGGeneratingLevel * GeneratingLevel, FRandomStream& RandomStream, FTransform Transform) const
{
	//just offset the transform by the entry point
	Transform = EntryPoint->GetComponentToWorld().Inverse() * Transform;

	return Super::Spawn(GeneratingLevel, RandomStream, Transform);
}

FTransform ALGSpawnablePrefabConnector::GetTransformFromEntryToExit()
{
	return ExitPoint->ComponentToWorld.GetRelativeTransform(EntryPoint->ComponentToWorld);
}
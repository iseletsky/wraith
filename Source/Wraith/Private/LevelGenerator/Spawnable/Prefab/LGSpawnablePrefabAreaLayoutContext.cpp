#include "Wraith.h"
#include "LGSpawnablePrefabArea.h"
#include "LGSpawnablePrefabAreaLayoutContext.h"

ELayoutContextStatus ULGSpawnablePrefabAreaLayoutContext::ContinueLayout(std::chrono::time_point<std::chrono::high_resolution_clock> BeginTime)
{
	return Parent->LayoutArea(GeneratingLevel, *RandomStream, Transform, EntryConnectorClass, EntryConnectorIndex, AreaTags, PassNames)
		? ELayoutContextStatus::SUCCESS
		: ELayoutContextStatus::FAIL;
}
#include "Wraith.h"
#include "LGPrefabSlotComponentActor.h"
#include "LGPrefabSlotComponentActorImportFactory.h"

#ifdef WITH_SCENE_IMPORT_FACTORIES

FString ULGPrefabSlotComponentActorImportFactory::GetObjectTypeName() const
{
	return FString(TEXT("LGPrefabSlotComponentActor"));
}

USceneComponent * ULGPrefabSlotComponentActorImportFactory::SpawnComponent(UObject * Outer) const
{
	return NewObject<ULGPrefabSlotComponentActor>(Outer, ULGPrefabSlotComponentActor::StaticClass());
}

bool ULGPrefabSlotComponentActorImportFactory::ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const
{
	ULGPrefabSlotComponentActor * SlotComponent = Cast<ULGPrefabSlotComponentActor>(SceneComponent);

	if (SlotComponent)
	{
		//actor class name
		{
			FString ClassName;

			if (SourceJson->TryGetStringField(TEXT("actorSpawnClass"), ClassName))
			{
				UClass * ActorClass = FindClass(*ClassName);

				if (ActorClass)
				{
					SlotComponent->ActorSpawnClass = ActorClass;
				}
				else
				{
					UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG actor slot component definition with an invalid actor spawn class %s."), *NameForErrors, *ClassName);
					return false;
				}
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG actor slot component definition with a missing actor spawn class."), *NameForErrors);
				return false;
			}
		}
		
		return Super::ImportComponent(SceneComponent, SourceJson, NameForErrors);
	}

	return false;
}

#endif
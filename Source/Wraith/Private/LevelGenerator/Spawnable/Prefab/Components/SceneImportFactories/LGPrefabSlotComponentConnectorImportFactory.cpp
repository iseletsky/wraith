#include "Wraith.h"
#include "LGPrefabSlotComponentConnector.h"
#include "LGPrefabSlotComponentConnectorImportFactory.h"

#ifdef WITH_SCENE_IMPORT_FACTORIES

FString ULGPrefabSlotComponentConnectorImportFactory::GetObjectTypeName() const
{
	return FString(TEXT("LGPrefabSlotComponentConnector"));
}

bool ULGPrefabSlotComponentConnectorImportFactory::ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const
{
	ULGPrefabSlotComponentConnector * SlotComponent = Cast<ULGPrefabSlotComponentConnector>(SceneComponent);

	if (SlotComponent)
	{
		//disabled
		bool bDisabled = false;

		SourceJson->TryGetBoolField("disabled", bDisabled);

		//connector types
		{
			const TArray<TSharedPtr<FJsonValue>> *ConnectorTypes;

			if (SourceJson->TryGetArrayField("connectorTypes", ConnectorTypes))
			{
				for (int32 ConnectorTypeInd = 0; ConnectorTypeInd < ConnectorTypes->Num(); ++ConnectorTypeInd)
				{
					const TSharedPtr< FJsonObject > * ConnectorObject;

					if (!(*ConnectorTypes)[ConnectorTypeInd]->TryGetObject(ConnectorObject))
					{
						UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an invalid connector component definition at index %d."), *NameForErrors, ConnectorTypeInd);
						return false;
					}
					
					//probability
					double Probability = 1.;

					(*ConnectorObject)->TryGetNumberField("probability", Probability);

					//wallPrefabClass
					UClass * WallPrefabClass = NULL;

					{
						FString ClassName;

						if ((*ConnectorObject)->TryGetStringField(TEXT("wallPrefabClass"), ClassName))
						{
							WallPrefabClass = FindClass(*ClassName);

							if (!WallPrefabClass)
							{
								UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG connector component definition with an invalid wall prefab class %s at index %d."), *NameForErrors, *ClassName, ConnectorTypeInd);
								return false;
							}
						}
					}

					//connectorPrefabClass
					UClass * ConnectorPrefabClass = NULL;

					{
						FString ClassName;

						if ((*ConnectorObject)->TryGetStringField(TEXT("connectorPrefabClass"), ClassName))
						{
							ConnectorPrefabClass = FindClass(*ClassName);

							if (!ConnectorPrefabClass)
							{
								UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG connector component definition with an invalid connector prefab class %s at index %d."), *NameForErrors, *ClassName, ConnectorTypeInd);
								return false;
							}
						}
					}

					//disabled
					bool bDisabled = false;

					(*ConnectorObject)->TryGetBoolField("disabled", bDisabled);

					SlotComponent->ConnectorTypes.Emplace();
					auto& ConnectorTypeInfo = SlotComponent->ConnectorTypes.Last();
					
					ConnectorTypeInfo.Probability = (float)Probability;
					ConnectorTypeInfo.WallPrefabClass = WallPrefabClass;
					ConnectorTypeInfo.ConnectorPrefabClass = ConnectorPrefabClass;
					ConnectorTypeInfo.bDisabled = bDisabled;
				}
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG connector component definition with missing connectorTypes."), *NameForErrors);
				return false;
			}
		}

		SlotComponent->bDisabled = bDisabled;

		return Super::ImportComponent(SceneComponent, SourceJson, NameForErrors);
	}

	return false;
}

#endif
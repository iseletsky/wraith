#include "Wraith.h"
#include "LGPrefabSlotComponentSpawn.h"
#include "LGPrefabSlotComponentSpawnImportFactory.h"

#ifdef WITH_SCENE_IMPORT_FACTORIES

FString ULGPrefabSlotComponentSpawnImportFactory::GetObjectTypeName() const
{
	return FString(TEXT("LGPrefabSlotComponentSpawn"));
}

USceneComponent * ULGPrefabSlotComponentSpawnImportFactory::SpawnComponent(UObject * Outer) const
{
	return NewObject<ULGPrefabSlotComponentSpawn>(Outer, ULGPrefabSlotComponentSpawn::StaticClass());
}

bool ULGPrefabSlotComponentSpawnImportFactory::ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const
{
	ULGPrefabSlotComponentSpawn * SlotComponent = Cast<ULGPrefabSlotComponentSpawn>(SceneComponent);

	if (SlotComponent)
	{
		//similarity id
		int32 SimilarityId = 0;

		SourceJson->TryGetNumberField("similarityId", SimilarityId);

		//disabled
		bool bDisabled = false;

		SourceJson->TryGetBoolField("disabled", bDisabled);

		//spawn types
		{
			const TArray<TSharedPtr<FJsonValue>> *SpawnTypes;

			if (SourceJson->TryGetArrayField("spawnTypes", SpawnTypes))
			{
				for (int32 SpawnTypeInd = 0; SpawnTypeInd < SpawnTypes->Num(); ++SpawnTypeInd)
				{
					const TSharedPtr< FJsonObject > * SpawnTypeObject;

					if (!(*SpawnTypes)[SpawnTypeInd]->TryGetObject(SpawnTypeObject))
					{
						UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG spawn component definition with an invalid prefab spawn type definition at index %d."), *NameForErrors, SpawnTypeInd);
						return false;
					}

					//probability
					double Probability = 1.;

					(*SpawnTypeObject)->TryGetNumberField("probability", Probability);

					//wallPrefabClass
					UClass * PrefabClass = NULL;

					{
						FString ClassName;

						if ((*SpawnTypeObject)->TryGetStringField(TEXT("prefabClass"), ClassName))
						{
							PrefabClass = FindClass(*ClassName);

							if (!PrefabClass)
							{
								UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG spawn component definition with an invalid prefab class %s at index %d."), *NameForErrors, *ClassName, SpawnTypeInd);
								return false;
							}
						}
					}

					//disabled
					bool bDisabled = false;

					(*SpawnTypeObject)->TryGetBoolField("disabled", bDisabled);

					SlotComponent->SpawnTypes.Emplace();
					auto& SpawnTypeInfo = SlotComponent->SpawnTypes.Last();

					SpawnTypeInfo.Probability = (float)Probability;
					SpawnTypeInfo.PrefabClass = PrefabClass;
					SpawnTypeInfo.bDisabled = bDisabled;
				}
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG spawn component definition with missing spawnTypes."), *NameForErrors);
				return false;
			}
		}

		SlotComponent->SimilarityId = SimilarityId;
		SlotComponent->bDisabled = bDisabled;

		return Super::ImportComponent(SceneComponent, SourceJson, NameForErrors);
	}

	return false;
}

#endif
#include "Wraith.h"
#include "LGPrefabSlotComponentImportFactory.h"

#ifdef WITH_SCENE_IMPORT_FACTORIES

bool ULGPrefabSlotComponentImportFactory::ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const
{
	//TODO: implement
	return Super::ImportComponent(SceneComponent, SourceJson, NameForErrors);
}

#endif
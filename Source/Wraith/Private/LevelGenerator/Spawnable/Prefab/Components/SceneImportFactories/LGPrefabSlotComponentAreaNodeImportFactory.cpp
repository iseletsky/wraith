#include "Wraith.h"
#include "LGPrefabSlotComponentAreaNode.h"
#include "LGPrefabSlotComponentAreaNodeImportFactory.h"

#ifdef WITH_SCENE_IMPORT_FACTORIES

FString ULGPrefabSlotComponentAreaNodeImportFactory::GetObjectTypeName() const
{
	return FString(TEXT("LGPrefabSlotComponentAreaNode"));
}

USceneComponent * ULGPrefabSlotComponentAreaNodeImportFactory::SpawnComponent(UObject * Outer) const
{
	return NewObject<ULGPrefabSlotComponentAreaNode>(Outer, ULGPrefabSlotComponentAreaNode::StaticClass());
}

bool ULGPrefabSlotComponentAreaNodeImportFactory::ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const
{
	return Super::ImportComponent(SceneComponent, SourceJson, NameForErrors);
}

#endif
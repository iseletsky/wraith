#include "Wraith.h"
#include "LGPrefabSlotComponentAreaConnector.h"
#include "LGPrefabSlotComponentAreaConnectorImportFactory.h"

#ifdef WITH_SCENE_IMPORT_FACTORIES

FString ULGPrefabSlotComponentAreaConnectorImportFactory::GetObjectTypeName() const
{
	return FString(TEXT("LGPrefabSlotComponentAreaConnector"));
}

USceneComponent * ULGPrefabSlotComponentAreaConnectorImportFactory::SpawnComponent(UObject * Outer) const
{
	return NewObject<ULGPrefabSlotComponentAreaConnector>(Outer, ULGPrefabSlotComponentAreaConnector::StaticClass());
}

bool ULGPrefabSlotComponentAreaConnectorImportFactory::ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const
{
	ULGPrefabSlotComponentAreaConnector * SlotComponent = Cast<ULGPrefabSlotComponentAreaConnector>(SceneComponent);

	if (SlotComponent)
	{
		//probability
		double Probability = 1.;

		SourceJson->TryGetNumberField(TEXT("probability"), Probability);

		//congruencyId
		int32 CongruencyId = 0;

		SourceJson->TryGetNumberField(TEXT("congruencyId"), CongruencyId);

		//priorityConnector
		bool bPriorityConnector = false;

		SourceJson->TryGetBoolField(TEXT("priorityConnector"), bPriorityConnector);

		//connectorName
		FString ConnectorName;

		SourceJson->TryGetStringField(TEXT("connectorName"), ConnectorName);

		SlotComponent->Probability = (float)Probability;
		SlotComponent->CongruencyId = CongruencyId;
		SlotComponent->bPriorityConnector = bPriorityConnector;
		SlotComponent->ConnectorName = FName(*ConnectorName);

		return Super::ImportComponent(SceneComponent, SourceJson, NameForErrors);
	}

	return false;
}

#endif
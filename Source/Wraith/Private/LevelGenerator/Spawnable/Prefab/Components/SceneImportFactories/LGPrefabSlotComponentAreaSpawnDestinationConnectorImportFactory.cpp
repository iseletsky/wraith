#include "Wraith.h"
#include "LGPrefabSlotComponentAreaSpawnDestinationConnector.h"
#include "LGPrefabSlotComponentAreaSpawnDestinationConnectorImportFactory.h"

#ifdef WITH_SCENE_IMPORT_FACTORIES

FString ULGPrefabSlotComponentAreaSpawnDestinationConnectorImportFactory::GetObjectTypeName() const
{
	return FString(TEXT("LGPrefabSlotComponentAreaSpawnDestinationConnector"));
}

USceneComponent * ULGPrefabSlotComponentAreaSpawnDestinationConnectorImportFactory::SpawnComponent(UObject * Outer) const
{
	return NewObject<ULGPrefabSlotComponentAreaSpawnDestinationConnector>(Outer, ULGPrefabSlotComponentAreaSpawnDestinationConnector::StaticClass());
}

bool ULGPrefabSlotComponentAreaSpawnDestinationConnectorImportFactory::ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const
{
	return Super::ImportComponent(SceneComponent, SourceJson, NameForErrors);
}

#endif
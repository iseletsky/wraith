#include "Wraith.h"
#include "LGPrefabSlotComponentPoint.h"
#include "LGPrefabSlotComponentPointImportFactory.h"

#ifdef WITH_SCENE_IMPORT_FACTORIES

FString ULGPrefabSlotComponentPointImportFactory::GetObjectTypeName() const
{
	return FString(TEXT("LGPrefabSlotComponentPoint"));
}

USceneComponent * ULGPrefabSlotComponentPointImportFactory::SpawnComponent(UObject * Outer) const
{
	return NewObject<ULGPrefabSlotComponentPoint>(Outer, ULGPrefabSlotComponentPoint::StaticClass());
}

bool ULGPrefabSlotComponentPointImportFactory::ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const
{
	return Super::ImportComponent(SceneComponent, SourceJson, NameForErrors);
}

#endif
#include "Wraith.h"
#include "LGPrefabSlotComponentAreaSpawnConnector.h"
#include "LGSceneImportJSONHelpers.h"
#include "LGPrefabSlotComponentAreaSpawnConnectorImportFactory.h"

#ifdef WITH_SCENE_IMPORT_FACTORIES

FString ULGPrefabSlotComponentAreaSpawnConnectorImportFactory::GetObjectTypeName() const
{
	return FString(TEXT("LGPrefabSlotComponentAreaSpawnConnector"));
}

USceneComponent * ULGPrefabSlotComponentAreaSpawnConnectorImportFactory::SpawnComponent(UObject * Outer) const
{
	return NewObject<ULGPrefabSlotComponentAreaSpawnConnector>(Outer, ULGPrefabSlotComponentAreaSpawnConnector::StaticClass());
}

bool ULGPrefabSlotComponentAreaSpawnConnectorImportFactory::ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const
{
	ULGPrefabSlotComponentAreaSpawnConnector * SlotComponent = Cast<ULGPrefabSlotComponentAreaSpawnConnector>(SceneComponent);

	if (SlotComponent)
	{
		//spawnable area probability list
		{
			const TArray<TSharedPtr<FJsonValue>> *SpawnTypes;

			if (!SourceJson->TryGetArrayField("generateableAreas", SpawnTypes))
			{
				UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG area spawn connector object with no generateableAreas defined."), *NameForErrors);
				return false;
			}
						
			if (!FLGSceneImportJSONHelpers::ReadSpawnableAreaProbabilityList(*SpawnTypes, SlotComponent->SpawnableAreas, NameForErrors))
			{
				UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG area spawn connector object with invalid generateableAreas."), *NameForErrors);
				return false;
			}

			//read the inbound connector names
			for (int32 GenerateableInd = 0; GenerateableInd < SpawnTypes->Num(); ++GenerateableInd)
			{
				const TSharedPtr< FJsonObject > * GenerateableTypeObject;

				if (!(*SpawnTypes)[GenerateableInd]->TryGetObject(GenerateableTypeObject))
				{
					UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG spawn component definition with a generateable definition with invalid  at index %d."), *NameForErrors, GenerateableInd);
					return false;
				}

				//inboundConnectorNames
				TArray<FString> ConnectorNames;

				SlotComponent->SpawnableAreaInboundConnector.Emplace();

				if ((*GenerateableTypeObject)->TryGetStringArrayField("inboundConnectorNames", ConnectorNames))
				{
					auto& NameList = SlotComponent->SpawnableAreaInboundConnector.Last();

					for (int32 Name = 0; Name < ConnectorNames.Num(); ++Name)
					{
						NameList.SpawnableAreaInboundConnectorNames.Add(*ConnectorNames[Name]);
					}
				}
			}
		}

		return Super::ImportComponent(SceneComponent, SourceJson, NameForErrors);
	}

	return false;
}

#endif
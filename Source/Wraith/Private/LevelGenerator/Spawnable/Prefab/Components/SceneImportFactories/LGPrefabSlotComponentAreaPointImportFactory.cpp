#include "Wraith.h"
#include "LGPrefabSlotComponentAreaPoint.h"
#include "LGPrefabSlotComponentAreaPointImportFactory.h"

#ifdef WITH_SCENE_IMPORT_FACTORIES

FString ULGPrefabSlotComponentAreaPointImportFactory::GetObjectTypeName() const
{
	return FString(TEXT("LGPrefabSlotComponentAreaPoint"));
}

USceneComponent * ULGPrefabSlotComponentAreaPointImportFactory::SpawnComponent(UObject * Outer) const
{
	return NewObject<ULGPrefabSlotComponentAreaPoint>(Outer, ULGPrefabSlotComponentAreaPoint::StaticClass());
}

bool ULGPrefabSlotComponentAreaPointImportFactory::ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const
{
	ULGPrefabSlotComponentAreaPoint * SlotComponent = Cast<ULGPrefabSlotComponentAreaPoint>(SceneComponent);

	if (SlotComponent)
	{
		//pointName
		FString PointName;

		SourceJson->TryGetStringField(TEXT("pointName"), PointName);

		SlotComponent->PointName = FName(*PointName);

		return Super::ImportComponent(SceneComponent, SourceJson, NameForErrors);
	}

	return false;
}

#endif
#include "Wraith.h"
#include "LGSceneImportJSONHelpers.h"
#include "Json.h"
#include "LGGenerateableArea.h"
#include "LGSpawnablePrefabArea.h"

bool FLGSceneImportJSONHelpers::ReadSpawnableAreaProbabilityList(const TArray<TSharedPtr<FJsonValue>>& Items, FLGSpawnableAreaProbabilityList& OutList, const FString& NameForErrors)
{
	for (int32 GenerateableInd = 0; GenerateableInd < Items.Num(); ++GenerateableInd)
	{
		const TSharedPtr< FJsonObject > * GenerateableTypeObject;

		if (!Items[GenerateableInd]->TryGetObject(GenerateableTypeObject))
		{
			UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG spawn component definition with an invalid generateable definition at index %d."), *NameForErrors, GenerateableInd);
			return false;
		}

		//probability
		double Probability = 1.;

		(*GenerateableTypeObject)->TryGetNumberField("probability", Probability);
			
		//level max spawn
		int32 LevelMaxSpawn = 0;

		(*GenerateableTypeObject)->TryGetNumberField("levelMaxSpawn", LevelMaxSpawn);

		//disabled
		bool bDisabled = false;

		(*GenerateableTypeObject)->TryGetBoolField("disabled", bDisabled);

		//spawnableClass
		UClass * SpawnableClass = NULL;
		
		{
			FString ClassName;

			if ((*GenerateableTypeObject)->TryGetStringField(TEXT("spawnableClass"), ClassName))
			{
				SpawnableClass = FindClass(*ClassName);

				if (!SpawnableClass)
				{
					UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG spawn component definition with an invalid spawnable class %s at index %d."), *NameForErrors, *ClassName, GenerateableInd);
					return false;
				}
			}
		}

		if (!SpawnableClass)
		{
			UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG spawn component definition with an unspecified spawnable class at index %d."), *NameForErrors, GenerateableInd);
			return false;
		}

		TSubclassOf<ALGGenerateableArea> GenerateableAreaClass;
		TSubclassOf<ALGSpawnablePrefabArea> SpawnablePrefabAreaClass;

		//figure out if it's a subclass of ALGGenerateableArea or ALGSpawnablePrefabArea
		{
			//TODO: is there a better way to check if a class is a subclass of some other class?
			if (UClass::FindCommonBase(ALGGenerateableArea::StaticClass(), SpawnableClass) != ALGGenerateableArea::StaticClass())
			{
				GenerateableAreaClass = SpawnableClass;
			}
			else if (UClass::FindCommonBase(ALGSpawnablePrefabArea::StaticClass(), SpawnableClass) != ALGSpawnablePrefabArea::StaticClass())
			{
				SpawnablePrefabAreaClass = SpawnableClass;
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Scene file '%s' has an LG spawn component definition with a spawnable class of name '%s' that is neither an ALGGenerateableArea or ALGSpawnablePrefabArea at index %d."), *NameForErrors, *SpawnableClass->GetName(), GenerateableInd);
				return false;
			}
		}

		OutList.SpawnableAreas.Emplace();
		auto& GenerateableTypeInfo = OutList.SpawnableAreas.Last();

		GenerateableTypeInfo.Probability = (float)Probability;
		GenerateableTypeInfo.LevelMaxSpawn = LevelMaxSpawn;
		GenerateableTypeInfo.bDisabled = bDisabled;
		GenerateableTypeInfo.GenerateableAreaClass = GenerateableAreaClass;
		GenerateableTypeInfo.SpawnablePrefabAreaClass = SpawnablePrefabAreaClass;
	}

	return true;
}
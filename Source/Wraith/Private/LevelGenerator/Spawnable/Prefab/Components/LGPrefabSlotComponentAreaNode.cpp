#include "Wraith.h"
#include "LGPrefabSlotComponentAreaNode.h"

ULGPrefabSlotComponentAreaNode::ULGPrefabSlotComponentAreaNode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	AreaCost(1.f)
{}
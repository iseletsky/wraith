#include "Wraith.h"
#include "LGPrefabSlotComponent.h"
#include "Components/ArrowComponent.h"

ULGPrefabSlotComponent::ULGPrefabSlotComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bHiddenInGame = true;

	ComponentTags.Add(COMPONENT_NO_SPAWN_TAG);
}

void ULGPrefabSlotComponent::OnRegister()
{
	Super::OnRegister();

#if WITH_EDITOR
	if (GetOwner() && !GetWorld()->IsGameWorld())
	{
		if (!EditorArrowX)
		{
			EditorArrowX = NewObject<UArrowComponent>(GetOwner(), UArrowComponent::StaticClass(), NAME_None, RF_Transactional | RF_TextExportTransient);

			EditorArrowX->SetupAttachment(this);
			EditorArrowX->AlwaysLoadOnClient = false;
			EditorArrowX->AlwaysLoadOnServer = false;
			EditorArrowX->ArrowColor = FColor::Red;
			EditorArrowX->ArrowSize = 1.f;

			EditorArrowX->RegisterComponent();
		}

		if (!EditorArrowY)
		{
			EditorArrowY = NewObject<UArrowComponent>(GetOwner(), UArrowComponent::StaticClass(), NAME_None, RF_Transactional | RF_TextExportTransient);

			EditorArrowY->SetupAttachment(this);
			EditorArrowY->AlwaysLoadOnClient = false;
			EditorArrowY->AlwaysLoadOnServer = false;
			EditorArrowY->ArrowColor = FColor::Green;
			EditorArrowY->ArrowSize = 1.f;
			EditorArrowY->RelativeRotation = FRotator(0.f, 90.f, 0.f);

			EditorArrowY->RegisterComponent();
		}

		if (!EditorArrowZ)
		{
			EditorArrowZ = NewObject<UArrowComponent>(GetOwner(), UArrowComponent::StaticClass(), NAME_None, RF_Transactional | RF_TextExportTransient);

			EditorArrowZ->SetupAttachment(this);
			EditorArrowZ->AlwaysLoadOnClient = false;
			EditorArrowZ->AlwaysLoadOnServer = false;
			EditorArrowZ->ArrowColor = FColor::Blue;
			EditorArrowZ->ArrowSize = 1.f;
			EditorArrowZ->RelativeRotation = FRotator(90.f, 0.f, 0.f);

			EditorArrowZ->RegisterComponent();
		}
	}
#endif
}
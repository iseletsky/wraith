#include "Wraith.h"
#include "LGPrefabSlotComponentAreaConnector.h"

ULGPrefabSlotComponentAreaConnector::ULGPrefabSlotComponentAreaConnector(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	Probability(1.f)
{
}
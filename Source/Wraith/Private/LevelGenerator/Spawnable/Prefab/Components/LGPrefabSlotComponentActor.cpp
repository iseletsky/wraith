#include "Wraith.h"
#include "LGGeneratingLevel.h"
#include "LGPrefabSlotComponentActor.h"

void ULGPrefabSlotComponentActor::SpawnActor(class ALGGeneratingLevel * GeneratingLevel) const
{
	//default behavior, just spawn an actor at the transform
	if (ActorSpawnClass)
	{
		AActor * Actor = GeneratingLevel->GetWorld()->SpawnActor(ActorSpawnClass);
		Actor->SetActorTransform(GetComponentToWorld());
	}
}
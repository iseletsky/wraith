#include "Wraith.h"
#include "LGSpawnablePrefabArea.h"
#include "LGPrefabSlotComponentAreaSpawnConnector.h"

void ULGPrefabSlotComponentAreaSpawnConnector::InitMetadata(class ALGGeneratingLevel * GeneratingLevel)
{
	Super::InitMetadata(GeneratingLevel);

	SpawnableAreas.InitMetadata(GeneratingLevel);

	for (int32 SpawnableAreaInd = 0; SpawnableAreaInd < SpawnableAreas.SpawnableAreas.Num(); ++SpawnableAreaInd)
	{
		auto SpawnablePrefabArea = SpawnableAreas.GetSpawnablePrefabArea(GeneratingLevel, SpawnableAreaInd);

		//just copy it first
		SpawnableAreaToInboundConnectorRandomBuckets.Emplace(SpawnablePrefabArea->AreaConnectorIndexRandomBuckets);

		//if the list of connector indices is empty, this means consider all connectors
		if (SpawnableAreaInboundConnector[SpawnableAreaInd].SpawnableAreaInboundConnectorNames.Num())
		{
			TSet<int32> IncludeSet;
			TSet<int32> ExcludeSet;

			//add corresponding indices from names to include set
			for (auto Iter = SpawnableAreaInboundConnector[SpawnableAreaInd].SpawnableAreaInboundConnectorNames.CreateConstIterator(); Iter; ++Iter)
			{
				for (auto MapIter = SpawnablePrefabArea->NamedAreaConnectorSlots.CreateConstKeyIterator(*Iter); MapIter; ++MapIter)
				{
					IncludeSet.Add(MapIter.Value());
				}
			}

			SpawnableAreaToInboundConnectorRandomBuckets[SpawnableAreaInd].GetResultSet(ExcludeSet);

			ExcludeSet = ExcludeSet.Difference(IncludeSet);

			for (auto Iter = ExcludeSet.CreateConstIterator(); Iter; ++Iter)
			{
				SpawnableAreaToInboundConnectorRandomBuckets[SpawnableAreaInd].RemoveResult(*Iter);
			}
		}
	}
}
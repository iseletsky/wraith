#include "Wraith.h"
#include "LGPrefabSlotComponentSpawn.h"

void ULGPrefabSlotComponentSpawn::InitMetadata(class ALGGeneratingLevel * GeneratingLevel)
{
	SpawnTypeRandomBuckets.Empty();

	if (SpawnTypes.Num())
	{
		for (int32 SpawnTypeInd = 0; SpawnTypeInd < SpawnTypes.Num(); ++SpawnTypeInd)
		{
			auto& SpawnTypeInfo = SpawnTypes[SpawnTypeInd];

			if (SpawnTypeInfo.bDisabled)
			{
				UE_LOG(LG, Warning, TEXT("ULGPrefabSlotComponentSpawn::InitMetadata(): Spawn type %d is disabled in the spawn list for debugging purposes.  Put a breakpoint here to find out which class."), SpawnTypeInd);
			}

			if (SpawnTypeInfo.PrefabClass)
			{
				if (!SpawnTypeInfo.bDisabled)
				{
					SpawnTypeRandomBuckets.PreAddResult(SpawnTypeInfo.Probability, SpawnTypeInd);
				}
			}
		}

		SpawnTypeRandomBuckets.NormalizeBuckets();
	}
}
#include "Wraith.h"
#include "LGPrefabSlotComponentConnector.h"

void ULGPrefabSlotComponentConnector::InitMetadata(class ALGGeneratingLevel * GeneratingLevel)
{
	ConnectorTypeRandomBuckets.Empty();

	if (ConnectorTypes.Num())
	{
		for (int32 ConnectorTypeInd = 0; ConnectorTypeInd < ConnectorTypes.Num(); ++ConnectorTypeInd)
		{
			auto& ConnectorTypeInfo = ConnectorTypes[ConnectorTypeInd];

			if (ConnectorTypeInfo.bDisabled)
			{
				UE_LOG(LG, Warning, TEXT("ULGPrefabSlotComponentConnector::InitMetadata(): Connector type %d is disabled in the connector list for debugging purposes.  Put a breakpoint here to find out which class."), ConnectorTypeInd);
			}
			
			if (ConnectorTypeInfo.ConnectorPrefabClass)
			{
				if (!ConnectorTypeInfo.bDisabled)
				{
					ConnectorTypeRandomBuckets.PreAddResult(ConnectorTypeInfo.Probability, ConnectorTypeInd);
				}
			}
		}

		ConnectorTypeRandomBuckets.NormalizeBuckets();

		//set up the reverse lookup
		for (int32 ConnectorTypeIndex = 0; ConnectorTypeIndex < ConnectorTypes.Num(); ++ConnectorTypeIndex)
		{
			ConnectorClassToConnectorTypeReverseLookup.Add(ConnectorTypes[ConnectorTypeIndex].ConnectorPrefabClass, ConnectorTypeIndex);
		}
	}
}

void ULGPrefabSlotComponentConnector::GetCommonConnectorClasses(const ULGPrefabSlotComponentConnector * OtherConnectorSlot, TSet<TSubclassOf<ALGSpawnablePrefabConnector>>& OutClasses) const
{
	//TODO: is there a more efficient way to do this?
	TArray<TSubclassOf<ALGSpawnablePrefabConnector>> ThisConnectorClasses;
	ConnectorClassToConnectorTypeReverseLookup.GetKeys(ThisConnectorClasses);

	TArray<TSubclassOf<ALGSpawnablePrefabConnector>> OtherConnectorClasses;
	OtherConnectorSlot->ConnectorClassToConnectorTypeReverseLookup.GetKeys(OtherConnectorClasses);

	TSet<TSubclassOf<ALGSpawnablePrefabConnector>> ThisConnectorClassesSet(ThisConnectorClasses);
	TSet<TSubclassOf<ALGSpawnablePrefabConnector>> OtherConnectorClassesSet(OtherConnectorClasses);

	OutClasses = ThisConnectorClassesSet.Intersect(OtherConnectorClassesSet);

	//remove the no connector class
	OutClasses.Remove((TSubclassOf<ALGSpawnablePrefabConnector>) NULL);
}
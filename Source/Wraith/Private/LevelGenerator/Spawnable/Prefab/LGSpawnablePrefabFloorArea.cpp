#include "Wraith.h"
#include "LGSpawnablePrefabFloorArea.h"

ALGSpawnablePrefabFloorArea::ALGSpawnablePrefabFloorArea(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	TopPoint = ObjectInitializer.CreateDefaultSubobject<ULGPrefabSlotComponentPoint>(this, TEXT("TopPoint"));
	TopPoint->SetupAttachment(RootComponent);

	BottomPoint = ObjectInitializer.CreateDefaultSubobject<ULGPrefabSlotComponentPoint>(this, TEXT("Bottom"));
	BottomPoint->SetupAttachment(RootComponent);
}

FTransform ALGSpawnablePrefabFloorArea::GetTransformFromBottomToTop()
{
	return TopPoint->ComponentToWorld.GetRelativeTransform(BottomPoint->ComponentToWorld);
}

FTransform ALGSpawnablePrefabFloorArea::GetTransformFromTopToBottom()
{
	return BottomPoint->ComponentToWorld.GetRelativeTransform(TopPoint->ComponentToWorld);
}

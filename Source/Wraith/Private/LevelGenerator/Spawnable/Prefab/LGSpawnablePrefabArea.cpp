#include "Wraith.h"
#include "LGSpawnablePrefabAreaLayoutContext.h"
#include "LGGeneratingLevel.h"
#include "LGGeneratedLevel.h"
#include "LGAreaInfo.h"
#include "LGSpawnablePrefabArea.h"
#include "LGPrefabSlotComponentAreaPoint.h"
#include "Components/ArrowComponent.h"

const float DESTINATION_CONNECTOR_CELL_SIZE = 30.f;

ALGSpawnablePrefabArea::ALGSpawnablePrefabArea(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	AreaNode = ObjectInitializer.CreateDefaultSubobject<ULGPrefabSlotComponentAreaNode>(this, TEXT("AreaNode"));
	AreaNode->SetupAttachment(RootComponent);
}

UClass * ALGSpawnablePrefabArea::GetSpawnableAreaClass() const
{
	return GetClass();
}

void ALGSpawnablePrefabArea::InitMetadata(class ALGGeneratingLevel * GeneratingLevel)
{
	ALGSpawnablePrefab::InitMetadata(GeneratingLevel);
		
	//normalize the buckets
	AreaConnectorIndexRandomBuckets.NormalizeBuckets();
	PriorityAreaConnectorIndexRandomBuckets.NormalizeBuckets();

	for (auto Iter = AreaConnectorClassToConnectorIndexRandomBuckets.CreateIterator(); Iter; ++Iter)
	{
		Iter->Value.NormalizeBuckets();
	}
}

bool ALGSpawnablePrefabArea::ProcessSceneComponent(USceneComponent * Component, class ALGGeneratingLevel * GeneratingLevel)
{
	if (Super::ProcessSceneComponent(Component, GeneratingLevel))
	{
		return true;
	}
	
	//check if area node component
	{
		ULGPrefabSlotComponentAreaNode * AreaNodeComponent = Cast<ULGPrefabSlotComponentAreaNode>(Component);

		if (AreaNodeComponent)
		{
			AreaNodeConnectorIndexRandomBuckets.Emplace();
			auto& NodeConnectorIndexRandomBuckets = AreaNodeConnectorIndexRandomBuckets.Last();

			AreaNodePriorityConnectorIndexRandomBuckets.Emplace();
			auto& NodePriorityConnectorIndexRandomBuckets = AreaNodePriorityConnectorIndexRandomBuckets.Last();

			AreaNodeToAreaCollision.Emplace();
			auto& LastAreaCollision = AreaNodeToAreaCollision.Last();

			AreaNodeToAreaPoint.Emplace();
			auto& AreaPoints = AreaNodeToAreaPoint.Last();

			//go over all the child components which should only be connectors and add them to the connector to area node map
			for (int32 ChildInd = 0; ChildInd < AreaNodeComponent->GetAttachChildren().Num(); ++ChildInd)
			{
				USceneComponent * Child = AreaNodeComponent->GetAttachChildren()[ChildInd];
				
				//check if area connector component
				auto ProcessChildComp = [&]()
				{
					//check if collision area component
					if (Child->ComponentHasTag(COMPONENT_AREA_COLLISION_TAG)
						|| Child->ComponentHasTag(COMPONENT_AREA_SPAWN_COLLISION_TAG)
						|| Child->ComponentHasTag(COMPONENT_AREA_OMIT_SPAWN_COLLISION_TAG))
					{
						UPrimitiveComponent * CollisionComponent = Cast<UPrimitiveComponent>(Child);

						if (CollisionComponent)
						{
							LastAreaCollision.Add(CollisionComponent);
						}

						return;
					}

					//area point component
					{
						ULGPrefabSlotComponentAreaPoint * AreaPointComponent = Cast<ULGPrefabSlotComponentAreaPoint>(Child);

						if (AreaPointComponent)
						{
							AreaPoints.Add(AreaPointComponent);

							return;
						}
					}

					//area connector component
					{
						ULGPrefabSlotComponentAreaConnector * AreaConnectorComponent = Cast<ULGPrefabSlotComponentAreaConnector>(Child);

						if (AreaConnectorComponent)
						{
							if (AreaConnectorComponent->bDisabled)
							{
								UE_LOG(LG, Warning, TEXT("ALGSpawnablePrefabArea::ProcessSceneComponent(): Connector slot component disabled in the spawnable prefab area for debugging purposes.  Put a breakpoint here to find out which class."));
								return;
							}

							int32 ConnectorInd = AreaConnectorSlots.Num();

							AreaConnectorComponent->InitMetadata(GeneratingLevel);

							AreaConnectorSlots.Add(AreaConnectorComponent);
							AreaConnectorIndexRandomBuckets.PreAddResult(AreaConnectorComponent->Probability, ConnectorInd);
							NodeConnectorIndexRandomBuckets.PreAddResult(AreaConnectorComponent->Probability, ConnectorInd);

							if (AreaConnectorComponent->bPriorityConnector)
							{
								PriorityAreaConnectorIndexRandomBuckets.PreAddResult(AreaConnectorComponent->Probability, ConnectorInd);
								NodePriorityConnectorIndexRandomBuckets.PreAddResult(AreaConnectorComponent->Probability, ConnectorInd);
							}

							if (AreaConnectorComponent->ConnectorName != FName(TEXT("None")))
							{
								NamedAreaConnectorSlots.Add(AreaConnectorComponent->ConnectorName, ConnectorInd);
							}

							for (int32 ConnectorTypeInd = 0; ConnectorTypeInd < AreaConnectorComponent->ConnectorTypes.Num(); ++ConnectorTypeInd)
							{
								auto& ConnectorTypeInfo = AreaConnectorComponent->ConnectorTypes[ConnectorTypeInd];

								if (ConnectorTypeInfo.ConnectorPrefabClass)
								{
									AreaConnectorClassToConnectorIndexRandomBuckets.FindOrAdd(ConnectorTypeInfo.ConnectorPrefabClass).PreAddResult(AreaConnectorComponent->Probability + ConnectorTypeInfo.Probability, ConnectorInd);
								}
							}

							ConnectorToAreaNodeInd.Add(AreaConnectorComponent, AreaNodes.Num());
							return;
						}
					}

					//check if area spawn connector component
					{
						ULGPrefabSlotComponentAreaSpawnConnector * AreaSpawnConnectorComponent = Cast<ULGPrefabSlotComponentAreaSpawnConnector>(Child);

						if (AreaSpawnConnectorComponent)
						{
							if (AreaSpawnConnectorComponent->bDisabled)
							{
								UE_LOG(LG, Warning, TEXT("ALGSpawnablePrefabArea::ProcessSceneComponent(): Connector slot component disabled in the spawnable prefab area for debugging purposes.  Put a breakpoint here to find out which class."));
								return;
							}

							AreaSpawnConnectorComponent->InitMetadata(GeneratingLevel);

							AreaSpawnConnectorSlots.Add(AreaSpawnConnectorComponent);

							ConnectorToAreaNodeInd.Add(AreaSpawnConnectorComponent, AreaNodes.Num());
							return;
						}
					}

					//check if area spawn destination connector component
					{
						ULGPrefabSlotComponentAreaSpawnDestinationConnector * AreaSpawnDestinationConnectorComponent = Cast<ULGPrefabSlotComponentAreaSpawnDestinationConnector>(Child);

						if (AreaSpawnDestinationConnectorComponent)
						{
							if (AreaSpawnDestinationConnectorComponent->bDisabled)
							{
								UE_LOG(LG, Warning, TEXT("ALGSpawnablePrefabArea::ProcessSceneComponent(): Connector slot component disabled in the spawnable prefab area for debugging purposes.  Put a breakpoint here to find out which class."));
								return;
							}

							int32 ConnectorInd = AreaSpawnDestinationConnectorSlots.Num();

							AreaSpawnDestinationConnectorComponent->InitMetadata(GeneratingLevel);

							AreaSpawnDestinationConnectorSlots.Add(AreaSpawnDestinationConnectorComponent);

							PositionToAreaSpawnDestinationConnectorSlots.Add(GridCellForPosition(AreaSpawnDestinationConnectorComponent->GetComponentToWorld().GetTranslation(), FVector(DESTINATION_CONNECTOR_CELL_SIZE)), ConnectorInd);

							ConnectorToAreaNodeInd.Add(AreaSpawnDestinationConnectorComponent, AreaNodes.Num());
							return;
						}
					}
				};

				ProcessChildComp();
			}

			NodeConnectorIndexRandomBuckets.NormalizeBuckets();
			NodePriorityConnectorIndexRandomBuckets.NormalizeBuckets();

			AreaNodes.Add(AreaNodeComponent);

			return true;
		}
	}

	return false;
}

void ALGSpawnablePrefabArea::GetConnectorTypes(TArray<TSubclassOf<ALGSpawnablePrefabConnector>>& OutArray) const
{
	OutArray.Empty();
	AreaConnectorClassToConnectorIndexRandomBuckets.GetKeys(OutArray);
}

void ALGSpawnablePrefabArea::InitCreationContext(CreationContext& Context, class ALGGeneratingLevel * GeneratingLevel, bool SetupPrespawnCollision) const
{
	Context.ParentPrefabArea = this;

	Context.AreaInfos.Empty();

	for (int32 AreaNodeInd = 0; AreaNodeInd < AreaNodes.Num(); ++AreaNodeInd)
	{
		Context.AreaInfos.Emplace();
		auto & ContextAreaInfo = Context.AreaInfos.Last();
				
		auto AreaNode = AreaNodes[AreaNodeInd];

		ContextAreaInfo.AreaInfo = (ALGAreaInfo *)GeneratingLevel->GetWorld()->SpawnActor(ALGAreaInfo::StaticClass());
		ContextAreaInfo.AreaInfo->SetActorTransform(AreaNode->GetComponentToWorld());
		
		auto& AreaCollision = AreaNodeToAreaCollision[AreaNodeInd];

		//Spawn the AreaCollision components in the ALGAreaLayoutInfoSpawnablePrefab
		for (int32 AreaCollisionIndex = 0; AreaCollisionIndex < AreaCollision.Num(); ++AreaCollisionIndex)
		{
			auto AreaCollisionComp = AreaCollision[AreaCollisionIndex];

			//skip creating this if we're not setting up prespawn collision and this component is only used in prespawn
			if (!SetupPrespawnCollision && AreaCollisionComp->ComponentHasTag(COMPONENT_AREA_SPAWN_COLLISION_TAG))
			{
				continue;
			}

			UPrimitiveComponent* NewComp = NewObject<UPrimitiveComponent>(ContextAreaInfo.AreaInfo, AreaCollisionComp->GetClass(), NAME_None, RF_NoFlags, AreaCollisionComp);
			NewComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
			NewComp->SetupAttachment(ContextAreaInfo.AreaInfo->GetRootComponent());
			//NewComp->AttachChildren.Empty();
			NewComp->SetRelativeTransform(AreaCollisionComp->GetComponentToWorld().GetRelativeTransform(AreaNode->GetComponentToWorld()));

			NewComp->RegisterComponent();

			if (SetupPrespawnCollision)
			{
				if (!NewComp->ComponentHasTag(COMPONENT_AREA_OMIT_SPAWN_COLLISION_TAG))
				{
					ContextAreaInfo.CheckAreaCollision.Add(NewComp);
				}

				if (NewComp->ComponentHasTag(COMPONENT_AREA_SPAWN_COLLISION_TAG))
				{
					Context.CheckAreaCollisionDeleteAfterLayout.Add(NewComp);
				}
			}

			if (NewComp->ComponentHasTag(COMPONENT_AREA_OMIT_BRANCH_SPAWN_COLLISION_TAG))
			{
				ContextAreaInfo.AreaOmitBranchSpawnCollision.Add(NewComp);
			}
		}
	}

	//Set up the ALGConnectorInfos
	Context.InitialConnectorIndex = GeneratingLevel->ConnectorLayoutInfos.Num();

	for (int32 ConnectorIndex = 0; ConnectorIndex < AreaConnectorSlots.Num(); ++ConnectorIndex)
	{
		auto ConnectorSlot = AreaConnectorSlots[ConnectorIndex];

		auto & ContextAreaInfo = Context.AreaInfos[ConnectorToAreaNodeInd[ConnectorSlot]];
				
		ALGConnectorInfo* ConnectorInfo = (ALGConnectorInfo *)GeneratingLevel->GetWorld()->SpawnActor(ALGConnectorInfo::StaticClass());

		ContextAreaInfo.ConnectorInfos.Emplace(ConnectorInfo);
		ContextAreaInfo.ConnectorLayoutInfos.Emplace();
		auto& ConnectorLayoutInfo = ContextAreaInfo.ConnectorLayoutInfos.Last();

		ConnectorInfo->AreaIndex = GeneratingLevel->AreaLayoutInfos.Num() + ConnectorToAreaNodeInd[ConnectorSlot];
		ConnectorLayoutInfo.bIsPrimaryEnd = false;
		ConnectorLayoutInfo.ConnectorClass = NULL;
		ConnectorInfo->OtherConnectorIndex = -1;
		ConnectorLayoutInfo.SourceComponent = ConnectorSlot;

		ConnectorLayoutInfo.SetupWallClass();
		ConnectorLayoutInfo.SetupAreaCollision(GeneratingLevel, ConnectorInfo);
	}
}

void ALGSpawnablePrefabArea::CreationContext::Destroy()
{
	for (int32 AreaInd = 0; AreaInd < AreaInfos.Num(); ++AreaInd)
	{
		auto& ContextAreaInfo = AreaInfos[AreaInd];

		ContextAreaInfo.AreaInfo->Destroy();

		for (int32 ConnectorInd = 0; ConnectorInd < ContextAreaInfo.ConnectorInfos.Num(); ++ConnectorInd)
		{
			ContextAreaInfo.ConnectorInfos[ConnectorInd]->Destroy();
		}
	}

	AreaInfos.Empty();
	CheckAreaCollisionDeleteAfterLayout.Empty();
}

bool ALGSpawnablePrefabArea::CreationContext::AreaCollisionIntersectionTest(ALGGeneratingLevel * GeneratingLevel,
	FTransform Transform,
	int32 EntryThisConnectorIndexToIgnore,
	int32 EntryLevelConnectorIndexToIgnore,
	int32 ConnectorCongruencyId,
	TSet<int32>& FailedCongruencyIds) const
{
	//visualize the intersection test
	GeneratingLevel->AddObjectToStep(ParentPrefabArea->GetClass(), Transform);
	GeneratingLevel->EndAddingToStep(false);

	//see if this spawnable area will fit
	TArray<FOverlapResult> OverlapResult;	//I don't actually need this list, just a boolean check

	std::function<void()> ReenableAreaCollisions;

	//ignore collision with other connector
	FComponentQueryParams MainQueryParams(LG_AREA_COLLISION_PROFILE);

	if (EntryLevelConnectorIndexToIgnore >= 0)
	{
		MainQueryParams.AddIgnoredActor(GeneratingLevel->GeneratedLevel->ConnectorInfos[EntryLevelConnectorIndexToIgnore]);
	}

	//temporarily disable collision on area collisions in other area marked for ignoring if branching from that area
	{
		auto& OtherAreaLayoutInfo = GeneratingLevel->AreaLayoutInfos[GeneratingLevel->GeneratedLevel->ConnectorInfos[EntryLevelConnectorIndexToIgnore]->AreaIndex];

		for (int32 CollisionInd = 0; CollisionInd < OtherAreaLayoutInfo.AreaOmitBranchSpawnCollision.Num(); ++CollisionInd)
		{
			OtherAreaLayoutInfo.AreaOmitBranchSpawnCollision[CollisionInd]->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}

		//lambda to reenable area collisions before returning
		ReenableAreaCollisions = [&]()
		{
			for (int32 CollisionInd = 0; CollisionInd < OtherAreaLayoutInfo.AreaOmitBranchSpawnCollision.Num(); ++CollisionInd)
			{
				OtherAreaLayoutInfo.AreaOmitBranchSpawnCollision[CollisionInd]->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
			}
		};
	}

	for (int32 AreaInd = 0; AreaInd < AreaInfos.Num(); ++AreaInd)
	{
		auto& ContextAreaInfo = AreaInfos[AreaInd];		

		//ignore the area layout info itself and the connector components being considered for entry
		FComponentQueryParams QueryParams(MainQueryParams);
		QueryParams.AddIgnoredActor(ContextAreaInfo.AreaInfo);

		for (int32 ConnectorIndex = 0; ConnectorIndex < ContextAreaInfo.ConnectorInfos.Num(); ++ConnectorIndex)
		{
			MainQueryParams.AddIgnoredActor(ContextAreaInfo.ConnectorInfos[ConnectorIndex]);
		}

		//intersection test against all AreaCollision components
		for (int32 AreaCollisionInd = 0; AreaCollisionInd < ContextAreaInfo.CheckAreaCollision.Num(); ++AreaCollisionInd)
		{
			FTransform CompTransform = ContextAreaInfo.CheckAreaCollision[AreaCollisionInd]->GetRelativeTransform() * ContextAreaInfo.AreaInfo->GetTransform() * Transform;	//TODO: make sure this is right
			
			if (GeneratingLevel->GetWorld()->ComponentOverlapMulti(OverlapResult, ContextAreaInfo.CheckAreaCollision[AreaCollisionInd],
				CompTransform.GetLocation(), CompTransform.Rotator(),
				QueryParams, FCollisionObjectQueryParams(COLLISION_LG_AREA)))
			{
				//if there's a congruency id on this connector, track its failure
				if (ConnectorCongruencyId != 0)
				{
					FailedCongruencyIds.Add(ConnectorCongruencyId);
				}
				ReenableAreaCollisions();
				return false;
			}
		}

		//intersection test against all connector collisions
		for (int32 ConnectorIndex = 0; ConnectorIndex < ContextAreaInfo.ConnectorInfos.Num(); ++ConnectorIndex)
		{
			if (EntryThisConnectorIndexToIgnore == ConnectorIndex)
			{
				continue;
			}
			
			auto& AreaCollisionComps = ContextAreaInfo.ConnectorInfos[ConnectorIndex]->GetRootComponent()->GetAttachChildren();

			for (int32 AreaCollisionInd = 0; AreaCollisionInd < AreaCollisionComps.Num(); ++AreaCollisionInd)
			{
				auto CollisionComp = Cast<UPrimitiveComponent>(AreaCollisionComps[AreaCollisionInd]);

				FTransform CompTransform = CollisionComp->GetRelativeTransform() * Transform;

				if (GeneratingLevel->GetWorld()->ComponentOverlapMulti(OverlapResult, CollisionComp,
					CompTransform.GetLocation(), CompTransform.Rotator(),
					QueryParams, FCollisionObjectQueryParams(COLLISION_LG_AREA)))
				{
					ReenableAreaCollisions();
					return false;
				}
			}
		}
	}

	ReenableAreaCollisions();

	return true;
}

bool ALGSpawnablePrefabArea::FindAreaLayoutTransform(class ALGGeneratingLevel * GeneratingLevel,
	FRandomStream& RandomStream,
	CreationContext& Context,
	FTransform& Transform,
	int32& ChosenConnectorSlotIndex,
	TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
	int32 EntryConnectorIndex) const
{
	ChosenConnectorSlotIndex = -1;
	
	InitCreationContext(Context, GeneratingLevel);

	//pick a connector socket for the entry connector class, or just spawn right at that spot if no connector
	if (EntryConnectorClass)
	{
		//make copy of the connector random buckets
		FAERandomBuckets ConnectorSlotBucketsCopy(AreaConnectorClassToConnectorIndexRandomBuckets[EntryConnectorClass]);

		//Keeps track of already known failed congruency id orientations to avoid needlessly checking all connectors.
		TSet<int32> FailedCongruencyIds;

		//keep trying all valid entry ways until the area fits
		if (!ConnectorSlotBucketsCopy.AttemptActions(RandomStream,
			[&](int32 ConnectorSlotIndex, FRandomStream& RandomStreamCopy)
		{
			ChosenConnectorSlotIndex = ConnectorSlotIndex;

			//get the connector slot
			ULGPrefabSlotComponentAreaConnector * AreaConnectorSlot = AreaConnectorSlots[ChosenConnectorSlotIndex];

			if (FailedCongruencyIds.Contains(AreaConnectorSlot->CongruencyId))
			{
				return false;
			}

			//get the area transform from that connector slot
			//Confusing 3D math that was hard to get right, AVOID MESSING WITH THIS
			FTransform PrefabTransform = AreaConnectorSlot->GetComponentToWorld().Inverse() * FTransform(FRotator(0.f, 180.f, 0.f)) * Transform;

			if (!Context.AreaCollisionIntersectionTest(GeneratingLevel, PrefabTransform, ConnectorSlotIndex, EntryConnectorIndex, AreaConnectorSlot->CongruencyId, FailedCongruencyIds))
			{
				return false;
			}

			Transform = PrefabTransform;
			
			return true;
		}))
		{
			//if doesn't fit, return false

			//now we must destroy the temporarily created area layout info
			Context.Destroy();

			return false;
		}
	}

	return true;
}

void ALGSpawnablePrefabArea::LayoutAreaWithCreationContext(CreationContext& Context,
	class ALGGeneratingLevel * GeneratingLevel,
	FTransform Transform,
	TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
	int32 EntryConnectorIndex,
	int32 ChosenConnectorSlotIndex,
	const TSet<FName>& AreaTags,
	const TArray<FName>& PassNames) const
{
	//UE_LOG(LG_RANDOM, Log, TEXT("AAAASSS Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
		
	GeneratingLevel->AddLogMessage(TEXT("Successfully laying out spawnable prefab area."));

	int32 InitialAreaInfoIndex = GeneratingLevel->AreaLayoutInfos.Num();

	//set up the area spawn info in the generating level
	{
		GeneratingLevel->AreaSpawnInfos.Emplace();

		GeneratingLevel->AreaSpawnInfos.Last().SpawnablePrefabAreaClass = GetClass();
		GeneratingLevel->AreaSpawnInfos.Last().Transform = Transform;
	}
	
	for (int32 AreaNodeInd = 0; AreaNodeInd < Context.AreaInfos.Num(); ++AreaNodeInd)
	{
		//set up the area index info
		for (auto Iter = PassNames.CreateConstIterator(); Iter; ++Iter)
		{
			GeneratingLevel->AreaIndexToPassNames.Add(GeneratingLevel->AreaLayoutInfos.Num(), *Iter);
		}

		//set up the area infos in the generating level
		auto AreaNode = AreaNodes[AreaNodeInd];
		auto& ContextAreaInfo = Context.AreaInfos[AreaNodeInd];
		
		GeneratingLevel->AreaLayoutInfos.Emplace();
		GeneratingLevel->GeneratedLevel->AreaInfos.Emplace(ContextAreaInfo.AreaInfo);
				
		//set up the area info
		ContextAreaInfo.AreaInfo->SetActorTransform(AreaNode->GetComponentToWorld() * Transform);
		ContextAreaInfo.AreaInfo->AreaTags = AreaTags;

		//set up the area layout info
		auto& AreaLayoutInfo = GeneratingLevel->AreaLayoutInfos.Last();

		AreaLayoutInfo.AreaCost = AreaNode->AreaCost;
		AreaLayoutInfo.AreaOmitBranchSpawnCollision = ContextAreaInfo.AreaOmitBranchSpawnCollision;
		
		//set up the area points for the area info
		for (int32 PointInd = 0; PointInd < AreaNodeToAreaPoint[AreaNodeInd].Num(); ++PointInd)
		{
			ULGPrefabSlotComponentAreaPoint * AreaPoint = AreaNodeToAreaPoint[AreaNodeInd][PointInd];

			ULGPrefabSlotComponentAreaPoint * NewAreaPoint = NewObject<ULGPrefabSlotComponentAreaPoint>(ContextAreaInfo.AreaInfo, AreaPoint->GetClass(), NAME_None, RF_NoFlags, AreaPoint);

			NewAreaPoint->SetupAttachment(ContextAreaInfo.AreaInfo->GetRootComponent());
			//NewAreaPoint->AttachChildren.Empty();
			NewAreaPoint->SetRelativeTransform(AreaPoint->GetComponentToWorld().GetRelativeTransform(AreaNode->GetComponentToWorld()));

			NewAreaPoint->RegisterComponent();

			ContextAreaInfo.AreaInfo->AreaPoints.Add(NewAreaPoint->PointName, NewAreaPoint);
		}
		
		//set up the connector buckets for the area info
		for (int32 BucketIndex = 0; BucketIndex < AreaNodeConnectorIndexRandomBuckets[AreaNodeInd].GetBuckets().Num(); ++BucketIndex)
		{
			auto& Bucket = AreaNodeConnectorIndexRandomBuckets[AreaNodeInd].GetBuckets()[BucketIndex];

			//sortof copying the buckets, but I'm referencing the connector index in the generating level instead of the area
			AreaLayoutInfo.ConnectorIndexBuckets.PreAddResult(Bucket.ProbabilityBoundary, Bucket.Result + Context.InitialConnectorIndex);
		}

		for (int32 BucketIndex = 0; BucketIndex < AreaNodePriorityConnectorIndexRandomBuckets[AreaNodeInd].GetBuckets().Num(); ++BucketIndex)
		{
			auto& Bucket = AreaNodePriorityConnectorIndexRandomBuckets[AreaNodeInd].GetBuckets()[BucketIndex];

			//sortof copying the buckets, but I'm referencing the connector index in the generating level instead of the area
			AreaLayoutInfo.PriorityConnectorIndexBuckets.PreAddResult(Bucket.ProbabilityBoundary, Bucket.Result + Context.InitialConnectorIndex);
		}

		//Set up the ALGConnectorInfos for this area and add it to GeneratingLevel	
		for (int32 ConnectorIndex = 0; ConnectorIndex < ContextAreaInfo.ConnectorInfos.Num(); ++ConnectorIndex)
		{			
			auto& ConnectorLayoutInfo = ContextAreaInfo.ConnectorLayoutInfos[ConnectorIndex];
			ALGConnectorInfo* ConnectorInfo = ContextAreaInfo.ConnectorInfos[ConnectorIndex];
			
			//safe to assume this is an area connector, and a good sanity check if this fails
			auto ConnectorSlot = Cast<ULGPrefabSlotComponentAreaConnector>(ConnectorLayoutInfo.SourceComponent);

			ConnectorInfo->SetActorTransform(ConnectorSlot->GetComponentToWorld() * Transform);

			if (ConnectorSlot->bPriorityConnector)
			{
				GeneratingLevel->AreaIndexToFreePriorityConnectorIndices.Add(ConnectorInfo->AreaIndex, GeneratingLevel->ConnectorLayoutInfos.Num());
			}

			GeneratingLevel->ConnectorLayoutInfos.Emplace(ConnectorLayoutInfo);
			GeneratingLevel->GeneratedLevel->ConnectorInfos.Emplace(ConnectorInfo);

			GeneratingLevel->ConnectorLayoutInfos.Last().ConnectorIndex = GeneratingLevel->ConnectorLayoutInfos.Num() - 1;
		}
	}

	//increment the number of times this class has been spawned
	GeneratingLevel->IncrementTimesClassSpawned(GetClass());
	
	//set up the connector link for the newly created connectors if an entry connector was used
	if (ChosenConnectorSlotIndex >= 0)
	{
		GeneratingLevel->SetupConnectorLink(EntryConnectorIndex, ChosenConnectorSlotIndex + Context.InitialConnectorIndex, EntryConnectorClass);
	}

	//delete the temporary spawn only connectors
	for (int32 AreaCollision = 0; AreaCollision < Context.CheckAreaCollisionDeleteAfterLayout.Num(); ++AreaCollision)
	{
		Context.CheckAreaCollisionDeleteAfterLayout[AreaCollision]->DestroyComponent();
	}

	//visualize the area that was added
	GeneratingLevel->AddObjectToStep(GetClass(), Transform);
	GeneratingLevel->EndAddingToStep(true);

	//lay out areas from the area spawn connectors
	for (int32 AreaSpawnConnectorInd = 0; AreaSpawnConnectorInd < AreaSpawnConnectorSlots.Num(); ++AreaSpawnConnectorInd)
	{
		auto AreaSpawnConnector = AreaSpawnConnectorSlots[AreaSpawnConnectorInd];
		int32 LevelConnectorIndex = GeneratingLevel->ConnectorLayoutInfos.Num();

		//create a connector in the generating level for this
		{
			ALGConnectorInfo* ConnectorInfo = (ALGConnectorInfo *)GeneratingLevel->GetWorld()->SpawnActor(ALGConnectorInfo::StaticClass());

			GeneratingLevel->ConnectorLayoutInfos.Emplace();
			auto& ConnectorLayoutInfo = GeneratingLevel->ConnectorLayoutInfos.Last();

			ConnectorInfo->AreaIndex = InitialAreaInfoIndex + ConnectorToAreaNodeInd[AreaSpawnConnector];
			ConnectorLayoutInfo.bIsPrimaryEnd = true;
			ConnectorLayoutInfo.ConnectorClass = NULL;
			ConnectorInfo->OtherConnectorIndex = -1;
			ConnectorLayoutInfo.SourceComponent = AreaSpawnConnector;
			ConnectorLayoutInfo.ConnectorIndex = LevelConnectorIndex;
			
			ConnectorLayoutInfo.SetupWallClass();
			ConnectorLayoutInfo.SetupAreaCollision(GeneratingLevel, ConnectorInfo);
			
			ConnectorInfo->SetActorTransform(AreaSpawnConnector->GetComponentToWorld() * Transform);

			GeneratingLevel->GeneratedLevel->ConnectorInfos.Emplace(ConnectorInfo);
		}		

		//choose a random area to spawn, it's assumed any areas here are spawnable no problem, so no need to check anything
		int32 AreaIndex = AreaSpawnConnector->SpawnableAreas.SpawnableAreaBuckets.GetResult(GeneratingLevel->RandomStream.GetFraction());

		if (AreaIndex >= 0)
		{
			int32 InboundConnectorIndex = AreaSpawnConnector->SpawnableAreaToInboundConnectorRandomBuckets[AreaIndex].GetResult(GeneratingLevel->RandomStream.GetFraction());
		
			auto SpawnableArea = AreaSpawnConnector->SpawnableAreas.GetSpawnablePrefabArea(GeneratingLevel, AreaIndex);
			check(SpawnableArea);

			auto OtherConnector = SpawnableArea->AreaConnectorSlots[InboundConnectorIndex];

			TSubclassOf<ALGSpawnablePrefabConnector> ConnectorClass = NULL;

			//choose an entry connector class that is in both sets, IT'S ASSUMED THERE IS ONE!
			{
				TSet<TSubclassOf<ALGSpawnablePrefabConnector>> Intersection;
				AreaSpawnConnector->GetCommonConnectorClasses(OtherConnector, Intersection);
			
				TArray<TSubclassOf<ALGSpawnablePrefabConnector>> IntersectionArray = Intersection.Array();

				check(IntersectionArray.Num());

				//choose a random connector class from the intersection array
				ConnectorClass = IntersectionArray[GeneratingLevel->RandomStream.RandHelper(IntersectionArray.Num())];
			}
		
			check(ConnectorClass);
		
			//cache the initial connector info index so all new ones are considered for joining
			int32 InitialConnectorIndex = GeneratingLevel->ConnectorLayoutInfos.Num();

			SpawnableArea->LayoutAreaWithEntry(GeneratingLevel, GeneratingLevel->RandomStream, ConnectorClass, LevelConnectorIndex, InboundConnectorIndex, AreaTags, PassNames);
		
			int32 AfterConnectorNum = GeneratingLevel->ConnectorLayoutInfos.Num();

			//find the new connector infos that align with the current area's area spawn destination connectors and set them up to be joined
			for (int32 ConnectorInd = InitialConnectorIndex; ConnectorInd < AfterConnectorNum; ++ConnectorInd)
			{
				auto ConnectorLayoutInfo = GeneratingLevel->ConnectorLayoutInfos[ConnectorInd];
				auto ConnectorInfo = GeneratingLevel->GeneratedLevel->ConnectorInfos[ConnectorInd];

				//find transform of the connector relative to the spawn transform of this area so we can perform the quick lookups
				FTransform RelativeConnectorTransform = ConnectorInfo->GetTransform().GetRelativeTransform(Transform);

				FIntVector DestinationLookup = GridCellForPosition(RelativeConnectorTransform.GetTranslation(), FVector(DESTINATION_CONNECTOR_CELL_SIZE));

				//try looking up in a 3x3 area around the transform to find a destination connector
				for (int32 XOff = -1; XOff <= 1; ++XOff)
				{
					for (int32 YOff = -1; YOff <= 1; ++YOff)
					{
						for (int32 ZOff = -1; ZOff <= 1; ++ZOff)
						{
							const int32 * DestinationIndex = PositionToAreaSpawnDestinationConnectorSlots.Find(DestinationLookup + FIntVector(XOff, YOff, ZOff));

							//found the destination connector, time to link them
							if (DestinationIndex)
							{
								auto DestinationComponent = AreaSpawnDestinationConnectorSlots[*DestinationIndex];

								//choose an entry connector class that is in both sets, IT'S ASSUMED THERE IS ONE!
								{
									TSet<TSubclassOf<ALGSpawnablePrefabConnector>> Intersection;
									ConnectorLayoutInfo.SourceComponent->GetCommonConnectorClasses(DestinationComponent, Intersection);

									TArray<TSubclassOf<ALGSpawnablePrefabConnector>> IntersectionArray = Intersection.Array();

									check(IntersectionArray.Num());

									//choose a random connector class from the intersection array
									ConnectorClass = IntersectionArray[GeneratingLevel->RandomStream.RandHelper(IntersectionArray.Num())];
								}

								//it's assumed that the transforms would align here, otherwise you shouldn't have set the area up this way
								//I also don't check if the destination is already used, it's assumed they will never be close enough to overlap

								//create a connector info for the destination object
								int32 NewConnectorIndex = GeneratingLevel->ConnectorLayoutInfos.Num();

								{
									ALGConnectorInfo* ConnectorInfo = (ALGConnectorInfo *)GeneratingLevel->GetWorld()->SpawnActor(ALGConnectorInfo::StaticClass());
									GeneratingLevel->ConnectorLayoutInfos.Emplace();
									auto& ConnectorLayoutInfo = GeneratingLevel->ConnectorLayoutInfos.Last();

									ConnectorInfo->AreaIndex = InitialAreaInfoIndex + ConnectorToAreaNodeInd[DestinationComponent];
									ConnectorLayoutInfo.bIsPrimaryEnd = false;
									ConnectorLayoutInfo.ConnectorClass = NULL;
									ConnectorInfo->OtherConnectorIndex = -1;
									ConnectorLayoutInfo.SourceComponent = DestinationComponent;
									ConnectorLayoutInfo.ConnectorIndex = NewConnectorIndex;

									ConnectorLayoutInfo.SetupWallClass();
									ConnectorLayoutInfo.SetupAreaCollision(GeneratingLevel, ConnectorInfo);

									ConnectorInfo->SetActorTransform(DestinationComponent->GetComponentToWorld() * Transform);

									GeneratingLevel->GeneratedLevel->ConnectorInfos.Emplace(ConnectorInfo);
								}

								//link the 2 connectors
								GeneratingLevel->SetupConnectorLink(ConnectorInd, NewConnectorIndex, ConnectorClass);

								break;
							}
						}
					}
				}
			}
		}
	}

	//UE_LOG(LG_RANDOM, Log, TEXT("CHEESE APPLE MOM Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
}

ULGSpawnableAreaLayoutContext * ALGSpawnablePrefabArea::BeginLayoutArea(float PercentLower, float PercentUpper,
	class ALGGeneratingLevel * GeneratingLevel,
	FRandomStream& RandomStream,
	FTransform Transform,
	TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
	int32 EntryConnectorIndex,
	const TSet<FName>& AreaTags,
	const TArray<FName>& PassNames) const
{
	ULGSpawnablePrefabAreaLayoutContext * Context = NewObject<ULGSpawnablePrefabAreaLayoutContext>(GeneratingLevel, ULGSpawnablePrefabAreaLayoutContext::StaticClass());
	Context->GeneratingLevel = GeneratingLevel;
	Context->RandomStream = &RandomStream;
	Context->Transform = Transform;
	Context->EntryConnectorClass = EntryConnectorClass;
	Context->EntryConnectorIndex = EntryConnectorIndex;
	Context->AreaTags = AreaTags;
	Context->PassNames = PassNames;

	Context->Parent = this;

	return Context;
}

bool ALGSpawnablePrefabArea::LayoutArea(class ALGGeneratingLevel * GeneratingLevel,
	FRandomStream& RandomStream,
	FTransform Transform,
	TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
	int32 EntryConnectorIndex,
	const TSet<FName>& AreaTags,
	const TArray<FName>& PassNames) const
{
	CreationContext Context;
	int32 ChosenConnectorSlotIndex;

	if (!FindAreaLayoutTransform(GeneratingLevel, RandomStream, Context, Transform, ChosenConnectorSlotIndex, EntryConnectorClass, EntryConnectorIndex))
	{
		return false;
	}

	LayoutAreaWithCreationContext(Context, GeneratingLevel, Transform, EntryConnectorClass, EntryConnectorIndex, ChosenConnectorSlotIndex, AreaTags, PassNames);

	return true;
}

bool ALGSpawnablePrefabArea::LayoutAreaWithEntry(class ALGGeneratingLevel * GeneratingLevel,
	FRandomStream& RandomStream,
	TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
	int32 EntryConnectorIndex,
	int32 ThisEntryConnectorIndex,
	const TSet<FName>& AreaTags,
	const TArray<FName>& PassNames) const
{
	CreationContext Context;

	InitCreationContext(Context, GeneratingLevel);

	//figure out the transform based on the entry connector index
	auto InboundConnectorSlot = AreaConnectorSlots[ThisEntryConnectorIndex];
	
	FTransform Transform = InboundConnectorSlot->GetComponentToWorld().Inverse() * FTransform(FRotator(0.f, 180.f, 0.f)) 
		* (GeneratingLevel->GetSpawnableInstance<ALGSpawnablePrefabConnector>(EntryConnectorClass)->GetTransformFromEntryToExit() * GeneratingLevel->GeneratedLevel->ConnectorInfos[EntryConnectorIndex]->GetTransform());
	
	LayoutAreaWithCreationContext(Context,
		GeneratingLevel,
		Transform,
		EntryConnectorClass,
		EntryConnectorIndex,
		ThisEntryConnectorIndex,
		AreaTags,
		PassNames);
	
	return true;
}
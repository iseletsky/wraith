#include "Wraith.h"
#include "LGGeneratingLevel.h"
#include "AEBlankActor.h"
#include "LGSpawnablePrefab.h"
#include "LGPrefabSlotComponentSpawn.h"
#include "LGPrefabSlotComponentActor.h"
#include "Components/ArrowComponent.h"

void ALGSpawnablePrefab::InitMetadata(class ALGGeneratingLevel * GeneratingLevel)
{
	TArray<USceneComponent*> SceneCompList;
	GetComponents<USceneComponent>(SceneCompList);
	
	//now walk through all components
	for (int32 Component = 0; Component < SceneCompList.Num(); ++Component)
	{
		SceneCompList[Component]->UpdateComponentToWorld(EUpdateTransformFlags::SkipPhysicsUpdate);
		ProcessSceneComponent(SceneCompList[Component], GeneratingLevel);
	}
}

bool ALGSpawnablePrefab::ProcessSceneComponent(USceneComponent * Component, class ALGGeneratingLevel * GeneratingLevel)
{
	//check if collision area component
	if (Component->ComponentHasTag(COMPONENT_AREA_COLLISION_TAG) 
		|| Component->ComponentHasTag(COMPONENT_AREA_SPAWN_COLLISION_TAG) 
		|| Component->ComponentHasTag(COMPONENT_AREA_OMIT_SPAWN_COLLISION_TAG))
	{
		UPrimitiveComponent * PrimitiveComponent = Cast<UPrimitiveComponent>(Component);

		if (PrimitiveComponent)
		{
			PrimitiveComponent->SetCollisionProfileName(LG_AREA_COLLISION_PROFILE);

			AreaCollision.Add(PrimitiveComponent);
		}

		return true;
	}

	//check if spawn slot component
	{
		ULGPrefabSlotComponentSpawn * SpawnComponent = Cast<ULGPrefabSlotComponentSpawn>(Component);

		if (SpawnComponent)
		{
			if (!SpawnComponent->bDisabled)
			{
				SpawnComponent->InitMetadata(GeneratingLevel);

				//check if spawn component has a similarity id
				if (SpawnComponent->SimilarityId > 0)
				{
					if (SpawnComponent->SpawnTypes.Num() && !ExistingSimilarSpawnGroups.Contains(SpawnComponent->SimilarityId))
					{
						PrimarySpawnSlots.Add(SpawnComponent);
						ExistingSimilarSpawnGroups.Add(SpawnComponent->SimilarityId);
					}
					else
					{
						SimilarSpawnSlots.Add(SpawnComponent->SimilarityId, SpawnComponent);
					}
				}
				else
				{
					if (SpawnComponent->SpawnTypes.Num())
					{
						PrimarySpawnSlots.Add(SpawnComponent);
					}
				}
			}
			else
			{
				UE_LOG(LG, Warning, TEXT("ALGSpawnablePrefab::ProcessSceneComponent(): Spawnable slot component is disabled for debugging purposes.  Put a breakpoint here to find out which class."));
			}

			return true;
		}
	}

	//check if actor spawn slot component
	{
		ULGPrefabSlotComponentActor * ActorComponent = Cast<ULGPrefabSlotComponentActor>(Component);

		if (ActorComponent)
		{
			ActorSpawnSlots.Add(ActorComponent);

			return true;
		}
	}

	return false;
}

bool ALGSpawnablePrefab::Spawn(class ALGGeneratingLevel * GeneratingLevel, FRandomStream& RandomStream, FTransform Transform) const
{	
	AAEBlankActor * BlankActor = (AAEBlankActor *)GeneratingLevel->GetWorld()->SpawnActor(AAEBlankActor::StaticClass());
	BlankActor->SetActorTransform(Transform);

	//spawn the components specified inside the prefab
    std::function<void(USceneComponent * Destination, USceneComponent * Source)> ComponentSpawnFunc = 
        [&](USceneComponent * Destination, USceneComponent * Source)
    {
        USceneComponent* NewDestComp;

        if (!Source->ComponentHasTag(COMPONENT_NO_SPAWN_TAG))
        {
            USceneComponent* NewComp = NewObject<USceneComponent>(BlankActor, Source->GetClass(), NAME_None, RF_NoFlags, Source);
			//NewComp->AttachChildren.Empty();	//TODO: Do I need to clear AttachChildren when spawning from a template?
			NewComp->SetupAttachment(Destination, NewComp->GetAttachSocketName());
            NewComp->RegisterComponent();

            NewDestComp = NewComp;
        }
        else
        {
            NewDestComp = Destination;
        }
        
        for (USceneComponent * ChildComp : Source->GetAttachChildren())
        {
            ComponentSpawnFunc(NewDestComp, ChildComp);
        }
    };

    ComponentSpawnFunc(BlankActor->GetRootComponent(), GetRootComponent());

	//spawn the things specified in the spawn slots
	for (auto Iter = PrimarySpawnSlots.CreateConstIterator(); Iter; ++Iter)
	{
		auto SpawnSlot = *Iter;

		//choose a spawnable class to spawn
		TSubclassOf<ALGSpawnablePrefabObject> SpawnClass = SpawnSlot->SpawnTypes[SpawnSlot->SpawnTypeRandomBuckets.GetResult(RandomStream.GetFraction())].PrefabClass;

		auto SpawnableInstance = GeneratingLevel->GetSpawnableInstance(SpawnClass);

		SpawnableInstance->Spawn(GeneratingLevel, RandomStream, SpawnSlot->GetComponentToWorld() * Transform);

		//spawn all of the similar slots
		if (SpawnSlot->SimilarityId > 0)
		{
			for (auto SimilarityIter = SimilarSpawnSlots.CreateConstKeyIterator(SpawnSlot->SimilarityId); SimilarityIter; ++SimilarityIter)
			{
				auto SimilarSpawnSlot = SimilarityIter.Value();

				SpawnableInstance->Spawn(GeneratingLevel, RandomStream, SimilarSpawnSlot->GetComponentToWorld() * Transform);
			}
		}
	}

	//spawn the actors specified in the actor slots
	for (int32 ActorSlotInd = 0; ActorSlotInd < ActorSpawnSlots.Num(); ++ActorSlotInd)
	{
		ActorSpawnSlots[ActorSlotInd]->SpawnActor(GeneratingLevel);
	}

	return true;
}

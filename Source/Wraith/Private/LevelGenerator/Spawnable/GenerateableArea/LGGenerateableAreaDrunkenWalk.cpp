#include "Wraith.h"
#include "LGGeneratingLevel.h"
#include "LGGenerateableAreaDrunkenWalk.h"
#include "LGGenerateableAreaDrunkenWalkLayoutContext.h"
#include "LGPrefabSlotComponentConnector.h"
#include "AERandomList.h"

void ALGGenerateableAreaDrunkenWalk::InitMetadata(class ALGGeneratingLevel * GeneratingLevel)
{
	for (int32 Pass = 0; Pass < GeneratorPasses.Num(); ++Pass)
	{
		GeneratorPasses[Pass].SpawnableAreas.InitMetadata(GeneratingLevel);
	}
}

void ALGGenerateableAreaDrunkenWalk::GetConnectorTypes(TArray<TSubclassOf<ALGSpawnablePrefabConnector>>& OutArray) const
{
	OutArray.Empty();
	GeneratorPasses[0].SpawnableAreas.ConnectorClassToSpawnableAreaBuckets.GetKeys(OutArray);
}

ULGSpawnableAreaLayoutContext * ALGGenerateableAreaDrunkenWalk::BeginLayoutArea(float PercentLower, float PercentUpper,
	class ALGGeneratingLevel * GeneratingLevel,
	FRandomStream& RandomStream,
	FTransform Transform,
	TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
	int32 EntryConnectorIndex,
	const TSet<FName>& AreaTags,
	const TArray<FName>& PassNames) const
{
	ULGGenerateableAreaDrunkenWalkLayoutContext * Context = NewObject<ULGGenerateableAreaDrunkenWalkLayoutContext>(GeneratingLevel, ULGGenerateableAreaDrunkenWalkLayoutContext::StaticClass());
	Context->GeneratingLevel = GeneratingLevel;
	Context->RandomStream = &RandomStream;
	Context->Transform = Transform;
	Context->EntryConnectorClass = EntryConnectorClass;
	Context->EntryConnectorIndex = EntryConnectorIndex;	
	Context->AreaTags = AreaTags;
	Context->AreaTags.Append(AreaGraphTags);
	Context->PassNames = PassNames;

	Context->Parent = this;

	Context->PercentLower = PercentLower;
	Context->PercentUpper = PercentUpper;
	Context->PercentInterval = (PercentUpper - PercentLower) / float(GeneratorPasses.Num());

	Context->InitialAreaIndex = GeneratingLevel->AreaLayoutInfos.Num();
	Context->LastAreaIndex = Context->InitialAreaIndex;
		
	Context->CurrentPassIndex = 0;
	Context->bInitPass = true;
	Context->CurrentChildContext = NULL;
	
	return Context;
}

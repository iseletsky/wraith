#include "Wraith.h"
#include "LGGeneratingLevel.h"
#include "LGGeneratedLevel.h"
#include "LGSpawnablePrefabFloorArea.h"
#include "LGGenerateableAreaMultifloorLayoutContext.h"
#include "LGGenerateableAreaMultifloor.h"

void ALGGenerateableAreaMultifloor::InitMetadata(class ALGGeneratingLevel * GeneratingLevel)
{
	ConnectorClassToIncomingFloorType.Empty();

	TSet<TSubclassOf<class ALGSpawnablePrefabConnector>> ConnectorsSet;

	auto ProcessFloorPrefab = [&](TSubclassOf<class ALGSpawnablePrefabArea> PrefabClass, GenerateableAreaFloorType FloorType)
	{
		if (PrefabClass)
		{
			auto Prefab = GeneratingLevel->GetSpawnableInstance<ALGSpawnablePrefabFloorArea>(PrefabClass);
			Prefab->GetConnectorTypes(CachedConnectorTypes);

			for (int32 ConnectorInd = 0; ConnectorInd < CachedConnectorTypes.Num(); ++ConnectorInd)
			{
				TArray<GenerateableAreaFloorType>& ValidFloors = ConnectorClassToIncomingFloorType.FindOrAdd(CachedConnectorTypes[ConnectorInd]);

				ValidFloors.Add(FloorType);
			}

			ConnectorsSet.Append(CachedConnectorTypes);
		}
	};

	ProcessFloorPrefab(BottomFloorPrefab, GenerateableAreaFloorType::BOTTOM);
	ProcessFloorPrefab(MiddleFloorPrefab, GenerateableAreaFloorType::MIDDLE);
	ProcessFloorPrefab(TopFloorPrefab, GenerateableAreaFloorType::TOP);
	
	CachedConnectorTypes = ConnectorsSet.Array();
}

void ALGGenerateableAreaMultifloor::GetConnectorTypes(TArray<TSubclassOf<class ALGSpawnablePrefabConnector>>& OutArray) const
{
	OutArray = CachedConnectorTypes;
}

ULGSpawnableAreaLayoutContext * ALGGenerateableAreaMultifloor::BeginLayoutArea(float PercentLower, float PercentUpper,
	class ALGGeneratingLevel * GeneratingLevel,
	FRandomStream& RandomStream,
	FTransform Transform,
	TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
	int32 EntryConnectorIndex,
	const TSet<FName>& AreaTags,
	const TArray<FName>& PassNames) const
{
	ULGGenerateableAreaMultifloorLayoutContext * Context = NewObject<ULGGenerateableAreaMultifloorLayoutContext>(GeneratingLevel, ULGGenerateableAreaMultifloorLayoutContext::StaticClass());
	Context->GeneratingLevel = GeneratingLevel;
	Context->RandomStream = &RandomStream;
	Context->Transform = Transform;
	Context->EntryConnectorClass = EntryConnectorClass;
	Context->EntryConnectorIndex = EntryConnectorIndex;
	Context->AreaTags = AreaTags;
	Context->AreaTags.Append(AreaGraphTags);
	Context->PassNames = PassNames;

	Context->Parent = this;

	return Context;
}

bool ALGGenerateableAreaMultifloor::LayoutArea(class ALGGeneratingLevel * GeneratingLevel,
	FRandomStream& RandomStream,
	FTransform Transform,
	TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
	int32 EntryConnectorIndex,
	const TSet<FName>& AreaTags,
	const TArray<FName>& PassNames) const
{
	GeneratingLevel->AddLogMessage(TEXT("Multifloor layout"));

	//merge AreaTags with our AreaTags
	TSet<FName> MergedAreaTags(AreaGraphTags);
	MergedAreaTags.Append(AreaTags);

	//figure out how many floors to try to spawn
	int32 NumFloorsToSpawn = RandomStream.RandRange(MinFloors, MaxFloors);

	GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Will try %d floors with Min %d and Max %d"), NumFloorsToSpawn, MinFloors, MaxFloors));
	GeneratingLevel->EndAddingToStep(false);

	//choose a random starting floor type based on incoming connector, and if we only have 2 floors to spawn don't allow middle
	GenerateableAreaFloorType FloorType = GenerateableAreaFloorType::BOTTOM;

	if (EntryConnectorIndex >= 0)
	{
		TArray<GenerateableAreaFloorType> FloorTypes = ConnectorClassToIncomingFloorType[EntryConnectorClass];

		if (NumFloorsToSpawn == 2)
		{
			FloorTypes.Remove(GenerateableAreaFloorType::MIDDLE);
		}

		if (!FloorTypes.Num())
		{
			return false;
		}

		FloorType = FloorTypes[GeneratingLevel->RandomStream.RandHelper(FloorTypes.Num())];
	}
	else
	{
		if (NumFloorsToSpawn == 2)
		{
			FloorType = GeneratingLevel->RandomStream.RandHelper(2) 
				? GenerateableAreaFloorType::BOTTOM
				: GenerateableAreaFloorType::TOP;
		}
		else
		{
			GenerateableAreaFloorType RandFloorType[] = {
				GenerateableAreaFloorType::BOTTOM,
				GenerateableAreaFloorType::MIDDLE,
				GenerateableAreaFloorType::TOP};

			FloorType = RandFloorType[GeneratingLevel->RandomStream.RandHelper(3)];
		}
	}
		
	//try to lay out the initial area	
	ALGSpawnablePrefabArea::CreationContext FirstAreaContext;
	FTransform FirstAreaTransform = Transform;
	int32 FirstAreaChosenConnectorSlotIndex;

	ALGSpawnablePrefabFloorArea * FirstArea = GeneratingLevel->GetSpawnableInstance<ALGSpawnablePrefabFloorArea>(PrefabClassForFloorType(FloorType));
	
	if (!FirstArea->FindAreaLayoutTransform(GeneratingLevel, GeneratingLevel->RandomStream, FirstAreaContext, FirstAreaTransform, FirstAreaChosenConnectorSlotIndex, EntryConnectorClass, EntryConnectorIndex))
	{
		GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Failed to lay out initial floor"), NumFloorsToSpawn, MinFloors, MaxFloors));
		GeneratingLevel->EndAddingToStep(false);

		FirstAreaContext.Destroy();
		return false;
	}
		
	//figure out the maximum amount of room there is to spawn below and above
	int32 MaxFloorsAbove = 0;
	int32 MaxFloorsBelow = 0;

	{
		FTransform CurrentFloorTransform = FirstAreaTransform;

		for (int32 FloorOffset = 0; ; )
		{
			CurrentFloorTransform = FirstArea->GetTransformFromBottomToTop() * CurrentFloorTransform;

			TSet<int32> DummySet;
			if (++FloorOffset >= NumFloorsToSpawn || !FirstAreaContext.AreaCollisionIntersectionTest(GeneratingLevel, CurrentFloorTransform, -1, -1, 0, DummySet))
			{
				break;
			}

			++MaxFloorsAbove;
		}
	}

	{
		FTransform CurrentFloorTransform = FirstAreaTransform;

		for (int32 FloorOffset = 0; ; )
		{
			CurrentFloorTransform = FirstArea->GetTransformFromTopToBottom() * CurrentFloorTransform;

			TSet<int32> DummySet;
			if (++FloorOffset >= NumFloorsToSpawn || !FirstAreaContext.AreaCollisionIntersectionTest(GeneratingLevel, CurrentFloorTransform, -1, -1, 0, DummySet))
			{
				break;
			}

			++MaxFloorsBelow;
		}
	}

	//check if the range still satisfies min floors
	int32 MaxFloorRange = MaxFloorsAbove + MaxFloorsBelow + 1;

	if (MaxFloorRange < MinFloors)
	{
		GeneratingLevel->AddLogMessage(TEXT("Aborint spawning multifloor area since it's not possible to spawn the minimum amount of floors"));
		GeneratingLevel->EndAddingToStep(false);

		FirstAreaContext.Destroy();
		return false;
	}

	if (NumFloorsToSpawn < MaxFloorRange)
	{
		NumFloorsToSpawn = MaxFloorRange;
	}

	auto ForceStartFloorChange = [&](GenerateableAreaFloorType NewFloorType)
	{
		FirstAreaContext.Destroy();

		if (!ConnectorClassToIncomingFloorType[EntryConnectorClass].Contains(NewFloorType))
		{
			return false;
		}

		FloorType = NewFloorType;

		FirstArea = GeneratingLevel->GetSpawnableInstance<ALGSpawnablePrefabFloorArea>(PrefabClassForFloorType(NewFloorType));
		FirstArea->InitCreationContext(FirstAreaContext, GeneratingLevel);

		return true;
	};

	//if there are no free floors below or above, force the starting floor to be either top or bottom
	if (MaxFloorsAbove == 0 && FloorType != GenerateableAreaFloorType::TOP)
	{
		if (!ForceStartFloorChange(GenerateableAreaFloorType::TOP))
		{
			GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Failed to force starting floor to convert to top level floor since it has no matching connectors for %s."), *(EntryConnectorClass->GetName())));
			GeneratingLevel->EndAddingToStep(false);

			return false;
		}
	}
	else if (MaxFloorsBelow == 0 && FloorType != GenerateableAreaFloorType::BOTTOM)
	{
		if (!ForceStartFloorChange(GenerateableAreaFloorType::BOTTOM))
		{
			GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Failed to force starting floor to convert to bottom level floor since it has no matching connectors %s."), *(EntryConnectorClass->GetName())));
			GeneratingLevel->EndAddingToStep(false);

			return false;
		}
	}
	//if unable to satisfy the min floors constraint by staying either a bottom or top floor, force it to become a middle floor
	else if ((FloorType == GenerateableAreaFloorType::TOP && MaxFloorsBelow + 1 < MinFloors)
		|| (FloorType == GenerateableAreaFloorType::BOTTOM && MaxFloorsAbove + 1 < MinFloors))
	{
		if (!ForceStartFloorChange(GenerateableAreaFloorType::MIDDLE))
		{
			GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Failed to force starting floor to convert to middle level floor since it has no matching connectors %s."), *(EntryConnectorClass->GetName())));
			GeneratingLevel->EndAddingToStep(false);

			return false;
		}
	}

	int32 FloorsAbove = 0;
	int32 FloorsBelow = 0;

	//Figure out how many floors on the top and bottom will actually spawn
	if (FloorType == GenerateableAreaFloorType::TOP)
	{
		FloorsBelow = MaxFloorsBelow;
	}
	else if (FloorType == GenerateableAreaFloorType::BOTTOM)
	{
		FloorsAbove = MaxFloorsAbove;
	}
	else
	{
		//find the allowable window within which to randomly generate floors above and below so that NumFloorsToSpawn spawns.
		int32 MinAbove = FMath::Max(1, FMath::Min(MaxFloorsAbove, NumFloorsToSpawn - MaxFloorsBelow - 1));
		int32 MaxAbove = FMath::Min(MaxFloorsAbove, NumFloorsToSpawn - 2);

		FloorsAbove = RandomStream.RandRange(MinAbove, MaxAbove);
		FloorsBelow = FMath::Min(MaxFloorsBelow, NumFloorsToSpawn - FloorsAbove - 1);

		NumFloorsToSpawn = FloorsAbove + FloorsBelow + 1;
		check(NumFloorsToSpawn >= MinFloors);
	}

	//spawn the first area
	int32 FirstMainAreaInfoIndex = GeneratingLevel->AreaLayoutInfos.Num();
	FirstArea->LayoutAreaWithCreationContext(FirstAreaContext, GeneratingLevel, FirstAreaTransform, EntryConnectorClass, EntryConnectorIndex, FirstAreaChosenConnectorSlotIndex, MergedAreaTags, PassNames);

	//spawn the areas above and below
	{
		FTransform CurrentFloorTransform = FirstAreaTransform;
		int32 CurrentParentAreaInfoIndex = FirstMainAreaInfoIndex;

		for (int32 FloorOffset = 0; FloorOffset < FloorsAbove; ++FloorOffset)
		{
			CurrentFloorTransform = FirstArea->GetTransformFromBottomToTop() * CurrentFloorTransform;
						
			if (FloorOffset == FloorsAbove - 1)
			{
				FloorType = GenerateableAreaFloorType::TOP;
			}
			else
			{
				FloorType = GenerateableAreaFloorType::MIDDLE;
			}
			
			int32 MainAreaInfoIndex = GeneratingLevel->AreaLayoutInfos.Num();

			ALGSpawnablePrefabArea::CreationContext NewAreaContext;
			ALGSpawnablePrefabFloorArea * NewArea = GeneratingLevel->GetSpawnableInstance<ALGSpawnablePrefabFloorArea>(PrefabClassForFloorType(FloorType));

			NewArea->InitCreationContext(NewAreaContext, GeneratingLevel, false);
			NewArea->LayoutAreaWithCreationContext(NewAreaContext, GeneratingLevel, CurrentFloorTransform, NULL, -1, -1, MergedAreaTags, PassNames);

			GeneratingLevel->LinkAreas(CurrentParentAreaInfoIndex, MainAreaInfoIndex, -1);

			CurrentParentAreaInfoIndex = MainAreaInfoIndex;
		}
	}

	{
		FTransform CurrentFloorTransform = FirstAreaTransform;
		int32 CurrentParentAreaInfoIndex = FirstMainAreaInfoIndex;

		for (int32 FloorOffset = 0; FloorOffset < FloorsBelow; ++FloorOffset)
		{
			CurrentFloorTransform = FirstArea->GetTransformFromTopToBottom() * CurrentFloorTransform;

			if (FloorOffset == FloorsBelow - 1)
			{
				FloorType = GenerateableAreaFloorType::BOTTOM;
			}
			else
			{
				FloorType = GenerateableAreaFloorType::MIDDLE;
			}

			int32 MainAreaInfoIndex = GeneratingLevel->AreaLayoutInfos.Num();

			ALGSpawnablePrefabArea::CreationContext NewAreaContext;
			ALGSpawnablePrefabFloorArea * NewArea = GeneratingLevel->GetSpawnableInstance<ALGSpawnablePrefabFloorArea>(PrefabClassForFloorType(FloorType));

			NewArea->InitCreationContext(NewAreaContext, GeneratingLevel, false);
			NewArea->LayoutAreaWithCreationContext(NewAreaContext, GeneratingLevel, CurrentFloorTransform, NULL, -1, -1, MergedAreaTags, PassNames);

			GeneratingLevel->LinkAreas(CurrentParentAreaInfoIndex, MainAreaInfoIndex, -1);

			CurrentParentAreaInfoIndex = MainAreaInfoIndex;
		}
	}

	GeneratingLevel->IncrementTimesClassSpawned(GetClass());

	GeneratingLevel->AddLogMessage(TEXT("Succeeded in spawning multifloor area."));
	GeneratingLevel->EndAddingToStep(true);

	return true;
}
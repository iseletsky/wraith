#include "Wraith.h"
#include "LGGenerateableAreaMultifloor.h"
#include "LGGenerateableAreaMultiFloorLayoutContext.h"

ELayoutContextStatus ULGGenerateableAreaMultifloorLayoutContext::ContinueLayout(std::chrono::high_resolution_clock::time_point BeginTime)
{
	return Parent->LayoutArea(GeneratingLevel, *RandomStream, Transform, EntryConnectorClass, EntryConnectorIndex, AreaTags, PassNames)
		? ELayoutContextStatus::SUCCESS
		: ELayoutContextStatus::FAIL;
}
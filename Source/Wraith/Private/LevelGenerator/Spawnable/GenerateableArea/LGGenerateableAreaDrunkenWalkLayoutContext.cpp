#include "Wraith.h"
#include "LGGeneratingLevel.h"
#include "LGGeneratedLevel.h"
#include "LGGenerateableAreaDrunkenWalk.h"
#include "LGPrefabSlotComponentConnector.h"
#include "LGGenerateableAreaDrunkenWalkLayoutContext.h"

ELayoutContextStatus ULGGenerateableAreaDrunkenWalkLayoutContext::ContinueLayout(std::chrono::high_resolution_clock::time_point BeginTime)
{
	//UE_LOG(LG_RANDOM, Log, TEXT("0000 Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

	if (CurrentChildContext)
	{
		//UE_LOG(LG_RANDOM, Log, TEXT("Rapistry Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

		GeneratingLevel->AddLogMessage(TEXT("Continuing layout of child context."));
		GeneratingLevel->EndAddingToStep(false);

		ELayoutContextStatus Result = CurrentChildContext->ContinueLayout(BeginTime);

		//UE_LOG(LG_RANDOM, Log, TEXT("Glarpist Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

		if (Result == ELayoutContextStatus::SUCCESS)
		{
			GeneratingLevel->AddLogMessage(TEXT("Layout of child context was a success."));
			GeneratingLevel->EndAddingToStep(false);

			CurrentChildContext = NULL;
			PostNewAreaLayout();
		}
		else if (Result == ELayoutContextStatus::FAIL)
		{
			GeneratingLevel->AddLogMessage(TEXT("Layout of child context was a failure."));
			GeneratingLevel->EndAddingToStep(false);

			//if fail got returned, something went seriously wrong because that must mean another drunken walk generator failed
			return ELayoutContextStatus::FAIL;
		}

		//UE_LOG(LG_RANDOM, Log, TEXT("Blorpist Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
	}

	while (true)
	{
		//UE_LOG(LG_RANDOM, Log, TEXT("Cheese! Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

		//we're done
		if (CurrentPassIndex >= Parent->GeneratorPasses.Num())
		{
			GeneratingLevel->AddLogMessage(TEXT("Drunken walk generator is done with all current passes."));
			GeneratingLevel->EndAddingToStep(false);

			return ELayoutContextStatus::SUCCESS;
		}

		auto& PassInfo = Parent->GeneratorPasses[CurrentPassIndex];

		//see if we need to initialize the pass
		if (bInitPass)
		{
			GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Drunken walk generator %s is initializing pass %d."), *(GetClass()->GetName()), CurrentPassIndex));
			
			//UE_LOG(LG_RANDOM, Log, TEXT("Init pass Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

			check(!CurrentChildContext);
			//bInitPass = false;	//this gets reset down below now

			PassPassNames = PassNames;
			PassPassNames.Add(PassInfo.PassName);

			PassAreaTags = AreaTags;
			PassAreaTags.Append(PassInfo.AreaGraphTags);
			PassAreaTags.Append(PassPassNames);			
			
			//choose random number of base areas to spawn
			AreaSpawnMaxCost = GeneratingLevel->RandomStream.FRandRange(PassInfo.MinAreaSpawnCost, PassInfo.MaxAreaSpawnCost);
			AreaSpawnedCost = 0;
			
			//index of the first area that would be spawned by this generator
			PassInitialAreaIndex = GeneratingLevel->AreaLayoutInfos.Num();
			
			//index of the first area spawned by a layout call
			LastAreaIndex = PassInitialAreaIndex;

			PassPercentLower = PercentLower + PercentInterval * CurrentPassIndex;

			GeneratingLevel->EndAddingToStep(false);

			//UE_LOG(LG_RANDOM, Log, TEXT("post init pass Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
		}

		//lambda for trying to lay out an area at a known entry connector and class
		FAERandomBuckets::RandomBucketAction AreaLayoutAction = [&](int32 AreaIndex, FRandomStream& RandomStreamCopy)
		{
			//check if the area meets the probability list's max spawn constraint
			if (PassInfo.SpawnableAreas.SpawnableAreas[AreaIndex].LevelMaxSpawn > 0 && !GeneratingLevel->CheckMaxTimesClassSpawnedConstraint(PassInfo.SpawnableAreas.SpawnableAreaSubclassArray[AreaIndex], PassInfo.SpawnableAreas.SpawnableAreas[AreaIndex].LevelMaxSpawn))
			{
				GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Spawnable Area %d not spawning because the LevelMaxSpawn constraint of %d is exceeding."), AreaIndex, PassInfo.SpawnableAreas.SpawnableAreas[AreaIndex].LevelMaxSpawn));
				GeneratingLevel->EndAddingToStep(false);

				return false;
			}

			//UE_LOG(LG_RANDOM, Log, TEXT("LELL Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

			return LayoutAreaAtConnector(PassPercentLower,
				PassPercentLower + PercentInterval,
				BeginTime, 
				PassInfo.SpawnableAreas.GetSpawnableArea(GeneratingLevel, AreaIndex), 
				RandomStreamCopy, 
				Transform, 
				EntryConnectorClass, 
				EntryConnectorIndex, 
				PassAreaTags,
				PassPassNames);

			//UE_LOG(LG_RANDOM, Log, TEXT("HUEHUE Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
		};

		//lambda for trying to lay out an area at a known entry connector but still needs to try all possible connector types from the connector
		FAERandomBuckets::RandomBucketAction AreaLayoutFromConnectorAction = [&](int32 ConnectorIndex, FRandomStream& RandomStreamCopy)
		{
			//check if this connector is usable in this generator

			ULGPrefabSlotComponentConnector * ConnectorSlot = GeneratingLevel->ConnectorLayoutInfos[ConnectorIndex].SourceComponent;

			FAERandomBuckets ConnectorTypeRandomBucketsCopy(ConnectorSlot->ConnectorTypeRandomBuckets);

			//UE_LOG(LG_RANDOM, Log, TEXT("BLORP Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

			bool bDidActionSucceed = ConnectorTypeRandomBucketsCopy.AttemptActions(RandomStreamCopy,
				[&](int32 ConnectorTypeIndex, FRandomStream& RandomStreamCopy)
			{
				TSubclassOf<ALGSpawnablePrefabConnector> ConnectorClass = ConnectorSlot->ConnectorTypes[ConnectorTypeIndex].ConnectorPrefabClass;

				//check if connector class is usable in this generator
				const FAERandomBuckets * SpawnableAreaBuckets = PassInfo.SpawnableAreas.ConnectorClassToSpawnableAreaBuckets.Find(ConnectorClass);

				if (SpawnableAreaBuckets)
				{
					EntryConnectorClass = ConnectorClass;
					EntryConnectorIndex = ConnectorIndex;

					ALGSpawnablePrefabConnector * Connector = GeneratingLevel->GetSpawnableInstance<ALGSpawnablePrefabConnector>(ConnectorClass);

					Transform = Connector->GetTransformFromEntryToExit() * GeneratingLevel->GeneratedLevel->ConnectorInfos[ConnectorIndex]->GetTransform();

					FAERandomBuckets ConnectorClassToSpawnableAreaBucketsCopy(*SpawnableAreaBuckets);
					
					return ConnectorClassToSpawnableAreaBucketsCopy.AttemptActions(RandomStreamCopy, AreaLayoutAction);
				}

				GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Connector class %s not usable in this generator.  Trying others."), *(ConnectorClass->GetName())));
				GeneratingLevel->EndAddingToStep(false);
								
				return false;
			});
						 
			if (!bDidActionSucceed)
			{
				GeneratingLevel->AddLogMessage(TEXT("All attempts to branch failed so marking this connector as not free."));
				GeneratingLevel->EndAddingToStep(false);

				int32 AreaIndex = GeneratingLevel->GeneratedLevel->ConnectorInfos[ConnectorIndex]->AreaIndex;

				GeneratingLevel->AreaLayoutInfos[AreaIndex].RemoveConnectorIndex(ConnectorIndex);

				GeneratingLevel->CheckAreaFreeConnectors(AreaIndex);
			}

			//UE_LOG(LG_RANDOM, Log, TEXT("GLARP Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

			return bDidActionSucceed;
		};

		bool bRanOnce = false;

		while (AreaSpawnedCost < AreaSpawnMaxCost)
		{
			//UE_LOG(LG_RANDOM, Log, TEXT("loop begin Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

			if (CurrentChildContext)
			{
				ELayoutContextStatus Result = CurrentChildContext->ContinueLayout(BeginTime);

				//UE_LOG(LG_RANDOM, Log, TEXT("Stuff in you Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

				if (Result == ELayoutContextStatus::SUCCESS)
				{
					GeneratingLevel->AddLogMessage(TEXT("Continue layout success"));
					GeneratingLevel->EndAddingToStep(false);

					CurrentChildContext = NULL;
					PostNewAreaLayout();

					//UE_LOG(LG_RANDOM, Log, TEXT("Still in me Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
				}
				else if (Result == ELayoutContextStatus::FAIL)
				{
					GeneratingLevel->AddLogMessage(TEXT("Continue layout failure"));
					GeneratingLevel->EndAddingToStep(false);

					//if fail got returned, something went seriously wrong because that must mean another drunken walk generator failed
					return ELayoutContextStatus::FAIL;
				}
			}

			//stop until next game tick
			if (bRanOnce)
			{
				if (std::chrono::high_resolution_clock::now() - BeginTime >= MAX_RUN_SECONDS)
				{
					return ELayoutContextStatus::NOT_FINISHED;
				}
			}
			else
			{
				bRanOnce = true;
			}

			//UE_LOG(LG_RANDOM, Log, TEXT("Wub wub Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

			//if first run, we must try to spawn at the specified connector
			if (bInitPass && CurrentPassIndex == 0)
			{
				if (EntryConnectorIndex >= 0)
				{
					GeneratingLevel->AddLogMessage(TEXT("First run of drunken walk, spawning at specified connector."));
					GeneratingLevel->EndAddingToStep(false);

					//UE_LOG(LG_RANDOM, Log, TEXT("PINGAS Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

					if (!AreaLayoutFromConnectorAction(EntryConnectorIndex, *RandomStream))
					{
						GeneratingLevel->AddLogMessage(TEXT("Failed to spawn first area in drunken walk generator."));
						GeneratingLevel->EndAddingToStep(false);

						return ELayoutContextStatus::FAIL;
					} 

					//UE_LOG(LG_RANDOM, Log, TEXT("TACO BELL Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
				}
				//if no entry connector, this is probably because this is the first ever area in the level
				else
				{
					GeneratingLevel->AddLogMessage(TEXT("First ever area in level."));
					GeneratingLevel->EndAddingToStep(false);

					FAERandomBuckets SpawnableAreaBucketsCopy(PassInfo.SpawnableAreas.SpawnableAreaBuckets);

					//UE_LOG(LG_RANDOM, Log, TEXT("GNAR Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

					if (!SpawnableAreaBucketsCopy.AttemptActions(*RandomStream, AreaLayoutAction))
					{
						GeneratingLevel->AddLogMessage(TEXT("Failed to spawn first ever area."));
						GeneratingLevel->EndAddingToStep(false);

						return ELayoutContextStatus::FAIL;
					}

					//UE_LOG(LG_RANDOM, Log, TEXT("PLAR Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
				}


				bInitPass = false;
			}
			else  //if not first run, try connectors from areas this generator has spawned, or any areas that this pass is allowed to branch off of if it's not branching once
			{
				//UE_LOG(LG_RANDOM, Log, TEXT("Blorginstorp Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

				//pass names allowed to branch off of
				TSet<FName> BranchablePassNames;

				//add current pass name even if it's the first time, it's possible another previous pass shares this pass name
				{
					auto List = GeneratingLevel->PassNameToAreaIndexList.Find(PassInfo.PassName);

					if (List && List->List.Num())
					{
						BranchablePassNames.Add(PassInfo.PassName);
					}
				}

				//try to add branchable passes if this is the first time or not the first time and this pass isn't branching once
				if (bInitPass || (!bInitPass && !PassInfo.bBranchOnce))
				{
					if (PassInfo.BranchablePassNames.Num())
					{
						for (auto Iter = PassInfo.BranchablePassNames.CreateConstIterator(); Iter; ++Iter)
						{
							if (GeneratingLevel->PassNameToAreaIndexList[*Iter].List.Num())
							{
								BranchablePassNames.Add(*Iter);
							}
						}
					}
					else  //if no pass names specified, use previous pass by default
					{
						FName PrevPassName = Parent->GeneratorPasses[CurrentPassIndex - 1].PassName;

						if (GeneratingLevel->PassNameToAreaIndexList[PrevPassName].List.Num())
						{
							BranchablePassNames.Add(PrevPassName);
						}
					}					
				}

				//UE_LOG(LG_RANDOM, Log, TEXT("Blipperp Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
												
				if (!BranchablePassNames.Num())
				{
					GeneratingLevel->AddLogMessage(TEXT("No branchable passes, so stopping."));
					GeneratingLevel->EndAddingToStep(false);

					break;
				}
				
				ULGGenerateableAreaDrunkenWalkChooseBranchArea::LayoutAtAreaFunc AreaLayoutFromAreaAction = [&](int32 AreaIndex, FRandomStream& RandomStreamCopy, bool bUsingPriorityConnectors)
				{
					//UE_LOG(LG_RANDOM, Log, TEXT("CHEESE Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

					FAERandomBuckets AvailableConnectorBucketsCopy(bUsingPriorityConnectors
						? GeneratingLevel->AreaLayoutInfos[AreaIndex].PriorityConnectorIndexBuckets
						: GeneratingLevel->AreaLayoutInfos[AreaIndex].ConnectorIndexBuckets);

					return AvailableConnectorBucketsCopy.AttemptActions(RandomStreamCopy, AreaLayoutFromConnectorAction);

					//UE_LOG(LG_RANDOM, Log, TEXT("CHOSE Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
				};

				//now figure out what logic to use to choose the branch point
				if (PassInfo.ChooseBranchAreaClass && bInitPass)
				{
					GeneratingLevel->AddLogMessage(TEXT("Using a branch callback object to choose an area"));
					GeneratingLevel->EndAddingToStep(false);

					bInitPass = false;

					//let the Branch callback choose an area
					if (!PassInfo.ChooseBranchAreaClass->GetDefaultObject<ULGGenerateableAreaDrunkenWalkChooseBranchArea>()->PassChooseBranchArea(GeneratingLevel, AreaLayoutFromAreaAction, BranchablePassNames, *RandomStream))
					{
						GeneratingLevel->AddLogMessage(TEXT("Branch callback didn't choose an area."));
						GeneratingLevel->EndAddingToStep(false);

						break;
					}
				}
				else
				{
					GeneratingLevel->AddLogMessage(TEXT("Using default logic to choose an area"));
					GeneratingLevel->EndAddingToStep(false);

					bInitPass = false;

					//UE_LOG(LG_RANDOM, Log, TEXT("Galrpistorp Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

					//choose a random Pass name to branch from
					FName BranchPassName = BranchablePassNames.Num() == 1
						? *BranchablePassNames.CreateConstIterator()
						: BranchablePassNames.Array()[RandomStream->RandHelper(BranchablePassNames.Num())];

					//try priority connectors, and if that fails use regular ones
					if (GeneratingLevel->PassNameToPriorityAreaIndexList[BranchPassName].List.Num()
						&& RandomStream->GetFraction() < PassInfo.PriorityBranchProbability)
					{						
						FAERandomList RandomAreaIndexListCopy(GeneratingLevel->PassNameToPriorityAreaIndexList[BranchPassName]);

						GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Trying to branch from priority connectors with %d elements"), RandomAreaIndexListCopy.List.Num()));
						GeneratingLevel->EndAddingToStep(false);

						//prepare for epic multiply nested lambdas!!!
						if (RandomAreaIndexListCopy.AttemptActions(*RandomStream,
							//area index chooser
							[&](int32 ListSize, FRandomStream& RandomStreamCopy)
						{
							//in this case, choose any index
							return RandomStreamCopy.RandHelper(ListSize);
						},

							//action
							[&](int32 AreaIndex, FRandomStream& RandomStreamCopy)
						{
							GeneratingLevel->AddLogMessage(TEXT("Current Priority Area Index List Contents"));
							for (int32 ListInd = 0; ListInd < RandomAreaIndexListCopy.List.Num(); ++ListInd)
							{
								GeneratingLevel->AddLogMessage(FString::Printf(TEXT("%d"), RandomAreaIndexListCopy.List[ListInd]));
							}
							GeneratingLevel->EndAddingToStep(false);

							return AreaLayoutFromAreaAction(AreaIndex, RandomStreamCopy, true);
						}))
						{
							continue;
						}
					}

					//UE_LOG(LG_RANDOM, Log, TEXT("Glirpistorp Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());

					FAERandomList RandomAreaIndexListCopy(GeneratingLevel->PassNameToAreaIndexList[BranchPassName]);

					GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Trying to branch from non-priority connectors with %d elements"), RandomAreaIndexListCopy.List.Num()));
					GeneratingLevel->EndAddingToStep(false);

					//prepare for epic multiply nested lambdas!!!
					if (!RandomAreaIndexListCopy.AttemptActions(*RandomStream,
						//area index chooser
						[&](int32 ListSize, FRandomStream& RandomStreamCopy)
					{
						//usually choose indices towards the end of the array, but sometimes decide to branch off somewhere else
						if (RandomStreamCopy.GetFraction() < PassInfo.BranchProbability)
						{
							int32 Index = RandomStreamCopy.RandHelper(ListSize);
							return Index;
						}
						else
						{
							return ListSize - 1;
						}
					},

						//action
						[&](int32 AreaIndex, FRandomStream& RandomStreamCopy)
					{
						GeneratingLevel->AddLogMessage(TEXT("Current Priority Area Index List Contents"));
						for (int32 ListInd = 0; ListInd < RandomAreaIndexListCopy.List.Num(); ++ListInd)
						{
							GeneratingLevel->AddLogMessage(FString::Printf(TEXT("%d"), RandomAreaIndexListCopy.List[ListInd]));
						}
						GeneratingLevel->EndAddingToStep(false);

						return AreaLayoutFromAreaAction(AreaIndex, RandomStreamCopy, false);
					}))
					{
						break;
					}

					//UE_LOG(LG_RANDOM, Log, TEXT("Glurpistorp Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
				}
			}
		}

		//run the post pass
		if (PassInfo.PostPassClass)
		{
			GeneratingLevel->AddLogMessage(TEXT("Running a post pass object"));
			GeneratingLevel->EndAddingToStep(false);

			PassInfo.PostPassClass->GetDefaultObject<ULGGenerateableAreaDrunkenWalkPostPass>()->PostPassEvent(GeneratingLevel, PassPassNames, *RandomStream);
		}
		
		++CurrentPassIndex;
		bInitPass = true;

		GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Advancing current pass index to %d"), CurrentPassIndex));
		GeneratingLevel->EndAddingToStep(false);

		GeneratingLevel->CurrentPercentDone = PercentLower + PercentInterval * CurrentPassIndex;

		//UE_LOG(LG_RANDOM, Log, TEXT("Glarpistorp Random Stream Seed %d"), GeneratingLevel->RandomStream.GetCurrentSeed());
	}

	GeneratingLevel->AddLogMessage(TEXT("Drunken Walk ContinueLayout failed"));
	GeneratingLevel->EndAddingToStep(false);

	return ELayoutContextStatus::FAIL;
}

void ULGGenerateableAreaDrunkenWalkLayoutContext::PostNewAreaLayout()
{
	GeneratingLevel->AddLogMessage(TEXT("Drunken Walk Post New Area Layout"));

	//add area indices to list of possible areas to branch from if there are free connectors
	for (int32 AreaIndex = LastAreaIndex; AreaIndex < GeneratingLevel->AreaLayoutInfos.Num(); ++AreaIndex)
	{
		AreaSpawnedCost += GeneratingLevel->AreaLayoutInfos[AreaIndex].AreaCost;
							
		for (auto Iter = GeneratingLevel->AreaIndexToPassNames.CreateConstKeyIterator(AreaIndex); Iter; ++Iter)
		{
			GeneratingLevel->GeneratedLevel->PassNameToAreaIndexList.FindOrAdd(Iter.Value()).List.Add(AreaIndex);

			if (GeneratingLevel->AreaLayoutInfos[AreaIndex].ConnectorIndexBuckets.GetBuckets().Num())
			{
				GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Adding area %d to regular list of Pass %s because it had %d free connectors"), 
					AreaIndex, 
					*(Iter.Value().ToString()),
					GeneratingLevel->AreaLayoutInfos[AreaIndex].ConnectorIndexBuckets.GetBuckets().Num()));

				GeneratingLevel->PassNameToAreaIndexList.FindOrAdd(Iter.Value()).List.Add(AreaIndex);

				auto& PriorityList = GeneratingLevel->PassNameToPriorityAreaIndexList.FindOrAdd(Iter.Value());

				if (GeneratingLevel->AreaLayoutInfos[AreaIndex].PriorityConnectorIndexBuckets.GetBuckets().Num())
				{					
					GeneratingLevel->AddLogMessage(FString::Printf(TEXT("Adding area %d to priority list of Pass %s because it had %d free priority connectors"), 
						AreaIndex, 
						*(Iter.Value().ToString()),
						GeneratingLevel->AreaLayoutInfos[AreaIndex].PriorityConnectorIndexBuckets.GetBuckets().Num()));

					PriorityList.List.Add(AreaIndex);
				}
			}
		}
	}

	GeneratingLevel->EndAddingToStep(true);

	//remove area index that was successfully branched off of if that was the last connector
	if (EntryConnectorIndex >= 0)
	{
		GeneratingLevel->CheckAreaFreeConnectors(GeneratingLevel->GeneratedLevel->ConnectorInfos[EntryConnectorIndex]->AreaIndex);
	}
	
	LastAreaIndex = GeneratingLevel->AreaLayoutInfos.Num();
}

bool ULGGenerateableAreaDrunkenWalkLayoutContext::LayoutAreaAtConnector(float PercentLower, 
	float PercentUpper,
	std::chrono::time_point<std::chrono::high_resolution_clock> BeginTime,
	const ILGSpawnableArea * SpawnableAreaTemplate,
	FRandomStream& RandomStreamCopy,
	FTransform Transform,
	TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
	int32 EntryConnectorIndex,
	const TSet<FName>& AreaTags,
	const TArray<FName>& PassNames)
{
	GeneratingLevel->AddLogMessage(TEXT("Drunken Walk Layout Area at Connector"));	
	GeneratingLevel->EndAddingToStep(false);

	ULGSpawnableAreaLayoutContext * Context = SpawnableAreaTemplate->BeginLayoutArea(PercentLower, 
		PercentUpper, 
		GeneratingLevel, 
		RandomStreamCopy, 
		Transform, 
		EntryConnectorClass, 
		EntryConnectorIndex, 
		AreaTags,
		PassNames);
	
	ELayoutContextStatus Result = Context->ContinueLayout(BeginTime);

	if (Result == ELayoutContextStatus::SUCCESS)
	{
		GeneratingLevel->AddLogMessage(TEXT("Succeeded in laying out area at connector"));
		GeneratingLevel->EndAddingToStep(false);

		PostNewAreaLayout();
		return true;
	}
	else if (Result == ELayoutContextStatus::NOT_FINISHED)
	{
		GeneratingLevel->AddLogMessage(TEXT("Didn't finish laying out area at connector"));
		GeneratingLevel->EndAddingToStep(false);

		CurrentChildContext = Context;
		return true;
	}
	else if (Result == ELayoutContextStatus::FAIL)
	{
		GeneratingLevel->AddLogMessage(TEXT("Failed to lay out area at connector"));
		GeneratingLevel->EndAddingToStep(false);

		return false;
	}

	check(false);
	
	return false;
}
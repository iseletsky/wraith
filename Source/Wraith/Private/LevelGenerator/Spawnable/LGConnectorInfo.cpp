#include "Wraith.h"
#include "LGPrefabSlotComponentConnector.h"
#include "LGConnectorInfo.h"
#include "LGGeneratingLevel.h"

ALGConnectorInfo::ALGConnectorInfo(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	RootComponent = ObjectInitializer.CreateDefaultSubobject<USceneComponent, USceneComponent>(this, TEXT("DummyRoot"));
}

void ALGConnectorInfo::ClearAreaCollision()
{
	for (int32 AreaCollisionIndex = 0; AreaCollisionIndex < GetRootComponent()->GetAttachChildren().Num(); ++AreaCollisionIndex)
	{
		GetRootComponent()->GetAttachChildren()[AreaCollisionIndex]->DestroyComponent();
	}
}
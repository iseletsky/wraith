#include "Wraith.h"
#include "LGAreaInfo.h"

ALGAreaInfo::ALGAreaInfo(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	RootComponent = ObjectInitializer.CreateDefaultSubobject<USceneComponent, USceneComponent>(this, TEXT("DummyRoot"));
}
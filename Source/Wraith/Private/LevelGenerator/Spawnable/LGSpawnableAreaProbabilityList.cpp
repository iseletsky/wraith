#include "Wraith.h"
#include "LGGeneratingLevel.h"
#include "LGSpawnablePrefabArea.h"
#include "LGSpawnableAreaProbabilityList.h"

void FLGSpawnableAreaProbabilityList::InitMetadata(class ALGGeneratingLevel * GeneratingLevel)
{
	SpawnableAreaBuckets.Empty();
	ConnectorClassToSpawnableAreaBuckets.Empty();

	for (int32 SpawnableAreaInd = 0; SpawnableAreaInd < SpawnableAreas.Num(); ++SpawnableAreaInd)
	{
		auto& SpawnableAreaInfo = SpawnableAreas[SpawnableAreaInd];

		if (SpawnableAreaInfo.bDisabled)
		{
			UE_LOG(LG, Warning, TEXT("ALGGenerateableAreaDrunkenWalk::InitMetadata(): SpawnableArea %d is disabled in the probability list for debugging purposes.  Put a breakpoint here to find out which class."), SpawnableAreaInd);
		}

		bool bIsValid = true;

		//make sure it has only either GenerateableAreaClass or SpawnablePrefabAreaClass
		if (SpawnableAreaInfo.GenerateableAreaClass)
		{
			if (!SpawnableAreaInfo.SpawnablePrefabAreaClass)
			{
				SpawnableAreaSubclassArray.Add(SpawnableAreaInfo.GenerateableAreaClass);
			}
			else
			{
				bIsValid = false;
				UE_LOG(LG, Warning, TEXT("ALGGenerateableAreaDrunkenWalk::InitMetadata(): Only one of either GenerateableAreaClass or SpawnablePrefabAreaClass should be specified.  Both are in SpawnableArea[%d]."), SpawnableAreaInd);
			}
		}
		else if (SpawnableAreaInfo.SpawnablePrefabAreaClass)
		{
			SpawnableAreaSubclassArray.Add(SpawnableAreaInfo.SpawnablePrefabAreaClass);
		}
		else
		{
			UE_LOG(LG, Warning, TEXT("ALGGenerateableAreaDrunkenWalk::InitMetadata(): One of either GenerateableAreaClass or SpawnablePrefabAreaClass should be specified.  Neither are in SpawnableArea[%d]."), SpawnableAreaInd);
			bIsValid = false;
		}

		if (bIsValid)
		{
			if (!SpawnableAreaInfo.bDisabled)
			{
				SpawnableAreaBuckets.PreAddResult(SpawnableAreaInfo.Probability, SpawnableAreaInd);
			}

			//for each connector name in this SpawnableArea, add it to the ConnectorClassToSpawnableAreaBuckets lookup
			TArray<TSubclassOf<ALGSpawnablePrefabConnector>> ConnectorTypes;

			GeneratingLevel->GetSpawnableInstance<ILGSpawnableArea>(SpawnableAreaSubclassArray[SpawnableAreaInd])->GetConnectorTypes(ConnectorTypes);

			if (!SpawnableAreaInfo.bDisabled)
			{
				for (int ConnectorType = 0; ConnectorType < ConnectorTypes.Num(); ++ConnectorType)
				{
					ConnectorClassToSpawnableAreaBuckets.FindOrAdd(ConnectorTypes[ConnectorType]).PreAddResult(SpawnableAreaInfo.Probability, SpawnableAreaInd);
				}
			}
		}
	}

	SpawnableAreaBuckets.NormalizeBuckets();

	for (auto Iter = ConnectorClassToSpawnableAreaBuckets.CreateIterator(); Iter; ++Iter)
	{
		Iter->Value.NormalizeBuckets();
	}
}

ILGSpawnableArea * FLGSpawnableAreaProbabilityList::GetSpawnableArea(class ALGGeneratingLevel * GeneratingLevel, int32 AreaIndex) const
{
	return GeneratingLevel->GetSpawnableInstance<ILGSpawnableArea>(SpawnableAreaSubclassArray[AreaIndex]);
}

ALGGenerateableArea * FLGSpawnableAreaProbabilityList::GetGenerateableArea(class ALGGeneratingLevel * GeneratingLevel, int32 AreaIndex) const
{
	if (SpawnableAreas[AreaIndex].GenerateableAreaClass)
	{
		return GeneratingLevel->GetSpawnableInstance<ALGGenerateableArea>(SpawnableAreas[AreaIndex].GenerateableAreaClass);
	}

	return NULL;
}

ALGSpawnablePrefabArea * FLGSpawnableAreaProbabilityList::GetSpawnablePrefabArea(class ALGGeneratingLevel * GeneratingLevel, int32 AreaIndex) const
{
	if (SpawnableAreas[AreaIndex].SpawnablePrefabAreaClass)
	{
		return GeneratingLevel->GetSpawnableInstance<ALGSpawnablePrefabArea>(SpawnableAreas[AreaIndex].SpawnablePrefabAreaClass);
	}

	return NULL;
}
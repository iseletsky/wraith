#include "Wraith.h"
#include "LGGeneratingLevel.h"
#include "LGGeneratedLevel.h"
#include "WRDevThemeTestChooseBranchArea.h"

struct PathLengthSortPredicate
{
	PathLengthSortPredicate(const AreaNodePathInfos& InPathInfos)
		: PathInfos(InPathInfos)
	{}

	const AreaNodePathInfos& PathInfos;

	bool operator()(const int32& Value, const int32& OtherValue) const
	{
		return PathInfos.PathDistances[Value].PathDistance < PathInfos.PathDistances[OtherValue].PathDistance;
	}
};

bool UWRDevThemeTestChooseBranchArea::PassChooseBranchArea(class ALGGeneratingLevel * GeneratingLevel,
	LayoutAtAreaFunc LayoutFunc,
	const TSet<FName>& BranchPassNames,
	FRandomStream& RandomStream) const
{
	AreaNodePathInfos PathInfos;
	
	//try creating an area far from the entry
	TSet<FName> RetreiveTags;
	RetreiveTags.Add(FName(TEXT("BeginningAreas")));
	
	GeneratingLevel->GeneratedLevel->GetAreaNodePathInfos(GeneratingLevel->PassNameToAreaIndexList[*BranchPassNames.CreateConstIterator()].List[0], RetreiveTags, PathInfos);
				
	FAERandomList RandomList;
		
	//add area indices
	for (auto Iter = PathInfos.PathDistances.CreateConstIterator(); Iter; ++Iter)
	{
		RandomList.List.Add(Iter->Key);
	}
	
	//sort in order of least to most distance
	RandomList.List.Sort(PathLengthSortPredicate(PathInfos));

	return RandomList.AttemptActions(RandomStream,
		//area index chooser
		[&](int32 ListSize, FRandomStream& RandomStreamCopy)
	{
		return ListSize - 1;
	},

		//action
		[&](int32 AreaIndex, FRandomStream& RandomStreamCopy)
	{
		return LayoutFunc(AreaIndex, RandomStreamCopy, false);
	});
}

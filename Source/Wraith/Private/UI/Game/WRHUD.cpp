#include "Wraith.h"
#include "WRHUD.h"
#include "WRCharacter.h"
#include "WRPlayerController.h"
#include "WRGameScreens.h"
#include "WRScreenManagerWidget.h"
#include "WRGameHUD.h"

const float AWRHUD::MAP_WORLD_SIZE = 10000.f;

AWRHUD::AWRHUD(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void AWRHUD::BeginPlay()
{
	Super::BeginPlay();

	SetupGameHUD();
	SetupGameScreens();
	
	////create an area of the world in which this player's map is rendered, and the 2D scene capture for rendering it in the UI
	//ULocalPlayer * Player = Cast<ULocalPlayer>(GetOwningPlayerController()->Player);

	//if (Player)
	//{
	//	//Map = GetWorld()->SpawnActor<AWRMap>(AWRMap::StaticClass());

	//	//if there are split screen local players, the map needs to be in a different location for each player so they don't overlap
	//	//GetWorld()->PersistentLevel->LevelBoundsActor()
	//}
}

void AWRHUD::SetupGameHUD_Implementation()
{
	if (GameHUDClass)
	{
		APlayerController * PlayerController = GetOwningPlayerController();

		GameHUD = CreateWidget<UWRGameHUD>(PlayerController, GameHUDClass);

		if (GameHUD)
		{
			GameHUD->AddToPlayerScreen();
		}
	}
}

void AWRHUD::SetupGameScreens_Implementation()
{
	if (GameScreensClass)
	{
		GameScreens = NewObject<UWRGameScreens>(this, GameScreensClass);

		if (GameScreens)
		{
			GameScreens->ParentHUD = this;

			APlayerController * PlayerController = GetOwningPlayerController();

			if (PlayerController)
			{
				EnableInput(GetOwningPlayerController());
			}

			GameScreens->SetupScreens(PlayerController, InputComponent);

			if (GameScreens->ScreenManagerWidget)
			{
				if (GameHUD)
				{
					GameHUD->AddGameScreensManagerWidget(GameScreens->ScreenManagerWidget);
				}
				else
				{
					GameScreens->ScreenManagerWidget->AddToPlayerScreen();
				}

				GameScreens->ScreenManagerWidget->SetVisibility(ESlateVisibility::Hidden);
			}
		}
	}
}
#include "Wraith.h"
#include "WRCharacterScreen.h"

UWRCharacterScreen::UWRCharacterScreen(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	OpenHotkeyActionName = "OpenCharacter";
}
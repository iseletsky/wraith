#include "Wraith.h"
#include "WRMapScreen.h"

UWRMapScreen::UWRMapScreen(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	OpenHotkeyActionName = "OpenMap";
}
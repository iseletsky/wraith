#include "Wraith.h"
#include "WRGameScreens.h"
#include "WRGameScreen.h"
#include "WRScreenManagerWidget.h"
#include "WRHUD.h"
#include "WRCharacter.h"

void UWRGameScreens::SetupScreens_Implementation(APlayerController *OwningPlayer, UInputComponent * Input)
{
	SetupHotkeys(Input);

	if (ScreenManagerWidgetClass)
	{
		ScreenManagerWidget = CreateWidget<UWRScreenManagerWidget>(OwningPlayer, ScreenManagerWidgetClass);
	}

	for (int ScreenInd = 0; ScreenInd < GameScreenClasses.Num(); ++ScreenInd)
	{
		UWRGameScreen * GameScreen = NewObject<UWRGameScreen>(this, GameScreenClasses[ScreenInd]);

		GameScreen->ParentGameScreens = this;
		GameScreen->CreateScreenWidget(OwningPlayer);
		GameScreen->SetupOpenHotkey(Input);

		if (ScreenManagerWidget)
		{
			ScreenManagerWidget->AddScreenWidget(GameScreen->ScreenWidget, OwningPlayer);
		}

		GameScreens.Add(GameScreen);
	}
}

void UWRGameScreens::OpenScreen_Implementation(UWRGameScreen * GameScreen)
{
	SetupShowingScreens();

	if (GameScreen)
	{
		check(GameScreens.Contains(GameScreen));

		GameScreen->OnScreenOpened();
	}
}

void UWRGameScreens::SetupShowingScreens_Implementation()
{
	if (ParentHUD)
	{
		ParentHUD->bBlockInput = true;

		if (ScreenManagerWidget)
		{
			ScreenManagerWidget->SetVisibility(ESlateVisibility::Visible);
		}

		if (ParentHUD->InputComponent)
		{
			ParentHUD->InputComponent->bBlockInput = true;
		}

		//Call OnLoseControl in the pawn
		AWRCharacter * WRCharacter = Cast<AWRCharacter>(ParentHUD->GetOwningPlayerController()->GetPawnOrSpectator());

		if (WRCharacter)
		{
			WRCharacter->OnLoseControl();
		}

		//TODO: have nicer mouse input later
		ParentHUD->GetOwningPlayerController()->bShowMouseCursor = true;
	}
}

void UWRGameScreens::TearDownShowingScreens_Implementation()
{
	if (ParentHUD)
	{
		ParentHUD->bBlockInput = false;

		if (ScreenManagerWidget)
		{
			ScreenManagerWidget->SetVisibility(ESlateVisibility::Hidden);
		}

		if (ParentHUD->InputComponent)
		{
			ParentHUD->InputComponent->bBlockInput = false;
		}

		//TODO: have nicer mouse input later
		ParentHUD->GetOwningPlayerController()->bShowMouseCursor = false;
	}
}

void UWRGameScreens::SetupHotkeys_Implementation(UInputComponent * Input)
{
	Input->BindAction("OpenScreen", IE_Pressed, this, &UWRGameScreens::OpenScreens);
	Input->BindAction("CloseScreens", IE_Pressed, this, &UWRGameScreens::CloseScreens);
}

void UWRGameScreens::OpenScreens_Implementation()
{
	SetupShowingScreens();
}

void UWRGameScreens::CloseScreens_Implementation()
{
	TearDownShowingScreens();
}
#include "Wraith.h"
#include "WRGameScreen.h"
#include "WRGameScreens.h"
#include "WRScreenWidget.h"

void UWRGameScreen::SetupOpenHotkey_Implementation(UInputComponent * Input)
{
	if (OpenHotkeyActionName != NAME_None)
	{
		Input->BindAction(OpenHotkeyActionName, IE_Pressed, this, &UWRGameScreen::OpenScreen);
	}
}

void UWRGameScreen::OpenScreen_Implementation()
{
	if (ParentGameScreens)
	{
		ParentGameScreens->OpenScreen(this);
	}
}

void UWRGameScreen::OnScreenOpened_Implementation()
{
	if (ScreenWidget)
	{
		ScreenWidget->OpenScreen();
	}
}

void UWRGameScreen::CreateScreenWidget_Implementation(APlayerController * OwningPlayer)
{
	if (ScreenWidgetClass)
	{
		ScreenWidget = CreateWidget<UWRScreenWidget>(OwningPlayer, ScreenWidgetClass);
	}
}
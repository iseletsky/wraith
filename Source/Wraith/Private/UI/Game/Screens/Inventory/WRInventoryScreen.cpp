#include "Wraith.h"
#include "WRInventoryScreen.h"

UWRInventoryScreen::UWRInventoryScreen(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	OpenHotkeyActionName = "OpenInventory";
}
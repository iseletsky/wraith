#include "Wraith.h"
#include "WRScreenButtonWidget.h"
#include "WRScreenWidget.h"

void UWRScreenButtonWidget::SetScreen_Implementation(UWRScreenWidget * InScreenWidget)
{
	if (ScreenWidget)
	{
		ScreenWidget->ScreenButton = NULL;
	}

	ScreenWidget = InScreenWidget;

	if (ScreenWidget)
	{
		ScreenWidget->ScreenButton = this;
	}
}

void UWRScreenButtonWidget::OpenScreen_Implementation()
{
	if (ScreenWidget)
	{
		ScreenWidget->OpenScreen();
	}
}
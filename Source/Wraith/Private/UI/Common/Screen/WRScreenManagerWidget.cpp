#include "Wraith.h"
#include "WRScreenManagerWidget.h"
#include "WRScreenWidget.h"

void UWRScreenManagerWidget::AddScreenWidget_Implementation(UWRScreenWidget * ScreenWidget, APlayerController * OwningPlayer)
{
	check(!ScreenWidgets.Contains(ScreenWidget));

	ScreenWidget->ScreenManager = this;
	ScreenWidgets.Add(ScreenWidget);
}

void UWRScreenManagerWidget::OpenScreen_Implementation(UWRScreenWidget * ScreenWidget)
{
	check(ScreenWidgets.Contains(ScreenWidget));

	ScreenWidget->OnScreenOpened();
}
#include "Wraith.h"
#include "WRScreenWidget.h"
#include "WRScreenManagerWidget.h"
#include "WRScreenButtonWidget.h"

void UWRScreenWidget::OpenScreen_Implementation()
{
	ScreenManager->OpenScreen(this);
}

void UWRScreenWidget::OnScreenOpened_Implementation()
{
	if (ScreenButton)
	{
		ScreenButton->OnScreenOpened();
	}
}

void UWRScreenWidget::OnScreenClosed_Implementation()
{
	if (ScreenButton)
	{
		ScreenButton->OnScreenClosed();
	}
}
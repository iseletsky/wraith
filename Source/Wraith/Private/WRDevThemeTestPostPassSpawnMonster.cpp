#include "Wraith.h"
#include "LGGeneratingLevel.h"
#include "LGGeneratedLevel.h"
#include "LGSpawnablePrefabArea.h"
#include "LGPrefabSlotComponentAreaPoint.h"
#include "WRDevThemeTestPostPassSpawnMonster.h"
#include "Runtime/Engine/Classes/Animation/SkeletalMeshActor.h"

void UWRDevThemeTestPostPassSpawnMonster::PostPassEvent(class ALGGeneratingLevel * GeneratingLevel,
	const TArray<FName>& PassNames,
	FRandomStream& RandomStream) const
{
	//only spawn things on the server
	if (GeneratingLevel->GetNetMode() != NM_Client)
	{
		//spawn the monster mesh at all spawn locations

		for (int32 AreaInd = 0; AreaInd < GeneratingLevel->GeneratedLevel->PassNameToAreaIndexList[PassNames[0]].List.Num(); ++AreaInd)
		{
			//spawn monster skeletal meshes
			int32 ActualAreaInd = GeneratingLevel->GeneratedLevel->PassNameToAreaIndexList[PassNames[0]].List[AreaInd];

			ALGAreaInfo * Room = GeneratingLevel->GeneratedLevel->AreaInfos[ActualAreaInd];

			for (auto Iter = Room->AreaPoints.CreateConstKeyIterator(FName(TEXT("TestMonsterSpawn"))); Iter; ++Iter)
			{
				auto Component = Iter.Value();

				ASkeletalMeshActor * MeshActor = GeneratingLevel->GetWorld()->SpawnActor<ASkeletalMeshActor>(ASkeletalMeshActor::StaticClass(), Component->GetComponentToWorld().GetLocation(), Component->GetComponentToWorld().Rotator());

				USkeletalMesh * SkeletalMesh = Cast<USkeletalMesh>(StaticLoadObject(USkeletalMesh::StaticClass(), NULL, TEXT("SkeletalMesh'/Game/Characters/Monsters/Thing/Meshes/SK_CH_Thing.SK_CH_Thing'")));

				if (SkeletalMesh)
				{
					MeshActor->GetSkeletalMeshComponent()->SetSkeletalMesh(SkeletalMesh);
				}
			}
		}
	}
}
#include "Wraith.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Wraith, "Wraith" );

DEFINE_LOG_CATEGORY(WR);

//LG logs
DEFINE_LOG_CATEGORY(LG);
DEFINE_LOG_CATEGORY(LG_VERBOSE);
DEFINE_LOG_CATEGORY(LG_RANDOM);

FCollisionResponseParams WorldResponseParams = []()
{
	FCollisionResponseParams Result(ECR_Ignore);
	Result.CollisionResponse.WorldStatic = ECR_Block;
	Result.CollisionResponse.WorldDynamic = ECR_Block;
	return Result;
}();
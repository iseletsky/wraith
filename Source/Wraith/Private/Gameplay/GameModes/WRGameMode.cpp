#include "Wraith.h"
#include "WRGameMode.h"
#include "WRCharacter.h"
#include "WRPlayerController.h"

AWRGameMode::AWRGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Characters/Human/BP_WRCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;
	PlayerControllerClass = AWRPlayerController::StaticClass();
	
	// use our custom HUD class
	static ConstructorHelpers::FClassFinder<AHUD> HUDClassFinder(TEXT("/Game/UI/Game/BP_WRSinglePlayerHUD"));
	HUDClass = HUDClassFinder.Class;

	bUseSeamlessTravel = true;
}

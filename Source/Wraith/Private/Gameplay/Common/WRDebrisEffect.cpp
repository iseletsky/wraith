#include "Wraith.h"
#include "WRDebrisEffect.h"

AWRDebrisEffect::AWRDebrisEffect(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    Mesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Mesh"));

    Mesh->SetSimulatePhysics(true);
    Mesh->SetIsReplicated(false);
    Mesh->SetCollisionProfileName("Debris");
    Mesh->bGenerateOverlapEvents = false;
    Mesh->SetCanEverAffectNavigation(false);
    Mesh->GetBodyInstance()->SetInstanceNotifyRBCollision(true);		//generate hit events
    Mesh->GetBodyInstance()->bUseCCD = true;
    Mesh->OnComponentHit.AddDynamic(this, &AWRDebrisEffect::OnImpact);
    Mesh->bReceivesDecals = false;
}

void AWRDebrisEffect::OnImpact_Implementation(UPrimitiveComponent* OverlappedComp, AActor * OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
    //TODO: a better system for surface to surface impact sounds
    if (DefaultImpactSound)
    {
        UGameplayStatics::PlaySoundAtLocation(this, DefaultImpactSound, GetActorLocation(), 1.f, 1.f, 0.f, DefaultImpactSoundAttenuation);
    }
}

void AWRDebrisEffect::Reset_Implementation()
{
    Destroy();
}
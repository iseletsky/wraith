#include "Wraith.h"
#include "WRImpactEffect.h"

AWRImpactEffect::AWRImpactEffect(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void AWRImpactEffect::Reset_Implementation()
{
	//TODO: test if placed in editor
	//Destroy();
}

AWRImpactEffect * AWRImpactEffect::SpawnEffect_Implementation(UWorld* World, 
	const FTransform& InTransform,
	const FVector& ImpactVelocity,
	UPrimitiveComponent* HitComp, 
	FName HitBone) const
{
	if (!World)
	{
		return NULL;
	}

	//Rotate transform because for some reason DecalActor points down -z
	FTransform SpawnXForm(InTransform); 
	SpawnXForm.ConcatenateRotation(FQuat(FRotator(90.f, 0.f, 0.f)));

	//TODO: Some checking based on quality settings on whether or not to avoid spawning the effect
	//Make this a separate function
		
	AWRImpactEffect * Res = World->SpawnActor<AWRImpactEffect>(GetClass(), SpawnXForm);

	//attach to the hit comp
	if (Res && HitComp)
	{
		Res->AttachToComponent(HitComp, FAttachmentTransformRules::KeepWorldTransform, HitBone);
	}

	return Res;
}
#include "Wraith.h"
#include "WRProjectile.h"
#include "WRProjectileMovementComponent.h"
#include "WRDamageType.h"
#include "WRImpactType.h"

AWRProjectile::AWRProjectile(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	SetActorTickEnabled(true);

	ProjectileCollision = ObjectInitializer.CreateOptionalDefaultSubobject<USphereComponent>(this, TEXT("ProjectileCollision"));
	
	ProjectileCollision->InitSphereRadius(1.0f);
	ProjectileCollision->BodyInstance.SetCollisionProfileName("Projectile");
	ProjectileCollision->OnComponentBeginOverlap.AddDynamic(this, &AWRProjectile::OnOverlapBegin);
	ProjectileCollision->OnComponentEndOverlap.AddDynamic(this, &AWRProjectile::OnOverlapEnd);
	ProjectileCollision->bTraceComplexOnMove = true;
	ProjectileCollision->bReceivesDecals = false;

	RootComponent = ProjectileCollision;

	ProjectileMovement = ObjectInitializer.CreateDefaultSubobject<UWRProjectileMovementComponent>(this, TEXT("ProjectileMovement"));
	ProjectileMovement->InitialSpeed = 1000.f;
	ProjectileMovement->ProjectileGravityScale = 0.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->OnProjectileStop.AddDynamic(this, &AWRProjectile::OnProjectileStop);
	ProjectileMovement->OnProjectileBounce.AddDynamic(this, &AWRProjectile::OnProjectileBounce);

	DamageMultiplier = 1.f;
	ExplosionRadiusMultiplier = 1.f;
}

void AWRProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	bool bEnableCanHitCreator = false;

	if (enableCanHitCreatorTickCountdown > 0)
	{
		--enableCanHitCreatorTickCountdown;

		if (enableCanHitCreatorTickCountdown == 0)
		{
			bEnableCanHitCreator = true;
		}
	}

	//check if the colliding creator hit should occur
	if (CollidingCreatorComponent)
	{
		enableCanHitCreatorTickCountdown = 0;

		FHitResult Hit;
		Hit.Actor = CollidingCreatorComponent->GetOwner();
		Hit.Component = CollidingCreatorComponent;
		Hit.Normal = -GetVelocity().GetSafeNormal();
		Hit.ImpactPoint = GetActorLocation();
		Hit.Location = Hit.ImpactPoint;
		Hit.TraceStart = Hit.ImpactPoint;
		Hit.TraceEnd = Hit.ImpactPoint;

		OnProjectileHit(Hit, GetVelocity());
	}
	else if(!bDidEverOverlapWithCreator || bEnableCanHitCreator)
	{
		enableCanHitCreatorTickCountdown = 0;
		
		//force this to true if not colliding with creator handle case when projectile spawns outside creator and never has a chance for the endOverlap call to happen
		bCanHitCreator = true;
	}
}

void AWRProjectile::Reset_Implementation()
{
	Destroy();
}

void AWRProjectile::OnProjectileStop_Implementation(const FHitResult& Hit)
{
	OnProjectileHit(Hit, GetVelocity());
}

void AWRProjectile::OnProjectileBounce_Implementation(const struct FHitResult& ImpactResult, const FVector& ImpactVelocity)
{
	EnableCanHitCreator();
	BounceNum++;

	OnProjectileHit(ImpactResult, ImpactVelocity, true);
}

void AWRProjectile::OnOverlapBegin_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!bOnOverlapReentrancyGuard)
	{
		TGuardValue<bool> OverlapGuard(bOnOverlapReentrancyGuard, true);

		FHitResult Hit;

		if (bFromSweep)
		{
			Hit = SweepResult;
		}
		else
		{
			OtherComp->LineTraceComponent(Hit, 
				GetActorLocation() - GetVelocity() * 10.0, 
				GetActorLocation() + GetVelocity(), 
				FCollisionQueryParams(GetClass()->GetFName(), 
				ProjectileCollision->bTraceComplexOnMove, this));
		}

		if (Creator && OtherActor == Creator)
		{
			CollidingCreatorComponent = OtherComp;
			bDidEverOverlapWithCreator = true;
		}

		OnProjectileHit(Hit, GetVelocity(), false, OtherActor, OtherComp);
	}
}

void AWRProjectile::OnOverlapEnd_Implementation(UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//start being able to hit creator 
	if (!GetCanHitCreator() && Creator && OtherActor == Creator)
	{
		CollidingCreatorComponent = NULL;
		EnableCanHitCreator();
	}
}

bool AWRProjectile::ShouldHitCreator_Implementation(const FVector& ImpactVelocity)
{
	if (Creator)
	{
		if (GetCanHitCreator())
		{
			return true;
		}
		else
		{
			//check if projectile is moving slower than creator and in generally the same direction, don't ignore the hit
			return FVector::DotProduct(Creator->GetVelocity().GetSafeNormal(), ImpactVelocity.GetSafeNormal()) > 0.f
				&& Creator->GetVelocity().SizeSquared() > ImpactVelocity.SizeSquared();
		}
	}

	return true;
}

void AWRProjectile::OnProjectileHit_Implementation(const FHitResult& Hit, 
	const FVector& ImpactVelocity, 
	bool bDidBounce, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp)
{
	if (!OtherActor)
	{
		OtherActor = Hit.Actor.Get();
	}

	if (!OtherComp)
	{
		OtherComp = Hit.Component.Get();
	}

	if (Creator && OtherActor == Creator && !ShouldHitCreator(ImpactVelocity))
	{
		return;
	}

	SpawnImpactEffects(Hit, ImpactVelocity, bDidBounce, OtherActor, OtherComp);
	DealDamage(Hit, ImpactVelocity, bDidBounce, OtherActor, OtherComp);

	if (ShouldDestroyOnHit(Hit, ImpactVelocity, bDidBounce, OtherActor, OtherComp))
	{
		Destroy();
	}
}

void AWRProjectile::SpawnImpactEffects_Implementation(const FHitResult& Hit,
	const FVector& ImpactVelocity,
	bool bDidBounce,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp)
{
	if (bDidBounce)
	{
		if (BounceImpactType)
		{
			BounceImpactType.GetDefaultObject()->SpawnImpactEffects(GetWorld(),
				Hit,
				ImpactVelocity,
				OtherComp);
		}
	}
	else
	{
		if (ImpactType)
		{
			ImpactType.GetDefaultObject()->SpawnImpactEffects(GetWorld(),
				Hit,
				ImpactVelocity,
				OtherComp);
		}
	}
}

void AWRProjectile::DealDamage_Implementation(const FHitResult& Hit,
	const FVector& ImpactVelocity,
	bool bDidBounce,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp)
{
	if (DamageType)
	{
		FVector ImpactDirection(ImpactVelocity);
		ImpactDirection.Normalize();

		DamageType.GetDefaultObject()->ApplyDamage(this,
			OtherActor,
			DamageMultiplier,
			ExplosionRadiusMultiplier,
			ImpactDirection,
			Hit, 
			Instigator->GetController(), 
			this);
	}
}

bool AWRProjectile::ShouldDestroyOnHit_Implementation(const FHitResult& Hit,
	const FVector& ImpactVelocity,
	bool bDidBounce,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp)
{
	return true;
}

void AWRProjectile::SetSpeed_Implementation(float Speed)
{
	ProjectileMovement->Velocity.Normalize();
	ProjectileMovement->Velocity *= Speed;
}
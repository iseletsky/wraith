#include "Wraith.h"
#include "WRDamageType.h"

UWRDamageType::UWRDamageType(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bDamageEnabled = true;
	MinDamage = 10.f;
	MaxDamage = 20.f;

	SplashDamageFalloff = 1.f;

	MinStoppingPower = 1.f;
	MaxStoppingPower = 2.f;
}

bool UWRDamageType::ApplyDamage_Implementation(const UObject* WorldContextObject,
	AActor* PointDamagedActor,
	float DamageMultiplier,
	float RadiusMultiplier,
	FVector const& PointHitFromDirection,
	FHitResult const& PointHitInfo,
	AController* EventInstigator,
	AActor* DamageCauser,
	ECollisionChannel RadialDamagePreventionChannel) const
{
	ApplyPointDamage(PointDamagedActor,
		DamageMultiplier, 
		PointHitFromDirection,
		PointHitInfo,
		EventInstigator,
		DamageCauser);

	return ApplyRadialDamage(WorldContextObject,
		DamageMultiplier,
		RadiusMultiplier,
		PointHitInfo.Location,
		DamageCauser,
		EventInstigator,
		RadialDamagePreventionChannel);
}

void UWRDamageType::ApplyPointDamage_Implementation(AActor* DamagedActor,
	float DamageMultiplier,
	FVector const& HitFromDirection,
	FHitResult const& HitInfo,
	AController* EventInstigator,
	AActor* DamageCauser) const
{
	if (!bDamageEnabled)
	{
		return;
	}

	//TODO: if needed, make this take a random seed
	float Damage = MinDamage;
	
	if (MaxDamage > Damage)
	{
		Damage = FMath::RandRange(Damage, MaxDamage);
	}

	Damage *= DamageMultiplier;

	UGameplayStatics::ApplyPointDamage(DamagedActor, Damage, HitFromDirection, HitInfo, EventInstigator, DamageCauser, GetClass());
}

bool UWRDamageType::ApplyRadialDamage_Implementation(const UObject* WorldContextObject,
	float DamageMultiplier,
	float RadiusMultiplier,
	const FVector& Origin,
	AActor* DamageCauser,
	AController* InstigatedByController,
	ECollisionChannel DamagePreventionChannel) const
{
	if (!bSplashDamageEnabled)
	{
		return false;
	}

	//TODO: if needed, make this take a random seed

	float InnerDamage = MinSplashDamageInner;
	
	if (MaxSplashDamageInner > InnerDamage)
	{
		InnerDamage = FMath::RandRange(InnerDamage, MaxSplashDamageInner);
	}

	InnerDamage *= DamageMultiplier;

	float OuterDamage = MinSplashDamageOuter;

	if (MaxSplashDamageOuter > OuterDamage)
	{
		OuterDamage = FMath::RandRange(OuterDamage, MaxSplashDamageOuter);
	}

	OuterDamage *= DamageMultiplier;

	float InnerRadius = SplashInnerRadius * RadiusMultiplier;
	float OuterRadius = SplashOuterRadius * RadiusMultiplier;

	return UGameplayStatics::ApplyRadialDamageWithFalloff(WorldContextObject, 
		InnerDamage, 
		OuterDamage, 
		Origin, 
		InnerRadius, 
		OuterRadius, 
		DamageFalloff, 
		GetClass(), 
		TArray<AActor *>(), 
		DamageCauser, 
		InstigatedByController, 
		DamagePreventionChannel);
}
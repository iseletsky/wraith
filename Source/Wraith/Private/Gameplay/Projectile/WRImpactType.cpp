#include "Wraith.h"
#include "WRImpactType.h"
#include "WRImpactEffect.h"

AWRImpactEffect * UWRImpactType::SpawnImpactEffects_Implementation(UWorld* World,
	const FHitResult& Hit,
	const FVector& ImpactVelocity,
	UPrimitiveComponent* ImpactComp) const
{
	//TODO: Figure out surface type
	//For now return default

	FName SurfaceType;

	const TSubclassOf<AWRImpactEffect> * EffectSpawnClass = ImpactSurfaceToEffectClass.Find(SurfaceType);

	//Select default
	if (!EffectSpawnClass)
	{
		EffectSpawnClass = ImpactSurfaceToEffectClass.Find(FName());

		if (!EffectSpawnClass)
		{
			UE_LOG_ON_SCREEN(WR, Error, 5.f, FColor::Red, TEXT("UWRImpactType \"%s\" tried spawning effect for surface type \"%s\" but doesn't have a default no named surface to fall back to."),
				*GetName(),
				*SurfaceType.ToString());

			return NULL;
		}
	}

	return EffectSpawnClass->GetDefaultObject()->SpawnEffect(World, 
		FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint),
		ImpactVelocity,
		ImpactComp ? ImpactComp : Hit.GetComponent(),
		Hit.BoneName);
}
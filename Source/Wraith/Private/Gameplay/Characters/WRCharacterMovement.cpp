#include "Wraith.h"
#include "WRCharacterMovement.h"
#include "WRCharacter.h"

UWRCharacterMovement::UWRCharacterMovement(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//make character walk up 45 degree slopes
	SetWalkableFloorAngle(45.f);

	MaxWalkSpeed = 625.f;
	bCanWalkOffLedgesWhenCrouching = true;
	bMaintainHorizontalGroundVelocity = false;
	WalkingJumpLateralVelocityPercentage = .5f;
	SprintingJumpLateralVelocityPercentage = .75f;
	SprintSpeed = 1100.f;
	SprintAcceleration = 6000.f;
	MaxWalkSpeedCrouched = 200.f;
	AirControl = 0.1f;
	MaxAcceleration = 6000.f;
	BrakingDecelerationWalking = 6600.f;
	BrakingDecelerationFalling = 0.f;
	BrakingDecelerationSwimming = 300.f;
	GravityScale = 1.f;
	MaxStepHeight = 45.0;
	NavAgentProps.AgentStepHeight = MaxStepHeight;
	CrouchedHalfHeight = 64.0f;

	StumbleBounceDampening = .8f;
	StumbleBounceThreshold = 200.f;
	StumbleCrouchThreshold = 100.f;
	StumbleCrouchFalloffMultiplier = 2.f;

	MaxSwimSpeed = 1000.f;
	Buoyancy = 1.f;

	JumpZVelocity = 420.f;

	NavAgentProps.bCanCrouch = true;

	OutofWaterZ = 700.f;
	JumpOutOfWaterPitch = -90.f;

	MovementSpeedDampening.MaxTotalMagnitude = 1.f;
	JumpHeightDampening.MaxTotalMagnitude = 1.f;

	//bUseControllerDesiredRotation = true;
}

void UWRCharacterMovement::DisplayDebug(UCanvas* Canvas, const FDebugDisplayInfo& DebugDisplay, float& YL, float& YPos)
{
	if (CharacterOwner == NULL)
	{
		return;
	}
	Super::DisplayDebug(Canvas, DebugDisplay, YL, YPos);

	Canvas->SetDrawColor(255, 255, 255);
	UFont* RenderFont = GEngine->GetSmallFont();
	FString T = FString::Printf(TEXT(""));
	T = FString::Printf(TEXT("Wants to Sprint %i, Sprinting %i"), bWantsToSprint, bIsSprinting);
	Canvas->DrawText(RenderFont, T, 4.0f, YPos);
	YPos += YL;
	T = FString::Printf(TEXT("MaxSpeed %f, JumpVelocity %f"), GetMaxSpeed(), JumpZVelocity);
	Canvas->DrawText(RenderFont, T, 4.0f, YPos);
	YPos += YL;
	T = FString::Printf(TEXT("SpeedDampening %f, JumpDampening %f"),
		MovementSpeedDampening.GetCurrentTotalMagnitude(),
		JumpHeightDampening.GetCurrentTotalMagnitude());
	Canvas->DrawText(RenderFont, T, 4.0f, YPos);
	YPos += YL;
	T = FString::Printf(TEXT("Stumble (%f, %f)"),
		StumbleDirection.X * StumbleMagnitudes.GetCurrentTotalMagnitude(),
		StumbleDirection.Y * StumbleMagnitudes.GetCurrentTotalMagnitude());
	Canvas->DrawText(RenderFont, T, 4.0f, YPos);
	YPos += YL;
}

void UWRCharacterMovement::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	bIsSprinting = CanSprint();

	MovementSpeedDampening.Update(DeltaTime);
	JumpHeightDampening.Update(DeltaTime);

	if (IsMovingOnGround())
	{
		float StumbleUpdateRate = DeltaTime;

		if (IsCrouching())
		{
			StumbleUpdateRate *= StumbleCrouchFalloffMultiplier;
		}

		StumbleMagnitudes.MaxTotalMagnitude = GetMaxStumblingSpeed();
		StumbleMagnitudes.Update(StumbleUpdateRate);		
	}
	else
	{
		//clear the stumble once you're no longer walking
		StumbleMagnitudes.Empty();
	}

	//process jump dampening
	JumpZVelocity = GetClass()->GetDefaultObject<UWRCharacterMovement>()->JumpZVelocity * (1.f - JumpHeightDampening.GetCurrentTotalMagnitude());

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

///////////////////////////////////////
//Jumping

bool UWRCharacterMovement::DoJump(bool bReplayingMoves)
{
	bool Res = Super::DoJump(bReplayingMoves);

	if (Res)
	{
		float Percentage = GetJumpLateralVelocityPercentage();

		//reduce horizontal velocity
		Velocity.X = Velocity.X * Percentage;
		Velocity.Y = Velocity.Y * Percentage;

		if (!bReplayingMoves)
		{
			AWRCharacter * WRCharacterOwner = Cast<AWRCharacter>(CharacterOwner);

			if (WRCharacterOwner)
			{
				WRCharacterOwner->Jumped();
			}
		}
	}

	return Res;
}

float UWRCharacterMovement::FallingDamageReduction_Implementation(float FallingDamage, const FHitResult& Hit)
{
	if (Hit.ImpactNormal.Z < GetWalkableFloorZ())
	{
		// Scale damage based on angle of wall we hit
		return FallingDamage * Hit.ImpactNormal.Z;
	}
	return 0.f;
}

void UWRCharacterMovement::HandleImpact(FHitResult const& Hit, float TimeSlice, const FVector& MoveDelta)
{
	Super::HandleImpact(Hit, TimeSlice, MoveDelta);

	//bounce the character's stumble while making the stumble velocity lower a little bit
	if (Hit.bBlockingHit && IsMovingOnGround() && StumbleMagnitudes.GetCurrentTotalMagnitude() > StumbleBounceThreshold)
	{
		//convert to 2D vectors
		FVector2D HitNormal = FVector2D(Hit.Normal);
		FVector2D MoveDir = FVector2D(Hit.TraceEnd - Hit.TraceStart);
		MoveDir.Normalize();

		//compute the bounce dir
		StumbleDirection = MoveDir - FVector2D::DotProduct(HitNormal, MoveDir) * HitNormal * 2.f;
		StumbleDirection.Normalize();	//just to be sure

		//lower the bounce magnitude
		StumbleMagnitudes.Multiply(StumbleBounceDampening);
	}
}

///////////////////////////////////////
//Crouching

bool UWRCharacterMovement::CanToggleCrouchInCurrentState() const
{
	return StumbleMagnitudes.GetCurrentTotalMagnitude() <= StumbleCrouchThreshold;
}

bool UWRCharacterMovement::CanCrouchInCurrentState() const
{
	return !IsFalling() && ((!IsCrouching() && CanToggleCrouchInCurrentState()) || IsCrouching()) && Super::CanCrouchInCurrentState();
}

void UWRCharacterMovement::Crouch(bool bClientSimulation)
{
	if (CanToggleCrouchInCurrentState())
	{
		Super::Crouch(bClientSimulation);
	}
}

void UWRCharacterMovement::UnCrouch(bool bClientSimulation)
{
	if (CanToggleCrouchInCurrentState())
	{
		Super::UnCrouch(bClientSimulation);
	}
}

///////////////////////////////////////
//Movement

void UWRCharacterMovement::CalcVelocity(float DeltaTime, float Friction, bool bFluid, float BrakingDeceleration)
{
	Super::CalcVelocity(DeltaTime, Friction, bFluid, BrakingDeceleration);

	//TODO: (Engine)
	//logic copied from super method to abort early, with every engine update be sure this is copied
	if (!HasValidData() || HasAnimRootMotion() || DeltaTime < MIN_TICK_TIME || (CharacterOwner && CharacterOwner->Role == ROLE_SimulatedProxy))
	{
		return;
	}

	//apply stumble velocity when on ground
	if (IsMovingOnGround())
	{
		Velocity = (Velocity + GetStumbleVelocity()).GetClampedToMaxSize(GetMaxStumblingSpeed());
	}
}

float UWRCharacterMovement::GetMaxAcceleration() const
{
	if (bIsSprinting)
	{
		return SprintAcceleration;
	}
	else
	{
		return Super::GetMaxAcceleration();
	}
}

float UWRCharacterMovement::GetMaxSpeed() const
{
	return (bIsSprinting ? SprintSpeed : Super::GetMaxSpeed()) * (1.f - MovementSpeedDampening.GetCurrentTotalMagnitude());
}

float UWRCharacterMovement::GetJumpLateralVelocityPercentage_Implementation()
{
	float CurrMaxSpeed = GetMaxSpeed();

	//If above running speed, just return SprintingJumpLateralVelocityPercentage
	if (CurrMaxSpeed >= SprintingJumpLateralVelocityPercentage)
	{
		return SprintingJumpLateralVelocityPercentage;
	}

	//If at negative speed, just return StandingJumpLateralVelocityPercentage
	if (CurrMaxSpeed <= 0.f)
	{
		return StandingJumpLateralVelocityPercentage;
	}

	//If above walking speed, find a percentage between WalkingJumpLateralVelocityPercentage and SprintingJumpLateralVelocityPercentage
	if (CurrMaxSpeed > MaxWalkSpeed)
	{
		float PctDifference = SprintingJumpLateralVelocityPercentage - WalkingJumpLateralVelocityPercentage;
		float SpeedDifference = SprintSpeed - MaxWalkSpeed;
		float SpeedFactor = (CurrMaxSpeed - MaxWalkSpeed) / SpeedDifference;

		return WalkingJumpLateralVelocityPercentage + (SpeedFactor * PctDifference);
	}

	//if below walking speed, find a percentage between StandingJumpLateralVelocityPercentage and WalkingJumpLateralVelocityPercentage

	float PctDifference = WalkingJumpLateralVelocityPercentage - StandingJumpLateralVelocityPercentage;
	float SpeedFactor = (CurrMaxSpeed - MaxWalkSpeed) / MaxWalkSpeed;

	return StandingJumpLateralVelocityPercentage + (SpeedFactor * PctDifference);
}

///////////////////////////////////////
//Stumble

void UWRCharacterMovement::AddStumble_Implementation(const FVector2D& Stumble, float Falloff)
{
	//I have to remove this check for being on the ground because sometimes the character's movement mode is still falling when this is called after landing
	//if (IsMovingOnGround())
	{
		//see how much this new stumble impulse affects the current stumble direction
		FVector2D CurrentStumbleDirection = StumbleDirection * StumbleMagnitudes.GetCurrentTotalMagnitude();

		StumbleDirection = StumbleDirection * StumbleMagnitudes.GetCurrentTotalMagnitude() + Stumble;
		StumbleDirection.Normalize();

		StumbleMagnitudes.AddEffect(Stumble.Size(), Falloff);
	}
}

FVector UWRCharacterMovement::GetStumbleVelocity_Implementation()
{
	return FVector(StumbleDirection, 0.f) * StumbleMagnitudes.GetCurrentTotalMagnitude();
}

float UWRCharacterMovement::GetMaxStumblingSpeed_Implementation() const
{
	return IsCrouching()
		? MaxWalkSpeedCrouched
		: SprintSpeed;
}

///////////////////////////////////////
//Sprint

bool UWRCharacterMovement::CanSprint_Implementation() const
{
	return bWantsToSprint && IsMovingOnGround() && !IsCrouching();
}
#include "Wraith.h"
#include "AEPlayerCameraManager.h"
#include "WRCharacterHumanoid.h"
#include "WRHUD.h"
#include "WRPlayerController.h"

const float WRPLAYERCONTROLLER_DEBUG_3P_MOVE_SPEED = 20.f;

AWRPlayerController::AWRPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bAssignToPawnTeamOnPossess = true;
}

void AWRPlayerController::GetSeamlessTravelActorList(bool bToEntry, TArray <AActor *> & ActorList)
{
	Super::GetSeamlessTravelActorList(bToEntry, ActorList);

	//if the character is alive, add to the seamless travel list along with the inventory
	//TODO: check in the game mode if this needs to happen
	if (true)
	{
		AWRCharacter * WRCharacter = Cast<AWRCharacter>(GetCharacter());

		if (WRCharacter)
		{
			ActorList.Add(WRCharacter);
		}
	}
}

void AWRPlayerController::UpdateRotation(float DeltaTime)
{
	if (PlayerCameraManager)
	{
		PlayerCameraManager->ViewPitchMin = GetMinViewPitch();

		AAEPlayerCameraManager * PCM = Cast<AAEPlayerCameraManager>(PlayerCameraManager);

		if (PCM)
		{
			PCM->SetRelativeRotation(GetRelativeCameraRotation());
			PCM->SetRelativeLocation(GetRelativeCameraLocation());
		}
	}

	Super::UpdateRotation(DeltaTime);
}

void AWRPlayerController::Possess(APawn * InPawn)
{
	if (bAssignToPawnTeamOnPossess)
	{
		SetGenericTeamId(FGenericTeamId::GetTeamIdentifier(InPawn));
	}

	Super::Possess(InPawn);
}

//////////////////
//Controls

void AWRPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("YawRight", this, &AWRPlayerController::YawRight);
	InputComponent->BindAxis("YawRightUnnormalized", this, &AWRPlayerController::YawRightUnnormalized);
	InputComponent->BindAxis("PitchUp", this, &AWRPlayerController::PitchUp);
	InputComponent->BindAxis("PitchUpUnnormalized", this, &AWRPlayerController::PitchUpUnnormalized);

	Debug3PMoveForwardBinding = &InputComponent->BindAxis("MoveForward", this, &AWRPlayerController::Debug3PMoveForward);
	Debug3PMoveRightBinding = &InputComponent->BindAxis("MoveRight", this, &AWRPlayerController::Debug3PMoveRight);
	Debug3PMoveUpBinding = &InputComponent->BindAxis("MoveUp", this, &AWRPlayerController::Debug3PMoveUp);

	CheckEnableDebug3PControl();
}

//Look
void AWRPlayerController::YawRight_Implementation(float Value)
{
	//TODO: instead of hardcoding 90, make this configurable?
	YawRightUnnormalized(Value * 90.f * GetWorld()->GetDeltaSeconds());
}

void AWRPlayerController::YawRightUnnormalized_Implementation(float Value)
{
	if (bIsDebugging3P && bIsDebugging3PControlling)
	{
		Debug3PViewRotation.Yaw += Value;
		return;
	}

	AddYawInput(Value);
}

void AWRPlayerController::PitchUp_Implementation(float Value)
{
	//TODO: instead of hardcoding 90, make this configurable?
	PitchUpUnnormalized(Value * 90.f * GetWorld()->GetDeltaSeconds());
}

void AWRPlayerController::PitchUpUnnormalized_Implementation(float Value)
{
	if (bIsDebugging3P && bIsDebugging3PControlling)
	{
		Debug3PViewRotation.Pitch -= Value;
		return;
	}

	AddPitchInput(Value);
}

///////////////////////////////////////
//Camera

FVector AWRPlayerController::GetRelativeCameraLocation_Implementation()
{
	if (bIsDebugging3P)
	{
		return (FRotator(0.f, GetControlRotation().Yaw, GetControlRotation().Roll) + Debug3PViewRotation).RotateVector(Debug3PViewLocation);
	}

	//For now nothing here
	return FVector::ZeroVector;
}

FRotator AWRPlayerController::GetRelativeCameraRotation_Implementation()
{
	if (bIsDebugging3P)
	{
		//remove camera pitch

		return FRotator(Debug3PViewRotation.Pitch - GetControlRotation().Pitch, Debug3PViewRotation.Yaw, Debug3PViewRotation.Roll);
	}

	FRotator Res = FRotator::ZeroRotator;

	AWRCharacterHumanoid * WRCharacter = Cast<AWRCharacterHumanoid>(GetCharacter());

	if (WRCharacter != NULL)
	{
		//recoil rotation offset (TODO: get this from the hand control object now)
		Res += WRCharacter->GetCurrentWeaponRecoilRotation() * (1.f - FMath::Max(FMath::Min(1.f, CameraRecoilRotationReduction), 0.f)) * .5f;
	}

	return Res;
}

UCameraAnimInst * AWRPlayerController::PlayCameraAnim(
	class UCameraAnim * Anim,
	float Rate,
	float Scale,
	float BlendInTime,
	float BlendOutTime,
	bool bLoop,
	bool bRandomStartTime,
	float Duration,
	ECameraAnimPlaySpace::Type PlaySpace,
	FRotator UserPlaySpaceRot)
{
	if (!PlayerCameraManager)
	{
		return NULL;
	}

	float TrueScale = Scale * (1.f - FMath::Max(FMath::Min(1.f, CameraAnimationReduction), 0.f));

	//don't even bother playing the animation
	if (TrueScale <= 0.f)
	{
		return NULL;
	}

	return PlayerCameraManager->PlayCameraAnim(Anim,
		Rate,
		TrueScale,
		BlendInTime,
		BlendOutTime,
		bLoop,
		bRandomStartTime,
		Duration,
		PlaySpace,
		UserPlaySpaceRot);
}

UCameraAnimInst * AWRPlayerController::PlayCameraAnimWithDuration(
	class UCameraAnim * Anim,
	float Duration,
	float Scale,
	float BlendInTime,
	float BlendOutTime,
	ECameraAnimPlaySpace::Type PlaySpace,
	FRotator UserPlaySpaceRot,
	bool bAccountForBlendoutTime)
{
	if (!PlayerCameraManager)
	{
		return NULL;
	}

	float TrueScale = Scale * (1.f - FMath::Max(FMath::Min(1.f, CameraAnimationReduction), 0.f));

	//don't even bother playing the animation
	if (TrueScale <= 0.f || Duration <= 0.f)
	{
		return NULL;
	}

	AAEPlayerCameraManager * PCM = Cast<AAEPlayerCameraManager>(PlayerCameraManager);

	if (PCM)
	{
		return PCM->PlayCameraAnimWithDuration(Anim,
			Duration,
			TrueScale,
			BlendInTime,
			BlendOutTime,
			PlaySpace,
			UserPlaySpaceRot,
			bAccountForBlendoutTime);
	}
	else
	{
		//copying logic from AEPlayerCameraManager
		return PlayerCameraManager->PlayCameraAnim(Anim,
			UAEGameplayStatics::GetCameraAnimRateForDuration(Anim, Duration, bAccountForBlendoutTime ? BlendOutTime : 0.f),
			TrueScale,
			BlendInTime,
			BlendOutTime,
			false,
			false,
			0.f,
			PlaySpace,
			UserPlaySpaceRot);
	}
}

void AWRPlayerController::StopAllCameraAnims(bool bImmediate)
{
	if (PlayerCameraManager)
	{
		PlayerCameraManager->StopAllCameraAnims(bImmediate);
	}
}

UCameraShake * AWRPlayerController::PlayCameraShake(
	TSubclassOf< class UCameraShake > ShakeClass,
	float Scale,
	enum ECameraAnimPlaySpace::Type PlaySpace,
	FRotator UserPlaySpaceRot)
{
	if (!PlayerCameraManager)
	{
		return NULL;
	}

	float TrueScale = Scale * (1.f - FMath::Max(FMath::Min(1.f, CameraShakeReduction), 0.f));

	//don't even bother playing the animation
	if (TrueScale <= 0.f)
	{
		return NULL;
	}

	return PlayerCameraManager->PlayCameraShake(ShakeClass,
		TrueScale,
		PlaySpace, 
		UserPlaySpaceRot);
}

float AWRPlayerController::GetMinViewPitch_Implementation() const
{
	AWRCharacter * WRCharacter = Cast<AWRCharacter>(GetCharacter());

	if (WRCharacter != NULL)
	{
		return WRCharacter->GetMinViewPitch();
	}
	else
	{
		return -90.f;
	}
}

//////////////////
//3rd Person Debug

void AWRPlayerController::CheckEnableDebug3PControl_Implementation()
{
	if (bIsDebugging3P && bIsDebugging3PControlling)
	{
		Debug3PMoveForwardBinding->bConsumeInput = true;
		Debug3PMoveRightBinding->bConsumeInput = true;
		Debug3PMoveUpBinding->bConsumeInput = true;
	}
	else
	{
		Debug3PMoveForwardBinding->bConsumeInput = false;
		Debug3PMoveRightBinding->bConsumeInput = false;
		Debug3PMoveUpBinding->bConsumeInput = false;
	}
}

void AWRPlayerController::StartDebug3P_Implementation()
{
	//TODO: Only allow if cheats enabled
	bIsDebugging3P = true;

	CheckEnableDebug3PControl();
}

void AWRPlayerController::StopDebug3P_Implementation()
{
	bIsDebugging3P = false;

	CheckEnableDebug3PControl();
}

void AWRPlayerController::StartDebug3PControl_Implementation()
{
	bIsDebugging3PControlling = true;

	CheckEnableDebug3PControl();
}

void AWRPlayerController::StopDebug3PControl_Implementation()
{
	bIsDebugging3PControlling = false;

	CheckEnableDebug3PControl();
}

void AWRPlayerController::Debug3PMoveForward_Implementation(float Value)
{
	Debug3PViewLocation += (FVector::ForwardVector * (Value * WRPLAYERCONTROLLER_DEBUG_3P_MOVE_SPEED * GetWorld()->GetDeltaSeconds()));
}

void AWRPlayerController::Debug3PMoveRight_Implementation(float Value)
{
	Debug3PViewLocation += (FVector::RightVector * (Value * WRPLAYERCONTROLLER_DEBUG_3P_MOVE_SPEED * GetWorld()->GetDeltaSeconds()));
}

void AWRPlayerController::Debug3PMoveUp_Implementation(float Value)
{
	Debug3PViewLocation += (FVector::UpVector * (Value * WRPLAYERCONTROLLER_DEBUG_3P_MOVE_SPEED * GetWorld()->GetDeltaSeconds()));
}
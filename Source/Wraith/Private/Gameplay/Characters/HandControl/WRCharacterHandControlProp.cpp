#include "Wraith.h"
#include "WRCharacterMovement.h"
#include "WRCharacterHandControlProp.h"
#include "WRProjectile.h"
#include "WRGameplayStatics.h"

UWRCharacterHandControlProp::UWRCharacterHandControlProp(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;

	IdleHandsAnimationBlend.SetBlendOption(EAlphaBlendOption::CubicInOut);

	DesiredLazyGunWeight = 1.f;
	LazyGunWeight = 1.f;

	DefaultNonDominantHandPropSocket = FName(TEXT("prop_hand_l"));
	DefaultDominantHandPropSocket = FName(TEXT("prop_hand_r"));
}

void UWRCharacterHandControlProp::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	//TODO: Move this out of here and put it into a lazy loader getter
	UpdateBaseRelativeToEyesTransform();

	UpdateWalkingSwayHandsOffset(DeltaTime);
	UpdateIdleHandsAnimation(DeltaTime);
	UpdateLazyGunOffsetRotation(DeltaTime);
	UpdateStumbleHandsOffset(DeltaTime);
	UpdateMovementHandViewOffset(DeltaTime);

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

FName UWRCharacterHandControlProp::GetNonDominantHandPropSocket_Implementation() const
{
	return DefaultNonDominantHandPropSocket;
}

FName UWRCharacterHandControlProp::GetDominantHandPropSocket_Implementation() const
{
	return DefaultDominantHandPropSocket;
}

FTransform UWRCharacterHandControlProp::GetHandWorldTransform_Implementation(bool bIncludeAnimationOnlyEffects) const
{
	return GetWorldTransformForHandEffectsToRelativeToEyesTransform(GetBaseRelativeToEyesTransform(), bIncludeAnimationOnlyEffects);
}

FTransform UWRCharacterHandControlProp::GetBaseWorldTransform_Implementation() const
{
	//TODO: (ilselets) when using VR, derive this from the controller location

	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (OwnerCharacter)
	{
		float GunAimPitchWeightInverse = OwnerCharacter->GetMesh()->GetAnimInstance()->GetCurveValue(FName(TEXT("GunAimPitchWeightInverse")));
		float GunAimPitchWeight = 1.f - GunAimPitchWeightInverse;

		FTransform EyesWorldTransform = OwnerCharacter->GetEyesWorldTransform();

		FRotator ClampedActorEyesRot = EyesWorldTransform.GetRotation().Rotator();
		ClampedActorEyesRot.Pitch = FMath::ClampAngle(ClampedActorEyesRot.Pitch, -90.f * GunAimPitchWeight, 90.f * (1.f - GunAimPitchWeightInverse * .5f));

		FTransform ClampedEyesWorldTransform(EyesWorldTransform);
		ClampedEyesWorldTransform.SetRotation(ClampedActorEyesRot.Quaternion());

		return FTransform(GetBaseRelativeToEyesLocation()) * ClampedEyesWorldTransform;
	}

	return FTransform::Identity;
}

void UWRCharacterHandControlProp::UpdateBaseRelativeToEyesTransform_Implementation()
{
	//TODO: (ilselets) when using VR, derive this from the controller location

	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (OwnerCharacter)
	{				
		//Put BaseWorldTransform back into eye space
		BaseRelativeToEyesTransform = GetBaseWorldTransform().GetRelativeTransform(OwnerCharacter->GetEyesWorldTransform());
	}
}

FVector UWRCharacterHandControlProp::GetBaseRelativeToEyesLocation_Implementation() const
{
	FVector Res = FVector::ZeroVector;
		
	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	//figure out the offset based on pitch
	if (HeldEquippable && OwnerCharacter)
	{
		float AimPitch = OwnerCharacter->GetAimOffsets().Pitch;

		float Alpha = FMath::Abs(AimPitch) / 90.f;
		Alpha = FMath::Pow(Alpha, 1.5f);

		if (AimPitch < 0.f)
		{
			Res = FMath::Lerp(HeldEquippable->GetCenterDominantHandViewOffset(), HeldEquippable->GetDownDominantHandViewOffset(), Alpha);
		}
		else
		{
			Res = FMath::Lerp(HeldEquippable->GetCenterDominantHandViewOffset(), HeldEquippable->GetUpDominantHandViewOffset(), Alpha);
		}
	}

	return Res;
}

FTransform UWRCharacterHandControlProp::GetWorldTransformForHandEffectsToRelativeToEyesTransform_Implementation(const FTransform& RelativeToEyesTransform, bool bIncludeAnimationOnlyEffects) const
{
	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (!OwnerCharacter)
	{
		return FTransform::Identity;
	}

	FTransform EyesWorldTransform = OwnerCharacter->GetEyesWorldTransform();

	if (bIncludeAnimationOnlyEffects)
	{
		//lazy gun (TODO: shouldn't apply lazy gun when using motion controls)

		FRotator EyesWorldRotation = EyesWorldTransform.Rotator();

		if (LazyGunWeight > 0.f)
		{
			EyesWorldRotation += LazyGunControlRotationOffset * LazyGunWeight;
		}

		EyesWorldTransform.SetRotation(EyesWorldRotation.Quaternion());
	}

	FTransform Res(EyesWorldTransform.GetLocation());

	//movement offset
	Res = FTransform(CurrentMovementHandViewRotation, CurrentMovementHandViewPosition) * Res;

	//stumble
	Res = FTransform(CurrentStumbleHandsRotation, CurrentStumbleHandsPosition) * Res;

	//actual rotation
	Res.ConcatenateRotation(EyesWorldTransform.GetRotation());

	//location relative to eyes
	Res = FTransform(RelativeToEyesTransform) * Res;

	if (bIncludeAnimationOnlyEffects)
	{
		//walking sway
		if (CurrentWalkingSwayHandsOffsetWeight > 0.f && CurrentWalkingSwayHandsOffsetEnableWeight > 0.f)
		{
			float TrueSwayWeight = CurrentWalkingSwayHandsOffsetWeight * CurrentWalkingSwayHandsOffsetEnableWeight;

			float SwayValue = OwnerCharacter->GetMesh()->GetAnimInstance()->GetCurveValue(FName(TEXT("GunSway")));

			Res = FTransform(FRotator(FMath::Abs(SwayValue), SwayValue * .5f, SwayValue * -.5f) * TrueSwayWeight,
				FVector(0.f, SwayValue, SwayValue * SwayValue * .5f) * TrueSwayWeight) * Res;
		}

		//idle (TODO: shouldn't apply idle when using motion controls)
		if (IdleHandsAnimationWeight > 0.f)
		{
			Res = FTransform(GetCurrentIdleHandsRotation() * IdleHandsAnimationWeight, GetCurrentIdleHandsPosition() * IdleHandsAnimationWeight) * Res;
		}
	}

	return Res;
}

void UWRCharacterHandControlProp::CharacterJumped_Implementation()
{
	ImpartJumpMovementHandViewOffset();
}

void UWRCharacterHandControlProp::CharacterLanded_Implementation(float FallingDamage = 0.f)
{
	ImpartLandMovementHandViewOffset(FallingDamage);
}

/////////////////////////////////////// 
//Projectile Shooting

FTransform UWRCharacterHandControlProp::GetSafeShootingSpawnWorldTransformFromProp_Implementation(const FVector& SpawnOffset,
	ECollisionChannel TraceChannel,
	float SpawnedActorRadius)
{
	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (!OwnerCharacter)
	{
		return FTransform::Identity;
	}

	FTransform BaseSpawnWorldTransform = GetBaseRelativeToEyesTransform() * OwnerCharacter->GetEyesWorldTransform();

	if (HeldEquippable)
	{
		BaseSpawnWorldTransform = HeldEquippable->GetDefaultProjectileSpawnOffsetFromCharacterAttachment() * BaseSpawnWorldTransform;
	}

	BaseSpawnWorldTransform = FTransform(SpawnOffset) * BaseSpawnWorldTransform;

	//cap the radius at 5 because this solution isn't that perfect and breaks down at high radii
	BaseSpawnWorldTransform = OwnerCharacter->GetSafeShootingSpawnWorldTransform(BaseSpawnWorldTransform.GetLocation(),
		TraceChannel,
		FMath::Min(SpawnedActorRadius, 5.f));

	return BaseSpawnWorldTransform;
}

FTransform UWRCharacterHandControlProp::GetRecoilAffectedShootingSpawnWorldTransformFromProp_Implementation(const FVector& SpawnOffset,
	ECollisionChannel TraceChannel,
	float SpawnedActorRadius)
{
	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (!OwnerCharacter)
	{
		return FTransform::Identity;
	}

	FTransform BaseSpawnWorldTransform = GetSafeShootingSpawnWorldTransformFromProp(SpawnOffset, COLLISION_TRACE_WEAPON, SpawnedActorRadius);

	FTransform SpawnWorldTransform = GetWorldTransformForHandEffectsToRelativeToEyesTransform(BaseSpawnWorldTransform.GetRelativeTransform(OwnerCharacter->GetEyesWorldTransform()));

	return SpawnWorldTransform;
}

TArray<AWRProjectile *> UWRCharacterHandControlProp::ShootProjectileFromProp_Implementation(TSubclassOf<AWRProjectile> ProjectileClass,
	const FVector& SpawnOffset,
	float RandomConeHalfAngleDegrees,
	int32 MinShots,
	int32 MaxShots)
{
	TArray<AWRProjectile *> Res;

	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (OwnerCharacter)
	{
		FTransform SpawnWorldTransform = GetRecoilAffectedShootingSpawnWorldTransformFromProp(SpawnOffset,
			COLLISION_TRACE_WEAPON,
			ProjectileClass->GetDefaultObject<AWRProjectile>()->ProjectileCollision->GetScaledSphereRadius());

		UAEGameplayStatics::PerformActionRandomTimes(MinShots, MaxShots, [&SpawnWorldTransform, RandomConeHalfAngleDegrees, ProjectileClass, OwnerCharacter, &Res](int32 TimeNumber)
		{
			Res.Add(UWRGameplayStatics::ShootProjectile(ProjectileClass, UAEGameplayStatics::ApplyRandomConeToTransform(SpawnWorldTransform, RandomConeHalfAngleDegrees), OwnerCharacter));
		});
	}

	return Res;
}

TArray<FHitResult> UWRCharacterHandControlProp::ShootSingleHitscanTraceFromProp_Implementation(TSubclassOf<UWRDamageType> DamageType,
	TSubclassOf<UWRImpactType> ImpactType,
	const FVector& SpawnOffset,
	float RandomConeHalfAngleDegrees,
	int32 MinShots,
	int32 MaxShots,
	float DamageMultiplier,
	float ExplosionRadiusMultiplier,
	float MaxDistance,
	bool bDebugDraw)
{
	TArray<FHitResult> Res;

	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (OwnerCharacter)
	{
		FTransform SpawnWorldTransform = GetRecoilAffectedShootingSpawnWorldTransformFromProp(SpawnOffset,
			COLLISION_TRACE_WEAPON,
			0.f);

		UAEGameplayStatics::PerformActionRandomTimes(MinShots, MaxShots, [&SpawnWorldTransform,
			DamageType,
			ImpactType,
			RandomConeHalfAngleDegrees,
			OwnerCharacter,
			&Res,
			DamageMultiplier,
			ExplosionRadiusMultiplier,
			MaxDistance,
			bDebugDraw]
			(int32 TimeNumber)
		{
			FHitResult HitResult;

			UWRGameplayStatics::ShootSingleHitscanTrace(HitResult,
				UAEGameplayStatics::ApplyRandomConeToTransform(SpawnWorldTransform, RandomConeHalfAngleDegrees),
				DamageType,
				ImpactType,
				OwnerCharacter,
				DamageMultiplier,
				ExplosionRadiusMultiplier,
				MaxDistance,
				OwnerCharacter->GetWorld(),
				bDebugDraw);

			Res.Add(HitResult);
		});
	}

	return Res;
}

///////////////////////////////////////
//Lazy Gun

//TODO: Make lazygun configurable so it's possible to lessen the effect for people who don't like it
const float LAZYGUN_MAX_OFFSET = 5.f;
const float LAZYGUN_RECOVER_SPEED = 20.f;

void UWRCharacterHandControlProp::UpdateLazyGunCharacterAim_Implementation(FRotator NewControlRotation, float DeltaTime)
{
	//TODO: Don't bother updating this if using VR motion controls

	//lazy gun rotation
	LazyGunControlRotation = FMath::RInterpTo(LazyGunControlRotation, NewControlRotation, DeltaTime, LAZYGUN_RECOVER_SPEED);

	FRotator LazyGunControlRotationDelta = LazyGunControlRotation - NewControlRotation;

	//constrain the offset so the gun doesn't lag behind too much
	LazyGunControlRotationDelta.Roll = FMath::ClampAngle(LazyGunControlRotationDelta.Roll, -LAZYGUN_MAX_OFFSET, LAZYGUN_MAX_OFFSET);
	LazyGunControlRotationDelta.Pitch = FMath::ClampAngle(LazyGunControlRotationDelta.Pitch, -LAZYGUN_MAX_OFFSET, LAZYGUN_MAX_OFFSET);
	LazyGunControlRotationDelta.Yaw = FMath::ClampAngle(LazyGunControlRotationDelta.Yaw, -LAZYGUN_MAX_OFFSET, LAZYGUN_MAX_OFFSET);

	//force the offset added back on as a quick way to constrain the lagged value
	LazyGunControlRotation = NewControlRotation + LazyGunControlRotationDelta;

	//Setting the actual LazyGunControlRotationOffset this way to prevent it from jitterring annoyingly as you look around
	LazyGunControlRotationOffset = FMath::RInterpTo(LazyGunControlRotationOffset, LazyGunControlRotationDelta, DeltaTime, 5.f);
}

const float LAZYGUN_WEIGHT_SPEED = 3.f;

void UWRCharacterHandControlProp::UpdateLazyGunOffsetRotation_Implementation(float DeltaSeconds)
{
	LazyGunWeight = FMath::FInterpTo(LazyGunWeight, DesiredLazyGunWeight, DeltaSeconds, LAZYGUN_WEIGHT_SPEED);
}

void UWRCharacterHandControlProp::EnableLazyGun_Implementation()
{
	DesiredLazyGunWeight = 1.f;
}

void UWRCharacterHandControlProp::DisableLazyGun_Implementation()
{
	DesiredLazyGunWeight = 0.f;
}

///////////////////////////////////////
//Movement Hands Offset

const float MOVEMENT_HAND_OFFSET_MOVE_TO_INTERP_SPEED = 5.f;
const float MOVEMENT_HAND_OFFSET_RETURN_INTERP_SPEED = 2.f;

const float AIR_HAND_OFFSET_JUMP_MAX_HORZ_OFFSET = 2.f;
const float AIR_HAND_OFFSET_JUMP_MAX_DAMAGED_HORZ_OFFSET = AIR_HAND_OFFSET_JUMP_MAX_HORZ_OFFSET * 2.f;

const float AIR_HAND_OFFSET_JUMP_DESIRED_HEIGHT = -3.f;
const float AIR_HAND_OFFSET_JUMP_DESIRED_DAMAGED_HEIGHT = AIR_HAND_OFFSET_JUMP_DESIRED_HEIGHT * 2.f;

const float AIR_HAND_OFFSET_JUMP_MIN_PITCH = -5.f;
const float AIR_HAND_OFFSET_JUMP_MIN_DAMAGED_PITCH = AIR_HAND_OFFSET_JUMP_MIN_PITCH * 2.f;
const float AIR_HAND_OFFSET_JUMP_MAX_PITCH = -3.f;

const float AIR_HAND_OFFSET_JUMP_MAX_YAW_OFFSET = 2.f;
const float AIR_HAND_OFFSET_JUMP_MAX_DAMGED_YAW_OFFSET = AIR_HAND_OFFSET_JUMP_MAX_YAW_OFFSET * 2.f;

const float AIR_HAND_OFFSET_JUMP_MAX_ROLL_OFFSET = 3.f;
const float AIR_HAND_OFFSET_JUMP_MAX_DAMAGED_ROLL_OFFSET = AIR_HAND_OFFSET_JUMP_MAX_ROLL_OFFSET * 2.f;

void UWRCharacterHandControlProp::UpdateMovementHandViewOffset_Implementation(float DeltaSeconds)
{
	//when desired weight is 0 it's returning back to normal, go slower, otherwise jump quickly to the desired position right after a jump or landing
	CurrentMovementHandViewPosition = FMath::VInterpTo(CurrentMovementHandViewPosition, DesiredMovementHandViewPosition, DeltaSeconds, CurrentMovementHandViewPosition.Z == 0.f
		? MOVEMENT_HAND_OFFSET_RETURN_INTERP_SPEED
		: MOVEMENT_HAND_OFFSET_MOVE_TO_INTERP_SPEED);

	CurrentMovementHandViewRotation = FMath::RInterpTo(CurrentMovementHandViewRotation, DesiredMovementHandViewRotation, DeltaSeconds, CurrentMovementHandViewPosition.Z == 0.f
		? MOVEMENT_HAND_OFFSET_RETURN_INTERP_SPEED
		: MOVEMENT_HAND_OFFSET_MOVE_TO_INTERP_SPEED);

	//if reached destination start going back to 0
	if (FMath::IsNearlyEqual(CurrentMovementHandViewPosition.Z, DesiredMovementHandViewPosition.Z, 1.f))
	{
		DesiredMovementHandViewPosition = FVector::ZeroVector;
		DesiredMovementHandViewRotation = FRotator::ZeroRotator;
	}
}

void UWRCharacterHandControlProp::ImpartJumpMovementHandViewOffset_Implementation()
{
	DesiredMovementHandViewPosition = FVector(
		FMath::FRandRange(-AIR_HAND_OFFSET_JUMP_MAX_HORZ_OFFSET, AIR_HAND_OFFSET_JUMP_MAX_HORZ_OFFSET),
		0.f,
		AIR_HAND_OFFSET_JUMP_DESIRED_HEIGHT);

	DesiredMovementHandViewRotation = FRotator(
		FMath::FRandRange(AIR_HAND_OFFSET_JUMP_MIN_PITCH, AIR_HAND_OFFSET_JUMP_MAX_PITCH),
		FMath::FRandRange(-AIR_HAND_OFFSET_JUMP_MAX_YAW_OFFSET, AIR_HAND_OFFSET_JUMP_MAX_YAW_OFFSET),
		FMath::FRandRange(-AIR_HAND_OFFSET_JUMP_MAX_ROLL_OFFSET, AIR_HAND_OFFSET_JUMP_MAX_ROLL_OFFSET));
}

void UWRCharacterHandControlProp::ImpartLandMovementHandViewOffset_Implementation(float FallingDamage)
{
	if (FallingDamage > 0.f)
	{
		//calculations here are similar to ImpartJumpMovementHandViewOffset only with increased magnitude and flipped in the up direction
		{
			float FallingDamageAnimFactor = FallingDamage * .1f;

			float HorzOffset = FMath::Min(AIR_HAND_OFFSET_JUMP_MAX_DAMAGED_HORZ_OFFSET, AIR_HAND_OFFSET_JUMP_MAX_HORZ_OFFSET + FallingDamageAnimFactor);

			DesiredMovementHandViewPosition = FVector(
				FMath::FRandRange(-HorzOffset, HorzOffset),
				0.f,
				FMath::Min(-AIR_HAND_OFFSET_JUMP_DESIRED_DAMAGED_HEIGHT, -AIR_HAND_OFFSET_JUMP_DESIRED_HEIGHT + FallingDamageAnimFactor));
		}

		{
			float FallingDamageAnimFactor = FallingDamage * .05f;

			float YawOffset = FMath::Min(AIR_HAND_OFFSET_JUMP_MAX_DAMGED_YAW_OFFSET, AIR_HAND_OFFSET_JUMP_MAX_YAW_OFFSET + FallingDamageAnimFactor);
			float RollOffset = FMath::Min(AIR_HAND_OFFSET_JUMP_MAX_DAMAGED_ROLL_OFFSET, AIR_HAND_OFFSET_JUMP_MAX_ROLL_OFFSET + FallingDamageAnimFactor);

			DesiredMovementHandViewRotation = FRotator(
				FMath::FRandRange(
					-AIR_HAND_OFFSET_JUMP_MAX_PITCH,
					FMath::Max(-AIR_HAND_OFFSET_JUMP_MIN_DAMAGED_PITCH, -AIR_HAND_OFFSET_JUMP_MIN_PITCH - FallingDamageAnimFactor)),
				FMath::FRandRange(-YawOffset, YawOffset),
				FMath::FRandRange(-RollOffset, RollOffset));
		}
	}
	else
	{
		//calculations here are similar to ImpartJumpMovementHandViewOffset only flipped in the up direction
		DesiredMovementHandViewPosition = FVector(
			FMath::FRandRange(-AIR_HAND_OFFSET_JUMP_MAX_HORZ_OFFSET, AIR_HAND_OFFSET_JUMP_MAX_HORZ_OFFSET),
			0.f,
			-AIR_HAND_OFFSET_JUMP_DESIRED_HEIGHT);

		DesiredMovementHandViewRotation = FRotator(
			FMath::FRandRange(-AIR_HAND_OFFSET_JUMP_MAX_PITCH, -AIR_HAND_OFFSET_JUMP_MIN_PITCH),
			FMath::FRandRange(-AIR_HAND_OFFSET_JUMP_MAX_YAW_OFFSET, AIR_HAND_OFFSET_JUMP_MAX_YAW_OFFSET),
			FMath::FRandRange(-AIR_HAND_OFFSET_JUMP_MAX_ROLL_OFFSET, AIR_HAND_OFFSET_JUMP_MAX_ROLL_OFFSET));
	}
}

///////////////////////////////////////
//Stumble Hands Offset

const float STUMBLE_MAX_POS = 8.f;
const float STUMBLE_MAX_ROT = 5.f;
const float STUMBLE_POS_INTERP_SPEED = STUMBLE_MAX_POS;
const float STUMBLE_ROT_INTERP_SPEED = STUMBLE_MAX_ROT;

void UWRCharacterHandControlProp::UpdateStumbleHandsOffset_Implementation(float DeltaSeconds)
{
	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (OwnerCharacter)
	{
		UWRCharacterMovement * WRMovement = Cast<UWRCharacterMovement>(OwnerCharacter->GetCharacterMovement());

		if (WRMovement)
		{
			FVector StumbleVelocity = WRMovement->GetStumbleVelocity();
			float StumbleMagnitude = StumbleVelocity.SizeSquared();

			FVector DesiredPos = FVector::ZeroVector;
			FRotator DesiredRot = FRotator::ZeroRotator;

			if (StumbleMagnitude > 0.f)
			{
				StumbleMagnitude = FMath::Sqrt(StumbleMagnitude);

				float MaxSpeedFactor = StumbleMagnitude / WRMovement->SprintSpeed;

				//position opposite the movement direction
				DesiredPos = -StumbleVelocity.GetClampedToMaxSize(STUMBLE_MAX_POS * MaxSpeedFactor);

				FVector StumbleDirection = StumbleVelocity.GetUnsafeNormal();
				FVector StumblePerpAxis = FVector::CrossProduct(StumbleDirection, FVector::UpVector);

				FQuat StumbleRot(StumblePerpAxis, FMath::DegreesToRadians(STUMBLE_MAX_ROT) * MaxSpeedFactor);

				//rotate in movement direction
				DesiredRot = StumbleRot.Rotator();
			}

			CurrentStumbleHandsPosition = FMath::VInterpTo(CurrentStumbleHandsPosition, DesiredPos, DeltaSeconds, STUMBLE_POS_INTERP_SPEED);
			CurrentStumbleHandsRotation = FMath::RInterpTo(CurrentStumbleHandsRotation, DesiredRot, DeltaSeconds, STUMBLE_ROT_INTERP_SPEED);
		}
	}
}

///////////////////////////////////////
//Walking Sway Hands Offset

void UWRCharacterHandControlProp::UpdateWalkingSwayHandsOffset_Implementation(float DeltaSeconds)
{
	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (OwnerCharacter)
	{
		if (OwnerCharacter->GetCharacterMovement()->IsMovingOnGround()) {
			DesiredWalkingSwayHandsOffsetWeight = FMath::Min(OwnerCharacter->GetVelocity().Size2D() * .0025f, 3.f);
		}
		else {
			DesiredWalkingSwayHandsOffsetWeight = 0.f;
		}

		CurrentWalkingSwayHandsOffsetWeight = FMath::FInterpTo(CurrentWalkingSwayHandsOffsetWeight, DesiredWalkingSwayHandsOffsetWeight, DeltaSeconds, 1.f);
		CurrentWalkingSwayHandsOffsetEnableWeight = FMath::FInterpTo(CurrentWalkingSwayHandsOffsetEnableWeight, DesiredWalkingSwayHandsOffsetEnableWeight, DeltaSeconds, 1.f);
	}
}

void UWRCharacterHandControlProp::EnableWalkingHandsSwayOffset_Implementation()
{
	DesiredWalkingSwayHandsOffsetEnableWeight = 1.f;
}

void UWRCharacterHandControlProp::DisableWalkingHandsSwayOffset_Implementation()
{
	DesiredWalkingSwayHandsOffsetEnableWeight = 0.f;
}

///////////////////////////////////////
//Idle Hands Animation

const float IDLE_WEIGHT_SPEED = 3.f;
const FVector IDLE_MAX_POSITION(1.f, 1.f, 1.f);
const FRotator IDLE_MAX_ROTATION(1.5f, 1.5f, 4.f);
const float IDLE_MIN_TIME = 4.f;
const float IDLE_MAX_TIME = 10.f;

void UWRCharacterHandControlProp::EnableIdleHandsAnimation_Implementation()
{
	DesiredIdleHandsAnimationWeight = 1.f;
	IdleHandsAnimationBlend.Reset();
	StartIdleHandsPosition = FVector::ZeroVector;
	StartIdleHandsRotation = FRotator::ZeroRotator;
}

void UWRCharacterHandControlProp::DisableIdleHandsAnimation_Implementation()
{
	DesiredIdleHandsAnimationWeight = 0.f;
}

void UWRCharacterHandControlProp::UpdateIdleHandsAnimation_Implementation(float DeltaSeconds)
{
	IdleHandsAnimationWeight = FMath::FInterpTo(IdleHandsAnimationWeight, DesiredIdleHandsAnimationWeight, DeltaSeconds, IDLE_WEIGHT_SPEED);

	IdleHandsAnimationBlend.Update(DeltaSeconds);

	if (IdleHandsAnimationBlend.IsComplete())
	{
		IdleHandsAnimationBlend.SetValueRange(0.f, 1.f);
		IdleHandsAnimationBlend.SetBlendTime(FMath::FRandRange(IDLE_MIN_TIME, IDLE_MAX_TIME));
		IdleHandsAnimationBlend.Reset();

		StartIdleHandsPosition = DestinationIdleHandsPosition;
		StartIdleHandsRotation = DestinationIdleHandsRotation;

		DestinationIdleHandsPosition = FMath::VRand() * IDLE_MAX_POSITION;
		DestinationIdleHandsRotation = FRotator(
			FMath::FRandRange(IDLE_MAX_ROTATION.Pitch, -IDLE_MAX_ROTATION.Pitch),
			FMath::FRandRange(IDLE_MAX_ROTATION.Yaw, -IDLE_MAX_ROTATION.Yaw),
			FMath::FRandRange(IDLE_MAX_ROTATION.Roll, -IDLE_MAX_ROTATION.Roll));
	}
}
#include "Wraith.h"
#include "WRCharacterMovement.h"
#include "WRCharacterHandControlWeapon.h"
#include "Kismet/KismetMathLibrary.h"

UWRCharacterHandControlWeapon::UWRCharacterHandControlWeapon(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	MaxRadialWeaponInaccuracyMagnitude = 2.f;
	
	RadialWeaponInaccuracyBlend.SetValueRange(0.f, 0.f);
	RadialWeaponInaccuracyBlend.SetBlendTime(0.f);
	RadialWeaponInaccuracyBlend.Reset();
	
	ConstantRadialWeaponInaccuracyBlend.SetValueRange(0.f, 0.f);
	ConstantRadialWeaponInaccuracyBlend.SetBlendTime(0.f);
	ConstantRadialWeaponInaccuracyBlend.Reset();
}

void UWRCharacterHandControlWeapon::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	UpdateHandRecoilAnimation(DeltaTime);
	UpdateWeaponRecoil(DeltaTime);
	UpdateRadialWeaponInaccuracy(DeltaTime);
	UpdateAnimBaseRelativeToEyesTransform(DeltaTime);

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

FTransform UWRCharacterHandControlWeapon::GetHandWorldTransform_Implementation(bool bIncludeAnimationOnlyEffects) const
{
	if (bIncludeAnimationOnlyEffects)
	{
		return GetWorldTransformForHandEffectsToRelativeToEyesTransform(AnimBaseRelativeToEyesTransform, true);
	}
	else
	{
		return Super::GetHandWorldTransform_Implementation(false);
	}
}

void UWRCharacterHandControlWeapon::UpdateBaseRelativeToEyesTransform_Implementation()
{
	//TODO: (ilselets) when using VR, derive this from the controller location

	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (OwnerCharacter)
	{
		float GunAimPitchWeightInverse = OwnerCharacter->GetMesh()->GetAnimInstance()->GetCurveValue(FName(TEXT("GunAimPitchWeightInverse")));
		float GunAimPitchWeight = 1.f - GunAimPitchWeightInverse;
		float AimAtCrosshairWeight = 1.f;	//TODO: OwnerCharacter->GetMesh()->GetAnimInstance()->GetCurveValue(FName(TEXT("GunAimAtCrosshairWeight")));

		//Put BaseWorldTransform back into eye space
		FTransform HandWorldTransform = GetBaseWorldTransform();

		//Figure out where the gun would be aiming based on where the crosshair is aiming
		if (HeldEquippable && AimAtCrosshairWeight > 0.f && GunAimPitchWeight > 0.f)
		{
			//find the aimed transform
			FTransform AimedDominantHandWorldTransform = UAEGameplayStatics::LookatAroundTransform(HandWorldTransform, HeldEquippable->GetDefaultProjectileSpawnOffsetFromCharacterAttachment(), OwnerCharacter->GetCrosshairLocation());

			//apply AimAtCrosshairWeight influence
			HandWorldTransform = UKismetMathLibrary::TLerp(HandWorldTransform, AimedDominantHandWorldTransform, AimAtCrosshairWeight * GunAimPitchWeight);

			/*DrawDebugLine(
			GetWorld(),
				(HeldEquippable->GetDefaultProjectileSpawnOffsetFromCharacterAttachment() * HandWorldTransform).GetLocation(),
				OwnerCharacter->GetCrosshairLocation(),
			FColor::Yellow,
			false, 1.f, 0,
			1);*/
		}

		//Put this back into eye space
		BaseRelativeToEyesTransform = HandWorldTransform.GetRelativeTransform(OwnerCharacter->GetEyesWorldTransform());
	}
}

void UWRCharacterHandControlWeapon::UpdateAnimBaseRelativeToEyesTransform_Implementation(float DeltaSeconds)
{
	AnimBaseRelativeToEyesTransform = UKismetMathLibrary::TInterpTo(AnimBaseRelativeToEyesTransform,
		BaseRelativeToEyesTransform,
		DeltaSeconds, 7.f);
}

FTransform UWRCharacterHandControlWeapon::GetWorldTransformForHandEffectsToRelativeToEyesTransform_Implementation(const FTransform& RelativeToEyesTransform, bool bIncludeAnimationOnlyEffects) const
{
	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (!OwnerCharacter)
	{
		return FTransform::Identity;
	}
	
	FTransform Res = Super::GetWorldTransformForHandEffectsToRelativeToEyesTransform_Implementation(RelativeToEyesTransform, bIncludeAnimationOnlyEffects);
	
	if (bIncludeAnimationOnlyEffects)
	{
		//hand recoil animation
		Res = FTransform(GetCurrentHandRecoilAnimationRotation(), GetCurrentHandRecoilAnimationPosition()) * Res;
	}

	//weapon recoil
	Res.ConcatenateRotation(GetCurrentWeaponRecoilRotation().Quaternion());

	//radial recoil
	Res.ConcatenateRotation(GetCurrentRadialWeaponInaccuracyRotation().Quaternion());

	return Res;
}

void UWRCharacterHandControlWeapon::CharacterJumped_Implementation()
{
	Super::CharacterJumped_Implementation();
	ImpartJumpRadialWeaponInaccuracy();
}

void UWRCharacterHandControlWeapon::CharacterLanded_Implementation(float FallingDamage)
{
	Super::CharacterLanded_Implementation(FallingDamage);
	ImpartLandRadialWeaponInaccuracy(FallingDamage);
}

///////////////////////////////////////
//Hand Recoil Animation

void UWRCharacterHandControlWeapon::UpdateHandRecoilAnimation_Implementation(float DeltaSeconds)
{
	if (!HandRecoilAnimationBlend.IsComplete())
	{
		//this means we're updating the recoil itself still
		if (HandRecoilAnimationRecoverTime > 0.f)
		{
			float RemainingTime = HandRecoilAnimationBlend.Update(DeltaSeconds);

			//start recovering
			if (HandRecoilAnimationBlend.IsComplete())
			{
				//restart the blend
				HandRecoilAnimationBlend.SetValueRange(0.f, 1.f);
				HandRecoilAnimationBlend.SetBlendTime(HandRecoilAnimationRecoverTime);
				HandRecoilAnimationRecoverTime = 0.f;

				StartHandRecoilAnimationPosition = DestinationHandRecoilAnimationPosition;
				StartHandRecoilAnimationRotation = DestinationHandRecoilAnimationRotation;

				DestinationHandRecoilAnimationPosition = FVector::ZeroVector;
				DestinationHandRecoilAnimationRotation = FRotator::ZeroRotator;

				HandRecoilAnimationBlend.Reset();

				//update with remaining time
				HandRecoilAnimationBlend.Update(RemainingTime);

				//if it's complete again be done recovering
				if (HandRecoilAnimationBlend.IsComplete())
				{
					StartHandRecoilAnimationPosition = FVector::ZeroVector;
					StartHandRecoilAnimationRotation = FRotator::ZeroRotator;
				}
			}
		}
		else  //We're recovering from recoil
		{
			HandRecoilAnimationBlend.Update(DeltaSeconds);

			//if done recovering
			if (HandRecoilAnimationBlend.IsComplete())
			{
				StartHandRecoilAnimationPosition = FVector::ZeroVector;
				StartHandRecoilAnimationRotation = FRotator::ZeroRotator;

				DestinationHandRecoilAnimationPosition = FVector::ZeroVector;
				DestinationHandRecoilAnimationRotation = FRotator::ZeroRotator;
			}
		}
	}
}

const FVector HAND_RECOIL_ANIMATION_STRENGTH_POSITION_FACTOR(-10.f, 3.f, 3.f);
const FRotator HAND_RECOIL_ANIMATION_STRENGTH_ROTATION_FACTOR(7.f, 5.f, 5.f);

const float HAND_RECOIL_ANIMATION_MIN_POSITION_FACTOR = 0.5f;
const float HAND_RECOIL_ANIMATION_MIN_ROTATION_FACTOR = 0.5f;

const FVector HAND_RECOIL_ANIMATION_MAX_POSITION = HAND_RECOIL_ANIMATION_STRENGTH_POSITION_FACTOR * 1.5f;
const FRotator HAND_RECOIL_ANIMATION_MAX_ROTATION = HAND_RECOIL_ANIMATION_STRENGTH_ROTATION_FACTOR * 1.5f;

void UWRCharacterHandControlWeapon::ImpartHandRecoilAnimation_Implementation(float Strength, float Time, float RecoverTime)
{
	FVector CurrentHandRecoilAnimationPosition = GetCurrentHandRecoilAnimationPosition();
	FRotator CurrentHandRecoilAnimationRotation = GetCurrentHandRecoilAnimationRotation();

	//figure out random desired values based on strength
	FVector PossibleDesiredPosition = HAND_RECOIL_ANIMATION_STRENGTH_POSITION_FACTOR * Strength;

	PossibleDesiredPosition.X = FMath::FRandRange(PossibleDesiredPosition.X * HAND_RECOIL_ANIMATION_MIN_POSITION_FACTOR, PossibleDesiredPosition.X);
	PossibleDesiredPosition.Y = FMath::FRandRange(PossibleDesiredPosition.Y * HAND_RECOIL_ANIMATION_MIN_POSITION_FACTOR, PossibleDesiredPosition.Y);
	PossibleDesiredPosition.Z = FMath::FRandRange(PossibleDesiredPosition.Z * HAND_RECOIL_ANIMATION_MIN_POSITION_FACTOR, PossibleDesiredPosition.Z);

	FRotator PossibleDesiredRotation = HAND_RECOIL_ANIMATION_STRENGTH_ROTATION_FACTOR * Strength;

	PossibleDesiredRotation.Pitch = FMath::FRandRange(PossibleDesiredRotation.Pitch * HAND_RECOIL_ANIMATION_MIN_ROTATION_FACTOR, PossibleDesiredRotation.Pitch);
	PossibleDesiredRotation.Yaw = FMath::FRandRange(PossibleDesiredRotation.Yaw * HAND_RECOIL_ANIMATION_MIN_ROTATION_FACTOR, PossibleDesiredRotation.Yaw);
	PossibleDesiredRotation.Roll = FMath::FRandRange(PossibleDesiredRotation.Roll * HAND_RECOIL_ANIMATION_MIN_ROTATION_FACTOR, PossibleDesiredRotation.Roll);

	//add on to the current higher value value

	//pos
	DestinationHandRecoilAnimationPosition.Y = FMath::Clamp(
		(FMath::Abs(CurrentHandRecoilAnimationPosition.Y) > FMath::Abs(DestinationHandRecoilAnimationPosition.Y)
			? CurrentHandRecoilAnimationPosition.Y
			: DestinationHandRecoilAnimationPosition.Y)

		+ PossibleDesiredPosition.Y * (FMath::RandBool() ? 1.f : -1.f),

		-HAND_RECOIL_ANIMATION_MAX_POSITION.Y,
		HAND_RECOIL_ANIMATION_MAX_POSITION.Y);

	DestinationHandRecoilAnimationPosition.X = FMath::Max(FMath::Min(DestinationHandRecoilAnimationPosition.X, CurrentHandRecoilAnimationPosition.X) + PossibleDesiredPosition.X, HAND_RECOIL_ANIMATION_MAX_POSITION.X);
	DestinationHandRecoilAnimationPosition.Z = FMath::Min(FMath::Max(DestinationHandRecoilAnimationPosition.Z, CurrentHandRecoilAnimationPosition.Z) + PossibleDesiredPosition.Z, HAND_RECOIL_ANIMATION_MAX_POSITION.Z);

	//rot
	DestinationHandRecoilAnimationRotation.Pitch = FMath::Min(FMath::Max(DestinationHandRecoilAnimationRotation.Pitch, CurrentHandRecoilAnimationRotation.Pitch) + PossibleDesiredRotation.Pitch,
		HAND_RECOIL_ANIMATION_MAX_ROTATION.Pitch);

	DestinationHandRecoilAnimationRotation.Yaw = FMath::Clamp(
		(FMath::Abs(CurrentHandRecoilAnimationRotation.Yaw) > FMath::Abs(DestinationHandRecoilAnimationRotation.Yaw)
			? CurrentHandRecoilAnimationRotation.Yaw
			: DestinationHandRecoilAnimationRotation.Yaw)

		+ PossibleDesiredRotation.Yaw * (FMath::RandBool() ? 1.f : -1.f),

		-HAND_RECOIL_ANIMATION_MAX_ROTATION.Yaw,
		HAND_RECOIL_ANIMATION_MAX_ROTATION.Yaw);

	DestinationHandRecoilAnimationRotation.Roll = FMath::Clamp(
		(FMath::Abs(CurrentHandRecoilAnimationRotation.Roll) > FMath::Abs(DestinationHandRecoilAnimationRotation.Roll)
			? CurrentHandRecoilAnimationRotation.Roll
			: DestinationHandRecoilAnimationRotation.Roll)

		+ PossibleDesiredRotation.Roll * (FMath::RandBool() ? 1.f : -1.f),

		-HAND_RECOIL_ANIMATION_MAX_ROTATION.Roll,
		HAND_RECOIL_ANIMATION_MAX_ROTATION.Roll);

	StartHandRecoilAnimationPosition = CurrentHandRecoilAnimationPosition;
	StartHandRecoilAnimationRotation = CurrentHandRecoilAnimationRotation;

	//compute new speeds
	{
		//we're still animating the recoil itself
		bool bIsAnimatingRecoil = !HandRecoilAnimationBlend.IsComplete() && HandRecoilAnimationRecoverTime > 0.f;

		if ((bIsAnimatingRecoil && Time < HandRecoilAnimationBlend.GetBlendTime()) || !bIsAnimatingRecoil)
		{
			HandRecoilAnimationBlend.SetBlendTime(Time);
		}
	}

	if (RecoverTime > HandRecoilAnimationRecoverTime)
	{
		HandRecoilAnimationRecoverTime = RecoverTime;
	}

	HandRecoilAnimationBlend.SetValueRange(0.f, 1.f);
	HandRecoilAnimationBlend.Reset();
}

///////////////////////////////////////
//Weapon Recoil

void UWRCharacterHandControlWeapon::ImpartWeaponRecoil_Implementation(float AddYawMax, float TotalYawMax,
	float AddPitchMin, float AddPitchMax, float TotalPitchMax,
	float Time, float RecoverTime, bool bDebugDraw)
{
	//TODO: make randomness here take a seed to make this deterministic across network games and replays

	KickRadialWeaponInaccuracyFuncParamTime();

	FRotator CurrentWeaponRecoilRotation = GetCurrentWeaponRecoilRotation();

	//figure out random desired values based inputs
	FRotator AddValue(
		FMath::FRandRange(AddPitchMin, AddPitchMax),
		FMath::FRand() * AddYawMax,
		0.f);

	//add on to the current higher value
	FRotator NewDestinationWeaponRecoilRotation(
		//pitch
		FMath::Max(DestinationWeaponRecoilRotation.Pitch, CurrentWeaponRecoilRotation.Pitch) + AddValue.Pitch,

		//yaw
		(FMath::Abs(CurrentWeaponRecoilRotation.Yaw) > FMath::Abs(DestinationWeaponRecoilRotation.Yaw)
			? CurrentWeaponRecoilRotation.Yaw
			: DestinationWeaponRecoilRotation.Yaw)

		+ AddValue.Yaw * (FMath::RandBool() ? 1.f : -1.f),

		//roll
		0.f);

	//now check if our destination values here don't exceed max input values, leave destination unmodified
	if (FMath::Abs(DestinationWeaponRecoilRotation.Yaw) < TotalYawMax)
	{
		DestinationWeaponRecoilRotation.Yaw = FMath::Clamp(NewDestinationWeaponRecoilRotation.Yaw, -TotalYawMax, TotalYawMax);
	}

	if (DestinationWeaponRecoilRotation.Pitch < TotalPitchMax)
	{
		DestinationWeaponRecoilRotation.Pitch = FMath::Min(NewDestinationWeaponRecoilRotation.Pitch, TotalPitchMax);
	}

	StartWeaponRecoilRotation = CurrentWeaponRecoilRotation;

	//compute new speeds
	{
		//we're still animating the recoil itself
		bool bIsAnimatingRecoil = !WeaponRecoilBlend.IsComplete() && WeaponRecoilRecoverTime > 0.f;

		if ((bIsAnimatingRecoil && Time < WeaponRecoilBlend.GetBlendTime()) || !bIsAnimatingRecoil)
		{
			WeaponRecoilBlend.SetBlendTime(Time);
		}
	}

	if (RecoverTime > WeaponRecoilRecoverTime)
	{
		WeaponRecoilRecoverTime = RecoverTime;
	}

	WeaponRecoilBlend.SetValueRange(0.f, 1.f);
	WeaponRecoilBlend.Reset();

	//Debug draw
	if (bDebugDraw)
	{
		//FVector SpawnLocation = GetDominantRightHandWorldTransform(false);

		//FVector CrosshairLocation = GetCrosshairLocation();

		////ideally I'd represent all rotations with a quat but unreal likes using Rotators, so I have to use a quat here for computing offset while avoiding gimbal lock
		//FQuat StraightDirQuat = (CrosshairLocation - SpawnLocation).Rotation().Quaternion();

		//FVector StraightLine = SpawnLocation + StraightDirQuat.RotateVector(FVector::ForwardVector * 1000.f);

		//FVector MaxConeLeftLine = SpawnLocation + (StraightDirQuat * FRotator(TotalPitchMax, -TotalYawMax, 0.f).Quaternion()).RotateVector(FVector::ForwardVector * 1000.f);

		//FVector MaxConeRightLine = SpawnLocation + (StraightDirQuat * FRotator(TotalPitchMax, TotalYawMax, 0.f).Quaternion()).RotateVector(FVector::ForwardVector * 1000.f);

		//FVector CurrentLine = SpawnLocation + (StraightDirQuat * CurrentWeaponRecoilRotation.Quaternion()).RotateVector(FVector::ForwardVector * 1000.f);

		//FVector DestinationLine = SpawnLocation + (StraightDirQuat * DestinationWeaponRecoilRotation.Quaternion()).RotateVector(FVector::ForwardVector * 1000.f);

		////Draw the straight line
		//DrawDebugLine(
		//	GetWorld(),
		//	SpawnLocation,
		//	StraightLine,
		//	FColor::Red,
		//	true, -1, 0,
		//	1);

		////left max cone
		//DrawDebugLine(
		//	GetWorld(),
		//	SpawnLocation,
		//	MaxConeLeftLine,
		//	FColor::Red,
		//	true, -1, 0,
		//	1);

		////right max cone
		//DrawDebugLine(
		//	GetWorld(),
		//	SpawnLocation,
		//	MaxConeRightLine,
		//	FColor::Red,
		//	true, -1, 0,
		//	1);

		////current line
		//DrawDebugLine(
		//	GetWorld(),
		//	SpawnLocation,
		//	CurrentLine,
		//	FColor::Green,
		//	true, -1, 0,
		//	1);

		////destination line
		//DrawDebugLine(
		//	GetWorld(),
		//	SpawnLocation,
		//	DestinationLine,
		//	FColor::Yellow,
		//	true, -1, 0,
		//	1);
	}
}

void UWRCharacterHandControlWeapon::UpdateWeaponRecoil_Implementation(float DeltaSeconds)
{
	if (!WeaponRecoilBlend.IsComplete())
	{
		//this means we're updating the recoil itself still
		if (WeaponRecoilRecoverTime > 0.f)
		{
			float RemainingTime = WeaponRecoilBlend.Update(DeltaSeconds);

			//start recovering
			if (WeaponRecoilBlend.IsComplete())
			{
				//restart the blend
				WeaponRecoilBlend.SetValueRange(0.f, 1.f);
				WeaponRecoilBlend.SetBlendTime(WeaponRecoilRecoverTime);
				WeaponRecoilRecoverTime = 0.f;

				StartWeaponRecoilRotation = DestinationWeaponRecoilRotation;
				DestinationWeaponRecoilRotation = FRotator::ZeroRotator;

				WeaponRecoilBlend.Reset();

				//update with remaining time
				WeaponRecoilBlend.Update(RemainingTime);

				//if it's complete again be done recovering
				if (WeaponRecoilBlend.IsComplete())
				{
					StartWeaponRecoilRotation = FRotator::ZeroRotator;
				}
			}
		}
		else  //We're recovering from recoil
		{
			WeaponRecoilBlend.Update(DeltaSeconds);

			//if done recovering
			if (WeaponRecoilBlend.IsComplete())
			{
				StartWeaponRecoilRotation = FRotator::ZeroRotator;
				DestinationWeaponRecoilRotation = FRotator::ZeroRotator;
			}
		}
	}
}

///////////////////////////////////////
//Radial Weapon Inaccuracy

const float RADIAL_WEAPON_INACCURACY_WALKSPEED_FACTOR = .0005f;
const float RADIAL_WEAPON_INACCURACY_WALKSPEED_RECOVER_FACTOR = .001f;

const float RADIAL_WEAPON_INACCURACY_STUMBLESPEED_FACTOR = .001f;

const float RADIAL_WEAPON_INACCURACY_AIR = .75f;
const float RADIAL_WEAPON_INACCURACY_AIR_RECOVER = .75f;

const float RADIAL_WEAPON_INACCURACY_MAX_RECOVER = 10.f;

const float JUMP_RADIAL_WEAPON_INACCURACY = 1.f;
const float JUMP_RADIAL_WEAPON_INACCURACY_RECOVER = 0.75f;

const float LAND_RADIAL_WEAPON_INACCURACY = 1.f;
const float LAND_RADIAL_WEAPON_INACCURACY_RECOVER = 0.75f;

const float DAMAGE_LAND_RADIAL_WEAPON_INACCURACY_MAX = 3.f;
const float DAMAGE_LAND_RADIAL_WEAPON_INACCURACY_FACTOR = .01f;
const float DAMAGE_LAND_RADIAL_WEAPON_INACCURACY_RECOVER_FACTOR = .005f;

void UWRCharacterHandControlWeapon::ImpartJumpRadialWeaponInaccuracy_Implementation()
{
	ImpartRadialWeaponInaccuracy(JUMP_RADIAL_WEAPON_INACCURACY, JUMP_RADIAL_WEAPON_INACCURACY,
		JUMP_RADIAL_WEAPON_INACCURACY,
		JUMP_RADIAL_WEAPON_INACCURACY_RECOVER * .5f, JUMP_RADIAL_WEAPON_INACCURACY_RECOVER);
}

void UWRCharacterHandControlWeapon::ImpartLandRadialWeaponInaccuracy_Implementation(float FallingDamage)
{
	if (FallingDamage > 0.f)
	{
		float FallingDamageInaccuracyFactor = LAND_RADIAL_WEAPON_INACCURACY + FallingDamage * DAMAGE_LAND_RADIAL_WEAPON_INACCURACY_FACTOR;
		float FallingDamageInaccuracyRecoverFactor = LAND_RADIAL_WEAPON_INACCURACY_RECOVER + FallingDamage * DAMAGE_LAND_RADIAL_WEAPON_INACCURACY_FACTOR;

		ImpartRadialWeaponInaccuracy(FallingDamageInaccuracyFactor, FallingDamageInaccuracyFactor,
			DAMAGE_LAND_RADIAL_WEAPON_INACCURACY_MAX,
			FallingDamageInaccuracyRecoverFactor * .5f, FallingDamageInaccuracyRecoverFactor);
	}
	else
	{
		ImpartRadialWeaponInaccuracy(LAND_RADIAL_WEAPON_INACCURACY, LAND_RADIAL_WEAPON_INACCURACY,
			LAND_RADIAL_WEAPON_INACCURACY,
			LAND_RADIAL_WEAPON_INACCURACY_RECOVER * .5f, LAND_RADIAL_WEAPON_INACCURACY_RECOVER);
	}
}

void UWRCharacterHandControlWeapon::UpdateRadialWeaponInaccuracy_Implementation(float DeltaSeconds)
{
	RadialWeaponInaccuracyFuncParamTime += DeltaSeconds * .05f * (bRadialWeaponInaccuracyFuncParamTimeNegativeDirection ? -1.f : 1.f);

	if (!RadialWeaponInaccuracyBlend.IsComplete())
	{
		//this means we're updating the recoil itself still
		if (RadialWeaponInaccuracyRecoverTime > 0.f)
		{
			float RemainingTime = RadialWeaponInaccuracyBlend.Update(DeltaSeconds);

			//start recovering
			if (RadialWeaponInaccuracyBlend.IsComplete())
			{
				//restart the blend
				RadialWeaponInaccuracyBlend.SetValueRange(RadialWeaponInaccuracyBlend.GetDesiredValue(), 0.f);
				RadialWeaponInaccuracyBlend.SetBlendTime(RadialWeaponInaccuracyRecoverTime);
				RadialWeaponInaccuracyBlend.Reset();

				//update with remaining time
				RadialWeaponInaccuracyBlend.Update(RemainingTime);
			}
		}
		else  //We're recovering from recoil
		{
			RadialWeaponInaccuracyBlend.Update(DeltaSeconds);
		}
	}

	ConstantRadialWeaponInaccuracyBlend.Update(DeltaSeconds);

	AWRCharacter * OwnerCharacter = GetOwnerCharacter();

	if (OwnerCharacter)
	{
		//impart radial weapon inaccuracy based on how the character is currently moving
		if (OwnerCharacter->GetCharacterMovement()->IsMovingOnGround())
		{
			float WalkSpeed = OwnerCharacter->GetVelocity().Size2D();
			float StumbleSpeed = 0.f;

			UWRCharacterMovement * WRMovement = Cast<UWRCharacterMovement>(OwnerCharacter->GetCharacterMovement());

			if (WRMovement)
			{
				StumbleSpeed = WRMovement->GetStumbleVelocity().Size2D();
			}

			float Magnitude = WalkSpeed * RADIAL_WEAPON_INACCURACY_WALKSPEED_FACTOR + StumbleSpeed * RADIAL_WEAPON_INACCURACY_STUMBLESPEED_FACTOR;

			ImpartConstantRadialWeaponInaccuracy(WalkSpeed * RADIAL_WEAPON_INACCURACY_WALKSPEED_FACTOR + StumbleSpeed * RADIAL_WEAPON_INACCURACY_STUMBLESPEED_FACTOR,
				(WalkSpeed + StumbleSpeed) * RADIAL_WEAPON_INACCURACY_WALKSPEED_RECOVER_FACTOR);
		}
		else if (OwnerCharacter->GetCharacterMovement()->IsFalling())
		{
			ImpartConstantRadialWeaponInaccuracy(RADIAL_WEAPON_INACCURACY_AIR, RADIAL_WEAPON_INACCURACY_AIR_RECOVER);
		}
	}

	/*GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Magnitude: %f ConstMagnitude: %f Total: %f"),
	RadialWeaponInaccuracyBlend.GetBlendedValue(),
	ConstantRadialWeaponInaccuracyBlend.GetBlendedValue(),
	GetCurrentRadialWeaponInaccuracyMagnitude()));*/
}

void UWRCharacterHandControlWeapon::ImpartRadialWeaponInaccuracy_Implementation(float AddMin, float AddMax, float TotalMax, float Time, float RecoverTime)
{
	//TODO: make randomness here take a seed to make this deterministic across network games and replays

	KickRadialWeaponInaccuracyFuncParamTime();

	//Limit this so extreme situations can't make this blow up too much
	RecoverTime = FMath::Min(RADIAL_WEAPON_INACCURACY_MAX_RECOVER, RecoverTime);

	float CurrentRadialWeaponInaccuracyMagnitude = RadialWeaponInaccuracyBlend.GetBlendedValue();

	//figure out random desired values based inputs
	float AddValue = FMath::FRandRange(AddMin, AddMax);

	//add on to the current higher value
	float NewDestination = FMath::Max(RadialWeaponInaccuracyBlend.GetDesiredValue(), RadialWeaponInaccuracyBlend.GetBeginValue()) + AddValue;

	//now check if our destination values here don't exceed max input values, leave destination unmodified
	if (RadialWeaponInaccuracyBlend.GetDesiredValue() < TotalMax)
	{
		NewDestination = FMath::Min(NewDestination, TotalMax);
	}

	//compute new speeds
	{
		//we're still animating the recoil itself
		bool bIsAnimatingRecoil = !RadialWeaponInaccuracyBlend.IsComplete() && RadialWeaponInaccuracyRecoverTime > 0.f;

		if ((bIsAnimatingRecoil && Time < RadialWeaponInaccuracyBlend.GetBlendTime()) || !bIsAnimatingRecoil)
		{
			RadialWeaponInaccuracyBlend.SetBlendTime(Time);
		}
	}

	if (RecoverTime > RadialWeaponInaccuracyRecoverTime)
	{
		RadialWeaponInaccuracyRecoverTime = RecoverTime;
	}

	RadialWeaponInaccuracyBlend.SetValueRange(CurrentRadialWeaponInaccuracyMagnitude, NewDestination);

	RadialWeaponInaccuracyBlend.Reset();
}

void UWRCharacterHandControlWeapon::ImpartConstantRadialWeaponInaccuracy_Implementation(float Value, float RecoverTime)
{
	if (Value <= 0.f)
	{
		return;
	}

	bool bNeedsReset = false;
	float ResetToValue = ConstantRadialWeaponInaccuracyBlend.GetBlendedValue();
	float ResetTimeValue = ConstantRadialWeaponInaccuracyBlend.GetBlendTimeRemaining();

	if (Value > ConstantRadialWeaponInaccuracyBlend.GetBlendedValue())
	{
		ResetToValue = Value;
		bNeedsReset = true;
	}

	if (RecoverTime > ConstantRadialWeaponInaccuracyBlend.GetBlendTimeRemaining())
	{
		ResetTimeValue = RecoverTime;
		bNeedsReset = true;
	}

	if (bNeedsReset)
	{
		ConstantRadialWeaponInaccuracyBlend.SetValueRange(ResetToValue, 0.f);
		ConstantRadialWeaponInaccuracyBlend.SetBlendTime(ResetTimeValue);
		ConstantRadialWeaponInaccuracyBlend.Reset();
	}
}

void UWRCharacterHandControlWeapon::KickRadialWeaponInaccuracyFuncParamTime_Implementation()
{
	RadialWeaponInaccuracyFuncParamTime += .05f * (bRadialWeaponInaccuracyFuncParamTimeNegativeDirection ? -1.f : 1.f);

	//TODO: make randomness here take a seed to make this deterministic across network games and replays
	if (FMath::RandRange(0, 5) == 0)
	{
		bRadialWeaponInaccuracyFuncParamTimeNegativeDirection = !bRadialWeaponInaccuracyFuncParamTimeNegativeDirection;
	}
}
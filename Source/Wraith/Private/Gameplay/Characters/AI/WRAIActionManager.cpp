#include "Wraith.h"
#include "WRAIAttackStateInterface.h"
#include "WREquippableState.h"
#include "WRAIActionManager.h"
#include "WRInventoryManager.h"

void AWRAIActionManager::Added_Implementation(UWRInventoryManager * InOwningManager)
{
	Super::Added_Implementation(InOwningManager);

	//This should never happen since it's only supposed to be added to AI
	UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIActionManager named \"%s\" was added to an inventory manager named \"%s\".  This should never happen.  It should only be used for AI."), 
		*GetName(), *InOwningManager->GetName());
}

bool AWRAIActionManager::SetupAndSanityCheck_Implementation()
{
	bool bErrors = false;

	for (int32 StateIndex = 0; StateIndex < AttackStateClass.Num(); ++StateIndex)
	{
		if (AttackStateClass[StateIndex])
		{
			StateManager->StateClasses.Add(AttackStateClass[StateIndex]);
		}
		else
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIActionManager named \"%s\" has an AttackState Class at index %d with a Null class."), *GetName(), StateIndex);
			bErrors = true;
		}
	}

	bErrors |= Super::SetupAndSanityCheck_Implementation();

	return bErrors;
}

void AWRAIActionManager::NotifyActionDone_Implementation()
{

}

///////////////////////////////////////////////
//Attack

UAEState * AWRAIActionManager::TryPerformAttack_Implementation(int32 AttackIndex)
{
	if (AttackStateClass.IsValidIndex(AttackIndex))
	{
		UAEState * State = StateManager->GetStateForClass(AttackStateClass[AttackIndex]);

		if (State && StateManager->AllowInterruptionByState(State))
		{			
			StateManager->ForceGotoState(State);

			return State;
		}
	}

	return NULL;
}

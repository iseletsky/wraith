#include "Wraith.h"
#include "WRAIPotentialTargetInfo.h"

//TODO: for now these are constants, later this should change depending on target and character
static const float BEST_POTENTIAL_TARGET_EXPIRE_TIME = 15.f;
static const float PERCEIVED_TARGET_EXPIRE_TIME = 7.f;

void FWRAIPotentialTargetInfo::DefaultUpdatePotentialTargetInfo(float CurrentWorldTime)
{
	//TODO: reset dead targets
	/*if (CurrentTarget->IsAlive())
	{
		CurrentTarget = NULL;
	}*/

	if (CurrentTarget)
	{
		//if current target hasn't been the best for too long
		if (CurrentBestPotentialTarget)
		{
			if (CurrentTarget == CurrentBestPotentialTarget)
			{
				//TargetNotPerceivedExpireWorldSeconds gets updated below
				TargetNotBestExpireWorldSeconds = CurrentWorldTime + BEST_POTENTIAL_TARGET_EXPIRE_TIME;
			}
			else if (CurrentWorldTime > TargetNotBestExpireWorldSeconds)
			{
				CurrentTarget = CurrentBestPotentialTarget;
				bCurrentTargetPerceived = true;

				//TargetNotPerceivedExpireWorldSeconds gets updated below 
				TargetNotBestExpireWorldSeconds = CurrentWorldTime + BEST_POTENTIAL_TARGET_EXPIRE_TIME;
			}
		}

		if (bCurrentTargetPerceived)
		{
			TargetNotPerceivedExpireWorldSeconds = CurrentWorldTime + PERCEIVED_TARGET_EXPIRE_TIME;
		}
		//if current target hasn't been perceived for too long
		else if (CurrentWorldTime > TargetNotPerceivedExpireWorldSeconds)
		{
			if (CurrentBestPotentialTarget)
			{
				CurrentTarget = CurrentBestPotentialTarget;

				TargetNotPerceivedExpireWorldSeconds = CurrentWorldTime + PERCEIVED_TARGET_EXPIRE_TIME;
				TargetNotBestExpireWorldSeconds = CurrentWorldTime + BEST_POTENTIAL_TARGET_EXPIRE_TIME;
			}
			else
			{
				CurrentTarget = NULL;
			}
		}
	}
	else if (CurrentBestPotentialTarget)
	{
		CurrentTarget = CurrentBestPotentialTarget;
		bCurrentTargetPerceived = true;

		TargetNotPerceivedExpireWorldSeconds = CurrentWorldTime + PERCEIVED_TARGET_EXPIRE_TIME;
		TargetNotBestExpireWorldSeconds = CurrentWorldTime + BEST_POTENTIAL_TARGET_EXPIRE_TIME;
	}
}
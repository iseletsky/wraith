#include "Wraith.h"
#include "WRAIController.h"
#include "WRAIChooseAttackContext.h"
#include "WRAIFindBestPotentialTargetPerceptionCallback.h"

void UWRAIFindBestPotentialTargetPerceptionCallback::OnStartProcessingActors_Implementation(bool bProcessingAll)
{
	Super::OnStartProcessingActors_Implementation(bProcessingAll);

	if (bProcessingAll)
	{
		bBestPotentialTargetAttackable = false;

		//reset status of current target being perceived, it'll get reset back to true if still perceived
		PotentialTargetInfo.bCurrentTargetPerceived = false;
	}
}

void UWRAIFindBestPotentialTargetPerceptionCallback::OnEndProcessingActors_Implementation()
{
	Super::OnEndProcessingActors_Implementation();

	AActor * Current = PotentialTargetInfo.CurrentTarget;
	AActor * Best = PotentialTargetInfo.CurrentBestPotentialTarget;

	bool bCheckBestBecameCurrent = Current != Best;

	PotentialTargetInfo.DefaultUpdatePotentialTargetInfo(GetWorld()->GetTimeSeconds());

	if (bCheckBestBecameCurrent && Current == Best)
	{
		AWRAIController * AIController = Cast<AWRAIController>(GetOuterAActor());

		if (AIController)
		{
			if (AIController->BestPotentialTargetChooseAttackContext)
			{
				AIController->BestPotentialTargetChooseAttackContext->MoveData(AIController->CurrentTargetChooseAttackContext);
			}
			else
			{
				UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" has a NULL BestPotentialTargetChooseAttackContext."),
					*AIController->GetName());
			}			
		}
	}
}

#include "Wraith.h"
#include "WRAILOSBestPotentialTargetPerceptionCallback.h"
#include "WRAIController.h"
#include "WRAIChooseAttackContext.h"
#include "Runtime/AIModule/Classes/Perception/AISense_Sight.h"

void UWRAILOSBestPotentialTargetPerceptionCallback::OnStartProcessingActors_Implementation(bool bProcessingAll)
{
	Super::OnStartProcessingActors_Implementation(bProcessingAll);

	AWRAIController * AIController = Cast<AWRAIController>(GetOuterAActor());

	if (AIController)
	{
		APawn *ControlledPawn = AIController->GetPawn();

		if (ControlledPawn)
		{
			ClosestAttackTargetInLOSDistanceSquared = 0.f;
			MyLocation = ControlledPawn->GetActorLocation();

			if (bProcessingAll)
			{
				//reset best potential if processing all actors so we find the new closest one in line of sight
				PotentialTargetInfo.CurrentBestPotentialTarget = NULL;
			}
			else if(PotentialTargetInfo.CurrentBestPotentialTarget)
			{
				ClosestAttackTargetInLOSDistanceSquared = FVector::DistSquared(MyLocation, PotentialTargetInfo.CurrentBestPotentialTarget->GetActorLocation());
			}
		}
		else
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAILOSBestPotentialTargetPerceptionCallback named \"%s\" has an AIController with no Pawn."),
				*GetName());
		}
	}
	else
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAILOSBestPotentialTargetPerceptionCallback named \"%s\" doesn't have an outer AIController."),
			*GetName());
	}
}

void UWRAILOSBestPotentialTargetPerceptionCallback::OnStartProcessingActor_Implementation(AActor * PerceivedActor)
{
	Super::OnStartProcessingActor_Implementation(PerceivedActor);
}

void UWRAILOSBestPotentialTargetPerceptionCallback::OnEndProcessingActor_Implementation(AActor * PerceivedActor)
{
	Super::OnEndProcessingActor_Implementation(PerceivedActor);
}

void UWRAILOSBestPotentialTargetPerceptionCallback::OnProcessActorStimulus_Implementation(AActor * PerceivedActor, const FAIStimulus& Stimulus)
{
	Super::OnProcessActorStimulus_Implementation(PerceivedActor, Stimulus);

	//if the perceived actor is in line of sight
	if (Stimulus.Type == UAISense::GetSenseID(UAISense_Sight::StaticClass()))
	{
		//check if perceived actor is attackable
		bool bIsAttackable = true;

		AWRAIController * AIController = Cast<AWRAIController>(GetOuterAActor());

		if (AIController)
		{
			APawn *ControlledPawn = AIController->GetPawn();

			if (ControlledPawn)
			{
				if (AIController->CheckingPerceivedTargetChooseAttackContext)
				{
					AIController->CheckingPerceivedTargetChooseAttackContext->SetupPossibleAttacks(ControlledPawn, AIController, PerceivedActor);
										
					bIsAttackable = AIController->CheckingPerceivedTargetChooseAttackContext->PossibleAttacks.Num() > 0;
				}
				else
				{
					UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" has a NULL CheckingPerceivedTargetChooseAttackContext."),
						*AIController->GetName()); 

					bIsAttackable = false;
				}				
			}
		}

		//if current target is one of the perceived actors, mark it as currently perceived
		if (PerceivedActor == PotentialTargetInfo.CurrentTarget)
		{
			PotentialTargetInfo.bCurrentTargetPerceived = true;

			if (AIController->CheckingPerceivedTargetChooseAttackContext)
			{
				AIController->CheckingPerceivedTargetChooseAttackContext->MoveData(AIController->CurrentTargetChooseAttackContext);
			}
		}

		float TargetDistSquared = FVector::DistSquared(MyLocation, PerceivedActor->GetActorLocation());

		//Found a better potential target
		if (!PotentialTargetInfo.CurrentBestPotentialTarget 
			|| (bIsAttackable && !bBestPotentialTargetAttackable) 
			|| (bIsAttackable == bBestPotentialTargetAttackable && TargetDistSquared < ClosestAttackTargetInLOSDistanceSquared))
		{
			bBestPotentialTargetAttackable = bIsAttackable;

			if (AIController->CheckingPerceivedTargetChooseAttackContext)
			{
				AIController->CheckingPerceivedTargetChooseAttackContext->MoveData(AIController->BestPotentialTargetChooseAttackContext);
			}
			else
			{
				UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" has a NULL CheckingPerceivedTargetChooseAttackContext."),
					*AIController->GetName());
			}

			PotentialTargetInfo.CurrentBestPotentialTarget = PerceivedActor;
			ClosestAttackTargetInLOSDistanceSquared = TargetDistSquared;
		}
	}
}
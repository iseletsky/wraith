#include "Wraith.h"
#include "WRAIReportToSquadPerceptionCallback.h"
#include "WRSquadAI.h"
#include "WRAIController.h"
#include "Runtime/AIModule/Classes/Perception/AISense_Sight.h"
#include "Runtime/AIModule/Classes/Perception/AISense_Hearing.h"

void UWRAIReportToSquadPerceptionCallback::OnStartProcessingActor_Implementation(AActor * PerceivedActor)
{
	BestKnownLocationCertainty = 0;
	BestKnownVelocityCertainty = 0;
}

void UWRAIReportToSquadPerceptionCallback::OnEndProcessingActor_Implementation(AActor * PerceivedActor)
{
	AWRAIController * AIController = Cast<AWRAIController>(GetOuterAActor());

	if (AIController && AIController->Squad)
	{
		AIController->Squad->UpdateTargetLastKnown(PerceivedActor, 
			BestKnownLocation, BestKnownVelocity,
			BestKnownLocationCertainty, BestKnownVelocityCertainty);
	}
}

float UWRAIReportToSquadPerceptionCallback::GetStimulusStrengthMultiplierForLocation_Implementation(const FAIStimulus& Stimulus) const
{
	if (Stimulus.Type == UAISense::GetSenseID(UAISense_Sight::StaticClass()))
	{
		return 1.f;
	}
	else if (Stimulus.Type == UAISense::GetSenseID(UAISense_Hearing::StaticClass()))
	{
		return 0.8f;
	}

	return 1.f;
}

float UWRAIReportToSquadPerceptionCallback::GetStimulusStrengthMultiplierForVelocity_Implementation(const FAIStimulus& Stimulus) const
{
	if (Stimulus.Type == UAISense::GetSenseID(UAISense_Sight::StaticClass()))
	{
		return 1.f;
	}
	else if (Stimulus.Type == UAISense::GetSenseID(UAISense_Hearing::StaticClass()))
	{
		return 0.1f;
	}

	return 0.1f;
}

void UWRAIReportToSquadPerceptionCallback::OnProcessActorStimulus_Implementation(AActor * PerceivedActor, const FAIStimulus& Stimulus)
{
	float LocationStimulusStrength = Stimulus.Strength * GetStimulusStrengthMultiplierForLocation(Stimulus);

	if (LocationStimulusStrength > BestKnownLocationCertainty)
	{
		BestKnownLocation = Stimulus.StimulusLocation;	//TODO: Fudge the location a bit based on velocity stimulus strength
		BestKnownLocationCertainty = LocationStimulusStrength;
	}

	FVector PerceivedActorVelocity = PerceivedActor->GetVelocity();
	float VelocityStimulusStrength = Stimulus.Strength * GetStimulusStrengthMultiplierForVelocity(Stimulus);

	if (VelocityStimulusStrength > BestKnownVelocityCertainty)
	{
		BestKnownVelocity = PerceivedActorVelocity;		//TODO: Fudge the velocity a bit based on velocity stimulus strength
		BestKnownVelocityCertainty = VelocityStimulusStrength;
	}
}

#include "Wraith.h"
#include "WRAIPerceptionCallback.h"

void UWRAIPerceptionCallback::OnStartProcessingActors_Implementation(bool bProcessingAll)
{

}

void UWRAIPerceptionCallback::OnEndProcessingActors_Implementation()
{

}

void UWRAIPerceptionCallback::OnStartProcessingActor_Implementation(AActor * PerceivedActor)
{

}

void UWRAIPerceptionCallback::OnEndProcessingActor_Implementation(AActor * PerceivedActor)
{

}

void UWRAIPerceptionCallback::OnProcessActorStimulus_Implementation(AActor * PerceivedActor, const FAIStimulus& Stimulus)
{

}
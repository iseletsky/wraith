#include "Wraith.h"
#include "WRSquadAI.h"
#include "WRAIController.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionComponent.h"

AWRSquadAI::AWRSquadAI(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	DefaultExpireSecondsSinceLastKnown = 30.f;
	bDestroySquadIfEmpty = true;

	//There's no pawn associated with squad AI
	bStopAILogicOnUnposses = false;
	bSetControlRotationFromPawnOrientation = false;
}

void AWRSquadAI::Possess(APawn* InPawn)
{
	UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRSquadAI named \"%s\" attempted to possess pawn \"%s\".  SquadAI should never possess pawns and only exists to manage groups of WRAIControllers."),
		*GetName(),
		*InPawn->GetName());
}

////////////////////////////
//Target

void AWRSquadAI::UpdateTargetStatuses_Implementation()
{
	TArray<const AActor *> TargetsToDelete;

	for (auto& Element : AttackTargets)
	{
		if (!IsTargetInfoValid(Element.Value))
		{
			TargetsToDelete.Add(Element.Key);
		}
	}

	for (const AActor * Target : TargetsToDelete)
	{
		RemoveTarget(Target);
	}
}

void AWRSquadAI::RemoveTarget_Implementation(const AActor * AttackTarget)
{
	AttackTargets.Remove(AttackTarget);
}

bool AWRSquadAI::IsTargetValid_Implementation(const AActor * AttackTarget) const
{
	const FWRTargetInfo * TargetInfo = AttackTargets.Find(AttackTarget);

	return TargetInfo && IsTargetInfoValid(*TargetInfo);
}

bool AWRSquadAI::IsTargetInfoValid_Implementation(const FWRTargetInfo& TargetInfo) const
{
	return TargetInfo.ExpireWorldSeconds > GetWorld()->GetTimeSeconds();
}

const float TARGET_AGE_THESHOLD = .25f;

void AWRSquadAI::UpdateTargetLastKnown_Implementation(AActor * AttackTarget,
	const FVector& Location, const FVector& Velocity,
	float LocationCertainty, float VelocityCertainty)
{
	if (LocationCertainty <= 0.f && VelocityCertainty <= 0.f)
	{
		return;
	}

	if (!AttackTarget)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRSquadAI named \"%s\" attempted to call UpdateTargetLastKnownLocation on a NULL target."),
			*GetName());
		return;
	}

	FWRTargetInfo& TargetInfo = AttackTargets.FindOrAdd(AttackTarget);

	//TODO: for characters get their ExpireSecondsSinceLastKnown multiplier time, so smaller weaker creatures are forgotten about quicker than big ones
	
	float CurrentWorldTime = GetWorld()->GetTimeSeconds();

	//Overwrite the location and velocity if the new one is more certain or the old one is slightly old

	if (LocationCertainty > TargetInfo.LastKnownLocationCertainty || TargetInfo.LastKnownWorldSeconds - CurrentWorldTime > TARGET_AGE_THESHOLD)
	{
		TargetInfo.LastKnownLocation = Location;
		TargetInfo.LastKnownLocationCertainty = LocationCertainty;
	}
	else
	{
		//Otherwise lower the certainty of the current location if getting this new uncertain location
		//This forumla could probably use some work
		TargetInfo.LastKnownLocationCertainty *= (1.f - LocationCertainty);
	}

	if (VelocityCertainty > TargetInfo.LastKnownVelocityCertainty || TargetInfo.LastKnownWorldSeconds - CurrentWorldTime > TARGET_AGE_THESHOLD)
	{
		TargetInfo.LastKnownVelocity = Velocity;
		TargetInfo.LastKnownVelocityCertainty = VelocityCertainty;
	}
	else
	{
		//Otherwise lower the certainty of the current velocity if getting this new uncertain velocity
		//This forumla could probably use some work
		TargetInfo.LastKnownVelocityCertainty *= (1.f - VelocityCertainty);
	} 
		
	TargetInfo.LastKnownWorldSeconds = CurrentWorldTime;
	TargetInfo.ExpireWorldSeconds = CurrentWorldTime + DefaultExpireSecondsSinceLastKnown;
}

bool AWRSquadAI::GetTargetLastKnown_Implementation(const AActor * AttackTarget, FVector& OutLocation, FVector& OutVelocity) const
{
	const FWRTargetInfo * TargetInfo = AttackTargets.Find(AttackTarget);

	if (TargetInfo && IsTargetInfoValid(*TargetInfo))
	{
		OutLocation = TargetInfo->LastKnownLocation;
		OutVelocity = TargetInfo->LastKnownVelocity;

		return true;
	}

	return false;
}

bool AWRSquadAI::IsTargetCurrentlyPerceived_Implementation(const AActor * AttackTarget) const
{
	const FWRTargetInfo * TargetInfo = AttackTargets.Find(AttackTarget);

	if (TargetInfo && IsTargetInfoValid(*TargetInfo))
	{
		return TargetInfo->LastKnownWorldSeconds - GetWorld()->GetTimeSeconds() > TARGET_AGE_THESHOLD;
	}

	return false;
}

AActor * AWRSquadAI::GetClosestPerceivedTargetToLocation_Implementation(const FVector& Location, float& OutDistSquared) const
{
	AActor * Res = NULL;
	float BestDistanceScore = 0.f;
	float CurrentWorldTime = GetWorld()->GetTimeSeconds();

	for (auto& Element : AttackTargets)
	{
		if (IsTargetInfoValid(Element.Value))
		{
			float DistSquared = FVector::DistSquared(Location, Element.Value.LastKnownLocation);
			float TimeSinceKnown = CurrentWorldTime - Element.Value.LastKnownWorldSeconds;

			//adjust this formula if needed
			//makes a target seem less good if it hasn't been perceived in a while even if it's closer than other targets
			//right now it's like adding a distance of 128 units per second not perceived
			float DistanceScore = DistSquared + (TimeSinceKnown * 128.f);

			if (!Res || DistanceScore < BestDistanceScore)
			{
				Res = Element.Key;
				OutDistSquared = DistSquared;
				BestDistanceScore = DistanceScore;
			}
		}
	}

	return Res;
}

////////////////////////////
//Members

void AWRSquadAI::AddSquadMember_Implementation(AWRAIController * Member)
{
	if (Member)
	{
		if (SquadMembers.Find(Member))
		{
			check(Member->Squad == this);

			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRSquadAI named \"%s\" attempted to call AddSquadMember on a member \"%s\" that's already a part of this squad."), 
				*GetName(),
				*Member->GetName());
			return;
		}

		check(Member->Squad != this);

		//Remove from the other squad if part of one
		if (Member->Squad)
		{
			Member->Squad->RemoveSquadMember(Member);
		}

		check(Member->Squad == NULL);

		SquadMembers.Add(Member);
		Member->Squad = this;
	}
}

void AWRSquadAI::RemoveSquadMember_Implementation(AWRAIController * Member)
{
	if (Member)
	{
		if (!SquadMembers.Find(Member))
		{
			check(Member->Squad != this);

			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRSquadAI named \"%s\" attempted to call RemoveSquadMember on a member \"%s\" that's not a part of this squad."),
				*GetName(),
				*Member->GetName());
			return;
		}

		check(Member->Squad == this);

		SquadMembers.Remove(Member);
		Member->Squad = NULL;
	}
}

bool AWRSquadAI::GetSquadMemberMoveLocation_Implementation(AWRAIController * Member, FVector& OutLocation) const
{
	if (Member)
	{
		const FWRSquadMemberInfo * MemberInfo = SquadMembers.Find(Member);

		if (!MemberInfo)
		{
			check(Member->Squad != this);

			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRSquadAI named \"%s\" attempted to call GetSquadMemberMoveLocation on a member \"%s\" that's not a part of this squad."),
				*GetName(),
				*Member->GetName());
			return false;
		}

		check(Member->Squad == this);

		//check if orders are valid
		if (!MemberInfo->bMemberHasValidOrders)
		{
			//TODO: have it so if a threshold of squad members call this within a certain time, squad AI recomputes orders for all members
			//may have to remove const on the function, or const cast
			return false;
		}

		OutLocation = MemberInfo->MoveToLocation;

		return true;
	}

	return false;
}

void AWRSquadAI::SetSquadMemberMoveLocation_Implementation(AWRAIController * Member, const FVector& Location)
{
	if (Member)
	{
		FWRSquadMemberInfo * MemberInfo = SquadMembers.Find(Member);

		if (!MemberInfo)
		{
			check(Member->Squad != this);

			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRSquadAI named \"%s\" attempted to call SetSquadMemberMoveLocation on a member \"%s\" that's not a part of this squad."),
				*GetName(),
				*Member->GetName());
			return;
		}

		check(Member->Squad == this);

		MemberInfo->bMemberHasValidOrders = true;
		MemberInfo->MoveToLocation = Location;
	}
}

AWRAIController * AWRSquadAI::GetClosestSquadMemberToLocation_Implementation(const FVector& Location, float& OutDistSquared) const
{
	AWRAIController * Res = NULL;
	OutDistSquared = 0.f;

	for (auto& Element : SquadMembers)
	{
		//if (SquadMemberValid(Element.Key))		//TODO: check if squad member is alive and all that
		{
			APawn * AIPawn = Element.Key->GetPawn();

			if (AIPawn)
			{
				float DistSquared = FVector::DistSquared(Location, AIPawn->GetActorLocation());

				if (!Res || DistSquared < OutDistSquared)
				{
					Res = Element.Key;
					OutDistSquared = DistSquared;
				}
			}
		}
	}

	return Res;
}
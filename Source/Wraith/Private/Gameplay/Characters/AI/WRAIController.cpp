#include "Wraith.h"
#include "WRAIController.h"
#include "WRCharacter.h"
#include "WRAIActionManager.h"
#include "WRSquadAI.h"
#include "WRGameplayStatics.h"
#include "WRAIChooseAttackContext.h"
#include "AEStateManager.h"
#include "WRAIAttackStateInterface.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionComponent.h"
#include "WRAIReportToSquadPerceptionCallback.h"
#include "WRAILOSBestPotentialTargetPerceptionCallback.h"

AWRAIController::AWRAIController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//SetPerceptionComponent(*ObjectInitializer.CreateDefaultSubobject<UAIPerceptionComponent>(this, TEXT("PerceptionComponent")));

	FocusControlRotationInterpSpeed = 10.f;

	ReportToSquadPerceptionCallbackClass = UWRAIReportToSquadPerceptionCallback::StaticClass();
	FindBestPotentialTargetPerceptionCallbackClass = UWRAILOSBestPotentialTargetPerceptionCallback::StaticClass();
}

void AWRAIController::BeginPlay()
{
	Super::BeginPlay();

	InitializePerceptionCallbacks();
	InitializeChooseAttackContexts();
}

void AWRAIController::ActorsPerceptionUpdated(const TArray<AActor *>& UpdatedActors)
{
	Super::ActorsPerceptionUpdated(UpdatedActors);

	ProcessSpecificPerceivedActors(UpdatedActors);
}

void AWRAIController::UpdateControlRotation(float DeltaTime, bool bUpdatePawn)
{
	//Basically completely overrides the behavior with a slightly different implementation
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		FRotator CurrentControlRotation = GetControlRotation();
		FRotator NewControlRotation(CurrentControlRotation);

		// Look toward focus
		const FVector FocalPoint = GetFocalPoint();
		if (FAISystem::IsValidLocation(FocalPoint))
		{
			NewControlRotation = (FocalPoint - MyPawn->GetPawnViewLocation()).Rotation();
		}
		else if (bSetControlRotationFromPawnOrientation)
		{
			NewControlRotation = MyPawn->GetActorRotation();
		}

		if (FocusControlRotationInterpSpeed > 0.f && !CurrentControlRotation.Equals(NewControlRotation, 5.f))
		{
			NewControlRotation = FMath::RInterpTo(CurrentControlRotation, NewControlRotation, DeltaTime, FocusControlRotationInterpSpeed);
		}

		SetControlRotation(NewControlRotation);

		if (bUpdatePawn)
		{
			const FRotator CurrentPawnRotation = MyPawn->GetActorRotation();

			if (CurrentPawnRotation.Equals(NewControlRotation, 1e-3f) == false)
			{
				MyPawn->FaceRotation(NewControlRotation, DeltaTime);
			}
		}
	}
}

///////////////////////////////////////
//Team
FGenericTeamId AWRAIController::GetGenericTeamId() const
{
	if (Squad)
	{
		return Squad->GetGenericTeamId();
	}
	else
	{
		return Super::GetGenericTeamId();
	}
}

///////////////////////////////////////
//Squad

void AWRAIController::SpawnSquadForSelf_Implementation(TSubclassOf<AWRSquadAI> SquadClass)
{
	AWRSquadAI * NewSquad = GetWorld()->SpawnActor<AWRSquadAI>(SquadClass);
	
	if (NewSquad)
	{
		NewSquad->SetGenericTeamId(GetGenericTeamId());
		NewSquad->AddSquadMember(this);
	}
}

////////////////////////////
//Perception

void AWRAIController::InitializePerceptionCallbacks_Implementation()
{
	ReportToSquadPerceptionCallback = Cast<UWRAIReportToSquadPerceptionCallback>(InitializePerceptionCallback(ReportToSquadPerceptionCallbackClass));
	FindBestPotentialTargetPerceptionCallback = Cast<UWRAIFindBestPotentialTargetPerceptionCallback>(InitializePerceptionCallback(FindBestPotentialTargetPerceptionCallbackClass));
}

UWRAIPerceptionCallback * AWRAIController::InitializePerceptionCallback_Implementation(TSubclassOf<UWRAIPerceptionCallback> CallbackClass)
{
	UWRAIPerceptionCallback * Callback = NewObject<UWRAIPerceptionCallback>(this, CallbackClass);

	PerceptionCallbackInstances.Add(Callback);
	return Callback;
}

void AWRAIController::ProcessAllPerceivedActors_Implementation()
{
	UAIPerceptionComponent * PerceptionComp = GetAIPerceptionComponent();

	if (!PerceptionComp)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" attempted to call ProcessPerceivedActors but it doesn't have a valid AI perception component."),
			*GetName());
		return;
	}

	TArray<AActor *> PerceivedActors;

	PerceptionComp->GetKnownPerceivedActors(NULL, PerceivedActors);

	ProcessSpecificPerceivedActors(PerceivedActors, true);
}

void AWRAIController::ProcessSpecificPerceivedActors_Implementation(const TArray<AActor *>& Actors, bool bProcessingAll = false)
{
	UAIPerceptionComponent * PerceptionComp = GetAIPerceptionComponent();
	
	if (!PerceptionComp)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" attempted to call ProcessSpecificPerceivedActors but it doesn't have a valid AI perception component."),
			*GetName());
		return;
	}
	
	for (UWRAIPerceptionCallback * PerceptionCallback : PerceptionCallbackInstances)
	{
		PerceptionCallback->OnStartProcessingActors(bProcessingAll);

		for (AActor * AttackTarget : Actors)
		{
			FActorPerceptionBlueprintInfo PerceptionInfo;

			if (PerceptionComp->GetActorsPerception(AttackTarget, PerceptionInfo) && PerceptionInfo.bIsHostile)
			{
				PerceptionCallback->OnStartProcessingActor(AttackTarget);

				for (const FAIStimulus& Stimulus : PerceptionInfo.LastSensedStimuli)
				{
					if (Stimulus.IsValid() && Stimulus.IsActive())
					{
						PerceptionCallback->OnProcessActorStimulus(AttackTarget, Stimulus);
					}
				}

				PerceptionCallback->OnEndProcessingActor(AttackTarget);
			}
		}

		PerceptionCallback->OnEndProcessingActors();
	}
}

////////////////////////////
//Target

AActor * AWRAIController::GetCurrentTarget_Implementation() const
{
	if (!FindBestPotentialTargetPerceptionCallback)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" attempted to call GetCurrentTarget but it doesn't have a valid FindBestPotentialTargetPerceptionCallback."),
			*GetName());

		return NULL;
	}

	return FindBestPotentialTargetPerceptionCallback->PotentialTargetInfo.CurrentTarget;
}

bool AWRAIController::IsCurrentTargetPerceived_Implementation() const
{
	if (!FindBestPotentialTargetPerceptionCallback)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" attempted to call IsCurrentTargetPerceived but it doesn't have a valid FindBestPotentialTargetPerceptionCallback."),
			*GetName());

		return NULL;
	}

	return FindBestPotentialTargetPerceptionCallback->PotentialTargetInfo.bCurrentTargetPerceived;
}

AActor * AWRAIController::GetBestPotentialTarget_Implementation() const
{
	if (!FindBestPotentialTargetPerceptionCallback)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" attempted to call GetBestPotentialTarget but it doesn't have a valid FindBestPotentialTargetPerceptionCallback."),
			*GetName());

		return NULL;
	}

	return FindBestPotentialTargetPerceptionCallback->PotentialTargetInfo.CurrentBestPotentialTarget;
}

////////////////////////////
//Attack

void AWRAIController::InitializeChooseAttackContexts_Implementation()
{
	if (ChooseAttackContextClass)
	{
		CurrentTargetChooseAttackContext = NewObject<UWRAIChooseAttackContext>(this, ChooseAttackContextClass);
		CurrentTargetChooseAttackContext->SetupPrerequisiteChecks();

		BestPotentialTargetChooseAttackContext = NewObject<UWRAIChooseAttackContext>(this, ChooseAttackContextClass);
		BestPotentialTargetChooseAttackContext->SetupPrerequisiteChecks();

		CheckingPerceivedTargetChooseAttackContext = NewObject<UWRAIChooseAttackContext>(this, ChooseAttackContextClass);
		CheckingPerceivedTargetChooseAttackContext->SetupPrerequisiteChecks();
	}
	else
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" has no ChooseAttackContextClass."),
			*GetName());
	}
}

int32 AWRAIController::ChooseAttackForCurrentTarget_Implementation()
{
	if (!CurrentTargetChooseAttackContext) {
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" called ChooseAttackForCurrentTarget but has a NULL CurrentTargetChooseAttackContext."),
			*GetName());
		return -1;
	}

	AActor * CurrentTarget = GetCurrentTarget();

	if (!CurrentTarget)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" called ChooseAttackForCurrentTarget but has no current target, AI should not have attempted this call."),
			*GetName());
		return -1;
	}

	return CurrentTargetChooseAttackContext->ChooseAttack(GetPawn(), this, CurrentTarget);
}

UAEState * AWRAIController::TryAttackCurrentTarget_Implementation(int32 AttackNumber)
{
	AWRCharacter * ControlledPawn = Cast<AWRCharacter>(GetPawn());

	if (ControlledPawn)
	{
		if (ControlledPawn->AIActionManager)
		{
			return ControlledPawn->AIActionManager->TryPerformAttack(AttackNumber);
		}
		else
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" called TryAttackCurrentTarget but the controlled pawn has a NULL AIActionManager."),
				*GetName());
		}
	}
	else
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIController named \"%s\" called TryAttackCurrentTarget but has no controlled pawn, AI should not have attempted this call."),
			*GetName());
	}

	return NULL;
}
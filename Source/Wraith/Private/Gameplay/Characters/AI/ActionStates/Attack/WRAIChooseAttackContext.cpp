#include "Wraith.h"
#include "WRCharacter.h"
#include "WRAIAttackStateInterface.h"
#include "WRAIChooseAttackPrerequisiteCheck.h"
#include "WRAIActionManager.h"
#include "WRAIChooseAttackContext.h"
#include "WREquippableState.h"

void UWRAIChooseAttackContext::SetupPrerequisiteChecks_Implementation()
{
	check(PrerequisiteChecks.Num() == 0);

	for (int32 CheckInd = 0; CheckInd < PrerequisiteCheckClasses.Num(); ++CheckInd)
	{		
		if (PrerequisiteCheckClasses[CheckInd])
		{
			PrerequisiteChecks.Add(NewObject<UWRAIChooseAttackPrerequisiteCheck>(this, PrerequisiteCheckClasses[CheckInd]));
		}
		else
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIChooseAttackContext named \"%s\" has a NULL Prerequisite Check class at Index %d."),
				*GetName(),
				CheckInd);
		}
	}
}

void UWRAIChooseAttackContext::RunPrerequisiteChecks_Implementation(AActor * Attacker, AAIController * AttackerAIController, AActor * Target)
{
	for (int32 CheckInd = 0; CheckInd < PrerequisiteChecks.Num(); ++CheckInd)
	{
		PrerequisiteChecks[CheckInd]->ProcessTarget(Attacker, AttackerAIController, Target);
	}
}

void UWRAIChooseAttackContext::SetupPossibleAttacks_Implementation(AActor * Attacker, AAIController * AttackerAIController, AActor * Target)
{
	RunPrerequisiteChecks(Attacker, AttackerAIController, Target);

	PossibleAttacks.Empty(PossibleAttacks.Num());
	AWRCharacter * Character = Cast<AWRCharacter>(Attacker);
	
	if (Character && Character->AIActionManager)
	{
		for (int32 AttackInd = 0; AttackInd < Character->AIActionManager->AttackStateClass.Num(); ++AttackInd)
		{
			UAEState * State = Character->AIActionManager->StateManager->GetStateForClass(Character->AIActionManager->AttackStateClass[AttackInd]);

			if (State)
			{
				if (State->GetClass()->ImplementsInterface(UWRAIAttackStateInterface::StaticClass()))
				{
					if (IWRAIAttackStateInterface::Execute_IsTargetAttackable(State, this, Target))
					{
						PossibleAttacks.Add(AttackInd);
					}
				}
				else
				{
					PossibleAttacks.Add(AttackInd);
				}
			}
		}
	}
}

int32 UWRAIChooseAttackContext::ChooseAttack_Implementation(AActor * Attacker, AAIController * AttackerAIController, AActor * Target)
{
	if (PossibleAttacks.Num() == 0)
	{
		return -1;
	}

	//TODO: take a random stream or seed or something to make this deterministic

	TArray<int32> AttackList;
	AWRCharacter * Character = Cast<AWRCharacter>(Attacker);

	for (int32 AttackInd = 0; AttackInd < PossibleAttacks.Num(); ++AttackInd)
	{
		UAEState * State = Character->AIActionManager->StateManager->GetStateForClass(Character->AIActionManager->AttackStateClass[PossibleAttacks[AttackInd]]);

		if (State && Character->AIActionManager->StateManager->AllowInterruptionByState(State))
		{
			if (State->GetClass()->ImplementsInterface(UWRAIAttackStateInterface::StaticClass()))
			{
				float RandomRoll = FMath::FRand();
				float Probability = IWRAIAttackStateInterface::Execute_GetAttackProbability(State, this, Target);

				if (RandomRoll <= Probability)
				{
					AttackList.Add(PossibleAttacks[AttackInd]);
				}
			}
			else
			{
				AttackList.Add(PossibleAttacks[AttackInd]);
			}
		}
	}
	
	if (AttackList.Num() == 0)
	{
		return -1;
	}

	int32 RandomRoll = FMath::RandHelper(AttackList.Num());

	return AttackList[RandomRoll];
}

bool UWRAIChooseAttackContext::MoveData_Implementation(UWRAIChooseAttackContext * Destination)
{
	if (Destination)
	{
		if (Destination->GetClass() == GetClass())
		{
			Destination->PossibleAttacks = PossibleAttacks;

			for (int32 CheckInd = 0; CheckInd < PrerequisiteChecks.Num(); ++CheckInd)
			{
				if (!PrerequisiteChecks[CheckInd]->MoveData(Destination->PrerequisiteChecks[CheckInd]))
				{
					UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIChooseAttackContext named \"%s\" tried to MoveData on one of its prerequisite checks at index %d and failed."),
						*GetName(),
						CheckInd);

					return false;
				}
			}
		}
		else
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIChooseAttackContext named \"%s\" with class \"%s\" passed MoveData Destination named \"%s\" with class \"%s\".  They need to be the same class."),
				*GetName(),
				*GetClass()->GetName(),
				*Destination->GetName(),
				*Destination->GetClass()->GetName());

			return false;
		}		
	}
	else
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIChooseAttackContext named \"%s\" passed NULL MoveData Destination."),
			*GetName());

		return false;
	}

	PossibleAttacks.Empty();
	return true;
}
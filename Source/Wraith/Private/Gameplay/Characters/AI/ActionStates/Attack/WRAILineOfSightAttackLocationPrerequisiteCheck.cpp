#include "Wraith.h"
#include "WRAILineOfSightAttackLocationPrerequisiteCheck.h"
#include "AITypes.h"

void UWRAILineOfSightAttackLocationPrerequisiteCheck::ProcessTarget_Implementation(AActor * Attacker, AAIController * AttackerAIController, AActor * Target)
{
	//TODO Have a way for targets to have multiple attackable points and try each of them, for now just go with target location

	AttackRelativeToTargetLocation = FVector::ZeroVector;
		//= FAISystem::InvalidLocation;
}

bool UWRAILineOfSightAttackLocationPrerequisiteCheck::MoveData_Implementation(UWRAIChooseAttackPrerequisiteCheck * Destination)
{
	if (Super::MoveData_Implementation(Destination))
	{
		Cast<UWRAILineOfSightAttackLocationPrerequisiteCheck>(Destination)->AttackRelativeToTargetLocation = AttackRelativeToTargetLocation;
		return true;
	}
	else
	{
		return false;
	}
}
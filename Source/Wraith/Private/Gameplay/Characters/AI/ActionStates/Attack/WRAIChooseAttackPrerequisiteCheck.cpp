#include "Wraith.h"
#include "WRAIChooseAttackPrerequisiteCheck.h"

void UWRAIChooseAttackPrerequisiteCheck::ProcessTarget_Implementation(AActor * Attacker, AAIController * AttackerAIController, AActor * Target)
{

}

bool UWRAIChooseAttackPrerequisiteCheck::MoveData_Implementation(UWRAIChooseAttackPrerequisiteCheck * Destination)
{
	if (Destination)
	{
		if (Destination->GetClass() == GetClass())
		{
			return true;
		}
		else
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIChooseAttackPrerequisiteCheck named \"%s\" with class \"%s\" passed MoveData Destination named \"%s\" with class \"%s\".  They need to be the same class."),
				*GetName(),
				*GetClass()->GetName(),
				*Destination->GetName(),
				*Destination->GetClass()->GetName());

			return false;
		}
	}
	else
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRAIChooseAttackPrerequisiteCheck named \"%s\" passed NULL MoveData Destination."),
			*GetName());

		return false;
	}
}
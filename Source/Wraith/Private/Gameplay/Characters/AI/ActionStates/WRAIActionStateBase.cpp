#include "Wraith.h"
#include "WRAIActionStateBase.h"

void UWRAIActionStateBase::OnInterrupt_Implementation(UAEState * InterruptingState)
{
	Super::OnInterrupt_Implementation(InterruptingState);
	TaskFinishAbort();
}

void UWRAIActionStateBase::OnEnd_Implementation(UAEState * NextState)
{
	Super::OnEnd_Implementation(NextState);

	PassTaskToState(NextState);
}

void UWRAIActionStateBase::TryGotoIdle_Implementation()
{
	//automatically call finishExecute and reset so OnEnd doesn't try to pass the AITask onto the Idle state
	TaskFinishExecute();

	Super::TryGotoIdle_Implementation();
}

void UWRAIActionStateBase::SetupAITask_Implementation(UBTTask_BlueprintBase * inAITask)
{
	AITask = inAITask;
}

void UWRAIActionStateBase::PassTaskToState_Implementation(UAEState * NextState)
{
	//Pass on the AI task to the next state
	if (AITask)
	{
		UWRAIActionStateBase * AIActionState = Cast<UWRAIActionStateBase>(NextState);

		if (AIActionState)
		{
			AIActionState->SetupAITask(AITask);
			ResetAITask();
		}
		else
		{
			TaskFinishExecute();
		}
	}
}

void UWRAIActionStateBase::ResetAITask_Implementation()
{
	AITask = NULL;
}

void UWRAIActionStateBase::TaskFinishExecute_Implementation(bool bSuccess)
{
	if (AITask)
	{
		AITask->FinishExecute(bSuccess);
		ResetAITask();
	}
}

void UWRAIActionStateBase::TaskFinishAbort_Implementation()
{
	if (AITask)
	{
		AITask->FinishAbort();
		ResetAITask();
	}
}
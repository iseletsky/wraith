#include "Wraith.h"
#include "WRDismemberableSkeletalMeshComponent.h"

UWRDismemberableSkeletalMeshComponent::UWRDismemberableSkeletalMeshComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//so we don't have ugly seams
	bLightAttachmentsAsGroup = true;
}

void UWRDismemberableSkeletalMeshComponent::BeginPlay()
{
	if (DismemberableParts.Num() && SanityCheck())
	{
		SetupInitialMeshes();
	}

	Super::BeginPlay();
}

void UWRDismemberableSkeletalMeshComponent::DetachBodyPart(FName BodyPartName, const FVector& Impulse, const FVector& HitLocation)
{
	FWRDismemberablePart * BodyPart = DismemberableParts.Find(BodyPartName);

	if (!BodyPart)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRDismemberableSkeletalMeshComponent named \"%s\" had DetachBodyPart called with an invalid Body part name \"%s\"."),
			*GetName(),
			*BodyPartName.ToString());

		return;
	}

	FWRDismemberablePart * ParentPart = BodyPart->ParentPartName == NAME_None ? NULL : DismemberableParts.Find(BodyPart->ParentPartName);

	if(ParentPart)
	{
		if (BodyPart->CurrentState == WRDismemberablePartState::ATTACHED_TO_PARENT)
		{
			BodyPart->CurrentState == WRDismemberablePartState::DETACHED_FROM_PARENT;

			BodyPart->ParentStump.SpawnStump(this,
				ParentPart->SkeletalMeshInstance ? ParentPart->SkeletalMeshInstance : this);

			BodyPart->MyStump.SpawnStump(this,
				ParentPart->SkeletalMeshInstance ? ParentPart->SkeletalMeshInstance : this);

			if (BodyPart->ParentJointConnectionMeshInstance)
			{
				BodyPart->ParentJointConnectionMeshInstance->DestroyComponent();
			}

			//TODO: spawn effects and play sounds

			//SetAllBodiesBelowSimulatePhysics(BodyPart->BoneName, true, true);

			BreakConstraint(Impulse, HitLocation, BodyPart->BoneName);
		}
	}
	else
	{
		check(BodyPartName == RootPart);
		DetachBodyPart(RootPartDetachFallbackPart, Impulse, HitLocation);
	}
}

void UWRDismemberableSkeletalMeshComponent::DestroyBodyPart(FName BodyPartName)
{
	FWRDismemberablePart * BodyPart = DismemberableParts.Find(BodyPartName);

	if (!BodyPart)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRDismemberableSkeletalMeshComponent named \"%s\" had DestroyBodyPart called with an invalid Body part name \"%s\"."),
			*GetName(),
			*BodyPartName.ToString());

		return;
	}

	//If this body part is already destroyed, abort
	if (BodyPart->CurrentState == WRDismemberablePartState::DESTROYED)
	{
		return;
	}

	std::function<FWRDismemberablePart * (const FName& CurrentPartName)> DestroyChildPartsHelper =
		[this, &DestroyChildPartsHelper](const FName& CurrentPartName)->FWRDismemberablePart * {
		FWRDismemberablePart& CurrentPart = DismemberableParts[CurrentPartName];

		//If this body part is destroyed, assume all children already destroyed
		if (CurrentPart.CurrentState == WRDismemberablePartState::DESTROYED)
		{
			return NULL;
		}

		//first traverse children
		for (int32 ChildInd = 0; ChildInd < CurrentPart.ChildPartNames.Num(); ++ChildInd)
		{
			FWRDismemberablePart * ChildPart = DestroyChildPartsHelper(CurrentPart.ChildPartNames[ChildInd]);

			//destroy the child's stump mesh from this mesh to that mesh if any
			if (ChildPart && ChildPart->ParentStump.StumpMeshInstance)
			{
				ChildPart->ParentStump.StumpMeshInstance->DestroyComponent();
			}
		}

		//then handle ourselves
		CurrentPart.CurrentState = WRDismemberablePartState::DESTROYED;

		//delete our meshes
		if (CurrentPart.ParentJointConnectionMeshInstance)
		{
			CurrentPart.ParentJointConnectionMeshInstance->DestroyComponent();
		}

		if (CurrentPart.MyStump.StumpMeshInstance)
		{
			CurrentPart.MyStump.StumpMeshInstance->DestroyComponent();
		}

		//TODO: spawn effects and play sounds

		return &CurrentPart;
	};

	DestroyChildPartsHelper(BodyPartName);

	//Calling hide bone by name will effectively make all children disappear now
	HideBoneByName(BodyPart->BoneName, EPhysBodyOp::PBO_Term);

	if (BodyPart->ParentPartName != NAME_None)
	{
		FWRDismemberablePart * ParentPart = DismemberableParts.Find(BodyPart->ParentPartName);

		//tell parent to spawn its stump
		if (ParentPart)
		{
			BodyPart->ParentStump.SpawnStump(this,
				ParentPart->SkeletalMeshInstance ? ParentPart->SkeletalMeshInstance : this);

			//TODO: spawn effects
		}
	}
}

FName UWRDismemberableSkeletalMeshComponent::GetBodyPartNameForBone(FName BoneName)
{
	FName * Res = BoneNameToPartName.Find(BoneName);

	if (Res)
	{
		return *Res;
	}

	//if didn't find it, traverse from this bone up the bone heirarchy until a body part is found
	FName ParentBoneName = GetParentBone(BoneName);

	if (ParentBoneName == NAME_None)
	{
		return NAME_None;
	}
	else
	{
		return GetBodyPartNameForBone(ParentBoneName);
	}
}

USkeletalMeshComponent * UWRDismemberableSkeletalMeshComponent::SpawnSkeletalMesh(USkeletalMesh * Mesh, USceneComponent * Parent)
{
	USkeletalMeshComponent * NewComponent = NewObject<USkeletalMeshComponent>(this, USkeletalMeshComponent::StaticClass(), NAME_None, RF_NoFlags, NULL);
	NewComponent->SetSkeletalMesh(Mesh, false);
	NewComponent->SetMasterPoseComponent(this);

	ConfigureSpawningMeshComponent(NewComponent, NAME_None, Parent);

	return NewComponent;
}

UStaticMeshComponent * UWRDismemberableSkeletalMeshComponent::SpawnStaticMesh(UStaticMesh * Mesh, FName SocketName, USceneComponent * Parent)
{
	UStaticMeshComponent * NewComponent = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass(), NAME_None, RF_NoFlags, NULL);
	NewComponent->SetStaticMesh(Mesh);

	ConfigureSpawningMeshComponent(NewComponent, SocketName, Parent);

	return NewComponent;
}

void UWRDismemberableSkeletalMeshComponent::ConfigureSpawningMeshComponent(UMeshComponent * NewComponent, FName SocketName, USceneComponent * Parent)
{
	NewComponent->bCastDynamicShadow = bCastDynamicShadow;
	NewComponent->bAffectDynamicIndirectLighting = bAffectDynamicIndirectLighting;

	ConfigureSpawningSceneComponent(NewComponent, SocketName, Parent);
}

void UWRDismemberableSkeletalMeshComponent::ConfigureSpawningSceneComponent(UPrimitiveComponent * NewComponent, FName SocketName, USceneComponent * Parent)
{
	NewComponent->SetupAttachment(Parent ? Parent : this, SocketName);
	NewComponent->bUseAttachParentBound = true;
	
	NewComponent->bGenerateOverlapEvents = false;
	NewComponent->SetCanEverAffectNavigation(false);
	
	NewComponent->RegisterComponent();
}

bool UWRDismemberableSkeletalMeshComponent::SanityCheck()
{
	bool bAnyErrors = false;
	
	for (auto& Pair : DismemberableParts)
	{
		const FName& PartName = Pair.Key;
		FWRDismemberablePart& DismemberablePart = Pair.Value;
				
		FName ParentBoneToCheck = NAME_None;

		if (DismemberablePart.ParentPartName == NAME_None)
		{
			//See if this is root and we already have root
			if (RootPart == NAME_None)
			{
				RootPart = PartName;
			}
			else
			{
				UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRDismemberableSkeletalMeshComponent named \"%s\" has another PartName named \"%s\" with a NULL parent name.  Another part \"%s\" has already been found with a NULL parent name and is set as the root of the parts heirarchy."), 
					*GetName(), 
					*PartName.ToString(),
					*RootPart.ToString());
				bAnyErrors = true;
			}
		}
		else
		{			
			FWRDismemberablePart * ParentDismemberablePart = DismemberableParts.Find(DismemberablePart.ParentPartName);

			if (ParentDismemberablePart)
			{
				ParentDismemberablePart->ChildPartNames.Add(PartName);
				
				ParentBoneToCheck = ParentDismemberablePart->BoneName;
			}
			else
			{
				UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRDismemberableSkeletalMeshComponent named \"%s\" has a part named \"%s\" with a parent name \"%s\" that isn't found in the DismemberableParts map."),
					*GetName(),
					*PartName.ToString(),
					*DismemberablePart.ParentPartName.ToString());
				bAnyErrors = true;
			}
		}

		auto AddBoneToLookup = [this, 
			&bAnyErrors, 
			&PartName, 
			&ParentBoneToCheck](const FName& BoneName)->void
		{
			int32 BoneIndex = GetBoneIndex(BoneName);

			if (BoneIndex == INDEX_NONE)
			{
				UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRDismemberableSkeletalMeshComponent named \"%s\" has a part named \"%s\" with an associated Bone \"%s\" name that doesn't exist in the skeleton."),
					*GetName(),
					*PartName.ToString(),
					*BoneName.ToString());
				bAnyErrors = true;
			}

			//check if the bone of this body part is also a descendant of the bone of the parent body part in the skeleton for consistency
			if (ParentBoneToCheck != NAME_None && !BoneIsChildOf(BoneName, ParentBoneToCheck))
			{
				UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRDismemberableSkeletalMeshComponent named \"%s\" has a part named \"%s\" with an associated Bone \"%s\" that isn't a child of expected bone \"%s\"."),
					*GetName(),
					*PartName.ToString(),
					*BoneName.ToString(),
					*ParentBoneToCheck.ToString());
				bAnyErrors = true;
			}

			FName * ExistingAssociatedPartName = BoneNameToPartName.Find(BoneName);

			if (ExistingAssociatedPartName)
			{
				UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRDismemberableSkeletalMeshComponent named \"%s\" has a part named \"%s\" with an associated Bone \"%s\" but that bone has already been associated with another part \"%s\"."),
					*GetName(),
					*PartName.ToString(),
					*BoneName.ToString(),
					*ExistingAssociatedPartName->ToString());
				bAnyErrors = true;
			}

			BoneNameToPartName.Emplace(BoneName, PartName);
		};

		//Add this part's bones to the reverse bone lookup
		if (DismemberablePart.BoneName != NAME_None)
		{
			AddBoneToLookup(DismemberablePart.BoneName);
		}
		else
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRDismemberableSkeletalMeshComponent named \"%s\" has a part named \"%s\" with an unspecified bone name."),
				*GetName(),
				*PartName.ToString(),
				*DismemberablePart.ParentPartName.ToString());
			bAnyErrors = true;
		}

		for (int32 AdditionalBoneInd = 0; AdditionalBoneInd < DismemberablePart.AdditionalBoneNames.Num(); ++AdditionalBoneInd)
		{
			AddBoneToLookup(DismemberablePart.AdditionalBoneNames[AdditionalBoneInd]);
		}
	}

	if (RootPart == NAME_None)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRDismemberableSkeletalMeshComponent named \"%s\" has no root body part.  Exactly one body part must have no Parent Name."),
			*GetName());

		bAnyErrors = true;
	}

	return !bAnyErrors;
}

void UWRDismemberableSkeletalMeshComponent::SetupInitialMeshes()
{
	std::function<void(const FName& CurrentPartName, FWRDismemberablePart * ParentPart)> SetupInitialMeshesHelper =
		[this, &SetupInitialMeshesHelper](const FName& CurrentPartName, FWRDismemberablePart * ParentPart)->void {
		FWRDismemberablePart& CurrentPart = DismemberableParts[CurrentPartName];

		//don't spawn this part or any children if 0 health body part
		if (CurrentPart.CurrentState == WRDismemberablePartState::DESTROYED)
		{
			//hide bone so the whole child heirarchy is disabled
			HideBoneByName(CurrentPart.BoneName, EPhysBodyOp::PBO_Term);

			//tell parent to spawn its stump
			if (ParentPart)
			{
				CurrentPart.ParentStump.SpawnStump(this, 
					ParentPart->SkeletalMeshInstance ? ParentPart->SkeletalMeshInstance : this);
			}

			return;
		}
		else if (ParentPart)
		{
			if (CurrentPart.Health <= 0 || CurrentPart.CurrentState == WRDismemberablePartState::DETACHED_FROM_PARENT)
			{
				CurrentPart.CurrentState == WRDismemberablePartState::DETACHED_FROM_PARENT;

				CurrentPart.ParentStump.SpawnStump(this,
					ParentPart->SkeletalMeshInstance ? ParentPart->SkeletalMeshInstance : this);

				CurrentPart.MyStump.SpawnStump(this,
					ParentPart->SkeletalMeshInstance ? ParentPart->SkeletalMeshInstance : this);

				SetAllBodiesBelowSimulatePhysics(CurrentPart.BoneName, true, true);

				BreakConstraint(FVector::ZeroVector, FVector::ZeroVector, CurrentPart.BoneName);
			}
			else //Body part is attached, spawn the joint connection mesh
			{
				if (CurrentPart.ParentJointConnectionMesh)
				{
					CurrentPart.ParentJointConnectionMeshInstance = SpawnSkeletalMesh(CurrentPart.ParentJointConnectionMesh);
				}
			}
		}

		//if there's a skeletal mesh, spawn it and attach it
		if (CurrentPart.SkeletalMesh)
		{
			CurrentPart.SkeletalMeshInstance = SpawnSkeletalMesh(CurrentPart.SkeletalMesh);
		}

		for (int32 ChildInd = 0; ChildInd < CurrentPart.ChildPartNames.Num(); ++ChildInd)
		{
			SetupInitialMeshesHelper(CurrentPart.ChildPartNames[ChildInd], &CurrentPart);
		}
	};

	SetupInitialMeshesHelper(RootPart, NULL);
}

void FWRDismemberableStumpInfo::SpawnStump(UWRDismemberableSkeletalMeshComponent * OwningDismemberable, USceneComponent * AttachParent)
{
	if (StumpMeshes.Num() > 0)
	{
		StumpMeshInstance = OwningDismemberable->SpawnStaticMesh(StumpMeshes[StumpMeshes.Num() == 1 ? 0 : FMath::RandHelper(StumpMeshes.Num())], 
			StumpMeshAttachSocketName, AttachParent);
	}
}
#include "Wraith.h"
#include "WRCharacter.h"
#include "WRCharacterMovement.h"
#include "WRCharacterStanceData.h"
#include "WRGameplayStatics.h"
#include "WRInventoryManager.h"
#include "WRAIActionManager.h"
#include "Runtime/AIModule/Classes/AISystem.h"

AWRCharacter::AWRCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UWRCharacterMovement>(ACharacter::CharacterMovementComponentName))
{
	//Collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.f);

	//Mesh
	GetMesh()->RelativeLocation = FVector(0.f, 0.f, -96.f);
	GetMesh()->RelativeRotation = FRotator(0.f, -90.f, 0.f);
		
	//Cameras
	Camera1P = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Camera1P"));
	Camera1P->SetupAttachment(GetCapsuleComponent());
	Camera1P->bUsePawnControlRotation = true;

	SpringArm3P = ObjectInitializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("SpringArm3P"));
	SpringArm3P->SetupAttachment(GetCapsuleComponent());
	SpringArm3P->bUsePawnControlRotation = true;

	Camera3P = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Camera3P"));
	Camera3P->SetupAttachment(SpringArm3P);
	Camera3P->bAutoActivate = false;
		
	//Inventory Manager
	InventoryManager = ObjectInitializer.CreateDefaultSubobject<UWRInventoryManager>(this, TEXT("InventoryManager"));
	
	BaseEyeHeight = 60.f;
	CrouchedEyeHeight = 56.f;

	MaxSafeFallSpeed = 800.f;
	MinRandomStumbleLateralToVerticalFallSpeedThreshold = .1f;

	//set blend object curve types
	CrouchEyeHeightFromFeetBlend.SetBlendOption(EAlphaBlendOption::HermiteCubic);
	CrouchEyeHeightFromFeetBlend.SetBlendTime(.2f);
		
	CurrentMinViewPitch = -90.f;
	StandingMinViewPitch = -80.f;
	SprintingMinViewPitch = -70.f;
	CrouchingMinViewPitch = -70.f;

	bRelativeEyesTransformNeedsUpdate = true;

	TeamIdEnum = WRTeam::NONE;
	TeamIdNum = WRTeam::NONE;
}

void AWRCharacter::Tick(float DeltaSeconds)
{
	bRelativeEyesTransformNeedsUpdate = true;

	Super::Tick(DeltaSeconds);
	
	UpdateMinViewPitch(DeltaSeconds);
	UpdateCrouchEyeHeightFromFeet(DeltaSeconds);
	
	UpdateTargetUsable();

	if (TargetUsable)
	{
		UE_LOG_ON_SCREEN(WR, Verbose, 1.f, FColor::Blue, TEXT("Usable text: %s"), *(IWRUsableInterface::Execute_UseActionText(TargetUsable.GetObject()).ToString()));
	}
}

void AWRCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	ComputeEyeHeightsFromFeet();
	InitAIActionManager();
}

void AWRCharacter::SpawnDefaultController()
{	
	FGenericTeamId MyTeamId = GetGenericTeamId();

	Super::SpawnDefaultController();

	//Assign AI controller to the team of the pawn
	IGenericTeamAgentInterface * ControllerTeam = Cast<IGenericTeamAgentInterface>(GetController());

	if (ControllerTeam)
	{
		ControllerTeam->SetGenericTeamId(MyTeamId);
	}
}

///////////////////////////////////////
//AI

bool AWRCharacter::CanBeSeenFrom(const FVector& ObserverLocation, FVector& OutSeenLocation, int32& NumberOfLoSChecksPerformed, float& OutSightStrength, const AActor* IgnoreActor) const
{
	static const FName NAME_AILineOfSight = FName(TEXT("WRCharacterAILineOfSight"));

	int32 LosChecksPerformed = 0;
	
	//Find a point on the character that may be in line of sight
	ECollisionChannel SightCollisionChannel = ECC_Visibility;

	UAISystem * AISystem = Cast<UAISystem>(GetWorld()->GetAISystem());

	if (AISystem)
	{
		SightCollisionChannel = AISystem->DefaultSightCollisionChannel;
	}
	
	auto CanBeSeenFromFunc = [this, 
		&LosChecksPerformed,
		&ObserverLocation,
		&OutSeenLocation,
		&OutSightStrength,
		SightCollisionChannel,
		IgnoreActor](const FVector& TargetLocation)
	{
		++LosChecksPerformed;

		FHitResult HitResult;

		bool bHit = GetWorld()->LineTraceSingleByChannel(HitResult, ObserverLocation, TargetLocation, 
			SightCollisionChannel, 
			FCollisionQueryParams(NAME_AILineOfSight, true, IgnoreActor));

		if (bHit == false || (HitResult.Actor.IsValid() && HitResult.Actor->IsOwnedBy(this)))
		{
			OutSeenLocation = TargetLocation;
			OutSightStrength = 1;
			return true;
		}

		return false;
	};
		
	bool bCanBeSeenFrom = false;

	//TODO: eventually check sides as well by projecting out relative to the ray to center direction

	//Middle
	FVector CheckLoc = GetActorLocation();
	bCanBeSeenFrom = CanBeSeenFromFunc(CheckLoc);

	if (!bCanBeSeenFrom)
	{
		float DistFromCenter = GetCapsuleComponent()->GetScaledCapsuleHalfHeight() * .9f;

		CheckLoc.Z += DistFromCenter;

		//Top
		bCanBeSeenFrom = CanBeSeenFromFunc(CheckLoc);

		if (!bCanBeSeenFrom)
		{
			CheckLoc.Z -= DistFromCenter * 2.f;

			//Bottom
			bCanBeSeenFrom = CanBeSeenFromFunc(CheckLoc);
		}
	}

	NumberOfLoSChecksPerformed = LosChecksPerformed;
	return bCanBeSeenFrom;
}

void AWRCharacter::InitAIActionManager_Implementation()
{
	if (AIActionManagerClass)
	{
		FActorSpawnParameters SpawnParams;

		SpawnParams.Owner = this;

		if (SpawnParams.Owner)
		{
			SpawnParams.Instigator = this;
		}

		AIActionManager = GetWorld()->SpawnActor<AWRAIActionManager>(AIActionManagerClass, SpawnParams);
	}
}

///////////////////////////////////////
//Team

FGenericTeamId AWRCharacter::GetGenericTeamId() const
{
	IGenericTeamAgentInterface * ControllerTeam = Cast<IGenericTeamAgentInterface>(GetController());

	if (ControllerTeam)
	{
		return ControllerTeam->GetGenericTeamId();
	}
	else
	{
		if (!bGenericTeamIdInitialized)
		{
			//I'm doing this because of lazy loading, forgive me!
			AWRCharacter * self = const_cast<AWRCharacter *>(this);

			if (TeamIdEnum == WRTeam::INVALID)
			{
				self->TeamID = UWRGameplayStatics::GetTeamForId(TeamIdNum);
			}
			else
			{
				self->TeamID = UWRGameplayStatics::GetTeamForId(TeamIdEnum);
			}

			self->bGenericTeamIdInitialized = true;
		}

		return TeamID;
	}
}

///////////////////////////////////////
//Input

void AWRCharacter::SetupPlayerInputComponent(UInputComponent * PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	SetupMovementInput(PlayerInputComponent);
	SetupUsableInput(PlayerInputComponent);
}

void AWRCharacter::DisableInput(class APlayerController * PlayerController)
{
	//TODO: also call this when pawn is unposessed
	OnLoseControl();
}

void AWRCharacter::OnLoseControl_Implementation()
{
	//it makes sense to turn off sprint here if it was held down or toggled
	UnSprintInput();
}

///////////////////////////////////////
//Movement

void AWRCharacter::SetupMovementInput_Implementation(UInputComponent * PlayerInputComponent)
{
	PlayerInputComponent->BindAxis("MoveForward", this, &AWRCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AWRCharacter::MoveRight);
	PlayerInputComponent->BindAxis("MoveUp", this, &AWRCharacter::MoveUp);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AWRCharacter::Jump);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AWRCharacter::CrouchInput);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &AWRCharacter::UnCrouchInput);
	PlayerInputComponent->BindAction("ToggleCrouch", IE_Pressed, this, &AWRCharacter::ToggleCrouch);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AWRCharacter::SprintInput);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AWRCharacter::UnSprintInput);
	PlayerInputComponent->BindAction("ToggleSprint", IE_Pressed, this, &AWRCharacter::ToggleSprint);
}

void AWRCharacter::OnStartCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust)
{
	float CurrentEyeHeight = CrouchEyeHeightFromFeetBlend.GetBlendedValue();

	Super::OnStartCrouch(HalfHeightAdjust, ScaledHalfHeightAdjust);

	CrouchEyeHeightFromFeetBlend.SetValueRange(CurrentEyeHeight, CrouchEyeHeightFromFeetBlend.GetBlendedValue());
	CrouchEyeHeightFromFeetBlend.Reset();
	BaseEyeHeight = CrouchEyeHeightFromFeetBlend.GetBlendedValue() - GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
}

void AWRCharacter::OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust)
{
	float CurrentEyeHeight = CrouchEyeHeightFromFeetBlend.GetBlendedValue();

	Super::OnEndCrouch(HalfHeightAdjust, ScaledHalfHeightAdjust);

	CrouchEyeHeightFromFeetBlend.SetValueRange(CurrentEyeHeight, CrouchEyeHeightFromFeetBlend.GetBlendedValue());
	CrouchEyeHeightFromFeetBlend.Reset();
	BaseEyeHeight = CrouchEyeHeightFromFeetBlend.GetBlendedValue() - GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
}

void AWRCharacter::FaceRotation(FRotator NewControlRotation, float DeltaTime)
{
	bRelativeEyesTransformNeedsUpdate = true;
	
	//turn in place animation

	//if not standing still, always make yaw equal rotation
	bool bStandingOnGround = false;
	float InterpSpeed = 3.f;
	float LastYaw = GetActorRotation().Yaw;
	NewControlRotation.Yaw = FMath::UnwindDegrees(NewControlRotation.Yaw);

	FRotator CurrentRot(GetActorRotation());

	if (GetCharacterMovement()->IsMovingOnGround())
	{
		if (GetVelocity().SizeSquared2D() == 0.f)
		{
			bStandingOnGround = true;

			float CurrentYawOffset = GetAimOffsets().Yaw;

			//make character turn in place if twisted too far
			if (CurrentYawOffset > GetRightMaxAimYawOffset())
			{
				CurrentRot.Yaw = FMath::UnwindDegrees(CurrentRot.Yaw + CurrentYawOffset - GetRightMaxAimYawOffset());
				DesiredYaw = NewControlRotation.Yaw;
			}
			else if (-CurrentYawOffset > GetLeftMaxAimYawOffset())
			{
				CurrentRot.Yaw = FMath::UnwindDegrees(CurrentRot.Yaw + CurrentYawOffset + GetLeftMaxAimYawOffset());
				DesiredYaw = NewControlRotation.Yaw;
			}
		}
	}

	if (!bStandingOnGround)
	{
		DesiredYaw = NewControlRotation.Yaw;
		InterpSpeed = 10.f;
	}

	FRotator DesiredRot(CurrentRot);
	DesiredRot.Yaw = DesiredYaw;

	if (!FMath::IsNearlyEqual(LastYaw, DesiredYaw))
	{
		SetActorRotation(FMath::RInterpTo(CurrentRot, DesiredRot, DeltaTime, InterpSpeed));

		//first find shortest delta angle
		YawSpeed = FMath::FindDeltaAngleDegrees(LastYaw, GetActorRotation().Yaw) / DeltaTime;
	}
	else
	{
		SetActorRotation(DesiredRot);
		YawSpeed = 0.f;
	}
}

void AWRCharacter::MoveForward_Implementation(float Value)
{
	if (Value != 0.0f)
	{
		// find out which way is forward
		const FRotator Rotation = GetControlRotation();
		FRotator YawRotation = FRotator(0, Rotation.Yaw, 0);

		// add movement in forward direction
		AddMovementInput(FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X), Value);
	}
}

void AWRCharacter::MoveRight_Implementation(float Value)
{
	if (Value != 0.0f)
	{
		// find out which way is right
		const FRotator Rotation = GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// add movement in right direction
		AddMovementInput(FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y), Value);
	}
}

void AWRCharacter::MoveUp_Implementation(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in up direction
		AddMovementInput(FVector(0.f, 0.f, 1.f), Value);
	}
}

float AWRCharacter::GetCrouchAmount_Implementation() const
{
	//TODO: When VR and other things are added, take into account the height of the camera above the floor relative to the calibrated character height
	//For now just returning the crouch head height curve
	float CurrentEyeHeightFromFeet = CrouchEyeHeightFromFeetBlend.GetBlendedValue();

	return 1.f - (CurrentEyeHeightFromFeet - GetCrouchedEyeHeightFromFeet()) / (GetStandingEyeHeightFromFeet() - GetCrouchedEyeHeightFromFeet());
}

void AWRCharacter::ProcessFallingDamage_Implementation(const FHitResult& Hit, float FallingSpeed)
{
	UWRCharacterMovement * WRCharacterMovement = Cast<UWRCharacterMovement>(GetMovementComponent());

	if (WRCharacterMovement)
	{
		float FallingDamage = 0.f;

		if (FallingSpeed < -1.f * MaxSafeFallSpeed)
		{
			if (FallingSpeed < -1.f * MaxSafeFallSpeed)
			{
				FallingDamage = -150.f * (FallingSpeed + MaxSafeFallSpeed) / MaxSafeFallSpeed;
				FallingDamage -= WRCharacterMovement->FallingDamageReduction(FallingDamage, Hit);				
			}
		}

		TakeFallingDamage(Hit, FallingSpeed, FallingDamage);
	}
}

void AWRCharacter::TakeFallingDamage_Implementation(const FHitResult& Hit, float FallingSpeed, float FallingDamage)
{
	UWRCharacterMovement * WRCharacterMovement = Cast<UWRCharacterMovement>(GetMovementComponent());

	if (Role == ROLE_Authority && FallingDamage >= 1.0f)
	{
		//FUTPointDamageEvent DamageEvent(FallingDamage, Hit, GetCharacterMovement()->Velocity.GetSafeNormal(), UUTDmgType_Fell::StaticClass());
		//TakeDamage(DamageEvent.Damage, DamageEvent, Controller, this);
	}

	//dampening
	float SafeFallSpeedFactor = FallingSpeed / (-1.f * MaxSafeFallSpeed);

	if (WRCharacterMovement)
	{
		WRCharacterMovement->JumpHeightDampening.AddEffect(SafeFallSpeedFactor * .5f, SafeFallSpeedFactor * .3f);
		WRCharacterMovement->MovementSpeedDampening.AddEffect(SafeFallSpeedFactor, SafeFallSpeedFactor);

		//stumble
		if (FallingDamage > 0.f)
		{
			FVector2D FallDirection = FVector2D(WRCharacterMovement->Velocity);

			//random stumble direction if lateral fall speed is too low
			if (FMath::Abs(FallDirection.Size() / FallingSpeed) < MinRandomStumbleLateralToVerticalFallSpeedThreshold)
			{
				FallDirection.X = FMath::FRand() * 2.f - 1.f;
				FallDirection.Y = FMath::FRand() * 2.f - 1.f;
			}

			FallDirection.Normalize();
			WRCharacterMovement->AddStumble(FallDirection * FallingDamage * 10.f, FallingDamage * 10.f);
		}
	}
}

void AWRCharacter::Landed_Implementation(const FHitResult& Hit)
{
	if (!bClientUpdating)
	{
		ProcessFallingDamage(Hit, GetCharacterMovement()->Velocity.Z);
	}

	Super::Landed(Hit);
}

void AWRCharacter::Jumped_Implementation()
{
	bJustJumped = true;
}

void AWRCharacter::SetWantsToSprint_Implementation(bool bWantsToSprint)
{
	UWRCharacterMovement * WRMovement = Cast<UWRCharacterMovement>(GetMovementComponent());

	if (WRMovement)
	{
		WRMovement->bWantsToSprint = bWantsToSprint;
	}
}

bool AWRCharacter::GetWantsToSprint_Implementation()
{
	UWRCharacterMovement * WRMovement = Cast<UWRCharacterMovement>(GetMovementComponent());

	if (WRMovement)
	{
		return WRMovement->bWantsToSprint;
	}

	return false;
}

bool AWRCharacter::GetIsSprinting_Implementation() const
{
	UWRCharacterMovement * WRMovement = Cast<UWRCharacterMovement>(GetMovementComponent());

	if (WRMovement)
	{
		return WRMovement->bIsSprinting;
	}

	return false;
}

void AWRCharacter::CrouchInput_Implementation()
{
	Crouch(false);
}

void AWRCharacter::UnCrouchInput_Implementation()
{
	UnCrouch(false);
}

void AWRCharacter::ToggleCrouch_Implementation()
{
	bIsCrouched ? UnCrouchInput() : CrouchInput();
}

void AWRCharacter::SprintInput_Implementation()
{
	SetWantsToSprint(true);
}

void AWRCharacter::UnSprintInput_Implementation()
{
	SetWantsToSprint(false);
}

void AWRCharacter::ToggleSprint_Implementation()
{
	SetWantsToSprint(!GetWantsToSprint());
}

///////////////////////////////////////
//Use button

void AWRCharacter::SetupUsableInput_Implementation(UInputComponent * PlayerInputComponent)
{
	PlayerInputComponent->BindAction("Use", IE_Pressed, this, &AWRCharacter::Use);
}

void AWRCharacter::UpdateTargetUsable_Implementation()
{
	//force reset the current target usable if it's destroyed or requires the player to be aiming or if it's behind a wall, it'll be reset below anyway        
	if (TargetUsable
		&& (TargetUsable.GetObject()->IsPendingKill()
			|| IWRUsableInterface::Execute_RequiresAim(TargetUsable.GetObject())
			|| GetWorld()->LineTraceTestByChannel(GetActorLocation(), Cast<AActor>(TargetUsable.GetObject())->GetActorLocation(), ECC_Pawn, FCollisionQueryParams(), WorldResponseParams)))
	{
		TargetUsable = TScriptInterface<IWRUsableInterface>();		
	}

	if (UsableObjects.Num())
	{
		FTransform EyesWorldTransform = GetEyesWorldTransform();
		
		float FarthestUsableDistSquared = 0.f;
		float ClosestUsableDistSquared = std::numeric_limits<float>::max();
		TScriptInterface<IWRUsableInterface> ClosestUsableNoAim = TScriptInterface<IWRUsableInterface>();

		//find the farthest Usable in the overlapping usables array so we know how long to make the trace
		//also find the closest usable that isn't behind a wall and doesn't require to be aimed at
		for (int32 PickupInd = 0; PickupInd < UsableObjects.Num(); ++PickupInd)
		{
			AActor * UsableActor = Cast<AActor>(UsableObjects[PickupInd].GetObject());

			if (UsableActor)
			{
				float UsableDistSquared = FVector::DistSquared(EyesWorldTransform.GetLocation(), UsableActor->GetActorLocation());

				if (UsableDistSquared > FarthestUsableDistSquared)
				{
					FarthestUsableDistSquared = UsableDistSquared;
				}

				//if it's a non aim usable and not behind a wall
				if (!IWRUsableInterface::Execute_RequiresAim(UsableObjects[PickupInd].GetObject())
					&& UsableDistSquared < ClosestUsableDistSquared
					&& !GetWorld()->LineTraceTestByChannel(GetActorLocation(), UsableActor->GetActorLocation(), ECC_Pawn, FCollisionQueryParams(), WorldResponseParams))
				{
					ClosestUsableDistSquared = UsableDistSquared;
					ClosestUsableNoAim = UsableObjects[PickupInd];
				}
			}
		}

		FVector UseTargetTraceEnd = EyesWorldTransform.GetLocation() + EyesWorldTransform.GetRotation().GetForwardVector() * FMath::Sqrt(FarthestUsableDistSquared);

		FHitResult HitResult;
		GetWorld()->LineTraceSingleByChannel(HitResult, EyesWorldTransform.GetLocation(), UseTargetTraceEnd, COLLISION_TRACE_USABLE, FCollisionQueryParams(), FCollisionResponseParams::DefaultResponseParam);

		//was the result a usable object?
		IWRUsableInterface * HitUsable = Cast<IWRUsableInterface>(HitResult.Actor.Get());

		if (HitUsable)
		{
			TargetUsable.SetInterface(HitUsable);
			TargetUsable.SetObject(HitResult.Actor.Get());
		}
		//For debug drawing traces
		/*	DrawDebugLine(
		GetWorld(),
		EyeLocation,
		UseTargetTraceEnd,
		FColor(0, 255, 0),
		false, 1, 0,
		1
		);
		}
		else if (!HitResult.Actor.Get())
		{
		DrawDebugLine(
		GetWorld(),
		EyeLocation,
		UseTargetTraceEnd,
		FColor(255, 0, 0),
		false, 1, 0,
		1
		);
		}
		else
		{
		DrawDebugLine(
		GetWorld(),
		EyeLocation,
		UseTargetTraceEnd,
		FColor(0, 0, 255),
		false, 1, 0,
		1
		);
		}*/

		//if not aiming at one, and there's a null target, select the closest
		if (!TargetUsable)
		{
			TargetUsable = ClosestUsableNoAim;
		}
	}

	//get player controller hud and set its usable display
	/*APlayerController * PC = Cast<AWRPlayerController>(Controller);

	if (PC)
	{
	AWRHUD * WRHUD = Cast<AWRHUD>(PC->GetHUD());

	if (WRHUD)
	{
	WRHUD->SetUsableObject(TargetUsable);
	}
	}*/
}

void AWRCharacter::Use_Implementation()
{
	if (TargetUsable)
	{
		IWRUsableInterface::Execute_Use(TargetUsable.GetObject(), this);
	}
}

void AWRCharacter::AddUsable_Implementation(const TScriptInterface<IWRUsableInterface>& Usable)
{
	UsableObjects.AddUnique(Usable);
}

void AWRCharacter::RemoveUsable_Implementation(const TScriptInterface<IWRUsableInterface>& Usable)
{
	if (TargetUsable.GetObject() == Usable.GetObject())
	{
		TargetUsable = TScriptInterface<IWRUsableInterface>();
	}

	UsableObjects.Remove(Usable);
}

///////////////////////////////////////
//Camera

bool AWRCharacter::Setup1P_Implementation()
{
	if (!IsFirstPerson())
	{
		check(Camera3P->bIsActive);

		Camera3P->SetActive(false);
		Camera1P->SetActive(true);

		return true;		
	}

	return false;
}

bool AWRCharacter::Setup3P_Implementation()
{
	if (IsFirstPerson())
	{
		check(Camera1P->bIsActive);

		Camera1P->SetActive(false);
		Camera3P->SetActive(true);

		return true;
	}

	return false;
}

bool AWRCharacter::IsFirstPerson_Implementation() const
{
	return Camera1P->bIsActive;
}

void AWRCharacter::RepositionCameraComponents_Implementation()
{
	Camera1P->SetRelativeLocation(GetRelativeEyesTransform().GetLocation());
	SpringArm3P->SetRelativeLocation(GetRelativeEyesTransform().GetLocation());
}

FVector AWRCharacter::GetCrosshairLocation_Implementation(ECollisionChannel TraceChannel, float MaxDistance, bool bDebugDraw) const
{
	FHitResult HitResult;

	UWRGameplayStatics::PerformSingleHitscanTrace(HitResult,
		GetEyesWorldTransform(),
		false,
		TraceChannel,		
		this,
		MaxDistance,
		NULL,
		bDebugDraw);

	return HitResult.Location;
}

FVector AWRCharacter::GetSafeActorSpawnLocation_Implementation(const FVector& SpawnedActorWorldLocation,
	ECollisionChannel TraceChannel,
	float SpawnedActorRadius,
	bool bDebugDraw)
{
	return UAEGameplayStatics::GetSafeActorSpawnLocation(
		GetCapsuleComponent(), 
		GetPawnViewLocation(), 
		SpawnedActorWorldLocation,
		TraceChannel,
		SpawnedActorRadius,
		bDebugDraw);
}

FTransform AWRCharacter::GetEyesWorldTransform_Implementation() const
{
	FVector ActorEyesPos;
	FRotator ActorEyesRot;

	GetActorEyesViewPoint(ActorEyesPos, ActorEyesRot);

	return FTransform(ActorEyesRot, ActorEyesPos);
}
	

//////////////////////////////////////
//Camera Animation

void AWRCharacter::RecalculateBaseEyeHeight()
{
	bRelativeEyesTransformNeedsUpdate = true;

	Super::RecalculateBaseEyeHeight();
	
	float NewValue = BaseEyeHeight + GetCapsuleComponent()->GetScaledCapsuleHalfHeight();

	CrouchEyeHeightFromFeetBlend.SetValueRange(NewValue, NewValue);
	CrouchEyeHeightFromFeetBlend.Reset();
}

FVector AWRCharacter::GetPawnViewLocation() const
{
	return GetHeadWorldTransform().GetLocation();
}

const float HEAD_MAX_YAW = 80.f;
const float HEAD_MAX_PITCH = 45.f;
const float HEAD_MIN_PITCH = -45.f;
const float HEAD_MAX_ROLL = 30.f;

void AWRCharacter::UpdateRelativeEyesTransform_Implementation()
{
	bRelativeEyesTransformNeedsUpdate = false;

	BaseEyeHeight = CrouchEyeHeightFromFeetBlend.GetBlendedValue() - GetCapsuleComponent()->GetScaledCapsuleHalfHeight();

	RelativeEyesTransform = FTransform::Identity;

	FRotator AimOffsets = GetAimOffsets();

	//figure out the offset based on pitch
	FVector OffsetVector;

	//TODO: remove a bunch of this code now that I don't need it because the camera is always fixed to an origin
	UWRCharacterStanceData * StanceData = GetCharacterStance();

	if (StanceData)
	{
		float Alpha = FMath::Abs(AimOffsets.Pitch) / 90.f;
		Alpha = FMath::Pow(Alpha, 1.5f);

		if (AimOffsets.Pitch < 0.f)
		{
			OffsetVector = FMath::Lerp(
				FVector(StanceData->HeadStraightForwardOffset, 0.f, StanceData->HeadStraightVerticalOffset),
				FVector(StanceData->HeadDownForwardOffset, 0.f, StanceData->HeadDownVerticalOffset),
				Alpha);
		}
		else
		{
			OffsetVector = FMath::Lerp(
				FVector(StanceData->HeadStraightForwardOffset, 0.f, StanceData->HeadStraightVerticalOffset),
				FVector(StanceData->HeadUpForwardOffset, 0.f, StanceData->HeadUpVerticalOffset),
				Alpha);
		}
	}
	else
	{
		OffsetVector = FVector::ZeroVector;
	}

	//offset vector by the crouch amount
	if (bIsCrouched && StanceData)
	{
		OffsetVector += FVector(0.f, GetCharacterStance()->HeadCrouchForwardOffset, GetCharacterStance()->HeadCrouchVerticalOffset) * GetCrouchAmount();
	}

	//rotate offset vector by AimOffsetYaw
	OffsetVector = FRotator(0.f, AimOffsets.Yaw, 0.f).RotateVector(OffsetVector);

	OffsetVector = FVector::ZeroVector;

	FRotator FinalRotation = AimOffsets;

	FinalRotation.Yaw = FMath::ClampAngle(FinalRotation.Yaw, -HEAD_MAX_YAW, HEAD_MAX_YAW);
	FinalRotation.Pitch = FMath::ClampAngle(FinalRotation.Pitch, HEAD_MIN_PITCH, HEAD_MAX_PITCH);
	FinalRotation.Roll = FMath::ClampAngle(FinalRotation.Roll, -HEAD_MAX_ROLL, HEAD_MAX_ROLL);
	
	RelativeEyesTransform.SetRotation(FinalRotation.Quaternion());
	RelativeEyesTransform.SetLocation(OffsetVector + FVector(0.f, 0.f, BaseEyeHeight));
	
	//DrawDebugPoint(
	//	GetWorld(),
	//	GetMesh()->ComponentToWorld.GetLocation(),
	//	10,  					//size
	//	FColor::Red,
	//	true
	//	);

	//DrawDebugPoint(
	//	GetWorld(),
	//	GetPawnViewLocation(),
	//	10,  					//size
	//	FColor::Blue,
	//	true
	//	);
}

float AWRCharacter::GetMinViewPitch_Implementation() const
{
	return CurrentMinViewPitch;
}

void AWRCharacter::UpdateMinViewPitch_Implementation(float DeltaTime)
{
	CurrentMinViewPitch = FMath::FInterpTo(CurrentMinViewPitch, GetDesiredMinViewPitch(), DeltaTime, 10.f);
}

float AWRCharacter::GetDesiredMinViewPitch_Implementation() const
{
	return FMath::Lerp(GetIsSprinting()
			? SprintingMinViewPitch
			: StandingMinViewPitch,
		CrouchingMinViewPitch,
		GetCrouchAmount());
}

///////////////////////////////////////
//Crouch Height Animation

void AWRCharacter::UpdateCrouchEyeHeightFromFeet_Implementation(float DeltaSeconds)
{
	CrouchEyeHeightFromFeetBlend.Update(DeltaSeconds);
	bRelativeEyesTransformNeedsUpdate = true;
}

void AWRCharacter::ComputeEyeHeightsFromFeet_Implementation()
{
	StandingEyeHeightFromFeet = GetDefault<AWRCharacter>(GetClass())->BaseEyeHeight + GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	CrouchedEyeHeightFromFeet = CrouchedEyeHeight + GetCharacterMovement()->CrouchedHalfHeight;
}

///////////////////////////////////////
//Stance Control

UWRCharacterStanceData * AWRCharacter::GetCharacterStance_Implementation() const
{
	return DefaultCharacterStance;
}

FRotator AWRCharacter::GetAimOffsets_Implementation() const
{
	FVector AimDirWS = GetControlRotation().Vector();
	FVector AimDirLS = ActorToWorld().InverseTransformVectorNoScale(AimDirWS);
	FRotator AimRotLS = AimDirLS.Rotation();

	return AimRotLS;
}

///////////////////////////////////////
//Turn In Place Animation

float AWRCharacter::GetLeftMaxAimYawOffset_Implementation() const
{
	//TODO: get this from the weapon
	return 45.f;
}

float AWRCharacter::GetRightMaxAimYawOffset_Implementation() const
{
	//TODO: get this from the weapon
	return 45.f;
}

///////////////////////////////////////
//Head Control

FTransform AWRCharacter::GetHeadWorldTransform_Implementation() const
{
	//TODO: (ilselets) When using VR, derive this from the headset position
	
	//TODO: Refactor so there's a function that returns Actor Eyes World Transform and this code is shared by GetPawnViewLocation
	FTransform WorldTransform = GetRelativeEyesTransform() * GetActorTransform();

	return WorldTransform;
}

/////////////////////////////////////// 
//Projectile Shooting

FTransform AWRCharacter::GetSafeShootingSpawnWorldTransform_Implementation(const FVector& SpawnedTraceOriginWorldLocation,
	ECollisionChannel TraceChannel,
	float SpawnedActorRadius,
	float MaxTestDist,
	bool bDebugDraw)
{
	return UAEGameplayStatics::GetSafeLineTraceSpawnTransform(
		GetCapsuleComponent(),
		GetPawnViewLocation(),
		SpawnedTraceOriginWorldLocation,
		GetCrosshairLocation(),
		COLLISION_TRACE_WEAPON,
		SpawnedActorRadius,
		MaxTestDist,
		bDebugDraw);
}

#include "Wraith.h"
#include "WRCharacter.h"
#include "WRPickup.h"

const float AWRPickup::DEFAULT_TOUCH_PICKUP_RADIUS = 48.f;

const float AWRPickup::DEFAULT_USE_PICKUP_RADIUS = 150.f;

AWRPickup::AWRPickup(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bUsable = true;
	PickupRadius = DEFAULT_USE_PICKUP_RADIUS;
}

void AWRPickup::PostInitializeComponents()
{
	//find components tagged with PickupWorld so they can be automatically set up with PickupWorld physics
	TArray<UActorComponent *> PickupWorldComponents = GetComponentsByTag(UPrimitiveComponent::StaticClass(), FName(TEXT("PickupWorld")));

	for (int32 CompInd = 0; CompInd < PickupWorldComponents.Num(); ++CompInd)
	{
		UPrimitiveComponent * Comp = Cast<UPrimitiveComponent>(PickupWorldComponents[CompInd]);

		Comp->SetSimulatePhysics(true);
		Comp->SetIsReplicated(true);
		Comp->SetCollisionProfileName(FName(TEXT("PickupWorld")));
		Comp->bGenerateOverlapEvents = false;
		Comp->SetCanEverAffectNavigation(false);
	}

	Super::PostInitializeComponents();

	//if there were no physics components found to attach pickup collision to, attach it to the root, or make it be the root
	if (!PickupCollision)
	{
		SetupPickupCollision(GetRootComponent());
	}
}

void AWRPickup::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	TryGiveToOverlappingCharacters();
}

void AWRPickup::LevelOrderComponentTraverse_Implementation(USceneComponent * Component)
{
	Super::LevelOrderComponentTraverse_Implementation(Component);

	//attach pickup collision to topmost physics component
	if (!PickupCollision && GetPhysicsComponents().Num())
	{
		SetupPickupCollision(GetPhysicsComponents()[0]);
	}
}

void AWRPickup::SetupPickupCollision_Implementation(USceneComponent * Parent)
{
	if (PickupCollision)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("Pickup named %s had SetupPickupCollision called when it already has Pickup Collision initialized."), *GetName());
		return;
	}

	PickupCollision = NewObject<USphereComponent>(this, USphereComponent::StaticClass(), NAME_None, RF_NoFlags, NULL);
	PickupCollision->InitSphereRadius(PickupRadius);
	PickupCollision->BodyInstance.SetCollisionProfileName(TEXT("Pickup"));
	PickupCollision->OnComponentBeginOverlap.AddDynamic(this, &AWRPickup::OnOverlapBegin);
	PickupCollision->OnComponentEndOverlap.AddDynamic(this, &AWRPickup::OnOverlapEnd);

	if (Parent)
	{
		PickupCollision->SetupAttachment(Parent);
	}
	else if(GetRootComponent())
	{
		PickupCollision->SetupAttachment(PickupCollision);
	}
	else
	{
		SetRootComponent(PickupCollision);
	}

	PickupCollision->RegisterComponent();
}

void AWRPickup::OnOverlapBegin_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult)
{
	APawn* Pawn = Cast<APawn>(OtherActor);

	if (Pawn && !Pawn->bTearOff)
	{
		if (bUsable)
		{
			AWRCharacter * WRChar = Cast<AWRCharacter>(OtherActor);

			if (WRChar)
			{
				UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("Pickup named %s added usable."), *GetName());
				WRChar->AddUsable(this);
			}
		}
		else
		{
			OverlappedCharacters.Add(Pawn);
		}
	}
}

void AWRPickup::OnOverlapEnd_Implementation(UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	APawn* Pawn = Cast<APawn>(OtherActor);

	if (Pawn)
	{
		if (bUsable)
		{
			AWRCharacter * WRChar = Cast<AWRCharacter>(OtherActor);

			if (WRChar)
			{
				UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("Pickup named %s removed usable."), *GetName());
				WRChar->RemoveUsable(this);
			}
		}
		else
		{
			OverlappedCharacters.Remove(Pawn);
		}
	}
}

bool AWRPickup::TryGiveTo_Implementation(APawn * Target)
{
	return false;
}

void AWRPickup::TryGiveToOverlappingCharacters_Implementation()
{
	for (APawn * Character : OverlappedCharacters)
	{

	}
}

void AWRPickup::OnPickedUp_Implementation()
{
	Destroy();
}

void AWRPickup::SetPickupEnabled_Implementation(bool bInPickupEnabled)
{
	if (!PickupCollision)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("Pickup named %s had SetPickupEnabled called and doesn't have PickupCollision set up."), *GetName());
		return;
	}

	if (bInPickupEnabled)
	{
		PickupCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		PickupCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

FText AWRPickup::GetPickupDisplayName_Implementation() const
{
	return DisplayName;
}

void AWRPickup::Use_Implementation(APawn * User)
{
	TryGiveTo(User);
}

bool AWRPickup::RequiresAim_Implementation() const
{
	return false;
}

FText AWRPickup::UseActionText_Implementation() const
{
	return FText::Format(NSLOCTEXT(WR_LOCTEXT_NS_INVENTORY, "UsePickupFormat", "Pick up {0}."), GetPickupDisplayName());
}

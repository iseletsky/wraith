#include "Wraith.h"
#include "AEGameplayStatics.h"
#include "WRCharacter.h"
#include "WRPickupInventory.h"
#include "WRCharacter.h"
#include "WRInventoryManager.h"

void AWRPickupInventory::Destroyed()
{
	Super::Destroyed();

	if (ContainedInventory)
	{
		ContainedInventory->Prop = NULL;
		ContainedInventory->Destroy();
	}
}

void AWRPickupInventory::BeginPlay()
{
	Super::BeginPlay();

	SpawnContainedInventory();
}

void AWRPickupInventory::OnPickedUp_Implementation()
{
	//Don't destroy on pickup
}

AWRInventoryPhysical * AWRPickupInventory::SpawnContainedInventory_Implementation()
{
	if (!InventoryClass)
	{
		UE_LOG_ON_SCREEN(WR, Error, 5.f, FColor::Red, TEXT("Calling SpawnContainedInventory on PickupInventory %s that has no InventoryClass"), *GetName());
		return NULL;
	}

	if (!ContainedInventory)
	{
		FActorSpawnParameters SpawnParams;

		SpawnParams.Owner = GetOwner();

		if (SpawnParams.Owner)
		{
			SpawnParams.Instigator = GetOwner()->Instigator;
		}

		ContainedInventory = GetWorld()->SpawnActor<AWRInventoryPhysical>(InventoryClass, SpawnParams);
				
		ContainedInventory->Prop = this;

		for (auto& AmmoIter : StartingAmmoCountOverride)
		{
			if (AmmoIter.Value > 0)
			{
				ContainedInventory->SetAmmoCount(AmmoIter.Key, AmmoIter.Value);
			}
		}
	}
	
	return ContainedInventory;
}

bool AWRPickupInventory::TryGiveTo_Implementation(APawn * Target)
{
	AWRCharacter * WRChar = Cast<AWRCharacter>(Target);

	if (!WRChar)
	{
		return false;
	}

	//for good measure
	SpawnContainedInventory();

	OnPickedUp();

	if (ContainedInventory->bIsStackable)
	{
		AWRInventoryPhysical * Existing = WRChar->InventoryManager->FindInventoryType(InventoryClass, true);

		if (Existing)
		{
			check(Existing->bIsStackable);

			Existing->SetStackCount(Existing->GetStackCount() + ContainedInventory->GetStackCount());

			//just destroy the whole inventory object and pickup			
			ContainedInventory->Destroy();

			return true;
		}
	}

	WRChar->InventoryManager->AddInventory(ContainedInventory);

	return true;
}

FText AWRPickupInventory::GetPickupDisplayName_Implementation() const 
{
	FText SuperText = Super::GetPickupDisplayName_Implementation();

	if (SuperText.IsEmpty())
	{
		return ContainedInventory->DisplayName;
	}
	else
	{
		return SuperText;
	}
}
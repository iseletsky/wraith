#include "Wraith.h"
#include "WRPickupAmmo.h"

AWRInventoryPhysical * AWRPickupAmmo::SpawnContainedInventory_Implementation()
{
	if (AmmoClass)
	{
		InventoryClass = AmmoClass;
	}
	else
	{
		UE_LOG_ON_SCREEN(WR, Error, 5.f, FColor::Red, TEXT("PickupAmmo with name %s doesn't have an AmmoClass specified.  Specify AmmoClass instead of InventoryClass."), *GetName());
		return NULL;
	}

	AWRInventoryPhysical * Res = Super::SpawnContainedInventory_Implementation();

	AWRInventoryAmmo * Ammo = Cast<AWRInventoryAmmo>(Res);

	if (Ammo)
	{
		Ammo->SetStackCount(AmmoCount);
	}

	return Res;
}
#include "Wraith.h"
#include "WRInventoryAmmo.h"

AWRInventoryAmmo::AWRInventoryAmmo(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bIsStackable = true;
}

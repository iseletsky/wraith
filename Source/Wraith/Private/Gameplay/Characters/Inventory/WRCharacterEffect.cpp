#include "Wraith.h"
#include "WRCharacter.h"
#include "WRCharacterEffect.h"

void AWRCharacterEffect::Added_Implementation(UWRInventoryManager * InOwningManager)
{
	Super::Added_Implementation(InOwningManager);

	OnEnabledForCharacter(Cast<AWRCharacter>(GetOwner()));
}

void AWRCharacterEffect::Removed_Implementation()
{
	OnDisabledForCharacter(Cast<AWRCharacter>(GetOwner()));

	Super::Removed_Implementation();
}

void AWRCharacterEffect::OnEnabledForCharacter_Implementation(AWRCharacter * Character)
{

}

void AWRCharacterEffect::OnDisabledForCharacter_Implementation(AWRCharacter * Character)
{

}
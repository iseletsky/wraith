#include "Wraith.h"
#include "AEGameplayStatics.h"
#include "WRGameplayStatics.h"
#include "WREquippableStateEquip.h"
#include "WREquippableStateUnequip.h"
#include "WREquippableStateIdle.h"
#include "WREquippableStateUseInterface.h"
#include "WREquippableStateReloadInterface.h"
#include "WREquippableStateUnloadInterface.h"
#include "WREquippableStateManager.h"
#include "WRInventoryPhysical.h"
#include "WRInventoryAmmo.h"
#include "WRPickupInventory.h"
#include "WRCharacterStanceData.h"
#include "WRInventoryManager.h"
#include "WREquippableManager.h"
#include "WRCharacterHandControlProp.h"

AWRInventoryPhysical::AWRInventoryPhysical(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//set this to true initially so anything between now and BeginPlay has a chance to set this to false
	bPassedSanityChecks = true;

	StateManager = ObjectInitializer.CreateDefaultSubobject<UWREquippableStateManager>(this, TEXT("StateManager"));

	HandControlClass = UWRCharacterHandControlProp::StaticClass();

	DefaultDominantHandAttachComponentSocket = FName(TEXT("hand_r"));
	DefaultNonDominantHandAttachIKComponentSocket = FName(TEXT("hand_l"));
	DefaultProjectileSpawnSocket = FName(TEXT("muzzle"));
}

void AWRInventoryPhysical::BeginPlay()
{
	Super::BeginPlay();

	//if still passed sanity checks along the way	
	bPassedSanityChecks = SetupAndSanityCheck() && bPassedSanityChecks;

	if (bOverrideFailedSanityCheck)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRInventoryPhysical named \"%s\" has the override failed sanity check flag set.  If it fails sanity check it may crash the game when you attempt to use it.  This should only be on when someone is developing."), *GetName());
	}

	if (!bPassedSanityChecks)
	{
		UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRInventoryPhysical named \"%s\" didn't pass sanity checks.  Check logs for why it didn't pass."), *GetName());
	}
}

void AWRInventoryPhysical::Added_Implementation(UWRInventoryManager * InOwningManager)
{
	Super::Added_Implementation(InOwningManager);

	//TODO: Right now assuming object is added into the inventory and immediately disappears
	//Check if actually auto equipping
	TearDownOnUnequip();
}

void AWRInventoryPhysical::Removed_Implementation()
{
	Super::Removed_Implementation();

	ResetStates();
}

/////////////////////////////////////
//Sanity check

bool AWRInventoryPhysical::SetupAndSanityCheck_Implementation()
{
	bool bErrors = false;

	//init all ammos
	for(auto& AmmoIter : Ammo)
	{
		FWRAmmoData& ThisAmmo = AmmoIter.Value;

		if (!ThisAmmo.AmmoClass)
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRInventoryPhysical named \"%s\" has an AmmoData with name \"%s\" that has a NULL Ammo class."), *GetName(), *AmmoIter.Key.ToString());
			
			bErrors = true;
			continue;
		}
		
		if (ThisAmmo.Count < 0)
		{
			ThisAmmo.Count = ThisAmmo.Capacity;
		}
	}
		
	EngagedUseMode.SetNumZeroed(UseStateClass.Num());
	PendingReloadMode.SetNumZeroed(ReloadUnloadStateClass.Num());
	PendingUnloadMode.SetNumZeroed(ReloadUnloadStateClass.Num());

	//automatically create missing states if needed
	if (IdleStateClass == NULL)
	{
		IdleStateClass = UWREquippableStateIdle::StaticClass();
	}

	StateManager->StateClasses.Add(IdleStateClass);

	if (CharacterStance)
	{
		if (!EquipStateClass && CharacterStance->DefaultEquipAnimation)
		{
			EquipStateClass = LoadClass<UWREquippableStateEquip>(NULL,
				TEXT("Blueprint'/Game/Characters/Inventory/EquippableStates/BP_WREquippableStateEquipDefault.BP_WREquippableStateEquipDefault_C'"));
		}

		if (!UnequipStateClass && CharacterStance->DefaultUnequipAnimation)
		{
			UnequipStateClass = LoadClass<UWREquippableStateUnequip>(NULL,
				TEXT("Blueprint'/Game/Characters/Inventory/EquippableStates/BP_WREquippableStateUnequipDefault.BP_WREquippableStateUnequipDefault_C'"));
		}
	}

	if (EquipStateClass)
	{
		StateManager->StateClasses.Add(EquipStateClass);
	}

	if (UnequipStateClass)
	{
		StateManager->StateClasses.Add(UnequipStateClass);
	}

	for (int32 StateIndex = 0; StateIndex < UseStateClass.Num(); ++StateIndex)
	{
		if (UseStateClass[StateIndex])
		{
			StateManager->StateClasses.Add(UseStateClass[StateIndex]);
		}
		else
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRInventoryPhysical named \"%s\" has a Use Mode Class at index %d with a Null class."), *GetName(), StateIndex);
			bErrors = true;
		}
	}

	for (int32 StateIndex = 0; StateIndex < ReloadUnloadStateClass.Num(); ++StateIndex)
	{
		if (!ReloadUnloadStateClass[StateIndex].ReloadStateClass
			&& !ReloadUnloadStateClass[StateIndex].UnloadStateClass)
		{
			
			bErrors = true;
		}
		else
		{
			if (ReloadUnloadStateClass[StateIndex].ReloadStateClass)
			{
				StateManager->StateClasses.Add(ReloadUnloadStateClass[StateIndex].ReloadStateClass);
			}

			if (ReloadUnloadStateClass[StateIndex].UnloadStateClass)
			{
				StateManager->StateClasses.Add(ReloadUnloadStateClass[StateIndex].UnloadStateClass);
			}
		}
	}
	
	bErrors = !StateManager->Initialize();

	//automatically create attached prop infos for attached magazine datas
	for (auto& AttachedMagazineIter : AttachedMagazines)
	{
		if (!AttachedMagazineIter.Value.PropClass)
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRInventoryPhysical named \"%s\" has an AttachedMagazineInfo with name \"%s\" that has a NULL Prop class."), *GetName(), *AttachedMagazineIter.Key.ToString());

			bErrors = true;
			continue;
		}

		if (AttachedProps.Contains(AttachedMagazineIter.Key))
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRInventoryPhysical named \"%s\" has an AttachedMagazineInfo with name \"%s\" that has the same name as an existing AttachedPropInfo."), *GetName(), *AttachedMagazineIter.Key.ToString());

			bErrors = true;
			continue;
		}
		
		FWRAttachedPropData& NewPropData = AttachedProps.Emplace(AttachedMagazineIter.Key);

		NewPropData.Prop = NULL;
		NewPropData.PropClass = AttachedMagazineIter.Value.PropClass;

		NewPropData.AmmoCount = 0;
		NewPropData.AmmoName = AttachedMagazineIter.Value.AmmoName;
		NewPropData.AmmoCapacity = AttachedMagazineIter.Value.AmmoCapacity;

		NewPropData.bDefaultVisible = true;
		NewPropData.ForcedDefaultVisibilityState = AEDefaultBoolOverrideState::NO_FORCED_OVERRIDE;

		NewPropData.bDefaultCollision = AttachedMagazineIter.Value.bAttachedCollisionEnabled;
		NewPropData.ForcedDefaultCollisionState = AEDefaultBoolOverrideState::NO_FORCED_OVERRIDE;

		NewPropData.bDefaultAttachToMainProp = true;
		NewPropData.DefaultOnMainPropAttachComponentSocket = AttachedMagazineIter.Value.DefaultOnMainPropAttachComponentSocket;
		NewPropData.DefaultToMainPropAttachComponentSocket = AttachedMagazineIter.Value.DefaultToMainPropAttachComponentSocket;
		NewPropData.DefaultNonDominantHandAttachComponentSocket = AttachedMagazineIter.Value.DefaultNonDominantHandAttachComponentSocket;

		NewPropData.bDestroyPropOnHide = false;
	}

	//check if all the attached prop infos have a class specified
	for (auto& AttachedPropIter : AttachedProps)
	{
		if (!AttachedPropIter.Value.PropClass)
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("WRInventoryPhysical named \"%s\" has an AttachedPropInfo with name \"%s\" that has a NULL Prop class."), *GetName(), *AttachedPropIter.Key.ToString());

			bErrors = true;
		}
	}

	if (Prop)
	{
		for (auto& AttachedPropIter : AttachedProps)
		{
			ResetAttachedPropToDefaultState(AttachedPropIter.Key);
		}
	}

	return !bErrors;
}

///////////////////////////////////
//Prop

AWRPickupInventory * AWRInventoryPhysical::SpawnProp_Implementation(const FTransform& SpawnLocation, bool bContainThisInventory, UWorld * World)
{
	if (PropClass)
	{
		if (!World)
		{
			World = GetWorld();
		}

		AWRPickupInventory * NewProp = World->SpawnActorDeferred<AWRPickupInventory>(PropClass, SpawnLocation, NULL, NULL, ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);
		
		if (bContainThisInventory)
		{
			//if existing prop, destroy it
			if (Prop)
			{
				for (auto& AttachedPropIter : AttachedProps)
				{
					AttachedPropIter.Value.Prop = NULL;
				}

				DestroyProp();
			}

			NewProp->ContainedInventory = this;
			Prop = NewProp;

			for (auto& AttachedPropIter : AttachedProps)
			{
				ResetAttachedPropToDefaultState(AttachedPropIter.Key);
			}
		}

		UGameplayStatics::FinishSpawningActor(NewProp, SpawnLocation);			

		return NewProp;
	}
	else
	{
		return NULL;
	}
}

bool AWRInventoryPhysical::SpawnPropForSelfIfNeeded_Implementation(const FTransform& SpawnLocation)
{
	if (Prop)
	{
		Prop->SetActorTransform(SpawnLocation, false, NULL, ETeleportType::TeleportPhysics);
		return false;
	}

	SpawnProp(SpawnLocation, true);

	return true;
}

bool AWRInventoryPhysical::DestroyProp_Implementation()
{
	if (Prop)
	{
		Prop->ContainedInventory = NULL;
		Prop->DestroyActorHeirarchy();
		Prop = NULL;

		return true;
	}	

	return false;
}

void AWRInventoryPhysical::SetupPropForWorld_Implementation(const FTransform& SpawnLocation)
{
	if (!SpawnPropForSelfIfNeeded(SpawnLocation))
	{
		Prop->SetActorHeirarchyHiddenInGame(false);
		Prop->SetActorHeirarchyEnableCollision(true);
		Prop->SetActorHeirarchyPhysicsEnabled(true);
	}
}

void AWRInventoryPhysical::SetupPropForCharacter_Implementation()
{
	SpawnPropForSelfIfNeeded(GetOwner() ? GetOwner()->GetTransform() : FTransform::Identity);

	Prop->SetActorHeirarchyHiddenInGame(false);
	Prop->SetActorHeirarchyEnableCollision(false);
	Prop->SetActorHeirarchyPhysicsEnabled(false);
}

void AWRInventoryPhysical::SetupPropHidden_Implementation()
{
	if (bDestroyPropOnHide)
	{
		DestroyProp();
	}
	else if (Prop)
	{
		Prop->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Prop->SetActorHeirarchyHiddenInGame(true);
		Prop->SetActorHeirarchyEnableCollision(false);
		Prop->SetActorHeirarchyPhysicsEnabled(false);
	}
}

FTransform AWRInventoryPhysical::GetDefaultProjectileSpawnOffsetFromCharacterAttachment_Implementation() const
{
	if (!DefaultProjectileSpawnSocket.IsNone())
	{
		AWRPickupInventory * PropActor = NULL;

		if (Prop)
		{
			PropActor = Prop;
		}
		else if (PropClass)
		{
			PropActor = PropClass->GetDefaultObject<AWRPickupInventory>();
		}

		if (PropActor)
		{
			return UAEGameplayStatics::GetRelativeTransformBetweenComponents(PropActor->GetRootComponent(), PropActor->GetRootComponent(),
				GetDominantHandAttachComponentSocket(), DefaultProjectileSpawnSocket) * FTransform(FRotator(0.f, -90.f, 0.f));
		}
	}

	return FTransform::Identity;
}

FTransform AWRInventoryPhysical::GetDefaultProjectileSpawnOffsetFromRoot_Implementation() const
{
	if (!DefaultProjectileSpawnSocket.IsNone())
	{
		AWRPickupInventory * PropActor = NULL;

		if (Prop)
		{
			PropActor = Prop;
		}
		else if (PropClass)
		{
			PropActor = PropClass->GetDefaultObject<AWRPickupInventory>();
		}

		if (PropActor)
		{
			return UAEGameplayStatics::GetRelativeTransformBetweenComponents(PropActor->GetRootComponent(), PropActor->GetRootComponent(),
				NAME_None, DefaultProjectileSpawnSocket);
		}
	}

	return FTransform::Identity;
}

///////////////////////////////////
//Attached Prop

void AWRInventoryPhysical::SpawnAttachedPropIfNeeded_Implementation(FName AttachedPropName, bool bForceSpawn)
{
	if (!Prop)
	{
		return;
	}

	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData && !PropData->Prop && PropData->PropClass)
	{
		if (!bForceSpawn)
		{
			if (!GetOverrideBoolValue(PropData->bDefaultVisible, PropData->ForcedDefaultVisibilityState)
				&& !GetOverrideBoolValue(PropData->bDefaultCollision, PropData->ForcedDefaultCollisionState))
			{
				return;
			}
		}

		FActorSpawnParameters SpawnParams;

		SpawnParams.Owner = this;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		PropData->Prop = GetWorld()->SpawnActor<AAEPhysicalActor>(PropData->PropClass, SpawnParams);

		//if the prop is a pickup, we don't want it to be pick uppable!  Such as magazines attached to weapons.
		AWRPickup * PickupProp = Cast<AWRPickup>(PropData->Prop);

		if (PickupProp)
		{
			PickupProp->SetPickupEnabled(false);
		}

		ResetAttachedPropToDefaultState(AttachedPropName);
	}
}

void AWRInventoryPhysical::SetAttachedPropVisibility_Implementation(FName AttachedPropName, bool bVisible)
{
	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		if (bVisible)
		{
			SpawnAttachedPropIfNeeded(AttachedPropName, true);

			if (PropData->Prop)
			{
				PropData->Prop->bForceHeirarchyHiddenInGame = false;
				PropData->Prop->SetActorHeirarchyHiddenInGame(false);
			}
		}
		else
		{
			if (PropData->Prop)
			{
				PropData->Prop->bForceHeirarchyHiddenInGame = true;
				PropData->Prop->SetActorHeirarchyHiddenInGame(true);
			}
		}
	}
}

void AWRInventoryPhysical::SetAttachedPropCollision_Implementation(FName AttachedPropName, bool bCollisionEnabled)
{
	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		if (bCollisionEnabled)
		{
			SpawnAttachedPropIfNeeded(AttachedPropName, true);

			if (PropData->Prop)
			{
				PropData->Prop->bForceHeirarchyCollisionDisabled = false;
				PropData->Prop->SetActorHeirarchyEnableCollision(true);
			}
		}
		else
		{
			if (PropData->Prop)
			{
				PropData->Prop->bForceHeirarchyCollisionDisabled = true;
				PropData->Prop->SetActorHeirarchyEnableCollision(false);
			}
		}
	}
}

AAEPhysicalActor * AWRInventoryPhysical::DisconnectAttachedProp_Implementation(FName AttachedPropName)
{
	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		SpawnAttachedPropIfNeeded(AttachedPropName, true);

		if (PropData->Prop)
		{
			AAEPhysicalActor * Res = PropData->Prop;

			//If pickup, we want pickup collision reenabled
			AWRPickup * PickupProp = Cast<AWRPickup>(Res);

			if (PickupProp)
			{
				PickupProp->SetPickupEnabled(true);
			}

			PropData->Prop = NULL;
			PropData->AmmoCount = NULL;

			Res->SetOwner(NULL);

			Res->SetActorHeirarchyEnableCollision(true);
			Res->SetActorHeirarchyPhysicsEnabled(true);
			Res->SetActorHeirarchyHiddenInGame(false);

			return Res;
		}
	}

	return NULL;
}

bool AWRInventoryPhysical::AttachedPropToCharacter_Implementation(FName AttachedPropName)
{
	if (GetAttachedPropState(AttachedPropName) == WRAttachedPropAttachment::ATTACHED_TO_CHARACTER)
	{
		return false;
	}

	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		UWRCharacterHandControlProp * HandControlProp = Cast<UWRCharacterHandControlProp>(HandControl);
		AWRCharacter * OwnerChar = HandControl->GetOwnerCharacter();

		if (HandControl && OwnerChar)
		{
			SpawnAttachedPropIfNeeded(AttachedPropName, true);

			if (PropData->Prop)
			{
				UAEGameplayStatics::AttachActorToComponent(PropData->Prop, 
					OwnerChar->GetMesh(),
					HandControlProp 
						? HandControlProp->GetNonDominantHandPropSocket() 
						: NAME_None, 
					PropData->DefaultNonDominantHandAttachComponentSocket,
					NULL, NULL, false);

				return true;
			}
		}
		else
		{
			SetAttachedPropCollision(AttachedPropName, false);
			SetAttachedPropVisibility(AttachedPropName, false);
		}
	}

	return false;
}

bool AWRInventoryPhysical::AttachedPropToProp_Implementation(FName AttachedPropName)
{
	if (GetAttachedPropState(AttachedPropName) == WRAttachedPropAttachment::ATTACHED_TO_PROP)
	{
		return false;
	}

	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		if (Prop)
		{
			SpawnAttachedPropIfNeeded(AttachedPropName, true);

			if (PropData->Prop)
			{
				UAEGameplayStatics::AttachActorToComponent(PropData->Prop, Prop->GetRootComponent(),
					PropData->DefaultOnMainPropAttachComponentSocket, PropData->DefaultToMainPropAttachComponentSocket,
					NULL, NULL, true);

				return true;
			}
		}
		else
		{
			SetAttachedPropCollision(AttachedPropName, false);
			SetAttachedPropVisibility(AttachedPropName, false);
		}
	}

	return false;
}

WRAttachedPropAttachment::Type AWRInventoryPhysical::GetAttachedPropState(FName AttachedPropName) const
{
	const FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData && PropData->Prop)
	{
		AActor * AttachParentActor = PropData->Prop->GetAttachParentActor();

		if (!AttachParentActor)
		{
			return WRAttachedPropAttachment::DETACHED;
		}
		else if (AttachParentActor == Prop)
		{
			return WRAttachedPropAttachment::ATTACHED_TO_PROP;
		}
		else if (AttachParentActor == GetOwner())
		{
			return WRAttachedPropAttachment::ATTACHED_TO_CHARACTER;
		}
	}

	return WRAttachedPropAttachment::DETACHED;
}

bool AWRInventoryPhysical::SetAttachedPropAmmoCount_Implementation(FName AttachedPropName, int32 InAmmoCount)
{
	if (GetAttachedPropState(AttachedPropName) == WRAttachedPropAttachment::DETACHED)
	{
		return false;
	}

	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		PropData->AmmoCount = InAmmoCount;

		AWRPickupInventory * Pickup = Cast<AWRPickupInventory>(GetAttachedProp(AttachedPropName));

		if (Pickup)
		{
			AWRInventoryAmmo * PickupAmmo = Cast<AWRInventoryAmmo>(Pickup->ContainedInventory);

			if (PickupAmmo)
			{
				PickupAmmo->SetStackCount(InAmmoCount);
			}
		}

		return true;
	}

	return false;
}

bool AWRInventoryPhysical::MoveAmmoFromWeaponToProp_Implementation(FName AttachedPropName)
{
	if (GetAttachedPropState(AttachedPropName) == WRAttachedPropAttachment::DETACHED)
	{
		return false;
	}

	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		int32 WeaponAmmoCount = GetAmmoCount(PropData->AmmoName);

		if (WeaponAmmoCount <= 0)
		{
			return false;
		}

		int32 PropAmmoCount = GetAttachedPropAmmoCount(AttachedPropName);
		int32 Capacity = PropData->AmmoCapacity > 0 ? PropData->AmmoCapacity : GetAmmoCapacity(PropData->AmmoName);

		int32 AmmoToMove = FMath::Max(0, Capacity - PropAmmoCount);
		AmmoToMove = FMath::Min(AmmoToMove, WeaponAmmoCount);

		if (AmmoToMove <= 0)
		{
			return false;
		}

		bool Res = SetAttachedPropAmmoCount(AttachedPropName, PropAmmoCount + AmmoToMove);

		if (Res)
		{
			SetAmmoCount(PropData->AmmoName, WeaponAmmoCount - AmmoToMove);
		}

		return Res;
	}

	return false;
}

bool AWRInventoryPhysical::MoveAmmoFromPropToWeapon_Implementation(FName AttachedPropName)
{
	if (GetAttachedPropState(AttachedPropName) == WRAttachedPropAttachment::DETACHED)
	{
		return false;
	}

	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		int32 PropAmmoCount = GetAttachedPropAmmoCount(AttachedPropName);

		if (PropAmmoCount <= 0)
		{
			return false;
		}

		bool Res = SetAttachedPropAmmoCount(AttachedPropName, 0);

		if (Res)
		{
			SetAmmoCount(PropData->AmmoName, GetAmmoCount(PropData->AmmoName) + PropAmmoCount);
		}

		return Res;
	}

	return false;
}

bool AWRInventoryPhysical::MoveAmmoFromPropToCharacter_Implementation(FName AttachedPropName)
{
	if (GetAttachedPropState(AttachedPropName) == WRAttachedPropAttachment::DETACHED)
	{
		return false;
	}

	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		int32 PropAmmoCount = GetAttachedPropAmmoCount(AttachedPropName);

		if (PropAmmoCount <= 0)
		{
			return false;
		}

		AWRInventoryAmmo * OwnerAmmo = GetOwnerReserveAmmo(PropData->AmmoName);

		if (OwnerAmmo)
		{
			bool Res = SetAttachedPropAmmoCount(AttachedPropName, 0);

			if (Res)
			{
				OwnerAmmo->SetStackCount(OwnerAmmo->GetStackCount() + PropAmmoCount);
			}

			return Res;
		}
	}

	return false;
}

bool AWRInventoryPhysical::MoveAmmoFromCharacterToProp_Implementation(FName AttachedPropName)
{
	if (GetAttachedPropState(AttachedPropName) == WRAttachedPropAttachment::DETACHED)
	{
		return false;
	}

	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		AWRInventoryAmmo * OwnerAmmo = GetOwnerReserveAmmo(PropData->AmmoName);

		if (OwnerAmmo)
		{
			int32 CharacterAmmoCount = OwnerAmmo->GetStackCount();

			if (CharacterAmmoCount <= 0)
			{
				return false;
			}

			int32 PropAmmoCount = GetAttachedPropAmmoCount(AttachedPropName);
			int32 Capacity = PropData->AmmoCapacity > 0 ? PropData->AmmoCapacity : GetAmmoCapacity(PropData->AmmoName);

			int32 AmmoToMove = FMath::Max(0, Capacity - PropAmmoCount);
			AmmoToMove = FMath::Min(AmmoToMove, CharacterAmmoCount);

			bool Res = SetAttachedPropAmmoCount(AttachedPropName, PropAmmoCount + AmmoToMove);

			if (Res)
			{
				OwnerAmmo->SetStackCount(OwnerAmmo->GetStackCount() - AmmoToMove);
			}

			return Res;
		}
	}

	return false;
}

void AWRInventoryPhysical::ResetAttachedPropToDefaultState_Implementation(FName AttachedPropName)
{
	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		SetAttachedPropVisibility(AttachedPropName, GetOverrideBoolValue(PropData->bDefaultVisible, PropData->ForcedDefaultVisibilityState));
		SetAttachedPropCollision(AttachedPropName, GetOverrideBoolValue(PropData->bDefaultCollision, PropData->ForcedDefaultCollisionState));

		if (PropData->Prop)
		{
			MoveAmmoFromPropToWeapon(AttachedPropName);

			if (PropData->bDefaultAttachToMainProp)
			{
				AttachedPropToProp(AttachedPropName);				
			}
			else
			{
				AttachedPropToCharacter(AttachedPropName);
			}
		}
	}
}

bool AWRInventoryPhysical::AttachedPropToCharacterAnimNotify_Implementation()
{
	UAEState * State = StateManager->GetCurrentState();

	if (State)
	{
		UWREquippableState * WREState = Cast<UWREquippableState>(State);

		if (WREState)
		{
			return WREState->AttachedPropToCharacterAnimNotify();
		}
	}

	return false;
}

bool AWRInventoryPhysical::AttachedPropToPropAnimNotify_Implementation()
{
	UAEState * State = StateManager->GetCurrentState();

	if (State)
	{
		UWREquippableState * WREState = Cast<UWREquippableState>(State);

		if (WREState)
		{
			return WREState->AttachedPropToPropAnimNotify();
		}
	}

	return false;
}

bool AWRInventoryPhysical::ShowAttachedPropAnimNotify_Implementation()
{
	UAEState * State = StateManager->GetCurrentState();

	if (State)
	{
		UWREquippableState * WREState = Cast<UWREquippableState>(State);

		if (WREState)
		{
			return WREState->ShowAttachedPropAnimNotify();
		}
	}

	return false;
}

bool AWRInventoryPhysical::HideAttachedPropAnimNotify_Implementation()
{
	UAEState * State = StateManager->GetCurrentState();

	if (State)
	{
		UWREquippableState * WREState = Cast<UWREquippableState>(State);

		if (WREState)
		{
			return WREState->HideAttachedPropAnimNotify();
		}
	}

	return false;
}

/////////////////////////////////////
//Attached Magazine

bool AWRInventoryPhysical::AttachMagazineToProp_Implementation(FName AttachedPropName)
{
	if (AttachedPropToProp(AttachedPropName))
	{
		MoveAmmoFromPropToWeapon(AttachedPropName);
		
		return true;
	}

	return false;
}

bool AWRInventoryPhysical::AttachMagazineToCharacter_Implementation(FName AttachedPropName)
{
	bool bWasAttachedToWeapon = GetAttachedPropState(AttachedPropName) == WRAttachedPropAttachment::ATTACHED_TO_PROP;

	if (AttachedPropToCharacter(AttachedPropName))
	{
		if (bWasAttachedToWeapon)
		{
			MoveAmmoFromWeaponToProp(AttachedPropName);
		}

		return true;
	}

	return false;
}

AAEPhysicalActor * AWRInventoryPhysical::DisconnectAttachedMagazine_Implementation(FName AttachedPropName)
{
	SpawnAttachedPropIfNeeded(AttachedPropName, true);

	if (GetAttachedPropState(AttachedPropName) == WRAttachedPropAttachment::ATTACHED_TO_PROP)
	{
		MoveAmmoFromWeaponToProp(AttachedPropName);
	}
	
	return DisconnectAttachedProp(AttachedPropName);
}

/////////////////////////////////////
//Ammo

AWRInventoryAmmo * AWRInventoryPhysical::GetOwnerReserveAmmo_Implementation(FName AmmoName) const
{
	const FWRAmmoData * AmmoData = Ammo.Find(AmmoName);

	if (!AmmoData)
	{
		return NULL;
	}
	
	if (GetOwnerInventoryManager())
	{
		return GetOwnerInventoryManager()->FindInventoryType<AWRInventoryAmmo>(AmmoData->AmmoClass, true);
	}

	return NULL;
}

bool AWRInventoryPhysical::CheckEnoughAmmo_Implementation(FName AmmoName, int32 Amount) const
{
	const FWRAmmoData * AmmoData = Ammo.Find(AmmoName);

	if (!AmmoData)
	{
		return true;
	}
	
	return AmmoData->Count >= Amount;
}

bool AWRInventoryPhysical::CheckFullAmmo_Implementation(FName AmmoName) const
{
	const FWRAmmoData * AmmoData = Ammo.Find(AmmoName);

	if (!AmmoData)
	{
		return false;
	}

	return AmmoData->Count >= AmmoData->Capacity;
}

int32 AWRInventoryPhysical::GetAmmoCapacity_Implementation(FName AmmoName) const
{
	const FWRAmmoData * AmmoData = Ammo.Find(AmmoName);

	if (!AmmoData)
	{
		return -1;
	}

	return AmmoData->Capacity;
}

int32 AWRInventoryPhysical::GetAmmoCount_Implementation(FName AmmoName) const
{
	const FWRAmmoData * AmmoData = Ammo.Find(AmmoName);

	if (!AmmoData)
	{
		return -1;
	}
	
	return AmmoData->Count;
}

void AWRInventoryPhysical::SetAmmoCount_Implementation(FName AmmoName, int32 Amount)
{
	FWRAmmoData * AmmoData = Ammo.Find(AmmoName);

	if (!AmmoData)
	{
		return;
	}

	AmmoData->Count = Amount;
}

int32 AWRInventoryPhysical::ConsumeAmmo_Implementation(FName AmmoName, int32 Amount = 1)
{
	FWRAmmoData * AmmoData = Ammo.Find(AmmoName);

	if (!AmmoData)
	{
		return 0;
	}
	
	AmmoData->Count -= Amount;

	//don't let this go below 0, usually checking if enough ammo is in the weapon first would prevent this from ever happening anyway
	if (AmmoData->Count < 0)
	{
		AmmoData->Count = 0;
	}

	//TODO: (ilselets) change the weight of the equippable

	return AmmoData->Count;
}

//int32 AWRInventoryPhysical::FillFromOwnerReserveAmmo_Implementation(AWRInventoryAmmo * OwnerAmmo, int32 Amount)
//{
//	//TODO: (ilselets) change the weight of the equippable
//
//	if (OwnerAmmo)
//	{
//		int32 MyAmmoIndex = GetAmmoIndexForClass(OwnerAmmo->GetClass());
//
//		if (MyAmmoIndex >= 0)
//		{
//			FWRAmmoData& AmmoData = Ammo[MyAmmoIndex];
//
//			int32 AmountNeeded = Amount <= 0
//				? AmmoData.Capacity - AmmoData.Count
//				: Amount;
//
//			int32 TransferAmount = FMath::Min(AmountNeeded, OwnerAmmo->GetStackCount());
//
//			if (TransferAmount > 0)
//			{
//				OwnerAmmo->SetStackCount(OwnerAmmo->GetStackCount() - TransferAmount);
//				AmmoData.Count += TransferAmount;
//			}
//
//			return TransferAmount;
//		}
//		else
//		{
//			UE_LOG(WR, Warning, TEXT("WRInventoryItemEquippable named \"%s\" had FillFromOwnerReserveAmmo called with an ammo class \"%s\" that is not used by this object."), *GetName(), *OwnerAmmo->GetClass()->GetName());
//			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("WRInventoryItemEquippable named \"%s\" had FillFromOwnerReserveAmmo called with an ammo class \"%s\" that is not used by this object."), *GetName(), *OwnerAmmo->GetClass()->GetName()));
//		}
//	}
//
//	return 0;
//}
//
//int32 AWRInventoryPhysical::ReturnToOwnerReserveAmmo_Implementation(AWRInventoryAmmo * OwnerAmmo, int32 Amount = 0)
//{
//	//TODO: (ilselets) change the weight of the equippable
//
//	if (OwnerAmmo)
//	{
//		int32 MyAmmoIndex = GetAmmoIndexForClass(OwnerAmmo->GetClass());
//
//		if (MyAmmoIndex >= 0)
//		{
//			FWRAmmoData& AmmoData = Ammo[MyAmmoIndex];
//
//			int32 TransferAmount = Amount <= 0
//				? AmmoData.Count
//				: FMath::Min(Amount, AmmoData.Count);
//
//			if (TransferAmount > 0)
//			{
//				OwnerAmmo->SetStackCount(OwnerAmmo->GetStackCount() + TransferAmount);
//				AmmoData.Count -= TransferAmount;
//			}
//
//			return TransferAmount;
//		}
//		else
//		{
//			UE_LOG(WR, Warning, TEXT("WRInventoryItemEquippable named \"%s\" had ReturnToOwnerReserveAmmo called with an ammo class \"%s\" that is not used by this object."), *GetName(), *OwnerAmmo->GetClass()->GetName());
//			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("WRInventoryItemEquippable named \"%s\" had ReturnToOwnerReserveAmmo called with an ammo class \"%s\" that is not used by this object."), *GetName(), *OwnerAmmo->GetClass()->GetName()));
//		}
//	}
//
//	return 0;
//}

///////////////////////////////////////////////
//Animation

UWRCharacterHandControl * AWRInventoryPhysical::CreateHandControlForEquippableManager_Implementation()
{
	if (EquippableManager && EquippableManager->GetOwner())
	{
		HandControl = NewObject<UWRCharacterHandControl>(EquippableManager->GetOwner(), HandControlClass);
		HandControl->RegisterComponent();

		UWRCharacterHandControlProp * HandControlProp = Cast<UWRCharacterHandControlProp>(HandControl);

		if (HandControlProp)
		{
			HandControlProp->HeldEquippable = this;
		}

		return HandControl;
	}
	
	return NULL;
}

UAnimSequence * AWRInventoryPhysical::GetDominantHandPose_Implementation() const
{
	return DefaultDominantHandPose;
}

UAnimSequence * AWRInventoryPhysical::GetNonDominantHandPose_Implementation() const
{
	return DefaultNonDominantHandPose;
}

UAnimSequence * AWRInventoryPhysical::GetIdleAnimation_Implementation() const
{
	if (!bIdleAnimationEnabled || !StateManager->GetCurrentState())
	{
		return NULL;
	}

	return DefaultIdleAnimation ? DefaultIdleAnimation : (CharacterStance ? CharacterStance->DefaultIdleAnimation : NULL);
}

FName AWRInventoryPhysical::GetDominantHandAttachComponentSocket_Implementation() const
{
	return DefaultDominantHandAttachComponentSocket;
}

FName AWRInventoryPhysical::GetNonDominantHandAttachIKComponentSocket_Implementation() const
{
	return DefaultNonDominantHandAttachIKComponentSocket;
}

USceneComponent * AWRInventoryPhysical::GetNonDominantHandAttachIKComponent_Implementation() const
{
	if (Prop)
	{
		return Prop->GetSkeletalMesh();
	}
	
	return NULL;
}

///////////////////////////////////////////////
//Dominant Hand View Offset

FVector AWRInventoryPhysical::GetUpDominantHandViewOffset_Implementation() const
{
	return DefaultUpDominantHandViewOffset;
}

FVector AWRInventoryPhysical::GetCenterDominantHandViewOffset_Implementation() const
{
	return DefaultCenterDominantHandViewOffset;
}

FVector AWRInventoryPhysical::GetDownDominantHandViewOffset_Implementation() const
{
	return DefaultDownDominantHandViewOffset;
}

///////////////////////////////////////////////
//State

void AWRInventoryPhysical::ResetStates_Implementation()
{
	bPendingUnequip = false;

	for (int32 Index = 0; Index < EngagedUseMode.Num(); ++Index)
	{
		EngagedUseMode[Index] = false;
	}

	for (int32 Index = 0; Index < PendingReloadMode.Num(); ++Index)
	{
		PendingReloadMode[Index] = false;
	}

	for (int32 Index = 0; Index < PendingUnloadMode.Num(); ++Index)
	{
		PendingUnloadMode[Index] = false;
	}
}

bool AWRInventoryPhysical::TryGotoStateOrIdleIfNull_Implementation(UAEState * State)
{
	if (!State)
	{
		State = StateManager->GetStateForClass(IdleStateClass);
	}

	return StateManager->TryGotoState(State);
}

///////////////////
//equip/unequip

void AWRInventoryPhysical::SetupOnEquip_Implementation()
{
	UWRCharacterHandControlProp * HandControlProp = Cast<UWRCharacterHandControlProp>(HandControl);
	AWRCharacter * OwnerChar = HandControl->GetOwnerCharacter();

	if (HandControl && OwnerChar)
	{
		SetupPropForCharacter();

		UAEGameplayStatics::AttachActorToComponent(Prop, 
			OwnerChar->GetMesh(),
			HandControlProp
				? HandControlProp->GetDominantHandPropSocket()
				: NAME_None,
			GetDominantHandAttachComponentSocket(),
			NULL, NULL, false);
	}
}

void AWRInventoryPhysical::TearDownOnUnequip_Implementation()
{
	StateManager->ForceGotoState(NULL);
	ResetStates();
	SetupPropHidden();
}

void AWRInventoryPhysical::Equip_Implementation()
{
	if (!bPassedSanityChecks)
	{
		if (bOverrideFailedSanityCheck)
		{
			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("Equipping WRInventoryPhysical named \"%s\" that has the override failed sanity check flag set.  It may crash the game when you attempt to use it.  This should only be on when someone is developing."), *GetName());
		}
		else
		{
			//notify pawn unequip is done
			if (EquippableManager)
			{
				EquippableManager->EquipPendingEquippable();
				EquippableManager = NULL;
			}

			UE_LOG_ON_SCREEN(WR, Warning, 5.f, FColor::Red, TEXT("Attempting to equip WRInventoryPhysical named \"%s\" that didn't pass sanity checks.  Check logs for why it didn't pass."), *GetName());
		}
	}

	bPendingUnequip = false;

	if (!StateManager->GetCurrentState())
	{
		//set up the props if the weapon was previously inactive
		SetupOnEquip();
	}

	TryGotoStateOrIdleIfNull(StateManager->GetStateForClass(EquipStateClass));
}

void AWRInventoryPhysical::Unequip_Implementation()
{
	bPendingUnequip = true;
	TryGotoUnequip();
}

bool AWRInventoryPhysical::TryGotoUnequip_Implementation()
{
	//check if we should unequip
	if (bPendingUnequip)
	{
		UAEState * UnequipState = StateManager->GetStateForClass(UnequipStateClass);

		if (UnequipState)
		{
			if (StateManager->TryGotoState(UnequipState))
			{
				bPendingUnequip = false;
				return true;
			}
		}
		else
		{
			if (StateManager->AllowInterruptionByState(NULL))
			{
				bPendingUnequip = false;
				OnDoneUnequip();
				return true;
			}
		}
	}

	return false;
}

void AWRInventoryPhysical::OnDoneUnequip_Implementation()
{
	ResetStates();
	TearDownOnUnequip();
	
	if (EquippableManager)
	{
		EquippableManager->EquipPendingEquippable();
		EquippableManager = NULL;
	}
}

///////////////////
//use

void AWRInventoryPhysical::EngageUse_Implementation(int32 UseMode)
{
	if (EngagedUseMode.IsValidIndex(UseMode) && !EngagedUseMode[UseMode])
	{
		EngagedUseMode[UseMode] = true;
		OnUseModeEngaged(UseMode);
	}
}

void AWRInventoryPhysical::DisengageUse_Implementation(int32 UseMode)
{
	if (EngagedUseMode.IsValidIndex(UseMode) && EngagedUseMode[UseMode])
	{
		EngagedUseMode[UseMode] = false;
		OnUseModeDisengaged(UseMode);
	}
}

void AWRInventoryPhysical::OnUseModeEngaged_Implementation(int32 UseMode)
{
	TryUseMode(UseMode);
}

void AWRInventoryPhysical::OnUseModeDisengaged_Implementation(int32 UseMode)
{
	UAEState * CurrentState = StateManager->GetCurrentState();

	if (CurrentState && CurrentState->GetIsActive() && CurrentState->GetClass()->ImplementsInterface(UWREquippableStateUseInterface::StaticClass()))
	{
		IWREquippableStateUseInterface::Execute_OnUseModeDisengaged(CurrentState);
	}
}

bool AWRInventoryPhysical::TryGotoUse_Implementation()
{
	//check if we should perform a generic use mode	
	for (int32 UseModeIndex = 0; UseModeIndex < EngagedUseMode.Num(); ++UseModeIndex)
	{
		if (EngagedUseMode[UseModeIndex])
		{
			if (TryUseMode(UseModeIndex))
			{
				return true;
			}
		}
	}

	return false;
}

bool AWRInventoryPhysical::TryUseMode_Implementation(int32 UseMode)
{
	if (UseStateClass.IsValidIndex(UseMode))
	{
		UAEState * State = StateManager->GetStateForClass(UseStateClass[UseMode]);

		if (State && StateManager->AllowInterruptionByState(State))
		{
			if (State->GetClass()->ImplementsInterface(UWREquippableStateUseInterface::StaticClass()))
			{
				if (!IWREquippableStateUseInterface::Execute_CheckEnoughAmmo(State))
				{
					if (TryReloadMode(IWREquippableStateUseInterface::Execute_GetReloadModeIndex(State)))
					{
						return true;
					}

					return false;
				}
			}

			StateManager->ForceGotoState(State);

			return true;
		}
	}

	return false;
}

bool AWRInventoryPhysical::EjectCasingAnimNotify_Implementation()
{
	UAEState * State = StateManager->GetCurrentState();

	if (State)
	{
		UWREquippableState * WREState = Cast<UWREquippableState>(State);

		if (WREState)
		{
			return WREState->EjectCasingAnimNotify();
		}
	}

	return false;
}

//AWRShellCasing * AWRInventoryItemEquippable::EjectCasing_Implementation(TSubclassOf<AWRShellCasing> CasingClass, FVector RelativeVelocity, FName CasingSpawnSocket)
//{
//	//TODO: check quality settings and other checks to make sure we're not creating too many shell casings
//	if (Prop && Prop->Mesh)
//	{
//		FTransform SpawnTransform = Prop->Mesh->GetSocketTransform(CasingSpawnSocket);
//
//		//add some slight variation to the rotation
//		SpawnTransform.ConcatenateRotation(FRotator(FMath::FRandRange(-2.f, 2.f), FMath::FRandRange(-2.f, 2.f), FMath::FRandRange(-2.f, 2.f)).Quaternion());
//
//		//add some slight variation the the velocity
//		RelativeVelocity += FMath::VRand() * FMath::FRandRange(.25f, 3.f);
//
//		FActorSpawnParameters Params;
//		Params.Instigator = this->GetInstigator();
//		Params.Owner = Params.Instigator;
//
//		//Make the shell casing fly out with the velocity relative to the moving prop
//		FVector PropVelocity;
//
//		if (Prop->GetIsAttached())
//		{
//			PropVelocity = Prop->GetAttachParentActor()->GetVelocity();
//		}
//		else
//		{
//			PropVelocity = Prop->GetVelocity();
//		}
//
//		AWRShellCasing * Casing = GetWorld()->SpawnActor<AWRShellCasing>(CasingClass, SpawnTransform, Params);
//		Casing->Mesh->SetAllPhysicsLinearVelocity(PropVelocity + SpawnTransform.GetRotation() * RelativeVelocity);
//
//		return Casing;
//	}
//
//	return NULL;
//}

///////////////////
//Projectile

FTransform AWRInventoryPhysical::GetProjectileSpawnLocationFromProp_Implementation(const FVector& SpawnOffset) const
{
	if (Prop)
	{
		FTransform SpawnWorldTransform = Prop->GetActorTransform();

		SpawnWorldTransform = GetDefaultProjectileSpawnOffsetFromRoot() * SpawnWorldTransform;
		SpawnWorldTransform = FTransform(SpawnOffset) * SpawnWorldTransform;

		//DrawDebugPoint(
		//	GetWorld(),
		//	SpawnWorldTransform.GetLocation(),
		//	10,  					//size
		//	FColor::Red,
		//	false, 20.f);
		
		return SpawnWorldTransform;
	}

	return FTransform::Identity;
}

TArray<AWRProjectile *> AWRInventoryPhysical::ShootProjectileFromProp_Implementation(TSubclassOf<AWRProjectile> ProjectileClass,
	const FVector& SpawnOffset,
	float RandomConeHalfAngleDegrees,
	int32 MinShots,
	int32 MaxShots)
{
	UWRCharacterHandControlProp * HandControlProp = Cast<UWRCharacterHandControlProp>(HandControl);

	if (HandControlProp)
	{
		return HandControlProp->ShootProjectileFromProp(ProjectileClass, SpawnOffset, RandomConeHalfAngleDegrees, MinShots, MaxShots);
	}
	else 
	{
		TArray<AWRProjectile *> Res;

		if (Prop)
		{
			FTransform SpawnWorldTransform = GetProjectileSpawnLocationFromProp(SpawnOffset);

			UAEGameplayStatics::PerformActionRandomTimes(MinShots, MaxShots, [&SpawnWorldTransform, 
				RandomConeHalfAngleDegrees, 
				ProjectileClass, 
				this,
				&Res]
				(int32 TimeNumber)
			{
				Res.Add(UWRGameplayStatics::ShootProjectile(ProjectileClass, UAEGameplayStatics::ApplyRandomConeToTransform(SpawnWorldTransform, RandomConeHalfAngleDegrees), Prop));
			});
		}

		return Res;
	}
}

TArray<FHitResult> AWRInventoryPhysical::ShootSingleHitscanTraceFromProp_Implementation(TSubclassOf<UWRDamageType> DamageType,
	TSubclassOf<UWRImpactType> ImpactType,
	const FVector& SpawnOffset,
	float RandomConeHalfAngleDegrees,
	int32 MinShots,
	int32 MaxShots,
	float DamageMultiplier,
	float ExplosionRadiusMultiplier,
	float MaxDistance,
	bool bDebugDraw)
{
	UWRCharacterHandControlProp * HandControlProp = Cast<UWRCharacterHandControlProp>(HandControl);

	if (HandControlProp)
	{
		return HandControlProp->ShootSingleHitscanTraceFromProp(DamageType, 
			ImpactType, 
			SpawnOffset, 
			RandomConeHalfAngleDegrees,
			MinShots,
			MaxShots,
			DamageMultiplier,
			ExplosionRadiusMultiplier,
			MaxDistance,
			bDebugDraw);
	}
	else
	{
		TArray<FHitResult> Res;

		if (Prop)
		{
			FTransform SpawnWorldTransform = GetProjectileSpawnLocationFromProp(SpawnOffset);

			UAEGameplayStatics::PerformActionRandomTimes(MinShots, MaxShots, [&SpawnWorldTransform,
				DamageType,
				ImpactType,
				RandomConeHalfAngleDegrees,
				this,
				&Res,
				DamageMultiplier,
				ExplosionRadiusMultiplier,
				MaxDistance,
				bDebugDraw]
				(int32 TimeNumber)
			{
				FHitResult HitResult;

				UWRGameplayStatics::ShootSingleHitscanTrace(HitResult,
					UAEGameplayStatics::ApplyRandomConeToTransform(SpawnWorldTransform, RandomConeHalfAngleDegrees),
					DamageType,
					ImpactType,
					Prop,
					DamageMultiplier,
					ExplosionRadiusMultiplier,
					MaxDistance,
					Prop->GetWorld(),
					bDebugDraw);

				Res.Add(HitResult);
			});
		}

		return Res;
	}
}

///////////////////
//reload

void AWRInventoryPhysical::Reload_Implementation(int32 ReloadMode)
{
	if (PendingReloadMode.IsValidIndex(ReloadMode) && !PendingReloadMode[ReloadMode])
	{
		PendingReloadMode[ReloadMode] = true;
		TryReloadMode(ReloadMode);
	}
}

bool AWRInventoryPhysical::TryGotoReload_Implementation()
{
	//check if we should reload
	for (int32 ReloadModeIndex = 0; ReloadModeIndex < PendingReloadMode.Num(); ++ReloadModeIndex)
	{
		if (PendingReloadMode[ReloadModeIndex])
		{
			if (TryReloadMode(ReloadModeIndex))
			{
				return true;
			}
		}
	}

	return false;
}

bool AWRInventoryPhysical::TryReloadMode_Implementation(int32 ReloadMode)
{
	if (ReloadUnloadStateClass.IsValidIndex(ReloadMode))
	{
		UAEState * State = StateManager->GetStateForClass(ReloadUnloadStateClass[ReloadMode].ReloadStateClass);

		if (State && StateManager->AllowInterruptionByState(State))
		{
			PendingReloadMode[ReloadMode] = false;
			PendingUnloadMode[ReloadMode] = false;

			if (State->GetClass()->ImplementsInterface(UWREquippableStateReloadInterface::StaticClass())
				&& !IWREquippableStateReloadInterface::Execute_CheckCanReload(State))
			{
				return false;
			}

			StateManager->ForceGotoState(State);

			return true;
		}
	}

	return false;
}

///////////////////
//unload

void AWRInventoryPhysical::Unload_Implementation(int32 UnloadMode)
{
	if (PendingUnloadMode.IsValidIndex(UnloadMode) && !PendingUnloadMode[UnloadMode])
	{
		PendingUnloadMode[UnloadMode] = true;
		TryReloadMode(UnloadMode);
	}
}

bool AWRInventoryPhysical::TryGotoUnload_Implementation()
{
	//check if we should unload
	for (int32 UnloadModeIndex = 0; UnloadModeIndex < PendingUnloadMode.Num(); ++UnloadModeIndex)
	{
		if (PendingUnloadMode[UnloadModeIndex])
		{
			if (TryUnloadMode(UnloadModeIndex))
			{
				return true;
			}
		}
	}

	return false;
}

bool AWRInventoryPhysical::TryUnloadMode_Implementation(int32 UnloadMode)
{
	if (ReloadUnloadStateClass.IsValidIndex(UnloadMode))
	{
		UAEState * State = StateManager->GetStateForClass(ReloadUnloadStateClass[UnloadMode].UnloadStateClass);

		if (State && StateManager->AllowInterruptionByState(State))
		{
			PendingReloadMode[UnloadMode] = false;
			PendingUnloadMode[UnloadMode] = false;

			if (State->GetClass()->ImplementsInterface(UWREquippableStateUnloadInterface::StaticClass())
				&& !IWREquippableStateUnloadInterface::Execute_CheckCanUnload(State))
			{
				return false;
			}

			StateManager->ForceGotoState(State);

			return true;
		}
	}

	return false;
}

//////////////////
//idle

void AWRInventoryPhysical::TryGotoIdle_Implementation()
{
	if (TryGotoUnequip())
	{
		return;
	}

	if (TryGotoUse())
	{
		return;
	}

	if (TryGotoUnload())
	{
		return;
	}

	if (TryGotoReload())
	{
		return;
	}

	StateManager->TryGotoState(StateManager->GetStateForClass(IdleStateClass));
}
#include "Wraith.h"
#include "WRInventory.h"
#include "WRInventoryManager.h"

void UWRInventoryManager::AddInventory(AWRInventory * Inventory)
{
	if (IsInInventory(Inventory))
	{
		return;
	}

	check(!Inventory->GetOwner());

	InventoryItems.Add(Inventory);
	Inventory->Added(this);
}

void UWRInventoryManager::RemoveInventory(AWRInventory * Inventory)
{
	if (!IsInInventory(Inventory))
	{
		return;
	}

	InventoryItems.Remove(Inventory);
	Inventory->Removed();
}

bool UWRInventoryManager::IsInInventory(AWRInventory * Inventory)
{
	if (Inventory->GetOwnerInventoryManager() == this)
	{
		check(InventoryItems.Contains(Inventory));
		return true;
	}
	else
	{
		check(!InventoryItems.Contains(Inventory));
		return false;
	}
}

void UWRInventoryManager::DiscardAllInventory()
{
	for (int32 InvItem = 0; InvItem < InventoryItems.Num(); ++InvItem)
	{
		InventoryItems[InvItem]->Removed();
	}

	InventoryItems.SetNum(0);
}

AWRInventory * UWRInventoryManager::K2_FindInventoryType(TSubclassOf<AWRInventory> Type, bool bExactClass) const
{
	for (int32 InvItem = 0; InvItem < InventoryItems.Num(); ++InvItem)
	{
		AWRInventory * Inventory = InventoryItems[InvItem];

		if (bExactClass ? (Inventory->GetClass() == Type) : Inventory->IsA(Type))
		{
			return Inventory;
		}
	}

	return NULL;
}
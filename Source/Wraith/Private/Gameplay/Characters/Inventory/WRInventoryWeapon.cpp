#include "Wraith.h"
#include "WRInventoryWeapon.h"
#include "WRInventoryManager.h"
#include "WRHotbarManager.h"
#include "WRCharacterHandControlWeapon.h"

AWRInventoryWeapon::AWRInventoryWeapon(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	HandControlClass = UWRCharacterHandControlWeapon::StaticClass();
}

void AWRInventoryWeapon::Added_Implementation(UWRInventoryManager * InOwningManager)
{
	Super::Added_Implementation(InOwningManager);

	//auto add weapon to player's hotbar
	if (InOwningManager->HotbarManager)
	{
		//AWRPlayerController * WRPC = Cast<AWRPlayerController>(WRCH->GetController());

		//TODO: uncomment this
		//if (WRPC && WRPC->bAutoAssignWeaponsToHotbar*/)
		{
			InOwningManager->HotbarManager->AutoAddToHotbar(this);
		}
	}
}
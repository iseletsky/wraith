#include "Wraith.h"
#include "WREquippableStateUnequip.h"

void UWREquippableStateUnequip::OnDoneUnequip_Implementation()
{
	GetEquippable()->OnDoneUnequip();
}
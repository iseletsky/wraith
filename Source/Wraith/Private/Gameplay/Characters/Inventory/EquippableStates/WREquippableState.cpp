#include "Wraith.h"
#include "Animation/AnimInstance.h"
#include "WREquippableState.h"
#include "WREquippableStateUnequip.h"

////////////////////////////////////
//State

bool UWREquippableState::AllowInterruptionByState_Implementation(UAEState * State)
{
	if (State->IsA(UWREquippableStateUnequip::StaticClass()))
	{
		return bAllowUnequipInterrupt;
	}

	return false;
}

void UWREquippableState::OnEnd_Implementation(UAEState * NextState)
{
	Super::OnEnd_Implementation(NextState);
	StopMontages();
}

////////////////////////////////////
//Equippable

void UWREquippableState::TryGotoIdle_Implementation()
{
	BecomeInactive();

	//go to the idle state
	GetEquippable()->TryGotoIdle();
}

////////////////////////////////////
//Effects

void UWREquippableState::PlayEffects(float Duration, const FWREquippableStatePlayEffectsArgs& InEffectArgs)
{
	check(GetIsActive());

	UAnimMontage * CharacterMontage = NULL;
	UAnimMontage * PropMontage = NULL;

	//if arrays of animations have matching number of animations, sync up the random index
	if (InEffectArgs.CharacterAnimation.Montage.Num() && InEffectArgs.CharacterAnimation.Montage.Num() == InEffectArgs.PropAnimation.Montage.Num())
	{
		int32 RandomInd = FMath::RandHelper(InEffectArgs.CharacterAnimation.Montage.Num());

		CharacterMontage = InEffectArgs.CharacterAnimation.Montage[RandomInd];
		PropMontage = InEffectArgs.PropAnimation.Montage[RandomInd];
	}
	else
	{
		if (InEffectArgs.CharacterAnimation.Montage.Num())
		{
			CharacterMontage = InEffectArgs.CharacterAnimation.Montage[FMath::RandHelper(InEffectArgs.CharacterAnimation.Montage.Num())];
		}

		if (InEffectArgs.PropAnimation.Montage.Num())
		{
			PropMontage = InEffectArgs.PropAnimation.Montage[FMath::RandHelper(InEffectArgs.PropAnimation.Montage.Num())];
		}
	}

	//TODO: handle syncing of attached prop random animations too

	//character anim
	if (CharacterMontage)
	{
		if (InEffectArgs.CharacterAnimation.bMontageMatchesDuration && Duration > 0.f)
		{
			PlayCharacterMontageWithDuration(CharacterMontage, Duration, InEffectArgs.CharacterAnimation.bAccountForBlendoutTime, InEffectArgs.CharacterAnimation.bMontageStopOnStateEnd);
		}
		else
		{
			PlayCharacterMontage(CharacterMontage, 1.f, InEffectArgs.CharacterAnimation.bMontageStopOnStateEnd);
		}
	}

	//prop anim
	if (PropMontage)
	{
		if (InEffectArgs.PropAnimation.bMontageMatchesDuration && Duration > 0.f)
		{
			PlayPropMontageWithDuration(PropMontage, Duration, InEffectArgs.CharacterAnimation.bAccountForBlendoutTime, InEffectArgs.PropAnimation.bMontageStopOnStateEnd);
		}
		else
		{
			PlayPropMontage(PropMontage, 1.f, InEffectArgs.PropAnimation.bMontageStopOnStateEnd);
		}
	}

	//sound
	//UGameplayStatics::

	//TODO: all the other things like sounds, camera animations, etc...
}

void UWREquippableState::PlayEffectsAndDoAction(float Duration, const FWREquippableStatePlayEffectsArgs& InEffectArgs, FTimerDynamicDelegate InDelegate)
{
	PlayEffects(Duration, InEffectArgs);

	if (Duration > 0.f)
	{
		FTimerHandle TimerHandle;

		GetWorld()->GetTimerManager().SetTimer(TimerHandle, InDelegate, Duration, false);
	}
	else
	{
		InDelegate.ExecuteIfBound();
	}
}

void UWREquippableState::PlayEffectsAndDoAction(float Duration, const FWREquippableStatePlayEffectsArgs& InEffectArgs, FTimerDelegate const& InDelegate)
{
	PlayEffects(Duration, InEffectArgs);

	if (Duration > 0.f)
	{
		FTimerHandle TimerHandle;

		GetWorld()->GetTimerManager().SetTimer(TimerHandle, InDelegate, Duration, false);
	}
	else
	{
		InDelegate.ExecuteIfBound();
	}
}

void UWREquippableState::PlayEffectsAndGotoState(float Duration, const FWREquippableStatePlayEffectsArgs& InEffectArgs, TSubclassOf<UAEState> StateClass)
{
	PlayEffectsAndDoAction(Duration, InEffectArgs,
		StateClass
		? FTimerDelegate::CreateUObject(this, &UWREquippableState::GotoState, StateClass)
		: FTimerDelegate::CreateUObject(this, &UWREquippableState::TryGotoIdle));
}

////////////////////////////////////
//Animation

void UWREquippableState::PlayMontageWithDuration(UAnimMontage * Montage, float Duration, UAnimInstance * AnimInstance, bool bAccountForBlendoutTime, bool bStopOnStateEnd)
{
	FAnimMontageInstance * AnimMontageInstance = ::PlayMontageWithDuration(Montage, AnimInstance, Duration, bAccountForBlendoutTime);

	if (bStopOnStateEnd && AnimMontageInstance)
	{
		SetupAnimMontageStopOnStateEnd(AnimInstance, AnimMontageInstance);
	}
}

void UWREquippableState::PlayMontage(UAnimMontage * Montage, UAnimInstance * AnimInstance, float Rate, bool bStopOnStateEnd)
{
	FAnimMontageInstance * AnimMontageInstance = ::PlayMontage(Montage, AnimInstance, Rate);

	if (bStopOnStateEnd && AnimMontageInstance)
	{
		SetupAnimMontageStopOnStateEnd(AnimInstance, AnimMontageInstance);
	}
}

void UWREquippableState::SetupAnimMontageStopOnStateEnd(UAnimInstance * AnimInstance, FAnimMontageInstance * AnimMontageInstance)
{
	if (AnimMontageInstance) {
		MontagesToStopOnStateEnd.Add(AnimInstance, AnimMontageInstance);

		AnimMontageInstance->OnMontageBlendingOutStarted.BindLambda([&MontagesToStopOnStateEnd = this->MontagesToStopOnStateEnd, AnimInstance, AnimMontageInstance](UAnimMontage * Montage, bool bInterrupted)
		{
			check(MontagesToStopOnStateEnd.FindPair(AnimInstance, AnimMontageInstance));
			AnimMontageInstance->OnMontageBlendingOutStarted.Unbind();
			MontagesToStopOnStateEnd.Remove(AnimInstance, AnimMontageInstance);
		});
	}
}

void UWREquippableState::StopMontages()
{
	for (auto& Elem : MontagesToStopOnStateEnd)
	{
		UAnimInstance * AnimInstance = Elem.Key;
		FAnimMontageInstance * AnimMontageInstance = Elem.Value;

		//At the moment they choose not to expose the AnimMontageInstance stop method with ENGINE_API to the outside world, so stop the montage in this convoluted way instead
		//AnimMontageInstance->Stop(AnimMontageInstance->Montage->BlendOut.GetBlendTime());

		FAnimMontageInstance * ActiveMontageInstance = AnimInstance->GetActiveInstanceForMontage(AnimMontageInstance->Montage);

		//checking if the stored AnimMontageInstace is the actively playing one
		if (AnimMontageInstance == ActiveMontageInstance)
		{
			AnimInstance->Montage_Stop(AnimMontageInstance->Montage->BlendOut.GetBlendTime(), AnimMontageInstance->Montage);
		}
	}

	MontagesToStopOnStateEnd.Empty();
}

////////////////////////////////////
//Animation Event

bool UWREquippableState::EjectCasingAnimNotify_Implementation()
{
	return false;
}

bool UWREquippableState::AttachedPropToCharacterAnimNotify_Implementation()
{
	return false;
}

bool UWREquippableState::AttachedPropToPropAnimNotify_Implementation()
{
	return false;
}

bool UWREquippableState::ShowAttachedPropAnimNotify_Implementation()
{
	return false;
}

bool UWREquippableState::HideAttachedPropAnimNotify_Implementation()
{
	return false;
}
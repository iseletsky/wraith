#include "Wraith.h"
#include "WREquippableStateEquip.h"
#include "WREquippableStateUnequip.h"

UWREquippableStateEquip::UWREquippableStateEquip(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bAllowUnequipInterrupt = true;
}

void UWREquippableStateEquip::OnBegin_Implementation(UAEState * PreviousState)
{
    Super::OnBegin_Implementation(PreviousState);

	AWRInventoryPhysical * Equippable = GetEquippable();

	if (Equippable)
	{
		Equippable->bIdleAnimationEnabled = false;
	}

	AWRPickupInventory * Prop = GetEquippableProp();

	if (Prop)
	{
		Prop->bIdleAnimationEnabled = false;
	}
}

void UWREquippableStateEquip::OnEnd_Implementation(UAEState * NextState)
{
    Super::OnEnd_Implementation(NextState);

	AWRInventoryPhysical * Equippable = GetEquippable();

	if (Equippable)
	{
		Equippable->bIdleAnimationEnabled = true;
	}

	AWRPickupInventory * Prop = GetEquippableProp();

	if (Prop)
	{
		Prop->bIdleAnimationEnabled = true;
	}
}
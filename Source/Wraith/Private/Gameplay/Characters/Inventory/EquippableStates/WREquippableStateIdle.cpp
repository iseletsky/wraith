#include "Wraith.h"
#include "WREquippableStateIdle.h"
#include "WRCharacterHandControlProp.h"

bool UWREquippableStateIdle::AllowInterruptionByState_Implementation(UAEState * State)
{
	return true;
}

void UWREquippableStateIdle::OnBegin_Implementation(UAEState * PreviousState)
{
    Super::OnBegin_Implementation(PreviousState);

	UWRCharacterHandControlProp * HandControl = Cast<UWRCharacterHandControlProp>(GetHandControl());

	if (HandControl)
	{
		HandControl->EnableIdleHandsAnimation();
		HandControl->EnableWalkingHandsSwayOffset();
	}
}

void UWREquippableStateIdle::OnEnd_Implementation(UAEState * NextState)
{
    Super::OnEnd_Implementation(NextState);

	UWRCharacterHandControlProp * HandControl = Cast<UWRCharacterHandControlProp>(GetHandControl());

	if (HandControl)
	{
		HandControl->DisableIdleHandsAnimation();
		HandControl->DisableWalkingHandsSwayOffset();
	}
}
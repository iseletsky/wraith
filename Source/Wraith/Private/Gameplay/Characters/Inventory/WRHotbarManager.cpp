#include "Wraith.h"
#include "WREquippableManager.h"
#include "WRHotbarManager.h"

UWRHotbarManager::UWRHotbarManager(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	DefaultMaxHotbarSlots = 10;
}

void UWRHotbarManager::OnRegister()
{
	Super::OnRegister();
	SetNumHotbarSlots(DefaultMaxHotbarSlots);
}

void UWRHotbarManager::AssignToHotbarSlot_Implementation(AWRInventoryPhysical * Item, int32 Slot)
{
	check(Slot > 0 && Slot <= HotbarItems.Num());

	Slot--;

	HotbarItems[Slot] = Item;

	//TODO: set up HUD widgets
}

void UWRHotbarManager::AutoAddToHotbar_Implementation(AWRInventoryPhysical * Item)
{
	for (int32 HotbarInd = 0; HotbarInd < HotbarItems.Num(); ++HotbarInd)
	{
		if (!HotbarItems[HotbarInd])
		{
			AssignToHotbarSlot(Item, HotbarInd + 1);
			return;
		}
	}
}

void UWRHotbarManager::HotbarUse_Implementation(int32 Slot)
{	
	AWRInventoryPhysical * Inventory = GetAtHotbarSlot(Slot);

	//TODO: Some items should just be quick used instead of equipped by default, like a medkit
	//Update SwitchEquippable to be able to quick use objects and track what was last equipped to quickly switch back

	if (EquippableManager)
	{
		EquippableManager->SwitchEquippable(Inventory);
	}

	/*if (Inventory)
	{
		
	}*/
}

void UWRHotbarManager::SetNumHotbarSlots_Implementation(int32 Slots)
{
	//TODO: Remove HUD widgets
	/*if (Slots < HotbarItems.Num())
	{
		
	}*/

	HotbarItems.SetNumZeroed(Slots);

	//TODO: Update HUD
}

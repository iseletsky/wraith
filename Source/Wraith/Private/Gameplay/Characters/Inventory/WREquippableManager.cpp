#include "Wraith.h"
#include "WREquippableManager.h"
#include "WRCharacterHandControl.h"
#include "WRInventoryManager.h"
#include "WRInventoryPhysical.h"

void UWREquippableManager::SwitchEquippable_Implementation(AWRInventoryPhysical* NewEquippable)
{
	if (NewEquippable != CurrentEquippable /*&& !IsDead()*/)
	{
		PendingEquippable = NewEquippable;

		if (!CurrentEquippable)
		{
			EquipPendingEquippable();
		}
		else
		{
			CurrentEquippable->Unequip();
		}
	}
}

void UWREquippableManager::EquipPendingEquippable_Implementation()
{
	//destroy the current equippable hand control
	if (DominantHandEquippableControl)
	{
		if (CurrentEquippable && CurrentEquippable->HandControl == DominantHandEquippableControl)
		{
			CurrentEquippable->HandControl = NULL;
		}

		DominantHandEquippableControl->DestroyComponent();
		DominantHandEquippableControl = NULL;
	}

	if (PendingEquippable && (!InventoryManager || (InventoryManager && InventoryManager->IsInInventory(PendingEquippable))))
	{
		CurrentEquippable = PendingEquippable;
		
		CurrentEquippable->EquippableManager = this;
		DominantHandEquippableControl = CurrentEquippable->CreateHandControlForEquippableManager();
		CurrentEquippable->Equip();
	}
	else
	{
		CurrentEquippable = NULL;
	}

	PendingEquippable = NULL;
}

void UWREquippableManager::EngageUse_Implementation(int32 UseMode)
{
	if (CurrentEquippable)
	{
		CurrentEquippable->EngageUse(UseMode);
	}
}

void UWREquippableManager::DisengageUse_Implementation(int32 UseMode)
{
	if (CurrentEquippable)
	{
		CurrentEquippable->DisengageUse(UseMode);
	}
}

void UWREquippableManager::Reload_Implementation(int32 ReloadMode)
{
	if (CurrentEquippable)
	{
		CurrentEquippable->Reload(ReloadMode);
	}
}

void UWREquippableManager::Unload_Implementation(int32 UnloadMode)
{
	if (CurrentEquippable)
	{
		CurrentEquippable->Unload(UnloadMode);
	}
}

///////////////////////////////////////
//Animation

UAnimSequence * UWREquippableManager::GetNonDominantHandPose_Implementation() const
{
	if (CurrentEquippable)
	{
		return CurrentEquippable->GetNonDominantHandPose();
	}

	return NULL;
}

UAnimSequence * UWREquippableManager::GetDominantHandPose_Implementation() const
{
	if (CurrentEquippable)
	{
		return CurrentEquippable->GetDominantHandPose();
	}

	return NULL;
}

UAnimSequence * UWREquippableManager::GetIdleAnimation_Implementation() const
{
	if (CurrentEquippable)
	{
		return CurrentEquippable->GetIdleAnimation();
	}

	return NULL;
}

USceneComponent * UWREquippableManager::GetNonDominantHandAttachIKComponent_Implementation() const
{
	if (CurrentEquippable)
	{
		return CurrentEquippable->GetNonDominantHandAttachIKComponent();
	}

	return NULL;
}

FName UWREquippableManager::GetNonDominantHandAttachIKComponentSocket_Implementation() const
{
	if (CurrentEquippable)
	{
		return CurrentEquippable->GetNonDominantHandAttachIKComponentSocket();
	}

	return NAME_None;
}

///////////////////////////////////////
//Animation Event

bool UWREquippableManager::EjectCasingAnimNotify_Implementation()
{
	if (CurrentEquippable)
	{
		return CurrentEquippable->EjectCasingAnimNotify();
	}

	return false;
}

bool UWREquippableManager::AttachedPropToCharacterAnimNotify_Implementation()
{
	if (CurrentEquippable)
	{
		return CurrentEquippable->AttachedPropToCharacterAnimNotify();
	}

	return false;
}

bool UWREquippableManager::AttachedPropToPropAnimNotify_Implementation()
{
	if (CurrentEquippable)
	{
		return CurrentEquippable->AttachedPropToPropAnimNotify();
	}

	return false;
}

bool UWREquippableManager::ShowAttachedPropAnimNotify_Implementation()
{
	if (CurrentEquippable)
	{
		return CurrentEquippable->ShowAttachedPropAnimNotify();
	}

	return false;
}

bool UWREquippableManager::HideAttachedPropAnimNotify_Implementation()
{
	if (CurrentEquippable)
	{
		return CurrentEquippable->HideAttachedPropAnimNotify();
	}

	return false;
}
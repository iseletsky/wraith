#include "Wraith.h"
#include "WRInventory.h"
#include "WRInventoryManager.h"

AWRInventory::AWRInventory(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SetReplicates(true);
	bOnlyRelevantToOwner = true;

	//set up some of the defaults similar to AInfo, override CanActorTick in subclasses if needed.
	bCanBeDamaged = false;
	bHidden = true;
	PrimaryActorTick.bCanEverTick = false;
}

void AWRInventory::Destroyed()
{
	if (OwnerInventoryManager)
	{
		OwnerInventoryManager->RemoveInventory(this);
	}
	GetWorldTimerManager().ClearAllTimersForObject(this);

	Super::Destroyed();
}

void AWRInventory::Added_Implementation(UWRInventoryManager * InOwningManager)
{
	check(!GetOwnerInventoryManager());
	check(InOwningManager);
	
	OwnerInventoryManager = InOwningManager;
	SetOwner(InOwningManager->GetOwner());
	Instigator = Cast<APawn>(GetOwner());	

	PrimaryActorTick.AddPrerequisite(GetOwner(), GetOwner()->PrimaryActorTick);
}

void AWRInventory::Removed_Implementation()
{
	if (GetOwner())
	{
		PrimaryActorTick.RemovePrerequisite(GetOwner(), GetOwner()->PrimaryActorTick);
	}
	
	Instigator = NULL;
	SetOwner(NULL);
	OwnerInventoryManager = NULL;
}

void AWRInventory::SetStackCount_Implementation(int32 InStackCount)
{
	if (bIsStackable)
	{
		StackCount = InStackCount;
	}
}

#include "Wraith.h"
#include "WRCharacterHumanoid.h"
#include "WRInventoryManager.h"
#include "WREquippableManager.h"
#include "WRHotbarManager.h"
#include "WRCharacterHandControlWeapon.h"

AWRCharacterHumanoid::AWRCharacterHumanoid(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//Hotbar Manager
	HotbarManager = ObjectInitializer.CreateDefaultSubobject<UWRHotbarManager>(this, TEXT("HotbarManager"));

	//Equippable Manager
	EquippableManager = ObjectInitializer.CreateDefaultSubobject<UWREquippableManager>(this, TEXT("EquippableManager"));

	EquippableManager->InventoryManager = InventoryManager;
	InventoryManager->HotbarManager = HotbarManager;
	HotbarManager->EquippableManager = EquippableManager;

	//Head Mesh
	HeadMesh = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("HeadMesh"));

	//TODO: (ilselets) Make this a convenience method for setting up any mesh part of a character
	HeadMesh->SetupAttachment(GetMesh());
	HeadMesh->SetMasterPoseComponent(GetMesh());
	HeadMesh->SetOwnerNoSee(true);
	HeadMesh->bCastHiddenShadow = true;
	HeadMesh->AlwaysLoadOnClient = true;
	HeadMesh->AlwaysLoadOnServer = true;
	HeadMesh->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::AlwaysTickPose;
	HeadMesh->bCastDynamicShadow = true;
	HeadMesh->bAffectDynamicIndirectLighting = true;
	HeadMesh->PrimaryComponentTick.TickGroup = TG_PrePhysics;
	static FName MeshCollisionProfileName(TEXT("CharacterMesh"));
	HeadMesh->SetCollisionProfileName(MeshCollisionProfileName);
	HeadMesh->bGenerateOverlapEvents = false;
	HeadMesh->SetCanEverAffectNavigation(false);
}

void AWRCharacterHumanoid::SetupPlayerInputComponent(UInputComponent * PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	SetupEquippableInput(PlayerInputComponent);
	SetupHotbarInput(PlayerInputComponent);
}

void AWRCharacterHumanoid::OnLoseControl_Implementation()
{
	Super::OnLoseControl_Implementation();

	OnStopFire();
	OnStopAltFire();
}

void AWRCharacterHumanoid::FaceRotation(FRotator NewControlRotation, float DeltaTime)
{
	Super::FaceRotation(NewControlRotation, DeltaTime);

	UWRCharacterHandControlProp * HandControlProp = Cast<UWRCharacterHandControlProp>(EquippableManager->DominantHandEquippableControl);

	if (HandControlProp)
	{
		HandControlProp->UpdateLazyGunCharacterAim(NewControlRotation, DeltaTime);
	}
}

void AWRCharacterHumanoid::TakeFallingDamage_Implementation(const FHitResult& Hit, float FallingSpeed, float FallingDamage)
{
	Super::TakeFallingDamage_Implementation(Hit, FallingSpeed, FallingDamage);

	UWRCharacterHandControlProp * CharacterHandControlProp = Cast<UWRCharacterHandControlProp>(EquippableManager->DominantHandEquippableControl);

	if (CharacterHandControlProp)
	{
		CharacterHandControlProp->CharacterLanded(FallingDamage);
	}
}

void AWRCharacterHumanoid::Jumped_Implementation()
{
	Super::Jumped_Implementation();

	UWRCharacterHandControlProp * CharacterHandControlProp = Cast<UWRCharacterHandControlProp>(EquippableManager->DominantHandEquippableControl);

	if (CharacterHandControlProp)
	{
		CharacterHandControlProp->CharacterJumped();
	}
}

UWRCharacterStanceData * AWRCharacterHumanoid::GetCharacterStance_Implementation() const
{
	return EquippableManager->CurrentEquippable 
		? EquippableManager->CurrentEquippable->CharacterStance 
		: Super::GetCharacterStance_Implementation();
}

bool AWRCharacterHumanoid::Setup1P_Implementation()
{
	if (Super::Setup1P_Implementation())
	{
		HeadMesh->SetOwnerNoSee(true);
		return true;
	}

	return false;
}

bool AWRCharacterHumanoid::Setup3P_Implementation()
{
	if (Super::Setup3P_Implementation())
	{
		HeadMesh->SetOwnerNoSee(false);
		return true;
	}

	return false;
}

/////////////////////////////////////// 
//Equippable

void AWRCharacterHumanoid::SetupEquippableInput_Implementation(UInputComponent * PlayerInputComponent)
{
	PlayerInputComponent->BindAction("StartFire", IE_Pressed, this, &AWRCharacterHumanoid::OnFire);
	PlayerInputComponent->BindAction("StopFire", IE_Released, this, &AWRCharacterHumanoid::OnStopFire);
	PlayerInputComponent->BindAction("StartAltFire", IE_Pressed, this, &AWRCharacterHumanoid::OnAltFire);
	PlayerInputComponent->BindAction("StopAltFire", IE_Released, this, &AWRCharacterHumanoid::OnStopAltFire);

	//TODO: later support unload by holding reload long enough
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AWRCharacterHumanoid::Reload);
	PlayerInputComponent->BindAction("Unload", IE_Pressed, this, &AWRCharacterHumanoid::Unload);
}

void AWRCharacterHumanoid::EngageUse_Implementation(int32 UseMode)
{
	if (EquippableManager)
	{
		EquippableManager->EngageUse(UseMode);
	}
}

void AWRCharacterHumanoid::DisengageUse_Implementation(int32 UseMode)
{
	if (EquippableManager)
	{
		EquippableManager->DisengageUse(UseMode);
	}
}

void AWRCharacterHumanoid::Reload_Implementation()
{
	if (EquippableManager)
	{
		//reload key usually just engages all reload modes
		EquippableManager->Reload(0);
	}
}

void AWRCharacterHumanoid::Unload_Implementation()
{
	if (EquippableManager)
	{
		//reload key usually just engages all unload modes
		EquippableManager->Unload(0);
	}
}

void AWRCharacterHumanoid::OnFire_Implementation()
{
	EngageUse(0);
}

void AWRCharacterHumanoid::OnStopFire_Implementation()
{
	DisengageUse(0);
}

void AWRCharacterHumanoid::OnAltFire_Implementation()
{
	EngageUse(1);
}

void AWRCharacterHumanoid::OnStopAltFire_Implementation()
{
	DisengageUse(1);
}

FRotator AWRCharacterHumanoid::GetCurrentWeaponRecoilRotation_Implementation()
{
	if (EquippableManager)
	{
		UWRCharacterHandControlWeapon * HandControl = Cast<UWRCharacterHandControlWeapon>(EquippableManager->DominantHandEquippableControl);

		if (HandControl)
		{
			return HandControl->GetCurrentWeaponRecoilRotation();
		}
	}

	return FRotator::ZeroRotator;
}

/////////////////////////////////////// 
//Hotbar

void AWRCharacterHumanoid::SetupHotbarInput_Implementation(UInputComponent * PlayerInputComponent)
{
	PlayerInputComponent->BindAction("HotbarUse1", IE_Pressed, this, &AWRCharacterHumanoid::HotbarUse1);
	PlayerInputComponent->BindAction("HotbarUse2", IE_Pressed, this, &AWRCharacterHumanoid::HotbarUse2);
	PlayerInputComponent->BindAction("HotbarUse3", IE_Pressed, this, &AWRCharacterHumanoid::HotbarUse3);
	PlayerInputComponent->BindAction("HotbarUse4", IE_Pressed, this, &AWRCharacterHumanoid::HotbarUse4);
	PlayerInputComponent->BindAction("HotbarUse5", IE_Pressed, this, &AWRCharacterHumanoid::HotbarUse5);
	PlayerInputComponent->BindAction("HotbarUse6", IE_Pressed, this, &AWRCharacterHumanoid::HotbarUse6);
	PlayerInputComponent->BindAction("HotbarUse7", IE_Pressed, this, &AWRCharacterHumanoid::HotbarUse7);
	PlayerInputComponent->BindAction("HotbarUse8", IE_Pressed, this, &AWRCharacterHumanoid::HotbarUse8);
	PlayerInputComponent->BindAction("HotbarUse8", IE_Pressed, this, &AWRCharacterHumanoid::HotbarUse8);
	PlayerInputComponent->BindAction("HotbarUse9", IE_Pressed, this, &AWRCharacterHumanoid::HotbarUse9);
	PlayerInputComponent->BindAction("HotbarUse10", IE_Pressed, this, &AWRCharacterHumanoid::HotbarUse10);
}

void AWRCharacterHumanoid::HotbarUse_Implementation(int32 Slot)
{
	if (HotbarManager)
	{
		HotbarManager->HotbarUse(Slot);
	}
}

void AWRCharacterHumanoid::HotbarUse1_Implementation()
{
	HotbarUse(1);
}

void AWRCharacterHumanoid::HotbarUse2_Implementation()
{
	HotbarUse(2);
}

void AWRCharacterHumanoid::HotbarUse3_Implementation()
{
	HotbarUse(3);
}

void AWRCharacterHumanoid::HotbarUse4_Implementation()
{
	HotbarUse(4);
}

void AWRCharacterHumanoid::HotbarUse5_Implementation()
{
	HotbarUse(5);
}

void AWRCharacterHumanoid::HotbarUse6_Implementation()
{
	HotbarUse(6);
}

void AWRCharacterHumanoid::HotbarUse7_Implementation()
{
	HotbarUse(7);
}

void AWRCharacterHumanoid::HotbarUse8_Implementation()
{
	HotbarUse(8);
}

void AWRCharacterHumanoid::HotbarUse9_Implementation()
{
	HotbarUse(9);
}

void AWRCharacterHumanoid::HotbarUse10_Implementation()
{
	HotbarUse(10);
}

///////////////////////////////////////
//Head Control

const FVector FIRST_PERSON_NECK_EYES_OFFSET(-12.f, 0.f, -10.f);
const FVector FIRST_PERSON_PELVIS_EYES_OFFSET(-30.f, 0.f, -72.f);

FTransform AWRCharacterHumanoid::GetNeckWorldTransform_Implementation() const
{
	FTransform ActorTransform = GetActorTransform();

	FVector HeadWorldPosition = (GetRelativeEyesTransform() * ActorTransform).GetLocation();

	FRotator AimRot = ActorTransform.Rotator();
	AimRot.Yaw += GetAimOffsets().Yaw;

	FVector NeckWorldPosition = HeadWorldPosition + AimRot.RotateVector(FIRST_PERSON_NECK_EYES_OFFSET);

	return FTransform(AimRot, NeckWorldPosition);
}

FTransform AWRCharacterHumanoid::GetPelvisWorldTransform_Implementation() const
{
	FTransform NeckWorldTransform = GetNeckWorldTransform();
	FTransform PelvisRelativeTransform = FTransform(FRotator::ZeroRotator, FIRST_PERSON_PELVIS_EYES_OFFSET - FIRST_PERSON_NECK_EYES_OFFSET);

	return PelvisRelativeTransform * NeckWorldTransform;
}

///////////////////////////////////////
//Hand Control

FTransform AWRCharacterHumanoid::GetDominantHandWorldTransform_Implementation(bool bIncludeAnimationOnlyEffects) const
{
	//TODO: (ilselets) If there are multiple hand control components active, combine them with weights
	if (EquippableManager->DominantHandEquippableControl)
	{
		return EquippableManager->DominantHandEquippableControl->GetHandWorldTransform(bIncludeAnimationOnlyEffects);
	}

	return FTransform::Identity;
}

FTransform AWRCharacterHumanoid::GetNonDominantHandWorldTransform_Implementation() const
{
	return FTransform::Identity;
}

///////////////////////////////////////
//Animation

UAnimSequence * AWRCharacterHumanoid::GetNonDominantHandPose_Implementation() const
{
	if (EquippableManager)
	{
		return EquippableManager->GetNonDominantHandPose();
	}

	return NULL;
}

UAnimSequence * AWRCharacterHumanoid::GetDominantHandPose_Implementation() const
{
	if (EquippableManager)
	{
		return EquippableManager->GetDominantHandPose();
	}

	return NULL;
}

UAnimSequence * AWRCharacterHumanoid::GetIdleAnimation_Implementation() const
{
	if (EquippableManager)
	{
		return EquippableManager->GetIdleAnimation();
	}

	return NULL;
}

USceneComponent * AWRCharacterHumanoid::GetNonDominantHandAttachIKComponent_Implementation() const
{
	if (EquippableManager)
	{
		return EquippableManager->GetNonDominantHandAttachIKComponent();
	}

	return NULL;
}

FName AWRCharacterHumanoid::GetNonDominantHandAttachIKComponentSocket_Implementation() const
{
	if (EquippableManager)
	{
		return EquippableManager->GetNonDominantHandAttachIKComponentSocket();
	}

	return NAME_None;
}

///////////////////////////////////////
//Animation Event

bool AWRCharacterHumanoid::EjectCasingAnimNotify_Implementation()
{
	if (EquippableManager)
	{
		return EquippableManager->EjectCasingAnimNotify();
	}

	return false;
}

bool AWRCharacterHumanoid::AttachedPropToCharacterAnimNotify_Implementation()
{
	if (EquippableManager)
	{
		return EquippableManager->AttachedPropToCharacterAnimNotify();
	}

	return false;
}

bool AWRCharacterHumanoid::AttachedPropToPropAnimNotify_Implementation()
{
	if (EquippableManager)
	{
		return EquippableManager->AttachedPropToPropAnimNotify();
	}

	return false;
}

bool AWRCharacterHumanoid::ShowAttachedPropAnimNotify_Implementation()
{
	if (EquippableManager)
	{
		return EquippableManager->ShowAttachedPropAnimNotify();
	}

	return false;
}

bool AWRCharacterHumanoid::HideAttachedPropAnimNotify_Implementation()
{
	if (EquippableManager)
	{
		return EquippableManager->HideAttachedPropAnimNotify();
	}

	return false;
}
#ifndef __WRAITH_H__
#define __WRAITH_H__

#include "Engine.h"
#include "AEGameplayStatics.h"

DECLARE_LOG_CATEGORY_EXTERN(WR, Log, All);

//LG logs
DECLARE_LOG_CATEGORY_EXTERN(LG, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LG_VERBOSE, Log, Verbose);
DECLARE_LOG_CATEGORY_EXTERN(LG_RANDOM, Log, Verbose);

#define WR_LOCTEXT_NS_INVENTORY "Inventory"

#define COLLISION_PROJECTILE ECC_GameTraceChannel1
#define COLLISION_TRACE_WEAPON ECC_GameTraceChannel2
#define COLLISION_PROJECTILE_SHOOTABLE ECC_GameTraceChannel3
#define COLLISION_TELEPORTING_OBJECT ECC_GameTraceChannel4
#define COLLISION_PAWNOVERLAP ECC_GameTraceChannel5
#define COLLISION_TRACE_WEAPONNOCHARACTER ECC_GameTraceChannel6
#define COLLISION_TRACE_USABLE ECC_GameTraceChannel7
#define COLLISION_LG_AREA ECC_GameTraceChannel18

/** used during random level generator phase to check if areas about to be spawned overlap areas already created */
const FName LG_AREA_COLLISION_PROFILE("LgAreaCollision");

/** component should not spawn */
const FName COMPONENT_NO_SPAWN_TAG("NoSpawn");

/** 
Tells the random generator that this component will be used for area collision.
This component will be used for area collision to check if the newly spawning area intersects existing area collision geometry.
After the area is successfully laid out, this component will exist as area collision geometry for other areas to test against.
*/
const FName COMPONENT_AREA_COLLISION_TAG("AreaCollision");
/** 
Tells the random generator that this component will be used for area collision only during an area's lay out stage.
It will be deleted after the area is laid out successfully.
Use this for large areas when you don't really know the exact size of the final area yet, like areas composed of Area Spawn Connectors.
*/
const FName COMPONENT_AREA_SPAWN_COLLISION_TAG("AreaSpawnCollision");
/** 
Tells the random generator that this component will be used for area collision only to check if newly spawning areas intersect with it, it won't be used for intersection tests during the layout phase.
It stays around after the area is successfully laid out.
Use this on a component when it's completely encompassed by a component marked with AreaSpawnCollision to avoid needlessly checking intersections during layout.
*/
const FName COMPONENT_AREA_OMIT_SPAWN_COLLISION_TAG("AreaOmitSpawnCollision");
/**
Used with either AreaCollision or AreaOmitSpawnCollision.
Tells the random generator to omit this collision area object from checks only for areas that are attempting to branch off of this already existing area.
*/
const FName COMPONENT_AREA_OMIT_BRANCH_SPAWN_COLLISION_TAG("AreaOmitBranchSpawnCollision");

/** handy response params for world-only checks */
extern FCollisionResponseParams WorldResponseParams;

/** returns a grid cell in 3D space for a float vector and cell size */
FORCEINLINE FIntVector GridCellForPosition(const FVector& InVector, const FVector& CellSize)
{
	FVector FRes;
	FIntVector Res;

	for (int32 i = 0; i < 3; ++i)
	{
		FRes[i] = InVector[i] / CellSize[i];
	}

	//for some reason FIntVector doesn't have the [] operator, so do this manually
	Res.X = FMath::FloorToInt(FRes.X);
	Res.Y = FMath::FloorToInt(FRes.Y);
	Res.Z = FMath::FloorToInt(FRes.Z);

	return Res;
}

FORCEINLINE UClass* FindClass(const TCHAR* ClassName)
{
	check(ClassName);

	UObject* ClassPackage = ANY_PACKAGE;

	if (UClass* Result = FindObject<UClass>(ClassPackage, ClassName))
	{
		return Result;
	}

	if (UObjectRedirector* RenamedClassRedirector = FindObject<UObjectRedirector>(ClassPackage, ClassName))
	{
		return CastChecked<UClass>(RenamedClassRedirector->DestinationObject);
	}
		
	return NULL;
}

/** Predicate that returns true if a value is less than the value this predicate was constructed with */
struct LessThanPredicate
{
	LessThanPredicate(int32 InLessThanValue)
		: LessThanValue(InLessThanValue)
	{}

	int32 LessThanValue;

	bool operator()(const int32& Value) const
	{
		return Value < LessThanValue;
	}
};

#endif

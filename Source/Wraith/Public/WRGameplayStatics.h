#pragma once

#include "WRTeams.h"
#include "WRAIPotentialTargetInfo.h"
#include "WRGameplayStatics.generated.h"

class AWRProjectile;
class UWRDamageType;
class UWRImpactType;

/**
Has useful info about processing an array of perception stimuli
*/
USTRUCT(Blueprintable)
struct FWRStimulusProcessInfo
{
	GENERATED_USTRUCT_BODY()

	/**
	How certain is the best known locaiton based on stimuli.
	If there's a stronger well known position from another set of stimuli, it should take precedence.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
	float BestKnownLocationCertainty;

	/**
	The best known location of the target that can be deduced from the stimuli
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
	FVector BestKnownLocation;

	/**
	Whether or not the target is in line of sight and was perceived with visibility senses
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
	uint8 bIsInLineOfSight:1;

	/**
	Whether or not the target is making noise and was perceived with sound senses
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
	uint8 bIsMakingNoise:1;
};

UCLASS(CustomConstructor)
class WRAITH_API UWRGameplayStatics : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:

	UWRGameplayStatics(const FObjectInitializer& ObjectInitializer)
		: Super(ObjectInitializer)
	{}

	////////////////////////////
	//Team

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Team")
	static FGenericTeamId GetTeam(TEnumAsByte<WRTeam::Type> Team);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Team")
	static FGenericTeamId GetTeamForId(uint8 TeamId);

	UFUNCTION(BlueprintCallable, Category = "Team")
	static void SetObjectTeam(UObject * Object, TEnumAsByte<WRTeam::Type> Team);

	UFUNCTION(BlueprintCallable, Category = "Team")
	static void SetObjectTeamForId(UObject * Object, uint8 TeamId);
	
	////////////////////////////
	//AI

	/**
	Exposes the default implementation for updating potential target info to Blueprint
	*/
	UFUNCTION(BlueprintCallable, Category = "AI")
	static void DefaultUpdatePotentialTargetInfo(UPARAM(ref) FWRAIPotentialTargetInfo& PotentialTargetInfo, float CurrentWorldTime);

	////////////////////////////
	//Inventory

	/**
	Sorts an array of inventory widgets by inventory item type.
	*/
	//UFUNCTION(BlueprintCallable, Category = Inventory)
	//static void SortInventoryWidgetsByType(UPARAM(ref) TArray<class UWRInventoryItemWidget *>& InventoryWidgetArray);

	///**
	//Inserts a widget into an array of widgets sorted by type and returns the index that was inserted into.
	//*/
	//UFUNCTION(BlueprintCallable, Category = Inventory)
	//static int32 InsertIntoWidgetsByType(class UWRInventoryItemWidget * const Widget, UPARAM(ref) TArray<class UWRInventoryItemWidget *>& InventoryWidgetArray);
		
	/////////////////////////////
	//Projectiles Shooting
	
	/**
	Fires a projectile of some type in a direction.
	*/
	UFUNCTION(BlueprintCallable, Category = "Projectiles")
	static AWRProjectile * ShootProjectile(TSubclassOf<AWRProjectile> ProjectileClass, const FTransform& SpawnTransform, AActor * Creator = NULL, UWorld * World = NULL);
		
	/**
	Performs a hitscan trace.
	*/
	UFUNCTION(BlueprintCallable, Category = "Projectiles")
	static bool PerformSingleHitscanTrace(struct FHitResult& OutHit, 
		const FTransform& OriginTransform,
		bool bTraceComplex = true,
		ECollisionChannel TraceChannel = /*COLLISION_TRACE_WEAPON*/ ECC_GameTraceChannel2,		 
		const AActor * Creator = NULL,
		float MaxDistance = 50000.0f,
		UWorld * World = NULL,		
		bool bDebugDraw = false);

	/**
	Shoots a single hitscan trace for damage in a direction.
	Doesn't do any work of creating particle effects for the trace itself.
	*/
	UFUNCTION(BlueprintCallable, Category = "Projectiles")
	static bool ShootSingleHitscanTrace(struct FHitResult& OutHit,
		const FTransform& OriginTransform,
		TSubclassOf<UWRDamageType> DamageType,
		TSubclassOf<UWRImpactType> ImpactType,
		AActor * Creator = NULL,
		float DamageMultiplier = 1.f,
		float ExplosionRadiusMultiplier = 1.f,
		float MaxDistance = 50000.0f,		
		UWorld * World = NULL,
		bool bDebugDraw = false);
};
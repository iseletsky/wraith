#pragma once 
#include "GameFramework/HUD.h"
#include "WRUsableInterface.h"
#include "WRHUD.generated.h"

class AWRInventoryPhysical;

class UWRGameHUD;
class UWRGameScreens;

UCLASS()
class AWRHUD : public AHUD
{
	GENERATED_BODY()
public:
	static const float MAP_WORLD_SIZE;

	AWRHUD(const FObjectInitializer& ObjectInitializer);
	
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameHUD")
	TSubclassOf<UWRGameHUD> GameHUDClass;

	UPROPERTY(BlueprintReadWrite, Category = "GameHUD")
	UWRGameHUD * GameHUD;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameHUD")
	void SetupGameHUD();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameScreens")
	TSubclassOf<UWRGameScreens> GameScreensClass;

	UPROPERTY(BlueprintReadWrite, Category = "GameScreens")
	UWRGameScreens * GameScreens;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameHUD")
	void SetupGameScreens();
};


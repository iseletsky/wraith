#pragma once

#include "UserWidget.h"
#include "WRCrosshairWidget.generated.h"

UCLASS()
class UWRCrosshairWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UWRCrosshairWidget(const FObjectInitializer& ObjectInitializer);

};
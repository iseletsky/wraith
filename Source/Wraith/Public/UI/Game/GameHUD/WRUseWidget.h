#pragma once

#include "UserWidget.h"
#include "WRUsableInterface.h"
#include "WRUseWidget.generated.h"

UCLASS()
class UWRUseWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UWRUseWidget(const FObjectInitializer& ObjectInitializer);

	/**
	Sets the usable object to prompt the player to hit the use button and activate it
	*/
	UFUNCTION(BlueprintImplementableEvent)
	void SetUsableObject(const TScriptInterface<IWRUsableInterface>& Usable);
};
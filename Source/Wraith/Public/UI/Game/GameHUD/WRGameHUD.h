#pragma once

#include "UserWidget.h"
#include "WRGameHUD.generated.h"

class UWRScreenManagerWidget;

UCLASS(abstract)
class UWRGameHUD : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "GameScreens")
	void AddGameScreensManagerWidget(UWRScreenManagerWidget * ScreenManagerWidget);
};
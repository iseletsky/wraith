#pragma once

#include "UserWidget.h"
#include "WRCharacterStatusWidget.generated.h"

UCLASS()
class UWRCharacterStatusWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UWRCharacterStatusWidget(const FObjectInitializer& ObjectInitializer);

};
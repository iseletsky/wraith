#pragma once

#include "UserWidget.h"
#include "WRWeaponStatusWidget.generated.h"

UCLASS()
class UWRWeaponStatusWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UWRWeaponStatusWidget(const FObjectInitializer& ObjectInitializer);

};
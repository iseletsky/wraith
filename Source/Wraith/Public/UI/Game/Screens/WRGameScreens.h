#pragma once

#include "UserWidget.h"
#include "WRGameScreens.generated.h"

class UWRGameScreen;
class UWRScreenManagerWidget;
class AWRHUD;

UCLASS(abstract, BlueprintType, Blueprintable)
class UWRGameScreens : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
	AWRHUD * ParentHUD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameScreens")
	TArray<TSubclassOf<UWRGameScreen>> GameScreenClasses;

	UPROPERTY(BlueprintReadWrite, Category = "GameScreens")
	TArray<UWRGameScreen *> GameScreens;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameScreens")
	TSubclassOf<UWRScreenManagerWidget> ScreenManagerWidgetClass;

	UPROPERTY(BlueprintReadWrite, Category = "GameScreens")
	UWRScreenManagerWidget * ScreenManagerWidget;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameScreens")
	void SetupScreens(APlayerController *OwningPlayer, UInputComponent * Input);
		
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameScreens")
	void OpenScreen(UWRGameScreen * GameScreen);

	/**
	Begins showing the mouse cursor and disables player controls.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameScreens")
	void SetupShowingScreens();

	/**
	Ends showing the mouse cursor and reenables player controls.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameScreens")
	void TearDownShowingScreens();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input")
	void SetupHotkeys(UInputComponent * Input);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input")
	void OpenScreens();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input")
	void CloseScreens();
};
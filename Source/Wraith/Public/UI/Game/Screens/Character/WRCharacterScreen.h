#pragma once

#include "WRGameScreen.h"
#include "WRCharacterScreen.generated.h"

UCLASS(abstract)
class UWRCharacterScreen : public UWRGameScreen
{
	GENERATED_BODY()

public:
	UWRCharacterScreen(const FObjectInitializer& ObjectInitializer);
};
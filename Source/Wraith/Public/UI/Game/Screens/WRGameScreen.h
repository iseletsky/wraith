#pragma once

#include "UserWidget.h"
#include "WRGameScreen.generated.h"

class UWRGameScreens;
class UWRScreenWidget;

UCLASS(abstract, BlueprintType, Blueprintable)
class UWRGameScreen : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Category = "UI")
	UWRGameScreens * ParentGameScreens;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	FName OpenHotkeyActionName;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input")
	void SetupOpenHotkey(UInputComponent * Input);
		
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI")
	void OpenScreen();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI")
	void OnScreenOpened();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI")
	void CreateScreenWidget(APlayerController * OwningPlayer);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UWRScreenWidget> ScreenWidgetClass;

	UPROPERTY(BlueprintReadWrite, Category = "UI")
	UWRScreenWidget * ScreenWidget;
};
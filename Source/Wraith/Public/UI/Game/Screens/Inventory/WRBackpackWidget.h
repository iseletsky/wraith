#pragma once

#include "UserWidget.h"
#include "WRBackpackWidget.generated.h"

class AWRInventoryPhysical;
class UWRInventoryManager;
class UWRInventoryItemWidget;

UCLASS()
class UWRBackpackWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UWRBackpackWidget(const FObjectInitializer& ObjectInitializer);
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void SetTitle(const FText& Title);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void SetWeight(float Weight);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void SetMaxWeight(float Weight);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void SetWeightHidden(bool bHidden);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	UWRInventoryItemWidget * AddInventoryItem(AWRInventoryPhysical * InventoryItem);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void RemoveInventoryItemWidget(UWRInventoryItemWidget * InventoryItemWidget);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void RemoveAllInventoryItemWidgets();
};
#pragma once

#include "UserWidget.h"
#include "WRInventoryItemWidget.generated.h"

class AWRInventoryPhysical;

UCLASS()
class UWRInventoryItemWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UWRInventoryItemWidget(const FObjectInitializer& ObjectInitializer);
	
	/**
	Reference to the corresponding InventoryItem.
	This should only be used to help the Inventory List HUD sort InventoryWidgets.
	All updating of weight text and stack count is handled by the InventoryItem itself.
	*/
	UPROPERTY(BlueprintReadWrite, Category = Inventory)
	AWRInventoryPhysical * InventoryItem;

	/**
	Makes the widget not point to any inventory item anymore by hiding all subwidgets 
	and making it and the corresponding inventory item no longer point to each other.
	This is useful in the hotbar.
	*/
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void ClearWidget();

	/**
	Sets the visiblity of the weight display
	*/
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void SetWeightTextVisible(bool bVisible);

	/**
	Sets the string in the weight display
	*/
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void SetWeightText(const FText& text);

	/**
	Sets the visiblity of the stack counter
	*/
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void SetStackTextVisible(bool bVisible);

	/**
	Sets the string in the stack counter
	*/
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void SetStackText(const FText& text);

	/**
	Sets the stack icon for the stack counter.
	This is expected to be a signed distance field texture.
	*/
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
	void SetStackIcon(UTexture * icon);
};
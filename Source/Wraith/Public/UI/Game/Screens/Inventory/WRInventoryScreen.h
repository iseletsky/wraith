#pragma once

#include "WRGameScreen.h"
#include "WRInventoryScreen.generated.h"

class UWRBackpackWidget;

UCLASS(abstract)
class UWRInventoryScreen : public UWRGameScreen
{
	GENERATED_BODY()

public:
	UWRInventoryScreen(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UWRBackpackWidget> CharacterBackpackWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UWRBackpackWidget> GroundBackpackWidgetClass;

	/**
	Widget that shows a character's inventory contents
	*/
	UPROPERTY(BlueprintReadWrite, Category = "UI")
	UWRBackpackWidget * CharacterBackpackWidget;

	/**
	Widget that shows what is on the ground that the character can pick up
	*/
	UPROPERTY(BlueprintReadWrite, Category = "UI")
	UWRBackpackWidget * GroundBackpackWidget;

	/**
	Widget that shows the other backpack that the character is interacting with, such as a box or another player.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "UI")
	UWRBackpackWidget * OtherBackpackWidget;

	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI")
	//void SetupShowingOtherBackpackWidget(UWRBackpackWidget * InOtherBackpackWidget);
};
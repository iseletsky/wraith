#pragma once 

#include "LGGeneratedLevel.h"
#include "WRMap.generated.h"

/**
The actor that handles rendering the Map UI for a player.
This actor may move around with the level bounds so it's always on the outside.

The server manages the map object for each player controller to make it more difficult for players to cheat with all map hacks.

It doesn't contribute to the level bounds itself.
It also contains the camera that is used to look at the map.
*/
UCLASS()
class AWRMap : public AActor
{
	GENERATED_BODY()
public:
	AWRMap(const FObjectInitializer& ObjectInitializer);
	
	/**
	The 
	*/
	UPROPERTY()
	TArray<UPrimitiveComponent *> MapComponents;

	/**
	Map of area index to the associated scene components to render for an area.
	This is only used by the server as a quick lookup to mark areas of the map as visible if they should be.
	*/
	TMap<int32, TArray<int32>> AreaIndexToComponentsIndex;
	
	virtual bool IsLevelBoundsRelevant() const override
	{
		return false;
	}

private:


};


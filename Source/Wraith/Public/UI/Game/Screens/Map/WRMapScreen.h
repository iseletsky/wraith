#pragma once

#include "WRGameScreen.h"
#include "WRMapScreen.generated.h"

UCLASS(abstract)
class UWRMapScreen : public UWRGameScreen
{
	GENERATED_BODY()

public:
	UWRMapScreen(const FObjectInitializer& ObjectInitializer);
};
#pragma once

#include "UserWidget.h"
#include "WRScreenManagerWidget.generated.h"

class UWRScreenWidget;

UCLASS(abstract)
class UWRScreenManagerWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI")
	void AddScreenWidget(UWRScreenWidget * ScreenWidget, APlayerController * OwningPlayer);

	UPROPERTY(BlueprintReadWrite, Category = "UI")
	TSet<UWRScreenWidget *> ScreenWidgets;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI")
	void OpenScreen(UWRScreenWidget * ScreenWidget);
};
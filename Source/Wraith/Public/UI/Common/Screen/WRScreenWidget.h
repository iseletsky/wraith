#pragma once

#include "UserWidget.h"
#include "WRScreenWidget.generated.h"

class UWRScreenManagerWidget;
class UWRScreenButtonWidget;

UCLASS(abstract)
class UWRScreenWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Category = "UI")
	UWRScreenManagerWidget * ScreenManager;

	UPROPERTY(BlueprintReadWrite, Category = "UI")
	UWRScreenButtonWidget * ScreenButton;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI")
	void OpenScreen();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI")
	void OnScreenOpened();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI")
	void OnScreenClosed();
};
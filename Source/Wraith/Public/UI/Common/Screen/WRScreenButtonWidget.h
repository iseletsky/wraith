#pragma once

#include "UserWidget.h"
#include "WRScreenButtonWidget.generated.h"

class UWRScreenWidget;

UCLASS(abstract)
class UWRScreenButtonWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Category = "UI")
	UWRScreenWidget * ScreenWidget;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI")
	void SetScreen(UWRScreenWidget * InScreenWidget);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI")
	void OpenScreen();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void OnScreenOpened();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void OnScreenClosed();
};
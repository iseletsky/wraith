#pragma once
#include "LGGenerateableAreaDrunkenWalkPostPass.h"
#include "WRDevThemeTestPostPassSpawnMonster.generated.h"

UCLASS()
class UWRDevThemeTestPostPassSpawnMonster : public ULGGenerateableAreaDrunkenWalkPostPass
{
	GENERATED_BODY()
public:
	virtual void PostPassEvent(class ALGGeneratingLevel * GeneratingLevel,
		const TArray<FName>& PassNames,
		FRandomStream& RandomStream) const override;
};


#pragma once
#include "LGGenerateableAreaDrunkenWalkChooseBranchArea.h"
#include "WRDevThemeTestChooseBranchArea.generated.h"

UCLASS()
class UWRDevThemeTestChooseBranchArea : public ULGGenerateableAreaDrunkenWalkChooseBranchArea
{
	GENERATED_BODY()
public:
	virtual bool PassChooseBranchArea(class ALGGeneratingLevel * GeneratingLevel,
		LayoutAtAreaFunc LayoutFunc,
		const TSet<FName>& BranchPassNames,
		FRandomStream& RandomStream) const override;
};


#pragma once

#include "LGSpawnable.generated.h"

UINTERFACE(MinimalAPI)
class ULGSpawnable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class ILGSpawnable
{
	GENERATED_IINTERFACE_BODY()

	/**
	Spawns the object given the transform.
	@param Transform The world transform to spawn this object at.

	@return Whether or not the spawnable was able to spawn with the given parameters.
	*/
	virtual bool Spawn(class ALGGeneratingLevel * GeneratingLevel, FRandomStream& RandomStream, FTransform Transform) const PURE_VIRTUAL(ILGSpawnable::Spawn, return false;);
	
	/**
	This is a slight hack because it modifies the CDO (Class Default Object).
	This sets up any metadata that is useful for the level generator system when working with spawnable objects.
	The generator system only works with CDO's and these objects are never actually spawned, but used as templates for how
	to spawn real objects in the world, so the CDO has to be modified.
	*/
	virtual void InitMetadata(class ALGGeneratingLevel * GeneratingLevel) PURE_VIRTUAL(ILGSpawnable::InitMetadata, );
};
#pragma once

#include "LGSpawnablePrefabConnector.h"
#include "LGSpawnableAreaLayoutContext.h"
#include "LGSpawnableArea.generated.h"

UINTERFACE(MinimalAPI)
class ULGSpawnableArea : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class ILGSpawnableArea
{
	GENERATED_IINTERFACE_BODY()

	virtual ULGSpawnableAreaLayoutContext * BeginLayoutArea(float PercentLower, float PercentUpper,
		class ALGGeneratingLevel * GeneratingLevel,
		FRandomStream& RandomStream,
		FTransform Transform,
		TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
		int32 EntryConnectorIndex,
		const TSet<FName>& AreaTags,
		const TArray<FName>& PassNames) const
		PURE_VIRTUAL(ILGSpawnableArea::BeginLayoutArea, return NULL;);
	
	/**
	Interfaces don't have the GetClass method so this is a workaround so I can still get the object's class using this method.
	Just make overriding classes call GetClass and return that.
	*/
	virtual UClass * GetSpawnableAreaClass() const PURE_VIRTUAL(ILGSpawnableArea::GetSpawnableAreaClass, return NULL;);

	/**
	Returns all connector types that are possible into this SpawnableArea.
	*/
	virtual void GetConnectorTypes(TArray<TSubclassOf<ALGSpawnablePrefabConnector>>& OutArray) const PURE_VIRTUAL(ILGSpawnableArea::GetConnectorTypes, );

	/**
	This is a slight hack because it modifies the CDO(Class Default Object).
	This sets up any metadata that is useful for the level generator system when working with spawnable objects.
	The generator system only works with CDO's and these objects are never actually spawned, but used as templates for how
	to spawn real objects in the world, so the CDO has to be modified.
	*/
	virtual void InitMetadata(class ALGGeneratingLevel * GeneratingLevel) PURE_VIRTUAL(ILGSpawnableArea::InitMetadata, );
};

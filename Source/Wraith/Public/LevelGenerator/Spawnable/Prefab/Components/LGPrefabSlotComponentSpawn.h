#pragma once

#include "AERandomBuckets.h"
#include "LGPrefabSlotComponent.h"
#include "LGSpawnablePrefabObject.h"
#include "LGPrefabSlotComponentSpawn.generated.h"

USTRUCT(BlueprintType)
struct FPrefabSpawnTypeInfo
{
	GENERATED_USTRUCT_BODY()

	FPrefabSpawnTypeInfo()
		: Probability(1.f)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Randomization")
	float Probability;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
	TSubclassOf<ALGSpawnablePrefabObject> PrefabClass;
		
	/**
	Allows you to disable this option for debugging purposes without having to delete it.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debugging")
	bool bDisabled;
};

UCLASS(meta = (BlueprintSpawnableComponent))
class ULGPrefabSlotComponentSpawn : public ULGPrefabSlotComponent
{
	GENERATED_BODY()
public:
	virtual void InitMetadata(class ALGGeneratingLevel * GeneratingLevel);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Objects")
	TArray<FPrefabSpawnTypeInfo> SpawnTypes;

	/**
	Spawn slot components in a room with the same SimilarityId's will choose the same PrefabClass to spawn.

	A SimilarityId of 0 means the object is untagged.

	It makes most sense to create one slot component with all of the SpawnTypes data specified.
	The rest can be blank since the first one will make the decision of what class is spawned.
	If they're not blank and have a different set of objects, that data will be ignored.
	The component that comes first in the list of components is the one that is most likely to be chosen first to try to spawn, but you shouldn't rely on this.
	Just know that you need to define the data once.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Randomization")
	int32 SimilarityId;

	/**
	Allows you to disable this object for debugging purposes without having to delete it.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debugging")
	bool bDisabled;

	FAERandomBuckets SpawnTypeRandomBuckets;
};
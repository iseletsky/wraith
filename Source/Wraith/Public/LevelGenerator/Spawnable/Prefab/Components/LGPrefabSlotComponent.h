#pragma once

#include "LGPrefabSlotComponent.generated.h"

class UArrowComponent;

UCLASS(Abstract, meta = (BlueprintSpawnableComponent), HideCategories = (Events, Rendering, Activation, LOD))
class ULGPrefabSlotComponent : public USceneComponent
{
	GENERATED_BODY()
public:
	ULGPrefabSlotComponent(const FObjectInitializer& ObjectInitializer);
		
	/**
	Normally ComponentToWorld gives this result, but these components only exist in SpawnablePrefab template objects.
	Those never get instantiated and only exist as CDO's so the ComponentToWorld transform is impossible to compute for blueprint spawned objects.
	I have to compute it manually and store it here.

	This gets computed by the InitMetadata step in 
	*/
	/*FTransform WorldTransform;

	bool bWorldTransformComputed;*/

	/** UActorComponent Interface */
	virtual void OnRegister() override;

protected:
#if WITH_EDITORONLY_DATA
	UPROPERTY(transient)
	UArrowComponent* EditorArrowX;

	UPROPERTY(transient)
	UArrowComponent* EditorArrowY;

	UPROPERTY(transient)
	UArrowComponent* EditorArrowZ;
#endif
};
#pragma once

#include "LGPrefabSlotComponentConnector.h"
#include "LGPrefabSlotComponentAreaSpawnDestinationConnector.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class ULGPrefabSlotComponentAreaSpawnDestinationConnector : public ULGPrefabSlotComponentConnector
{
	GENERATED_BODY()
public:
};
#pragma once

#include "LGPrefabSlotComponent.h"
#include "LGPrefabSlotComponentAreaNode.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class ULGPrefabSlotComponentAreaNode : public ULGPrefabSlotComponent
{
	GENERATED_BODY()
public:

	ULGPrefabSlotComponentAreaNode(const FObjectInitializer& ObjectInitializer);

	/**
	Area cost allows an area to count as part of an area or as multiple areas.
	A typical room is 1.  A tiny hallway might be .25 or .5.  A large room could count as 1.5 or 2.0 or whatever makes sense.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Objects")
	float AreaCost;
};
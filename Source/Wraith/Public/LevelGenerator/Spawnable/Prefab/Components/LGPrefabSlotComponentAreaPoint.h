#pragma once

#include "LGPrefabSlotComponent.h"
#include "LGPrefabSlotComponentAreaPoint.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class ULGPrefabSlotComponentAreaPoint : public ULGPrefabSlotComponent
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Objects")
	FName PointName;
};
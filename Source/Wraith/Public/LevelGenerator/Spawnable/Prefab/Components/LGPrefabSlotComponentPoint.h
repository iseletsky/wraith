#pragma once

#include "LGPrefabSlotComponent.h"
#include "LGPrefabSlotComponentPoint.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class ULGPrefabSlotComponentPoint : public ULGPrefabSlotComponent
{
	GENERATED_BODY()
public:
};
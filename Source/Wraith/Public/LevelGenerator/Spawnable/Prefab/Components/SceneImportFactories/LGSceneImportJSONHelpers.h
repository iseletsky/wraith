#pragma once

#include "LGSpawnableAreaProbabilityList.h"

class FLGSceneImportJSONHelpers
{
public:
	static bool ReadSpawnableAreaProbabilityList(const TArray<TSharedPtr<FJsonValue>>& Items, FLGSpawnableAreaProbabilityList& OutList, const FString& NameForErrors);

private:
	FLGSceneImportJSONHelpers() {}
};
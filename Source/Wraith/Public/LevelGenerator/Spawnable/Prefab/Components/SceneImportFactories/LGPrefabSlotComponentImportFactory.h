#pragma once

#ifdef WITH_SCENE_IMPORT_FACTORIES
#include "Runtime/Engine/Classes/Components/SceneImportFactories/SceneComponentImportFactory.h"
#endif

#include "LGPrefabSlotComponentImportFactory.generated.h"

UCLASS(transient, Abstract)
class ULGPrefabSlotComponentImportFactory : public UObject//USceneComponentImportFactory
{
	GENERATED_BODY()

#ifdef WITH_SCENE_IMPORT_FACTORIES

public:
	virtual bool ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const override;

#endif
};

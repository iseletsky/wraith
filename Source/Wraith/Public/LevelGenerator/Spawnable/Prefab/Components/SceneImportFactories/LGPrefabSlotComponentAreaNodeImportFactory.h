#pragma once

#include "LGPrefabSlotComponentImportFactory.h"
#include "LGPrefabSlotComponentAreaNodeImportFactory.generated.h"

UCLASS(transient)
class ULGPrefabSlotComponentAreaNodeImportFactory : public ULGPrefabSlotComponentImportFactory
{
	GENERATED_BODY()

#ifdef WITH_SCENE_IMPORT_FACTORIES

public:
	virtual FString GetObjectTypeName() const override;

	virtual USceneComponent * SpawnComponent(UObject * Outer) const override;

	virtual bool ImportComponent(USceneComponent * SceneComponent, const TSharedPtr<FJsonObject>& SourceJson, const FString& NameForErrors) const override;

#endif
};
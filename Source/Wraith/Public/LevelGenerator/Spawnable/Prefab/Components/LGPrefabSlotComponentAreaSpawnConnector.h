#pragma once

#include "LGSpawnableAreaProbabilityList.h"
#include "LGPrefabSlotComponentConnector.h"
#include "LGPrefabSlotComponentAreaSpawnConnector.generated.h"

/** I must create a UStruct for this since TArray in TArray properties aren't supported. */
USTRUCT(BlueprintType)
struct FSpawnableAreaInboundConnectorInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Randomization)
	TArray<FName> SpawnableAreaInboundConnectorNames;
};

UCLASS(meta = (BlueprintSpawnableComponent))
class ULGPrefabSlotComponentAreaSpawnConnector : public ULGPrefabSlotComponentConnector
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Randomization)
	FLGSpawnableAreaProbabilityList SpawnableAreas;

	/** For each spawnable area, you must also specify which connector names in that area are usable */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Randomization)
	TArray<FSpawnableAreaInboundConnectorInfo> SpawnableAreaInboundConnector;

	virtual void InitMetadata(class ALGGeneratingLevel * GeneratingLevel) override;

	/** Derived from SpawnableAreaInboundConnector */
	TArray<FAERandomBuckets> SpawnableAreaToInboundConnectorRandomBuckets;
};
#pragma once

#include "LGPrefabSlotComponentConnector.h"
#include "LGPrefabSlotComponentAreaConnector.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class ULGPrefabSlotComponentAreaConnector : public ULGPrefabSlotComponentConnector
{
	GENERATED_BODY()
public:
	ULGPrefabSlotComponentAreaConnector(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Randomization")
	float Probability;

	/**
	Tag connector slots with the same number if their orientations would result in congruent room orientations.
	For example, in a rectangular room with connectors coming out, they'd be tagged in this way.

	  2 3 4
	 +-----+
	1|     |1
	 +-----+
	  4 3 2

	A tag of 0 is default and means it's untagged.

	This is only an optimization to help the layout stage avoid checking if a room will fit into a space if it already knows
	the room won't fit from a congruent orientation.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Randomization")
	int32 CongruencyId;
	
	/**
	Priority Connectors should be used for areas that would be strongly preferred to have an area coming out of them, like hallway ends, or staircase ends.
	Otherwise it'll just be a strange dead end.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Objects")
	bool bPriorityConnector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Objects")
	FName ConnectorName;
};
#pragma once

#include "AERandomBuckets.h"
#include "LGPrefabSlotComponent.h"
#include "LGSpawnablePrefabConnector.h"
#include "LGPrefabSlotComponentConnector.generated.h"

USTRUCT(BlueprintType)
struct FPrefabConnectorTypeInfo
{
	GENERATED_USTRUCT_BODY()

	FPrefabConnectorTypeInfo()
		: Probability(1.f)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Randomization")
	float Probability;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
	TSubclassOf<ALGSpawnablePrefabObject> WallPrefabClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
	TSubclassOf<ALGSpawnablePrefabConnector> ConnectorPrefabClass;

	/**
	Allows you to disable this option for debugging purposes without having to delete it.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debugging")
	bool bDisabled;
};

UCLASS(Abstract, meta = (BlueprintSpawnableComponent))
class ULGPrefabSlotComponentConnector : public ULGPrefabSlotComponent
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Objects")
	TArray<FPrefabConnectorTypeInfo> ConnectorTypes;

	virtual void InitMetadata(class ALGGeneratingLevel * GeneratingLevel);

	void GetCommonConnectorClasses(const ULGPrefabSlotComponentConnector * OtherConnectorSlot, TSet<TSubclassOf<ALGSpawnablePrefabConnector>>& OutClasses) const;

	/**
	Allows you to disable this connector for debugging purposes without having to delete it.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debugging")
	bool bDisabled;

	FAERandomBuckets ConnectorTypeRandomBuckets;
	TMap<TSubclassOf<ALGSpawnablePrefabConnector>, int32> ConnectorClassToConnectorTypeReverseLookup;
};
#pragma once

#include "LGPrefabSlotComponent.h"
#include "LGPrefabSlotComponentActor.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class ULGPrefabSlotComponentActor : public ULGPrefabSlotComponent
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Objects")
	TSubclassOf<AActor> ActorSpawnClass;

	virtual void SpawnActor(class ALGGeneratingLevel * GeneratingLevel) const;
};
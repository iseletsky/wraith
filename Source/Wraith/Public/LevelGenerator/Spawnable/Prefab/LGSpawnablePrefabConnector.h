#pragma once

#include "LGSpawnablePrefabObject.h"
#include "LGPrefabSlotComponentPoint.h"
#include "LGSpawnablePrefabConnector.generated.h"

UCLASS(Abstract)
class ALGSpawnablePrefabConnector : public ALGSpawnablePrefabObject
{
	GENERATED_BODY()
public:
	ALGSpawnablePrefabConnector(const FObjectInitializer& ObjectInitializer);

	virtual bool Spawn(class ALGGeneratingLevel * GeneratingLevel, FRandomStream& RandomStream, FTransform Transform) const override;

	FTransform GetTransformFromEntryToExit();

	UPROPERTY(VisibleAnywhere, Category = ConnectorAttachPoints)
	ULGPrefabSlotComponentPoint * EntryPoint;

	UPROPERTY(VisibleAnywhere, Category = ConnectorAttachPoints)
	ULGPrefabSlotComponentPoint * ExitPoint;
};
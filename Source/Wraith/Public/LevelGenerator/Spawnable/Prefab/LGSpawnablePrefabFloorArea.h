#pragma once

#include "LGSpawnablePrefabArea.h"
#include "LGPrefabSlotComponentPoint.h"
#include "LGSpawnablePrefabFloorArea.generated.h"

UCLASS(Abstract)
class ALGSpawnablePrefabFloorArea : public ALGSpawnablePrefabArea
{
	GENERATED_BODY()
public:
	ALGSpawnablePrefabFloorArea(const FObjectInitializer& ObjectInitializer);

	FTransform GetTransformFromBottomToTop();

	FTransform GetTransformFromTopToBottom();

	UPROPERTY(VisibleAnywhere, Category = FloorAttachPoints)
	ULGPrefabSlotComponentPoint * TopPoint;

	UPROPERTY(VisibleAnywhere, Category = FloorAttachPoints)
	ULGPrefabSlotComponentPoint * BottomPoint;
};
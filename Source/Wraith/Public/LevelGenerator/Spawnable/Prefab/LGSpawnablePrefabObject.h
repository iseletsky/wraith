#pragma once

#include "LGSpawnablePrefab.h"
#include "LGSpawnablePrefabObject.generated.h"

UCLASS(Abstract)
class ALGSpawnablePrefabObject : public ALGSpawnablePrefab
{
	GENERATED_BODY()
public:
};
#pragma once

#include "LGSpawnable.h"
#include "LGSpawnablePrefab.generated.h"

UCLASS(Abstract, NotPlaceable, HideCategories = (Rendering, Replication, Input, Actor, Tags))
class ALGSpawnablePrefab : public AActor, public ILGSpawnable
{
	GENERATED_BODY()
public:		
	virtual void InitMetadata(class ALGGeneratingLevel * GeneratingLevel) override;

	virtual bool Spawn(class ALGGeneratingLevel * GeneratingLevel, FRandomStream& RandomStream, FTransform Transform) const override;
	
	TArray<UPrimitiveComponent *> AreaCollision;

	TSet<int32> ExistingSimilarSpawnGroups;
	TSet<class ULGPrefabSlotComponentSpawn *> PrimarySpawnSlots;
	TMultiMap<int32, class ULGPrefabSlotComponentSpawn *> SimilarSpawnSlots;

	TArray<class ULGPrefabSlotComponentActor *> ActorSpawnSlots;

protected:
	/**
	Called for each scene component found during the InitMetadata stage.
	@return Whether or not the function successfully processed the component, or if it's up to the sub class to try to process it
	*/
	virtual bool ProcessSceneComponent(USceneComponent * Component, class ALGGeneratingLevel * GeneratingLevel);
};
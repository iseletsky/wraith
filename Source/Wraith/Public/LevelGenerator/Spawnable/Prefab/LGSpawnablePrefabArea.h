#pragma once

#include "AERandomBuckets.h"
#include "LGGeneratingLevel.h"
#include "LGSpawnablePrefab.h"
#include "LGPrefabSlotComponentAreaNode.h"
#include "LGPrefabSlotComponentAreaConnector.h"
#include "LGPrefabSlotComponentAreaSpawnConnector.h"
#include "LGPrefabSlotComponentAreaSpawnDestinationConnector.h"
#include "LGSpawnableArea.h"
#include "LGAreaInfo.h"
#include "LGSpawnablePrefabArea.generated.h"

UCLASS(Abstract)
class ALGSpawnablePrefabArea : public ALGSpawnablePrefab, public ILGSpawnableArea
{
	GENERATED_BODY()
public:
	ALGSpawnablePrefabArea(const FObjectInitializer& ObjectInitializer);

	/**
	AreaNodes have connectors parented to them.  This acts as the origin of the area graph node when setting up the area connectedness graph.
	A prefab area can have multiple area nodes with a different set of connectors attached to them so that a single area can appear to be multiple areas in the graph.
	This would be especially useful when composing areas out of multiple AreaSpawnConnectors.
	*/
	UPROPERTY(VisibleAnywhere, Category = "Level Objects")
	ULGPrefabSlotComponentAreaNode * AreaNode;

	/**
	Holds info about how to lay out the area after setting up all the area collision objects and connectors that will be used.
	*/
	struct CreationContext
	{
		struct CreationContextAreaInfo
		{
			ALGAreaInfo * AreaInfo;
			TArray<UPrimitiveComponent *> CheckAreaCollision;
			TArray<UPrimitiveComponent *> AreaOmitBranchSpawnCollision;

			TArray<ALGConnectorInfo *> ConnectorInfos;
			TArray<FLGConnectorLayoutInfo> ConnectorLayoutInfos;
		};

		const ALGSpawnablePrefabArea * ParentPrefabArea;

		TArray<CreationContextAreaInfo> AreaInfos;
		
		TArray<UPrimitiveComponent *> CheckAreaCollisionDeleteAfterLayout;
		int32 InitialConnectorIndex;

		/** Only call this if the Area associated with this context won't be spawned since it destroyes the ALGAreaInfos and ALGConnectorInfos */
		void Destroy();

		/**
		Checks if the collision objects associated with the area as it's being laid out intersect with existing area collision geometry
		*/
		bool AreaCollisionIntersectionTest(class ALGGeneratingLevel * GeneratingLevel,
			FTransform Transform,
			int32 EntryThisConnectorIndexToIgnore,
			int32 EntryLevelConnectorIndexToIgnore,
			int32 ConnectorCongruencyId,
			TSet<int32>& FailedCongruencyIds) const;
	};
			
	virtual void InitMetadata(class ALGGeneratingLevel * GeneratingLevel) override;
	
	virtual void GetConnectorTypes(TArray<TSubclassOf<ALGSpawnablePrefabConnector>>& OutArray) const override;

	virtual UClass * GetSpawnableAreaClass() const override;

	virtual ULGSpawnableAreaLayoutContext * BeginLayoutArea(float PercentLower, float PercentUpper,
		class ALGGeneratingLevel * GeneratingLevel,
		FRandomStream& RandomStream,
		FTransform Transform,
		TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
		int32 EntryConnectorIndex,
		const TSet<FName>& AreaTags,
		const TArray<FName>& PassNames) const override;

	bool LayoutArea(class ALGGeneratingLevel * GeneratingLevel,
		FRandomStream& RandomStream,
		FTransform Transform,
		TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
		int32 EntryConnectorIndex,
		const TSet<FName>& AreaTags,
		const TArray<FName>& PassNames) const;

	/**
	Finds a valid transform for the area given the params that normally go into LayoutArea.
	The resulting transform is returned through the Transform reference parameter.
	It also sets up the layout context that can be used for laying out the area in later steps.

	Returns true if finds successful location.
	False if not.
	*/
	bool FindAreaLayoutTransform(class ALGGeneratingLevel * GeneratingLevel,
		FRandomStream& RandomStream,
		CreationContext& Context,
		FTransform& Transform,
		int32& ChosenConnectorSlotIndex,
		TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
		int32 EntryConnectorIndex) const;

	/**
	Lays out the area when you already know this area's entry connector slot that will be used.
	Intersection checks will be skipped since it's assumed that the area is known to fit here already.
	This is usually called when laying out areas from AreaSpawnConnectorSlots.
	*/
	bool LayoutAreaWithEntry(class ALGGeneratingLevel * GeneratingLevel,
		FRandomStream& RandomStream,
		TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
		int32 EntryConnectorIndex,
		int32 ChosenConnectorSlotIndex,
		const TSet<FName>& AreaTags,
		const TArray<FName>& PassNames) const;

	/**
	Sets up a context that has shared info between the different area layout methods.
	*/
	void InitCreationContext(CreationContext& Context, class ALGGeneratingLevel * GeneratingLevel, bool SetupPrespawnCollision = true) const;

	/**
	Finally lays out an area when you know the exact location to spawn it from
	*/
	void LayoutAreaWithCreationContext(CreationContext& Context,
		class ALGGeneratingLevel * GeneratingLevel,
		FTransform Transform,
		TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
		int32 EntryConnectorIndex,
		int32 ChosenConnectorSlotIndex,
		const TSet<FName>& AreaTags,
		const TArray<FName>& PassNames) const;

	TMultiMap<FName, int32> NamedAreaConnectorSlots;
	TArray<ULGPrefabSlotComponentAreaConnector *> AreaConnectorSlots;
	FAERandomBuckets AreaConnectorIndexRandomBuckets;
	FAERandomBuckets PriorityAreaConnectorIndexRandomBuckets;
	TMap<TSubclassOf<ALGSpawnablePrefabConnector>, FAERandomBuckets> AreaConnectorClassToConnectorIndexRandomBuckets;

	TArray<ULGPrefabSlotComponentAreaSpawnConnector *> AreaSpawnConnectorSlots;

	TArray<ULGPrefabSlotComponentAreaSpawnDestinationConnector *> AreaSpawnDestinationConnectorSlots;
	/** Positions are converted to 100x100x100 grid cell locations. */
	TMap<FIntVector, int32> PositionToAreaSpawnDestinationConnectorSlots;

	TArray<ULGPrefabSlotComponentAreaNode *> AreaNodes;
	TArray<TArray<UPrimitiveComponent *>> AreaNodeToAreaCollision;
	TMap<ULGPrefabSlotComponentConnector *, int32> ConnectorToAreaNodeInd;
	TArray<FAERandomBuckets> AreaNodeConnectorIndexRandomBuckets;
	TArray<FAERandomBuckets> AreaNodePriorityConnectorIndexRandomBuckets;
	TArray<TArray<class ULGPrefabSlotComponentAreaPoint *>> AreaNodeToAreaPoint;

protected:
	virtual bool ProcessSceneComponent(USceneComponent * Component, class ALGGeneratingLevel * GeneratingLevel) override;
};
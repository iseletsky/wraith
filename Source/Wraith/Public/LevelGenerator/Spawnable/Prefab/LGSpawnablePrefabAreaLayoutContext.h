#pragma once

#include "LGSpawnableAreaLayoutContext.h"
#include "LGSpawnablePrefabAreaLayoutContext.generated.h"

class ALGSpawnablePrefabArea;

UCLASS(transient)
class ULGSpawnablePrefabAreaLayoutContext : public ULGSpawnableAreaLayoutContext
{
	GENERATED_BODY()
public:
	virtual ELayoutContextStatus ContinueLayout(std::chrono::high_resolution_clock::time_point BeginTime) override;

	UPROPERTY(transient)
	const class ALGSpawnablePrefabArea * Parent;
};

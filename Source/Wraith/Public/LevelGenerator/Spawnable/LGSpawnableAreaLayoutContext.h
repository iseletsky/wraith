#pragma once

#include <chrono>
#include "LGSpawnableAreaLayoutContext.generated.h"

UENUM(BlueprintType)
enum class ELayoutContextStatus : uint8
{
	NOT_FINISHED	UMETA(DisplayName = "Not Finished"),
	SUCCESS			UMETA(DisplayName = "Success"),
	FAIL			UMETA(DisplayName = "Fail")
};

UCLASS(Abstract, transient)
class ULGSpawnableAreaLayoutContext : public UObject
{
	GENERATED_BODY()
public:
	/**
	How much time the ContinueLayout call should run per game tick before ending work and letting the next game tick run.
	*/
	const std::chrono::high_resolution_clock::duration MAX_RUN_SECONDS = std::chrono::high_resolution_clock::duration(1);

	virtual ELayoutContextStatus ContinueLayout(std::chrono::high_resolution_clock::time_point BeginTime) PURE_VIRTUAL(ULGSpawnableAreaLayoutContext::ContinueLayout, return ELayoutContextStatus::FAIL;);

	UPROPERTY(transient)
	class ALGGeneratingLevel * GeneratingLevel;

	FRandomStream * RandomStream;
	FTransform Transform;
	TSubclassOf<class ALGSpawnablePrefabConnector> EntryConnectorClass;
	int32 EntryConnectorIndex;
	TSet<FName> AreaTags;
	TArray<FName> PassNames;
};

#pragma once

#include "AERandomBuckets.h"
#include "LGGenerateableArea.h"
#include "LGSpawnableAreaProbabilityList.generated.h"

class ALGGenerateableArea;
class ALGSpawnablePrefabArea;
class ALGGeneratingLevel;

USTRUCT(BlueprintType)
struct FWRGenerateableAreaListInfo
{
	GENERATED_USTRUCT_BODY()

	FWRGenerateableAreaListInfo()
		: Probability(1.f)
	{}

	/**
	Probability relative to other objects in the list that this object will be chosen.
	The probabilities will be normalized, so you can have objects that have a probability of 900, 80, 100, 5
	and in the end it'll normalize to .82, .07, .09, .004
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Randomization")
	float Probability;
	
	/**
	Maximum number of times this SpawnableArea should spawn in the entire level.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generator Params")
	int32 LevelMaxSpawn;

	/**
	Allows you to disable this option for debugging purposes without having to delete it.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debugging")
	bool bDisabled;

	/**
	GenerateableAreaClass to spawn.  Specify only either a GenerateableAreaClass or SpawnablePrefabAreaClass.
	Ideally I wouldn't have two properties this way, but Unreal doesn't seem to support using interface types as TSubclassOf Properties.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnableArea")
	TSubclassOf<ALGGenerateableArea> GenerateableAreaClass;
	
	/**
	SpawnablePrefabAreaClass to spawn.  Specify only either a GenerateableAreaClass or SpawnablePrefabAreaClass.
	Ideally I wouldn't have two properties this way, but Unreal doesn't seem to support using interface types as TSubclassOf Properties.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnableArea")
	TSubclassOf<ALGSpawnablePrefabArea> SpawnablePrefabAreaClass;
};

USTRUCT(BlueprintType)
struct FLGSpawnableAreaProbabilityList
{
	GENERATED_USTRUCT_BODY()
		
	FLGSpawnableAreaProbabilityList()
	{}
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnableArea")
	TArray<FWRGenerateableAreaListInfo> SpawnableAreas;
	 
	void InitMetadata(ALGGeneratingLevel * GeneratingLevel);
	
	ILGSpawnableArea * GetSpawnableArea(ALGGeneratingLevel * GeneratingLevel, int32 AreaIndex) const;

	ALGGenerateableArea * GetGenerateableArea(ALGGeneratingLevel * GeneratingLevel, int32 AreaIndex) const;

	ALGSpawnablePrefabArea * GetSpawnablePrefabArea(ALGGeneratingLevel * GeneratingLevel, int32 AreaIndex) const;

	/**
	Derived from SpawnableAreas to cache the base class.
	*/
	TArray<UClass *> SpawnableAreaSubclassArray;

	FAERandomBuckets SpawnableAreaBuckets;
	TMap<TSubclassOf<ALGSpawnablePrefabConnector>, FAERandomBuckets> ConnectorClassToSpawnableAreaBuckets;
};
#pragma once 
#include "AERandomBuckets.h"
#include "LGAreaInfo.generated.h"

/**
Info about a spawned area
*/
UCLASS(NotPlaceable)
class ALGAreaInfo : public AActor
{
	GENERATED_BODY()
	
public:
	ALGAreaInfo(const FObjectInitializer& ObjectInitializer);

	/** Tags that will be used in graph stuff */
	TSet<FName> AreaTags;
	
	/** Area points are used for game specific metadata like where to spawn players or monsters or guns for example */
	TMultiMap<FName, class ULGPrefabSlotComponentAreaPoint *> AreaPoints;
};
#pragma once

#include "LGSpawnableArea.h"
#include "LGGenerateableArea.generated.h"

UCLASS(Abstract, NotPlaceable, HideCategories = (Rendering, Replication, Input, Actor, Tags))
class ALGGenerateableArea : public AActor, public ILGSpawnableArea
{
	GENERATED_BODY()
public:
	virtual UClass * GetSpawnableAreaClass() const;

	/** 
	Areas spawned by this generateable area will have their associated graph nodes tagged with these tags.
	These tags can be used for customizing some scripted area generation for a level to make for special events.
	For example, mark all areas that are locked by a key, so the key will never spawn in areas with a certain tag.

	These tags are also added to any areas spawned by generateable areas that are spawned by this generateable area.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Objects")
	TArray<FName> AreaGraphTags;
};
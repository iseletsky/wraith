#pragma once

#include "LGSpawnableAreaLayoutContext.h"
#include "LGGenerateableAreaDrunkenWalkLayoutContext.generated.h"

class ALGGenerateableAreaDrunkenWalk;
class ILGSpawnableArea;

UCLASS(transient)
class ULGGenerateableAreaDrunkenWalkLayoutContext : public ULGSpawnableAreaLayoutContext
{
	GENERATED_BODY()
public:
	virtual ELayoutContextStatus ContinueLayout(std::chrono::high_resolution_clock::time_point BeginTime) override;

	bool LayoutAreaAtConnector(float PercentLower,
		float PercentUpper,
		std::chrono::high_resolution_clock::time_point BeginTime,
		const ILGSpawnableArea * SpawnableAreaTemplate,
		FRandomStream& RandomStreamCopy,
		FTransform Transform,
		TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
		int32 EntryConnectorIndex,
		const TSet<FName>& AreaTags,
		const TArray<FName>& PassNames);

protected:

	void PostNewAreaLayout();

public:
	
	const class ALGGenerateableAreaDrunkenWalk * Parent;
	
	/** Lower bounds on percentage completion for this layout context */
	float PercentLower;

	/** Upper bounds on percentage completion for this layout context */
	float PercentUpper;

	/** Percentage completion interval between passes for this layout context */
	float PercentInterval;

	/** Index of the first area spawned by the first pass */
	int32 InitialAreaIndex;
			
	/*
	indices of areas spawned so far by this drunken walk generator that have available connectores
	in order of areas spawned
	used to help choose another room to spawn off from
	*/
	//FAERandomList RandomAreaIndexList;
	
	/**
	Similar to RandomAreaIndexList but holds a list of areas with priority connectors
	*/
	//FAERandomList PriorityRandomAreaIndexList;


	///////////////////
	//Pass related	
	int32 CurrentPassIndex;
	
	TSet<FName> PassAreaTags;

	TArray<FName> PassPassNames;

	/** Lower bounds on percentage completion for this layout context */
	float PassPercentLower;
	
	/** If true, will initialize the current pass index */
	bool bInitPass;

	/** If the generator is laying out  */
	UPROPERTY(transient)
	ULGSpawnableAreaLayoutContext * CurrentChildContext;

	/** Max cost of areas to spawn before being considered done */
	float AreaSpawnMaxCost;

	/** Cost of areas spawned in the pass so far */
	float AreaSpawnedCost;

	/** index of the first area that would be spawned by this pass */
	int32 PassInitialAreaIndex;
	
	/** index of the first area spawned by a layout call */
	int32 LastAreaIndex;
};

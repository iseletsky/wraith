#pragma once

#include "LGGenerateableArea.h"
#include "LGGenerateableAreaMultifloor.generated.h"

UENUM(BlueprintType)
enum class GenerateableAreaFloorType : uint8
{
	BOTTOM		UMETA(DisplayName = "Bottom"),
	MIDDLE		UMETA(DisplayName = "Middle"),
	TOP			UMETA(DisplayName = "Top")
};

UCLASS(Abstract)
class ALGGenerateableAreaMultifloor : public ALGGenerateableArea
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnableArea")
	TSubclassOf<class ALGSpawnablePrefabFloorArea> BottomFloorPrefab;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnableArea")
	TSubclassOf<class ALGSpawnablePrefabFloorArea> MiddleFloorPrefab;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnableArea")
	TSubclassOf<class ALGSpawnablePrefabFloorArea> TopFloorPrefab;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generator Params")
	int32 MinFloors;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generator Params")
	int32 MaxFloors;

	virtual void InitMetadata(class ALGGeneratingLevel * GeneratingLevel) override;

	virtual void GetConnectorTypes(TArray<TSubclassOf<class ALGSpawnablePrefabConnector>>& OutArray) const override;

	virtual ULGSpawnableAreaLayoutContext * BeginLayoutArea(float PercentLower, float PercentUpper,
		class ALGGeneratingLevel * GeneratingLevel,
		FRandomStream& RandomStream,
		FTransform Transform,
		TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
		int32 EntryConnectorIndex,
		const TSet<FName>& AreaTags,
		const TArray<FName>& PassNames) const override;

	bool LayoutArea(class ALGGeneratingLevel * GeneratingLevel,
		FRandomStream& RandomStream,
		FTransform Transform,
		TSubclassOf<ALGSpawnablePrefabConnector> EntryConnectorClass,
		int32 EntryConnectorIndex,
		const TSet<FName>& AreaTags,
		const TArray<FName>& PassNames) const;

	FORCEINLINE TSubclassOf<class ALGSpawnablePrefabFloorArea> PrefabClassForFloorType(GenerateableAreaFloorType FloorType) const
	{
		switch (FloorType)
		{
		case GenerateableAreaFloorType::BOTTOM:
			return BottomFloorPrefab;

		case GenerateableAreaFloorType::MIDDLE:
			return MiddleFloorPrefab;

		case GenerateableAreaFloorType::TOP:
			return TopFloorPrefab;

		default:
			check(false);
		}

		return NULL;
	}

protected:
	TArray<TSubclassOf<class ALGSpawnablePrefabConnector>> CachedConnectorTypes;

	/** Map of connector class to which floors use that connector */
	TMap<TSubclassOf<class ALGSpawnablePrefabConnector>, TArray<GenerateableAreaFloorType>> ConnectorClassToIncomingFloorType;
};
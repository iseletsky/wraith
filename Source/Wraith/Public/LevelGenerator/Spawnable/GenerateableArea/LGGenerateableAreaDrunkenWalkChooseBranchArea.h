#pragma once

#include <functional>
#include "LGGenerateableAreaDrunkenWalkChooseBranchArea.generated.h"

UCLASS(Abstract/*, BlueprintType, Blueprintable*/)
class ULGGenerateableAreaDrunkenWalkChooseBranchArea : public UObject
{
	GENERATED_BODY()
public:
	typedef std::function<bool(int32, FRandomStream&, bool)> LayoutAtAreaFunc;

	/** The event that allows the choose branch object to find a branch point */
	//UFUNCTION(BlueprintImplementableEvent)
	virtual bool PassChooseBranchArea(class ALGGeneratingLevel * GeneratingLevel,
		LayoutAtAreaFunc LayoutFunc,
		const TSet<FName>& BranchPassNames, 
		FRandomStream& RandomStream) const PURE_VIRTUAL(ULGGenerateableAreaDrunkenWalkChooseBranchArea::PassChooseBranchArea, return false;);
		
protected:
	
};
#pragma once

#include "AERandomList.h"
#include "LGGenerateableAreaDrunkenWalkPostPass.generated.h"

UCLASS(Abstract/*, BlueprintType, Blueprintable*/)
class ULGGenerateableAreaDrunkenWalkPostPass : public UObject
{
	GENERATED_BODY()
public:
	
	virtual void PostPassEvent(class ALGGeneratingLevel * GeneratingLevel,
		const TArray<FName>& PassNames,
		FRandomStream& RandomStream) const PURE_VIRTUAL(ULGGenerateableAreaDrunkenWalkPostPass::PostPassEvent, );
	
protected:
	
};
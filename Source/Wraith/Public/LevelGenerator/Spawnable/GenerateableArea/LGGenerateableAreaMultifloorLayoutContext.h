#pragma once

#include "LGSpawnableAreaLayoutContext.h"
#include "LGGenerateableAreaMultifloorLayoutContext.generated.h"

class ALGGenerateableAreaMultifloor;

UCLASS(transient)
class ULGGenerateableAreaMultifloorLayoutContext : public ULGSpawnableAreaLayoutContext
{
	GENERATED_BODY()
public:
	virtual ELayoutContextStatus ContinueLayout(std::chrono::high_resolution_clock::time_point BeginTime) override;

	UPROPERTY(transient)
	const class ALGGenerateableAreaMultifloor * Parent;
};

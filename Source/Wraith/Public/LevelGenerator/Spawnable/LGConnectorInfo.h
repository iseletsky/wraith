#pragma once 
#include "LGConnectorInfo.generated.h"

/**
Info about a connector between areas
*/
UCLASS(NotPlaceable)
class ALGConnectorInfo : public AActor
{
	GENERATED_BODY()
public:
	ALGConnectorInfo(const FObjectInitializer& ObjectInitializer);

	/**
	Clears the collision components.
	*/
	void ClearAreaCollision();
		
	/** Area index this connector belongs to */
	int32 AreaIndex;
		
	/** Other connector index this connector corresponds to */
	int32 OtherConnectorIndex;	
};
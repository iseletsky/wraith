#pragma once 
#include "LGGenerateableAreaDrunkenWalk.h"
#include "LGGeneratingLevel.generated.h"

USTRUCT()
struct FLGAreaLayoutInfo
{
	GENERATED_USTRUCT_BODY()

	FLGAreaLayoutInfo()
		: bConnectorIndexBucketsJustEmptied(false),
		bPriorityConnectorIndexBucketsJustEmptied(false)
	{}

	/**
	Call this to remove a connector index from the buckets.
	After a batch of connector index removal call CheckAreaFreeConnectors for the area index so the area is properly handled if it has no more free connectors.
	*/
	void RemoveConnectorIndex(int32 ConnectorIndex);

	/**
	Random buckets of available connector from this area to connector indices
	*/
	FAERandomBuckets ConnectorIndexBuckets;

	/**
	Whether or not the connector buckets were just emptied.
	*/
	bool bConnectorIndexBucketsJustEmptied;

	/**
	Random buckets of available priority connectors from this area to connector indices
	*/
	FAERandomBuckets PriorityConnectorIndexBuckets;

	/**
	Whether or not the priority connector buckets were just emptied.
	*/
	bool bPriorityConnectorIndexBucketsJustEmptied;
	
	/**
	Area cost allows an area to count as part of an area or as multiple areas.
	A typical room is 1.  A tiny hallway might be .25 or .5.  A large room could count as 1.5 or 2.0 or whatever makes sense.
	*/
	float AreaCost;

	/**
	Components in the corresponding ALGAreaInfo to omit from area collision checks if an area is trying to branch off of this area.
	*/
	TArray<UPrimitiveComponent *> AreaOmitBranchSpawnCollision;
};

USTRUCT()
struct FLGSpawnablePrefabAreaSpawnInfo
{
	GENERATED_USTRUCT_BODY()

	/**
	Transform with which to spawn the prefab area
	*/
	FTransform Transform;

	/**
	Prefab Area class to spawn
	*/
	TSubclassOf<class ALGSpawnablePrefabArea> SpawnablePrefabAreaClass;
};

/**
Info about a connector between areas as it's being laid out
*/
USTRUCT()
struct FLGConnectorLayoutInfo
{
	GENERATED_USTRUCT_BODY()

	/** Source component this connector was based on */
	class ULGPrefabSlotComponentConnector * SourceComponent;
	
	/** If this is the primary connector, it's responsible for also spawning the Connector prefab class at spawn time */
	bool bIsPrimaryEnd;

	/** When ready, this is the connector class that will be spawned */
	TSubclassOf<class ALGSpawnablePrefabConnector> ConnectorClass;

	/** When ready, this is the wall class that will be spawned */
	TSubclassOf<class ALGSpawnablePrefabObject> WallClass;

	/** Keeps own index cached to help the setup methods reference the corresponding connector info object */
	int32 ConnectorIndex;
	
	void SetupWallClass();

	/**
	Sets up the collision components for the connector info for the walls only connector class.
	This is used for area collision until the connector gets occupied, in case the wall instance takes up space.
	*/
	void SetupAreaCollision(class ALGGeneratingLevel * GeneratingLevel);

	void SetupAreaCollision(class ALGGeneratingLevel * GeneratingLevel, class ALGConnectorInfo * ConnectorInfo);
	
	void SetupAreaCollisionComponents(class ALGConnectorInfo * ConnectorInfo, const TArray<UPrimitiveComponent *>& TemplateCollisionAreas, bool bIsWall);
};

/**
Put this actor into a blank map to begin random level generation.
*/
UCLASS(Abstract, HideCategories = (Rendering, Replication, Input, Actor, Tags))
class ALGGeneratingLevel : public AActor
{
	GENERATED_BODY()
public:
	ALGGeneratingLevel(const FObjectInitializer& ObjectInitializer);
			
	virtual bool IsLevelBoundsRelevant() const override
	{
		return false;
	}

	virtual void Tick(float DeltaTime) override;
	
	virtual void BeginPlay() override;
	
	////////////////////////////////////////////////
	//customizeable blueprint properties
	
	/** If enabled, saves debug playback data */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Debugging)
	bool bDebugPlayback;

	/** If enabled, creates area graph visualization objects. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Debugging)
	bool bDebugAreaGraph;

	/** The starting random seed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Randomization)
	int32 RandomSeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Randomization)
	TSubclassOf<ALGGenerateableAreaDrunkenWalk> RootGenerateableArea;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Level)
	FName PersistentLevelName;
		
	////////////////////////////////////////////////
	//loading screen stuff

	/** Begins showing the loading screen */
	UFUNCTION(BlueprintImplementableEvent)
	void BeginLoadingScreen();

	/** Ends showing the loading screen */
	UFUNCTION(BlueprintImplementableEvent)
	void EndLoadingScreen();

	/** Set the message that shows up during the loading screen */
	UFUNCTION(BlueprintImplementableEvent)
	void SetLoadingScreenMessage(const FString& Message);

	/** Set the percentage complete number */
	UFUNCTION(BlueprintImplementableEvent)
	void SetLoadingScreenPercentage(float Percentage);

	////////////////////////////////////////////////
	//debug stuff
	UPROPERTY()
	class ALGGeneratorDebugData * DebugData;

	void AddObjectToStep(TSubclassOf<class ALGSpawnablePrefab> InSpawnablePrefabClass, FTransform InTransform);
	void AddLogMessage(const FString& LogMessage);
	void AddAreaIndexToStep(int32 AreaIndex);

	void EndAddingToStep(bool bSuccessfulStep);

	////////////////////////////////////////////////
	//generator stuff	
	FRandomStream RandomStream;	
	float CurrentPercentDone;
	
	/**
	Called by the server on the GameMode or the PlayerController on the client.
	*/
	void ReceiveSeed(int32 Seed);

	void SetupConnectorLink(int32 FromConnectorIndex, int32 ToConnectorIndex, TSubclassOf<ALGSpawnablePrefabConnector> ConnectorClass);

	void LinkAreas(int32 AreaA, int32 AreaB, int32 FromConnector);
	
	TMap<int32, int32> AreaIndexToFreePriorityConnectorIndices;
	TArray<FLGSpawnablePrefabAreaSpawnInfo> AreaSpawnInfos;
	TArray<FLGAreaLayoutInfo> AreaLayoutInfos;
	TArray<FLGConnectorLayoutInfo> ConnectorLayoutInfos;

	enum class State {
		AWAIT_SEED,
		RECEIVED_SEED,
		LAYOUT,
		CREATE_FILE,
		SPAWN,
		POST
	};

	State GeneratorState;

	UPROPERTY(transient)
	ULGSpawnableAreaLayoutContext * RootLayoutContext;

	/** The generated level object that is connected to this generating level object */
	UPROPERTY(transient)
	class ALGGeneratedLevel * GeneratedLevel;

	////////////////////////////////////////////////
	//area index list management
	/**
	Checks if the area has no more free connectors.  If so, removes it from the pass names list so no more areas try to branch from it.
	*/
	void CheckAreaFreeConnectors(int32 AreaIndex);

	TMap<FName, FAERandomList> PassNameToAreaIndexList;
	TMap<FName, FAERandomList> PassNameToPriorityAreaIndexList;

	/** For each area, this keeps track of pass names an area index belongs to since due to the heirarchical nature, it could belong to multiple layers of passes */
	TMultiMap<int, FName> AreaIndexToPassNames;

	////////////////////////////////////////////////
	//max times class spawned

	/**
	Checks if an area class has spawned too many times.
	Returns true if it hasn't and the constraint isn't broken, false otherwise.
	*/
	bool CheckMaxTimesClassSpawnedConstraint(UClass * AreaClass, int32 Maximum) const;

	/**
	Call this to increment the number of times an area class has spawned
	*/
	void IncrementTimesClassSpawned(UClass * AreaClass);

	/** Used to track how many times a certain prefab class has spawned in the level to help enforce the LevelMaxSpawn constraint in SpawnableAreaProbabilityList */
	TMap<UClass *, int32> NumTimesClassSpawned;

	////////////////////////////////////////////////
	//spawnable instances
	
	template<typename SpawnableType = class ILGSpawnable>
	FORCEINLINE SpawnableType * GetSpawnableInstance(TSubclassOf<AActor> Class)
	{
		//see if it's in the map of objects already
		{
			AActor ** Object = InstantiatedSpawnables.Find(Class);

			if (Object)
			{
				return Cast<SpawnableType>(*Object);
			}
		}
		
		//spawn an instance
		AActor * NewInstanceActor = GetWorld()->SpawnActor(Class);
		InstantiatedSpawnables.Add(Class, NewInstanceActor);

		//disable collision on the instance so its area collision doesn't collide with anything
		NewInstanceActor->SetActorEnableCollision(false);

		//disable visibility so it can't render on accident during the loading screen
		NewInstanceActor->SetActorHiddenInGame(true);

		SpawnableType * NewInstance = Cast<SpawnableType>(NewInstanceActor);
		NewInstance->InitMetadata(this);

		return NewInstance;
	}

	/** Instantiated template objects */
	TMap<TSubclassOf<AActor>, AActor *> InstantiatedSpawnables;	
};
#pragma once

#include "LGPlayerState.generated.h"

UCLASS()
class ALGPlayerState : public APlayerState
{
	GENERATED_BODY()
public:

	/**
	Used to show how much is left to go for a player when generating the random map.
	This is replicated from server to clients.
	The owning client should notify the server of their percentage using ALGPlayerController::ServerNotifyMapGeneratedPercentage.
	The client should then call ServerNotifyMapGenerated when it's fully ready.
	*/
	UPROPERTY(BlueprintReadOnly, Replicated)
	float MapGeneratedPercentage;
};
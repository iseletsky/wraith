#pragma once

#include "LGGameState.generated.h"

UCLASS(Config = Game)
class ALGGameState : public AGameState
{
	GENERATED_BODY()
public:
	UPROPERTY(Replicated)
	int32 MapSeed;

	/** Returns true if the match state is InProgress or later */
	virtual bool HasMatchStarted() const override;
	
	/** Match state has changed */
	virtual void OnRep_MatchState() override;
};
#pragma once

#include "LGCampaignState.generated.h"

/**
Maintains campaign state between map transitions by being added to the seamless travel actor list.
Helps transfer over player inventories.
Keeps track of saved map data for visited areas.
Should only exist on the server.


For now this isn't used, only keeping since there's no point in deleting work that was started.
I will probably use these in an expansion pack later.
*/
UCLASS(Config = Game, NotPlaceable, BlueprintType, Blueprintable,
	HideCategories = (Info, Rendering, MovementReplication, Replication, Actor))
class ALGCampaignState : public AInfo
{
	GENERATED_BODY()
public:
	/**
	Path to the saved games directory.
	This will store generated map data and persisted map states.
	*/
	UPROPERTY()
	FString SaveGamePath;

	/**
	Map names that have already been generated during this campaign.
	If a map name is in this array, load its corresponding saved map files when reentering an area.
	*/
	UPROPERTY()
	TArray<FName> GeneratedMaps;
};
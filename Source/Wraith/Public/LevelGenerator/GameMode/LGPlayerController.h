#pragma once

#include "AEPlayerController.h"
#include "LGPlayerController.generated.h"

/**
Tracks the state of a client's map generation process.
*/
UENUM(BlueprintType)
enum class EClientMapState : uint8
{
	AWAITING_SEED	UMETA(DisplayName = "Awaiting Seed"),
	GENERATING		UMETA(DisplayName = "Generating"),
	GENERATED		UMETA(DisplayName = "Generated")
};

/**
Since only PlayerController exists locally for clients for a player, it must be the object that notifies the server that this client
has its randomly generated map ready
*/
UCLASS()
class ALGPlayerController : public AAEPlayerController
{
	GENERATED_BODY()

public:
	ALGPlayerController(const FObjectInitializer& ObjectInitializer);
	
	/**
	Tracks the state of a client's map generation process.
	*/
	UPROPERTY(BlueprintReadOnly, Replicated)
	EClientMapState ClientMapState;
		
	/**
	Making this public.
	*/
	virtual void SetSpawnLocation(const FVector& NewLocation) override;

	void NotifyMapGenerateStart(int32 MapSeed);
	
	/**
	Notifies the server that the randomly generated map for this player controller is ready.
	*/
	UFUNCTION(Server, WithValidation, Reliable)
	void ServerNotifyMapGenerated();

	/**
	Notifies the server what the player's map generation percentage is.
	The value is stored in ALGPlayerState
	*/
	UFUNCTION(Server, WithValidation, Unreliable)
	void ServerNotifyMapGeneratedPercentage(float InMapGeneratedPercentage);
	
protected:
	/**
	Notifies the client that it should start generating its map.
	*/
	UFUNCTION(Client, Reliable)
	void ClientNotifyMapGenerateStart(int32 MapSeed);	
};
#pragma once

#include "AEGameMode.h"
#include "LGGameMode.generated.h"

namespace MatchState
{
	//TODO: change to LG_API when I split level generator off into its own module
	extern WRAITH_API const FName GeneratingMap;				// We are generating the map
	extern WRAITH_API const FName GeneratedMapWaitingToStart;	// We have generated the map and are waiting to start the actual game
};

UCLASS(Config = Game)
class ALGGameMode : public AAEGameMode
{
	GENERATED_BODY()
public:
	ALGGameMode(const FObjectInitializer& ObjectInitializer);

	//For now, Campaign State isn't used.

	/**
	If NULL, doesn't use CampaignState at all, which is good for regular multiplayer games.
	*/
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Classes)
	//TSubclassOf<class ALGCampaignState> CampaignStateClass;

	///**
	//Campaign state if one is needed.
	//*/
	//UPROPERTY()
	//ALGCampaignState * CampaignState;
	
	virtual void Tick(float DeltaSeconds) override;

	/** Returns true if the match state is InProgress or later */
	virtual bool HasMatchStarted() const override;

	/** Returns true if the server has loaded its map. */
	virtual bool HasServerMapLoaded() const;

	/** Transitions to WaitingToStart and calls BeginPlay on actors. */
	virtual void StartPlay() override;

	virtual void RemotePlayerControllerMapReady(APlayerController * PlayerController);
	virtual void LocalMapReady();
	
	virtual TSubclassOf<class AGameSession> GetGameSessionClass() const override;

	virtual void GetSeamlessTravelActorList(bool bToEntry, TArray<AActor*>& ActorList) override;

	virtual void PostSeamlessTravel() override;

	virtual void PostLogin(APlayerController* NewPlayer) override;	

	//TODO: for players who are still generating the map, force them to have no spawn actor so they start far away

protected:

	/** @return True if ready to Start Match. Games should override this */
	virtual bool ReadyToStartMatch();

	/** Updates the match state and calls the appropriate transition functions */
	virtual void SetMatchState(FName NewState) override;

	/** Called when the state transitions to WaitingToStart.
	Kicks off the map generating process.*/
	virtual void HandleMatchIsWaitingToStart() override;

	/** Called when the state transitions to MatchGeneratingMap */
	virtual void HandleMatchGeneratingMap();

	/** Called when the state transitions to GeneratedMapWaitingToStart.
	Logic from GameMode::HandleMatchIsWaitingToStart is called here.*/
	virtual void HandleMatchGeneratedMapIsWaitingToStart();

	/**
	Resets a player controller's start position to some valid start location that isn't beyond net cull distance
	*/
	virtual void ResetPlayerControllerStart(APlayerController * PlayerController);

	/** Player controllers whose maps were generated */
	TArray<APlayerController *> GeneratedMapPlayerControllers;
};
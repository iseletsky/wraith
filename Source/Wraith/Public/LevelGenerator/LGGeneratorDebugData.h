#pragma once 

#include "LGGeneratorDebugData.generated.h"

struct FGeneratorDebugStep
{
	TArray<USceneComponent *> Components;
	TArray<int32> AreaIndices;
	TArray<FString> LogMessages;

	bool bIsSuccessful;
};

/** 
Stores the states of area layout attempts so you can replay them and visualize what happened.
*/
UCLASS(NotPlaceable, HideCategories = (Rendering, Replication, Input, Actor, Tags))
class ALGGeneratorDebugData : public AActor
{
	GENERATED_BODY()
public:
	ALGGeneratorDebugData(const FObjectInitializer& ObjectInitializer);
	
	virtual void BeginPlay() override;

	void AddObjectToStep(TSubclassOf<class ALGSpawnablePrefab> InSpawnablePrefabClass, FTransform InTransform, class ALGGeneratingLevel * GeneratingLevel);
	void AddLogMessage(const FString& LogMessage);
	void AddAreaIndexToStep(int32 AreaIndex);
	
	void EndAddingToStep(bool bSuccessfulStep);

	int32 GetCurrentStep();
	int32 GetNumSteps();

	void GoToStep(int32 Step);
	
	void RestartVis();
	void NextVisStep();
	void PrevVisStep();
	void FinalVisStep();

protected:
	TArray<FGeneratorDebugStep> GeneratorSteps;
	int32 CurrentStep;
};
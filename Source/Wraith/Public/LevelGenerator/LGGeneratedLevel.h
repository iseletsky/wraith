#pragma once 
#include "LGConnectorInfo.h"
#include "LGAreaInfo.h"
#include "AERandomList.h"
#include "LGGeneratedLevel.generated.h"

/**
Info about an area's link to another area
*/
struct AreaEdgeInfo
{
	/** Distance between two nodes, used for shortest path calculations */
	float Distance;

	/** Which source connector indices are the source of this link.  It may be necessary to turn these connectors into a locked door sometimes. */
	TSet<int32> FromConnectorInd;
};

struct AreaEdge
{
	AreaEdge(int32 InOtherLinkInd, int32 InAreaEdgeInfoInd)
		: OtherLinkInd(InOtherLinkInd),
		AreaEdgeInfoInd(InAreaEdgeInfoInd)
	{}

	bool operator==(const AreaEdge& Other) const
	{
		return Other.OtherLinkInd == OtherLinkInd;
	}
	
	int32 OtherLinkInd;
	int32 AreaEdgeInfoInd;	
};

inline uint32 GetTypeHash(const AreaEdge& InAreaEdge)
{
	return ::GetTypeHash(InAreaEdge.OtherLinkInd);
}

struct AreaNodePathInfo
{
	AreaNodePathInfo()
		: PathDistance(std::numeric_limits<float>::max()),
		AreaNodeIndex(-1),
		ParentAreaNodeIndex(-1)
	{}

	float PathDistance;
	int32 AreaNodeIndex;
	int32 ParentAreaNodeIndex;
};

struct AreaNodePathInfos
{
	TMap<int32, AreaNodePathInfo> PathDistances;
};

/** 
Stores the state of a generated level and its area connection graph
*/
UCLASS(NotPlaceable, HideCategories = (Rendering, Replication, Input, Actor, Tags))
class ALGGeneratedLevel : public AInfo
{
	GENERATED_BODY()
public:
	ALGGeneratedLevel(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(transient)
	TArray<ALGConnectorInfo *> ConnectorInfos;

	UPROPERTY(transient)
	TArray<ALGAreaInfo *> AreaInfos;
		
	/** Stores links between areas */
	TMap<int32, TSet<AreaEdge>> AreaGraph;
	TArray<AreaEdgeInfo> AreaGraphEdges;

	TMap<FName, FAERandomList> PassNameToAreaIndexList;
	
	/**
	Basically performs Dijkstras algorithm but not necessarily used to find the shortest path itself.
	Does a search from a source area for all linked nodes in the subgraph who's area node tags are in the AreaNodeTags list.
	If the list is empty, there is no node tag restriction.
	Stores shortest paths from SourceAreaIndex to each node.
	This info can be used, for example, to find the farthest node from an area, to make that be the level end.
	It can also be used to get a shortest path from SourceAreaIndex to that node.
	*/
	void GetAreaNodePathInfos(int32 SourceAreaIndex, const TSet<FName>& AreaNodeTags, AreaNodePathInfos& Destination);

	/**
	Performs the A star algorithm to find the shortest path between two areas whos area node tags are in the AreaNodeTags list.
	If the list is empty, there is no node tag restriction.
	It puts the data into the same structure used by GetAreaNodePathInfos, and the shortest path can be rebuilt by starting at FarthestNodeIndex and following the parent chain back to the start.
	*/
	void GetAreaNodePathInfosForAstarPath(int32 SourceAreaIndex, int32 DestinationAreaIndex, const TSet<FName>& AreaNodeTags, AreaNodePathInfos& Destination);	
};
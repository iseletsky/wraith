#pragma once
#include "LGGenerateableAreaDrunkenWalkPostPass.h"
#include "WRDevThemeTestPostPass.generated.h"

UCLASS()
class UWRDevThemeTestPostPass : public ULGGenerateableAreaDrunkenWalkPostPass
{
	GENERATED_BODY()
public:
	virtual void PostPassEvent(class ALGGeneratingLevel * GeneratingLevel,
		const TArray<FName>& PassNames,
		FRandomStream& RandomStream) const override;
};


#pragma once

#include "WRUsableInterface.generated.h"

// implement this interface for Actors that should be usable by a player using the use key.
// These might be buttons, switches, items that can be picked up via use key, etc...
UINTERFACE(BlueprintType)
class UWRUsableInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class WRAITH_API IWRUsableInterface
{
	GENERATED_IINTERFACE_BODY()

	/**
	When this Usable is a pawn's use target, the pawn will call this on the usable object
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Game)
	void Use(APawn * User);

	/**
	Returns if a Pawn must be aiming at the usable to be able to use it.
	Things like weapons don't need to be aimed at to pick up, but a switch on a wall does need to be aimed at, for example.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Game)
	bool RequiresAim() const;

	/**
	Returns the action string that shows up for using this object.  Be sure to localize this.
	Some examples are: "press the button.", "open the door.", "pickup the shotgun."
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Game)
	FText UseActionText() const;
};

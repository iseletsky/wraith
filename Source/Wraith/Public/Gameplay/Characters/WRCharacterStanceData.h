#pragma once

#include "Animation/AimOffsetBlendSpace.h"
#include "WRCharacterStanceData.generated.h"

UCLASS(Blueprintable, BlueprintType)
class WRAITH_API UWRCharacterStanceData : public UDataAsset
{
	GENERATED_BODY()

public:
	/**
	The blend space used for the character aiming around
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation")
	UAimOffsetBlendSpace * AimBlendSpace;

	/**
	If an equippable doesn't override the idle animation to use, it uses this
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation")
	UAnimSequence * DefaultIdleAnimation;

	/**
	If an equippable doesn't override the equip animation to use, it uses this
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation")
	UAnimMontage * DefaultEquipAnimation;

	/**
	If an equippable doesn't override the unequip animation to use, it uses this
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation")
	UAnimMontage * DefaultUnequipAnimation;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation Control")
	float HeadCrouchVerticalOffset;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation Control")
	float HeadCrouchForwardOffset;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation Control")
	float HeadDownVerticalOffset;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation Control")
	float HeadDownForwardOffset;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation Control")
	float HeadStraightVerticalOffset;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation Control")
	float HeadStraightForwardOffset;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation Control")
	float HeadUpVerticalOffset;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation Control")
	float HeadUpForwardOffset;
};
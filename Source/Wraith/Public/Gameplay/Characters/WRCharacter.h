#pragma once
#include "GameFramework/Character.h"
#include "WRUsableInterface.h"
#include "WRInventoryPhysical.h"
#include "WRPlayerController.h"
#include "WRTeams.h"
#include "Runtime/AIModule/Classes/Perception/AISightTargetInterface.h"
#include "Runtime/GameplayTags/Classes/GameplayTagAssetInterface.h"
#include "WRCharacter.generated.h"

class AWRProjectile;
class AWRAIActionManager;
class UWRInventoryManager;

UCLASS()
class AWRCharacter : public ACharacter, public IAISightTargetInterface, public IGenericTeamAgentInterface, public IGameplayTagAssetInterface
{
	GENERATED_BODY()
public:
	AWRCharacter(const FObjectInitializer& ObjectInitializer);
		
	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	virtual void SpawnDefaultController() override;

public:
	///////////////////////////////////////
	//Gameplay Tag

	UFUNCTION(BlueprintCallable, Category = GameplayTags)
	virtual void GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const override 
	{ 
		TagContainer = GameplayTags;
	}
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameplayTags")
	FGameplayTagContainer GameplayTags;

public:
	///////////////////////////////////////
	//AI
	virtual bool CanBeSeenFrom(const FVector& ObserverLocation, FVector& OutSeenLocation, int32& NumberOfLoSChecksPerformed, float& OutSightStrength, const AActor* IgnoreActor = NULL) const;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	TSubclassOf<AWRAIActionManager> AIActionManagerClass;
		
	UPROPERTY(BlueprintReadWrite, Category = "AI")
	AWRAIActionManager * AIActionManager;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AI")
	void InitAIActionManager();
		
public:
	///////////////////////////////////////
	//Team

	/**
	This is just used to set up the character's FGenericTeamId object.
	If this is set to Invalid, it falls back to whatever value is set for TeamIdNum
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
	TEnumAsByte<WRTeam::Type> TeamIdEnum;

	/**
	This is just used to set up the character's FGenericTeamId object.
	This is ignored if TeamIDEnum is set to a value other than Invalid.
	Otherwise, it falls back to creating a unique team with this id number.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
	uint8 TeamIdNum;
	
	UPROPERTY(BlueprintReadWrite, Category = "Team")
	FGenericTeamId TeamID;

	virtual void SetGenericTeamId(const FGenericTeamId& NewTeamID) override {
		TeamID = TeamID;
	}

	virtual FGenericTeamId GetGenericTeamId() const override;

protected:
	/**
	Used to lazy load the character's GenericTeamId based on the initializer values.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Team")
	uint32 bGenericTeamIdInitialized:1;

public:
	///////////////////////////////////////
	//Input

	virtual void SetupPlayerInputComponent(UInputComponent * PlayerInputComponent) override;

	virtual void DisableInput(class APlayerController * PlayerController) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input")
	void OnLoseControl();

public:
	///////////////////////////////////////
	//Movement

	/**
	Sets up movement input for an input component.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void SetupMovementInput(UInputComponent * PlayerInputComponent);

	virtual void OnStartCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;
	virtual void OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;
	virtual void FaceRotation(FRotator NewControlRotation, float DeltaTime) override; 

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void MoveForward(float Val);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void MoveRight(float Val);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void MoveUp(float Val);

	/**
	Helps control some aspects of character movement as well as driving the character animation.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	float GetCrouchAmount() const;

	/** When landing and ratio of lateral velocity to vertical is less than this, the stumble direction is random  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float MinRandomStumbleLateralToVerticalFallSpeedThreshold;

	/** Landing at faster than this velocity results in damage (note: use positive number) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float MaxSafeFallSpeed;

	/** amount of falling damage dealt if the player's fall speed is double MaxSafeFallSpeed (scaled linearly from there) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float FallingDamageFactor;

	/** amount of damage dealt to other characters we land on per 100 units of speed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float CrushingDamageFactor;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void ProcessFallingDamage(const FHitResult& Hit, float FallingSpeed);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void TakeFallingDamage(const FHitResult& Hit, float FallingSpeed, float FallingDamage);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void Landed(const FHitResult& Hit) override;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
    void Jumped();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void SetWantsToSprint(bool bWantsToSprint);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	bool GetWantsToSprint();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	bool GetIsSprinting() const;
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void CrouchInput();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void UnCrouchInput();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void ToggleCrouch();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void SprintInput();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void UnSprintInput();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	void ToggleSprint();
		
public:
	///////////////////////////////////////
	//Use button

	/**
	Sets up use input for an input component.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Usable")
	void SetupUsableInput(UInputComponent * PlayerInputComponent);

	/**
	Current use target that will be activated the player hits the use button
	//TODO: make this protected and add a getter?
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Usable")
	TScriptInterface<IWRUsableInterface> TargetUsable;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Usable")
	void UpdateTargetUsable();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Usable")
	void Use();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Usable")
	void AddUsable(const TScriptInterface<IWRUsableInterface>& Usable);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Usable")
	void RemoveUsable(const TScriptInterface<IWRUsableInterface>& Usable);
	
protected:

	/**
	Usable Objects the character is currently overlapping with that can be possibly picked up with the action key.
	*/
	UPROPERTY(BlueprintReadWrite, Category = Usable)
	TArray<TScriptInterface<IWRUsableInterface>> UsableObjects;
	
public:
	/////////////////////////////////////// 
	//Camera

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	UCameraComponent * Camera1P;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	USpringArmComponent * SpringArm3P;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	UCameraComponent * Camera3P;
	
	/**
	Returns true if set up for 1P, false if already in 1P
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Camera")
	bool Setup1P();

	/**
	Returns true if set up for 3P, false if already in 3P
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Camera")
	bool Setup3P();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Camera")
	bool IsFirstPerson() const;

	/**
	Convenience method to more easily retreive the camera manager from the player controller
	*/
	UFUNCTION(BlueprintCallable, Category = "Camera")
	APlayerCameraManager * GetCameraManager() const;

	/**
	A somewhat odd hack I had to do to prevent the character's hand from lagging behind by a frame while crouching
	https://forums.unrealengine.com/showthread.php?128577-Frame-appears-to-render-in-the-middle-of-a-game-Tick
	The anim blueprint should call this because that's the right time in the game tick to position the camera.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Camera")
	void RepositionCameraComponents();

	/**
	Gets the location the crosshair is aiming at from the eyes point of view.
	Does a trace against the first object that would collide with the specified trace channel.

	This would usually be used to allow a gun to fire any projectiles right at the crosshair and always be on point even when fired from an offset.
	Projectiles start at the EquippableShootOffset and fly towards the ShootLocation.
	*/
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Camera")
	FVector GetCrosshairLocation(ECollisionChannel TraceChannel = /*COLLISION_TRACE_WEAPON*/ ECC_GameTraceChannel2, float MaxDistance = 50000.0f, bool bDebugDraw = false) const;

	/**
	Gets a safe spawn location for an actor that the character is spawning.
	Usually used for dropped inventory items.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Camera")
	FVector GetSafeActorSpawnLocation(const FVector& SpawnedActorWorldLocation,
		ECollisionChannel TraceChannel = /*COLLISION_TRACE_WEAPON*/ ECC_GameTraceChannel2,
		float SpawnedActorRadius = 1.f,
		bool bDebugDraw = false);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Camera")
	FTransform GetEyesWorldTransform() const;

public:
	//////////////////////////////////////
	//Camera Animation

	virtual void RecalculateBaseEyeHeight() override;

	virtual FVector GetPawnViewLocation() const override;
	
	/**
	Wrapper around playing a camera animation.
	Also checks if a player is force disabling camera animations in some way or applies dampening if players want to tone down effects.
	*/
	UFUNCTION(BlueprintCallable, Category = "Camera Animation")
	UCameraAnimInst * PlayCameraAnim(
		class UCameraAnim * Anim,
		float Rate = 1.f, 
		float Scale = 1.f, 
		float BlendInTime = 0.f, 
		float BlendOutTime = 0.f, 
		bool bLoop = false,
		bool bRandomStartTime = false, 
		float Duration = 0.f, 
		ECameraAnimPlaySpace::Type PlaySpace = ECameraAnimPlaySpace::CameraLocal, 
		FRotator UserPlaySpaceRot = FRotator::ZeroRotator);

	/**
	Wrapper around playing a camera animation with a duration rather than a rate.
	Duration here is different from duration provided in the other implementation as it just scales the playrate to make the animation take the right time but play fully.
	Also checks if a player is force disabling camera animations in some way or applies dampening if players want to tone down effects.
	*/
	UFUNCTION(BlueprintCallable, Category = "Camera Animation")
	UCameraAnimInst * PlayCameraAnimWithDuration(
		class UCameraAnim * Anim,
		float Duration,
		float Scale = 1.f,
		float BlendInTime = 0.f,
		float BlendOutTime = 1.f,
		ECameraAnimPlaySpace::Type PlaySpace = ECameraAnimPlaySpace::CameraLocal,
		FRotator UserPlaySpaceRot = FRotator::ZeroRotator,
		bool bAccountForBlendoutTime = true);

	/**
	Convenience wrapper method for stopping all camera animations.
	This is a good thing to do when an equippable state is interrupted for example...
	*/
	UFUNCTION(BlueprintCallable, Category = "Camera Animation")
	void StopAllCameraAnims(bool bImmediate = false);

	/**
	Wrapper around playing a camera shake.
	Also checks if a player is force disabling camera shakes in some way or applies dampening if players want to tone down effects.
	*/
	UFUNCTION(BlueprintCallable, Category = "Camera Animation")
	UCameraShake * PlayCameraShake(
		TSubclassOf< class UCameraShake > ShakeClass,
		float Scale = 1.f, 
		enum ECameraAnimPlaySpace::Type PlaySpace = ECameraAnimPlaySpace::CameraLocal, 
		FRotator UserPlaySpaceRot = FRotator::ZeroRotator);

	/**
	The location of the eyes of the character based on various parameters.
	Used to position the character's head and eyes and first person camera.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Camera Animation")
	void UpdateRelativeEyesTransform();

	UFUNCTION(BlueprintCallable, Category = "Camera Animation")
	FTransform GetRelativeEyesTransform() const;
	
	UPROPERTY(BlueprintReadWrite, Category = "Camera Animation")
	uint32 bRelativeEyesTransformNeedsUpdate:1;

	/**
	Minimum pitch while standing.
	Try to tweak this so that it's as close to -90 as possible while still not clipping too much through legs.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Animation")
	float StandingMinViewPitch;

	/**
	Minimum pitch while sprinting.
	This is separate from StandingMinViewPitch because legs fly further out when running than whan walking and
	you're not going to really run well if looking down anyway so it may be OK to mess with the player's view a bit
	since they may not be as likely to be looking straight down when running anyway.
	Try to tweak this so that it's as close to -90 as possible while still not clipping too much through legs.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Animation")
	float SprintingMinViewPitch;

	/**
	Minimum pitch while crouch.
	Try to tweak this so that it's as close to -90 as possible while still not clipping too much through legs.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Animation")
	float CrouchingMinViewPitch;

	/**
	Returns the current minimum view pitch.
	This is intended to avoid awkward angles where the players arms will start clipping into the legs.
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Camera Animation")
	float GetMinViewPitch() const;

	/**
	Updates CurrentMinViewPitch
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Camera Animation")
	void UpdateMinViewPitch(float DeltaTime);

	/**
	Returns the desired min view pitch so CurrentMinViewPitch can interp to this value.
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Camera Animation")
	float GetDesiredMinViewPitch() const;

protected:
	/**
	The location of the eyes of the character relative to the feet of the character. 
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Camera Animation")
	FTransform RelativeEyesTransform;

	/**
	The current minimum view pitch.
	The Character will smoothly interp this to the value returned by GetDesiredMinViewPitch
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Camera Animation")
	float CurrentMinViewPitch;
	
public:
	///////////////////////////////////////
	//Crouch Height Animation

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Crouch Height Animation")
    void UpdateCrouchEyeHeightFromFeet(float DeltaSeconds);

	UPROPERTY(BlueprintReadWrite, Category = "Crouch Height Animation")
	FAlphaBlend CrouchEyeHeightFromFeetBlend;

	UFUNCTION(BlueprintCallable, Category = "Crouch Height Animation")
	float GetStandingEyeHeightFromFeet() const;

	UFUNCTION(BlueprintCallable, Category = "Crouch Height Animation")
	float GetCrouchedEyeHeightFromFeet() const;

	/**
	This should be called in BeginPlay as well as if values for deriving crouched eye height or standing eye height are ever updated.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Crouch Height Animation")
	void ComputeEyeHeightsFromFeet();

protected:
	/**
	Derived from BaseEyeHeight and standing capsule size.
	Used to determine crouch percentage.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Crouch Height Animation")
	float StandingEyeHeightFromFeet;

	/**
	Derived from CrouchedEyeHeight and crouching capsule size.
	Used to determine crouch percentage.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Crouch Height Animation")
	float CrouchedEyeHeightFromFeet;

public:
	///////////////////////////////////////
	//Stance Control

	/**
	The default aim offset blendspace to use on the character when nothing else is setting it.
	Usually an equipped weapon will change this.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation Stance Control")
	UWRCharacterStanceData * DefaultCharacterStance;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Stance Control")
	UWRCharacterStanceData * GetCharacterStance() const;

	/**
	Aim offsets relative to character
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Stance Control")
	FRotator GetAimOffsets() const;

public:
	///////////////////////////////////////
	//Turn In Place Animation

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Turn In Place Animation")
	float GetLeftMaxAimYawOffset() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Turn In Place Animation")
	float GetRightMaxAimYawOffset() const;
		
	UPROPERTY(BlueprintReadWrite, Category = "Turn In Place Animation")
	float DesiredYaw;

	UPROPERTY(BlueprintReadWrite, Category = "Turn In Place Animation")
	float YawSpeed;
	
public:
	///////////////////////////////////////
	//Head Control

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Head Control")
	FTransform GetHeadWorldTransform() const;
			
public:
	///////////////////////////////////////
	//Animation
	
	UFUNCTION(BlueprintCallable, Category = "Animation")
    bool GetJustJumpedAndReset();

protected:
    /**
    Gets set to true right after a jump and gets set to false as soon as GetJustJumped is called.
    Used for animation to play the jump animation.
    */
    UPROPERTY(BlueprintReadWrite, Category = "Animation")
    uint32 bJustJumped:1;

public:
	/////////////////////////////////////// 
	//Inventory

	UPROPERTY(BlueprintReadWrite, Category = "Inventory")
	UWRInventoryManager * InventoryManager;
	
public:
	/////////////////////////////////////// 
	//Projectile Shooting

	/**
	Gets a safe spawn location for something that shoots out from the player.
	Usually used for projectiles.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	FTransform GetSafeShootingSpawnWorldTransform(const FVector& SpawnedTraceOriginWorldLocation,
		ECollisionChannel TraceChannel = /*COLLISION_TRACE_WEAPON*/ ECC_GameTraceChannel2,
		float SpawnedActorRadius = 1.f,
		float MaxTestDist = 256.f,
		bool bDebugDraw = false);
};

///////////////////////////////////////
//Camera

FORCEINLINE_DEBUGGABLE APlayerCameraManager * AWRCharacter::GetCameraManager() const
{
	APlayerController * PC = Cast<APlayerController>(Controller);

	if (PC)
	{
		return PC->PlayerCameraManager;
	}

	return NULL;
}

///////////////////////////////////////
//Camera Animation

FORCEINLINE_DEBUGGABLE UCameraAnimInst * AWRCharacter::PlayCameraAnim(
	class UCameraAnim * Anim,
	float Rate,
	float Scale,
	float BlendInTime,
	float BlendOutTime,
	bool bLoop,
	bool bRandomStartTime,
	float Duration,
	ECameraAnimPlaySpace::Type PlaySpace,
	FRotator UserPlaySpaceRot)
{
	AWRPlayerController * PC = Cast<AWRPlayerController>(Controller);

	if (PC)
	{
		return PC->PlayCameraAnim(Anim, Rate, Scale, BlendInTime, BlendOutTime, bLoop, bRandomStartTime, Duration, PlaySpace, UserPlaySpaceRot);
	}

	return NULL;
}

FORCEINLINE_DEBUGGABLE UCameraAnimInst * AWRCharacter::PlayCameraAnimWithDuration(
	class UCameraAnim * Anim,
	float Duration,
	float Scale,
	float BlendInTime,
	float BlendOutTime,
	ECameraAnimPlaySpace::Type PlaySpace,
	FRotator UserPlaySpaceRot,
	bool bAccountForBlendoutTime)
{
	AWRPlayerController * PC = Cast<AWRPlayerController>(Controller);

	if (PC)
	{
		return PC->PlayCameraAnimWithDuration(Anim, Duration, Scale, BlendInTime, BlendOutTime, PlaySpace, UserPlaySpaceRot, bAccountForBlendoutTime);
	}

	return NULL;
}

FORCEINLINE_DEBUGGABLE void AWRCharacter::StopAllCameraAnims(bool bImmediate)
{
	AWRPlayerController * PC = Cast<AWRPlayerController>(Controller);

	if (PC)
	{
		return PC->StopAllCameraAnims(bImmediate);
	}
}

FORCEINLINE_DEBUGGABLE UCameraShake * AWRCharacter::PlayCameraShake(
	TSubclassOf< class UCameraShake > ShakeClass,
	float Scale,
	enum ECameraAnimPlaySpace::Type PlaySpace,
	FRotator UserPlaySpaceRot)
{
	AWRPlayerController * PC = Cast<AWRPlayerController>(Controller);

	if (PC)
	{
		return PC->PlayCameraShake(ShakeClass, Scale, PlaySpace, UserPlaySpaceRot);
	}

	return NULL;
}

///////////////////////////////////////
//Crouch Height Animation

FORCEINLINE_DEBUGGABLE float AWRCharacter::GetStandingEyeHeightFromFeet() const
{
	return StandingEyeHeightFromFeet;
}

FORCEINLINE_DEBUGGABLE float AWRCharacter::GetCrouchedEyeHeightFromFeet() const
{
	return CrouchedEyeHeightFromFeet;
}

FORCEINLINE_DEBUGGABLE FTransform AWRCharacter::GetRelativeEyesTransform() const
{
	if (bRelativeEyesTransformNeedsUpdate)
	{
		//I'm doing this because of lazy loading, forgive me!
		const_cast<AWRCharacter *>(this)->UpdateRelativeEyesTransform();
	}

	return RelativeEyesTransform;
}

///////////////////////////////////////
//Animation

FORCEINLINE_DEBUGGABLE bool AWRCharacter::GetJustJumpedAndReset()
{
	bool Res = bJustJumped;
	bJustJumped = false;

	return Res;
}

#pragma once

#include "WRDismemberableSkeletalMeshComponent.generated.h"

struct FWRDismemberablePart;
class UWRDismemberableSkeletalMeshComponent;

USTRUCT(BlueprintType)
struct FWRDismemberableStumpInfo
{
	GENERATED_USTRUCT_BODY()
		
	/**
	Chooses from a random mesh to attach as the covering for a dismembered limb stump
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Stump")
	TArray<UStaticMesh *> StumpMeshes;

	/**
	Current instance of the spawned stump
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Dismemberable Part")
	UStaticMeshComponent * StumpMeshInstance;

	/**
	Socket name on the main skeleton that the stump mesh attaches to
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Stump")
	FName StumpMeshAttachSocketName;

	/**
	Chooses from random particle systems to spawn when the stump is spawned, like a blood spurt
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Stump")
	TArray<UParticleSystem *> StumpSpawnEffects;

	/**
	Like StumpSpawnEffects, but the limb isn't attached to the main body.
	For example, a hose of blood should shoot out if attached to the main body because there's a heart beat.
	But if it's a limb that's already blown off, or the limb that is being blown off, it should only throw out a little splash.
	If this is empty, chooses from OnStumpSpawnEffects.
	You can add a single NULL element to force no spawning of effects if detached.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Stump")
	TArray<UParticleSystem *> StumpSpawnEffectsDetached;

	/**
	Current instance of the StumpSpawnEffects
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Dismemberable Part")
	UParticleSystemComponent * StumpSpawnEffectsInstance;

	/**
	If specified, spawns particle effects at this socket instead of StumpMeshAttachSocketName
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Stump")
	FName EffectAttachSocketName;

	/**
	Spawns a stump mesh.  Can attach it to a different skeletal mesh optionally, or to the main mesh.
	*/
	void SpawnStump(UWRDismemberableSkeletalMeshComponent * OwningDismemberable, USceneComponent * AttachParent);
};

UENUM(BlueprintType)
namespace WRDismemberablePartState
{
	enum Type
	{
		/**
		The body part is attached to its parent.
		*/
		ATTACHED_TO_PARENT							UMETA(DisplayName = "Attached to parent"),

		/**
		The body part is detached from its parent.  Children may still be attached.
		*/
		DETACHED_FROM_PARENT						UMETA(DisplayName = "Detached from parent"),

		/**
		The body part and all of its children are destroyed.
		*/
		DESTROYED									UMETA(DisplayName = "Destroyed"),

		MAX					                        UMETA(Hidden)
	};
}

USTRUCT(BlueprintType)
struct FWRDismemberablePart
{
	GENERATED_USTRUCT_BODY()
	
	FWRDismemberablePart()
		: Health(1),
		CurrentState(WRDismemberablePartState::ATTACHED_TO_PARENT)
	{}

	///////////////////////////////////////////
	//Dismemberable Part

	/**
	How much health does this body part have.
	If you override it to 0 for a spawning mesh this body part will automatically be forced to be detached from parent instead of attached.
	This is good for spawning a corpse with missing limbs, for example.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Part")
	int32 Health;

	/**
	State of the body part.  Can be overridden for spawning a corpse with missing limbs.
	If set to Detached, it'll immediately detach upon spawning and lie somewhere next to the corpse depending on where the physics take it.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Part")
	TEnumAsByte<WRDismemberablePartState::Type> CurrentState;

	/**
	Bone name on the skeleton that this body part is associated with
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Part")
	FName BoneName;

	/**
	Any other bones this body part is associated with to quickly reverse look up which body part was hit by something.
	For example the neck would assocate with the head, the fingers with the hand...
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Part")
	TArray<FName> AdditionalBoneNames;

	/**
	An optional skeletal mesh for this part of the body.  This can also instead be on the main skeletal mesh if desired.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Part")
	USkeletalMesh * SkeletalMesh;

	/**
	Current spawned instance of the SkeletalMesh
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Dismemberable Part")
	USkeletalMeshComponent * SkeletalMeshInstance;

	/**
	The parent dismemberable part name in the body parts heirarchy.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Part")
	FName ParentPartName;
	
	/**
	The cached children of this part that are calculated after parsing the DismemberableParts
	*/
	UPROPERTY(BlueprintReadonly, Category = "Dismemberable Part")
	TArray<FName> ChildPartNames;

	///////////////////////////////////////////
	//Joint Effects

	/**
	Info about the stump to spawn on our side of the parent child connection.
	Not applicable if there is no ParentPartName specified since this is the root.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Joint Effects")
	FWRDismemberableStumpInfo MyStump;

	/**
	Info about the stump to spawn on the parent side of the parent child connection.
	Not applicable if there is no ParentPartName specified since this is the root.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Joint Effects")
	FWRDismemberableStumpInfo ParentStump;

	/**
	Mesh to spawn between the two body parts when the part is attached, like the knee joint.
	This gets destroyed when the connection is destroyed and the stumps are spawned on both sides.
	Not applicable if there is no ParentPartName specified since this is the root.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Joint Effects")
	USkeletalMesh * ParentJointConnectionMesh;

	/**
	Current spawned instance of the ParentJointConnectionMesh
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Dismemberable Part")
	USkeletalMeshComponent * ParentJointConnectionMeshInstance;

	/**
	Effects to spawn when destroying the connection of this body part to the parent.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Joint Effects")
	TArray<UParticleSystem *> DestroyJointEffects;

	/**
	Optional socket to spawn joint destroy effects.  If not specified, uses the bone location itself.  
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Joint Effects")
	FName DestroyJointEffectsSpawnSocketName;

	/**
	Sound to play when the joint is destroyed
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Joint Effects")
	USoundCue * DestroyJointSound;

	///////////////////////////////////////////
	//Part Effects

	/**
	Effects to spawn when the body part itself is destroyed.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Part Effects")
	TArray<UParticleSystem *> DestroyPartEffects;

	/**
	Optional socket to spawn body part destroy effects.  If not specified, uses the bone location itself.  
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Part Effects")
	FName DestroyPartEffectsSpawnSocketName;

	/**
	Sound to play when the body part is destroyed.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Part Effects")
	USoundCue * DestroyPartSound;
};

UCLASS(meta = (BlueprintSpawnableComponent))
class WRAITH_API UWRDismemberableSkeletalMeshComponent : public USkeletalMeshComponent
{
	GENERATED_BODY()

public:
	UWRDismemberableSkeletalMeshComponent(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Part")
	TMap<FName, FWRDismemberablePart> DismemberableParts;

	/**
	Handles an edge case where calling detach on the root part.
	This will instead force this part to detach instead.
	Most characters will be set up so the pelvis is the root body part.
	This can be set to be the torso that attaches to the pelvis so the torso comes off rather than a leg when the character is blown in half.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dismemberable Part")
	FName RootPartDetachFallbackPart;

	/**
	Mapping of bone name to body part assocated with bone.
	This is so a quick lookup can happen of which body part was hit depending on bone.
	*/
	UPROPERTY(BlueprintReadOnly, Category = "Dismemberable Part")
	TMap<FName, FName> BoneNameToPartName;

	/**
	The root body part of the body parts heirarchy
	*/
	UPROPERTY(BlueprintReadonly, Category = "Dismemberable Part")
	FName RootPart;

	/**
	Detaches a part of the body and allows it to remain in the world
	*/
	UFUNCTION(BlueprintCallable, Category = "Dismemberable Part")
	void DetachBodyPart(FName BodyPartName, const FVector& Impulse, const FVector& HitLocation);

	/**
	Destroys a part of the body and all its children
	Unfortunately there's currently no easy way to allow children to remain when a body part is destroyed because of how Unreal handles skeletal meshes.
	*/
	UFUNCTION(BlueprintCallable, Category = "Dismemberable Part")
	void DestroyBodyPart(FName BodyPartName);

	/**
	Gets the body part name given a bone
	*/
	UFUNCTION(BlueprintCallable, Category = "Dismemberable Part")
	FName GetBodyPartNameForBone(FName BoneName);

protected:
	/**
	Spawns a skeletal mesh and configures all of the default settings.
	Allows passing in a Parent, if NULL, attaches to this by default.
	*/
	virtual USkeletalMeshComponent * SpawnSkeletalMesh(USkeletalMesh * Mesh, USceneComponent * Parent = NULL);

	/**
	Spawns a skeletal mesh and configures all of the default settings.
	Allows passing in a Parent, if NULL, attaches to this by default.
	*/
	virtual UStaticMeshComponent * SpawnStaticMesh(UStaticMesh * Mesh, FName SocketName, USceneComponent * Parent = NULL);

	/**
	Configures a spawning mesh component with all of the default settings.
	Allows passing in a Parent, if NULL, attaches to this by default.
	*/
	virtual void ConfigureSpawningMeshComponent(UMeshComponent * NewComponent, FName SocketName, USceneComponent * Parent = NULL);

	/**
	Configures a spawning scene component with all of the default settings.
	Allows passing in a Parent, if NULL, attaches to this by default.
	*/
	virtual void ConfigureSpawningSceneComponent(UPrimitiveComponent * NewComponent, FName SocketName, USceneComponent * Parent = NULL);
	
	/**
	Goes through the DismemberableParts map and checks for consistency and sets up the heirarchy.
	Returns true if succeeded, or false if any errors occured.
	*/
	virtual bool SanityCheck();

	/**
	If sanity check was successful, goes through the parts heirarchy and spawns the meshes.
	*/
	virtual void SetupInitialMeshes();

	friend FWRDismemberableStumpInfo;
};
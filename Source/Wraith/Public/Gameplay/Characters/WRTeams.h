#pragma once

#include "Runtime/AIModule/Classes/GenericTeamAgentInterface.h"
#include "WRTeams.generated.h"

UENUM(BlueprintType)
namespace WRTeam
{
	enum Type
	{
		HUMANS			UMETA(DisplayName = "Humans"),

		MONSTERS		UMETA(DisplayName = "Monsters"),

		INVALID = 254	UMETA(DisplayName = "Invalid"),

		NONE = 255		UMETA(DisplayName = "None"),
		
		MAX				UMETA(Hidden)
	};
}

extern FGenericTeamId WRHumansTeam;
extern FGenericTeamId WRMonstersTeam;

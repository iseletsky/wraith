#pragma once

#include "AETimedEffect.h"
#include "WRCharacterMovement.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class UWRCharacterMovement : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	UWRCharacterMovement(const FObjectInitializer& ObjectInitializer);

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	virtual void DisplayDebug(UCanvas* Canvas, const FDebugDisplayInfo& DebugDisplay, float& YL, float& YPos) override;

public:
	///////////////////////////////////////
	//Jumping

	virtual bool DoJump(bool bReplayingMoves) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Jumping")
	float FallingDamageReduction(float FallingDamage, const FHitResult& Hit);

protected:
	virtual void HandleImpact(FHitResult const& Hit, float TimeSlice = 0.f, const FVector& MoveDelta = FVector::ZeroVector) override;

	
protected:
	///////////////////////////////////////
	//Crouching

	/**
	Returns whether or not the character can change the crouch state
	*/
	virtual bool CanToggleCrouchInCurrentState() const;

	virtual bool CanCrouchInCurrentState() const override;

	virtual void Crouch(bool bClientSimulation = false) override;

	virtual void UnCrouch(bool bClientSimulation = false) override;

public:
	///////////////////////////////////////
	//Movement

	virtual void CalcVelocity(float DeltaTime, float Friction, bool bFluid, float BrakingDeceleration) override;

	virtual float GetMaxAcceleration() const override;

	virtual float GetMaxSpeed() const override;
	    
	/** What percentage of the character's horizontal velocity is maintained after a jump when standing still.
	This is to reduce super human jump lengths while still letting characters walk pretty quickly.
	When a character's move speed is below walking, a value between StandingJumpLateralVelocityPercentage and WalkingJumpLateralVelocityPercentage is chosen.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float StandingJumpLateralVelocityPercentage;

	/** What percentage of the character's horizontal velocity is maintained after a jump when walking.
	This is to reduce super human jump lengths while still letting characters walk pretty quickly.
	When a character's move speed is below walking, a value between StandingJumpLateralVelocityPercentage and WalkingJumpLateralVelocityPercentage is chosen.
	When a character's move speed is above walking, a value between WalkingJumpLateralVelocityPercentage and SprintingJumpLateralVelocityPercentage is chosen.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float WalkingJumpLateralVelocityPercentage;

	/** What percentage of the character's horizontal velocity is maintained after a jump when sprinting.
	This is to reduce super human jump lengths while still letting characters sprint pretty quickly.
	When a character's move speed is above walking, a value between WalkingJumpLateralVelocityPercentage and SprintingJumpLateralVelocityPercentage is chosen.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SprintingJumpLateralVelocityPercentage;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement")
	float GetJumpLateralVelocityPercentage();

	UPROPERTY(BlueprintReadWrite, Category = "Movement")
	FAETimedEffectManager MovementSpeedDampening;

	UPROPERTY(BlueprintReadWrite, Category = "Movement")
	FAETimedEffectManager JumpHeightDampening;
				
public:
	///////////////////////////////////////
	//Stumble
	
	/**
	Adds stumble to the character.
	Falloff controls how quickly the magnitude wears off.
	There is a single global StumbleDirection vector that gets contributed to.
	The magnitudes are maintained separately and additively just like other dampening factors.

	@param Stumble The velocity of stumble to apply.  This applies to the current StumbleDirection and tracks the magnitude.
	@param Falloff The falloff amount.  If the current falloff is lower than the input, it's ignored to maintain the more intense falloff.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Stumble")
	void AddStumble(const FVector2D& Stumble, float Falloff);

	/**
	By how much is the stumble velocity dampened when the character bounces against a wall.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stumble")
	float StumbleBounceDampening;
	
	/**
	What is the minimum bounce speed for a character to bounce against walls when stumbling instead of just sliding along.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stumble")
	float StumbleBounceThreshold;

	/**
	What is the minimum stumble speed at which a character is allowed to toggle crouch state.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stumble")
	float StumbleCrouchThreshold;

	/**
	When crouching the stumble magnitudes fall off at their normal rate but mutliplied by this amount.
	This will let crouching make stumbling less intense.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stumble")
	float StumbleCrouchFalloffMultiplier;

	UPROPERTY(BlueprintReadWrite, Category = "Stumble")
	FVector2D StumbleDirection;

	UPROPERTY(BlueprintReadWrite, Category = "Stumble")
	FAETimedEffectManager StumbleMagnitudes;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Stumble")
	FVector GetStumbleVelocity();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Stumble")
	float GetMaxStumblingSpeed() const;

public:
	///////////////////////////////////////
	//Sprint
	
	/** 
	Max speed when sprinting. 
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sprint")
	float SprintSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sprint")
	float SprintAcceleration;
	
	UPROPERTY(BlueprintReadWrite, Category = "Sprint")
	uint32 bIsSprinting:1;
	
	/** 
	True if player is holding the sprint button. 
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Sprint")
	uint32 bWantsToSprint:1;
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Sprint")
	bool CanSprint() const;
};
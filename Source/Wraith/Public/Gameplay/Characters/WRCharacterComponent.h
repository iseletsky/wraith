#pragma once

#include "WRCharacter.h"
#include "WRCharacterComponent.generated.h"

UCLASS(abstract)
class UWRCharacterComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = "Owner")
	AWRCharacter * GetOwnerCharacter() const;
};

FORCEINLINE_DEBUGGABLE AWRCharacter * UWRCharacterComponent::GetOwnerCharacter() const
{
	return Cast<AWRCharacter>(GetOwner());
}
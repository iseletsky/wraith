#pragma once

#include "WRCharacterHumanoid.h"
#include "WRPlayerCharacter.generated.h"

UCLASS()
class AWRPlayerCharacter : public AWRCharacterHumanoid
{
	GENERATED_BODY()
public:
	AWRPlayerCharacter(const FObjectInitializer& ObjectInitializer);
};
#pragma once

#include "WRCharacter.h"
#include "WRCharacterHumanoid.generated.h"

class UWRHotbarManager;
class UWREquippableManager;

UCLASS()
class AWRCharacterHumanoid : public AWRCharacter
{
	GENERATED_BODY()
public:
	AWRCharacterHumanoid(const FObjectInitializer& ObjectInitializer);

	virtual void SetupPlayerInputComponent(UInputComponent * PlayerInputComponent) override;

	virtual void OnLoseControl_Implementation() override;

	virtual void FaceRotation(FRotator NewControlRotation, float DeltaTime) override;

	virtual void TakeFallingDamage_Implementation(const FHitResult& Hit, float FallingSpeed, float FallingDamage) override;

	virtual void Jumped_Implementation() override;

	virtual UWRCharacterStanceData * GetCharacterStance_Implementation() const override;

	virtual bool Setup1P_Implementation() override;

	virtual bool Setup3P_Implementation() override;

public:
	///////////////////////////////////////
	//Mesh Parts
	USkeletalMeshComponent* GetHeadMesh() const;

private:
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* HeadMesh;

public:
	/////////////////////////////////////// 
	//Equippable
		
	UPROPERTY(BlueprintReadWrite, Category = "Equippable")
	UWREquippableManager * EquippableManager;
		    
	/**
	Sets up equippable input for an input component.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void SetupEquippableInput(UInputComponent * PlayerInputComponent);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void EngageUse(int32 UseMode);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void DisengageUse(int32 UseMode);
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
    void Reload();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
    void Unload();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void OnFire();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void OnStopFire();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void OnAltFire();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void OnStopAltFire();

	/**
	Useful for camera animations
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	FRotator GetCurrentWeaponRecoilRotation();

public:
	/////////////////////////////////////// 
	//Hotbar

	UPROPERTY(BlueprintReadWrite, Category = "Hotbar")
	UWRHotbarManager * HotbarManager;
	
	/**
	Sets up hotbar input for an input component.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void SetupHotbarInput(UInputComponent * PlayerInputComponent);
	
	/**
	It's expected that Slot passed in here is 1 based.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse(int32 Slot);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse1();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse2();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse3();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse4();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse5();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse6();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse7();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse8();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse9();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse10();

public:
	///////////////////////////////////////
	//Head Control
	
	/**
	In first person mode, this is where the neck is positioned.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Head Control")
	FTransform GetNeckWorldTransform() const;

	/**
	In first person mode, this is where the pelvis is positioned.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Head Control")
	FTransform GetPelvisWorldTransform() const;

public:
	///////////////////////////////////////
	//Hand Control
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Control")
	FTransform GetDominantHandWorldTransform(bool bIncludeAnimationOnlyEffects = false) const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Control")
	FTransform GetNonDominantHandWorldTransform() const;

public:
	///////////////////////////////////////
	//Animation

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	UAnimSequence * GetNonDominantHandPose() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	UAnimSequence * GetDominantHandPose() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
    UAnimSequence * GetIdleAnimation() const;
    

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	USceneComponent * GetNonDominantHandAttachIKComponent() const;
    
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	FName GetNonDominantHandAttachIKComponentSocket() const;

public:
	///////////////////////////////////////
	//Animation Event

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool EjectCasingAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool AttachedPropToCharacterAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool AttachedPropToPropAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool ShowAttachedPropAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool HideAttachedPropAnimNotify();
};

FORCEINLINE_DEBUGGABLE USkeletalMeshComponent* AWRCharacterHumanoid::GetHeadMesh() const
{
	return HeadMesh;
}
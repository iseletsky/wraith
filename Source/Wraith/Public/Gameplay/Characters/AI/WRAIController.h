#pragma once

#include "Runtime/AIModule/Classes/DetourCrowdAIController.h"
#include "WRAIController.generated.h"

class AWRSquadAI;
class UWRAIPerceptionCallback;
class UWRAIReportToSquadPerceptionCallback;
class UWRAIFindBestPotentialTargetPerceptionCallback;
class UWRAIChooseAttackContext;

UCLASS()
class AWRAIController : public ADetourCrowdAIController
{
	GENERATED_BODY()

public:
	AWRAIController(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	virtual void ActorsPerceptionUpdated(const TArray<AActor *>& UpdatedActors) override;
	
	virtual void UpdateControlRotation(float DeltaTime, bool bUpdatePawn = true) override;

	/**
	Controls the current speed at which the character looks towards the focus.
	<= 0 means instantaneously snap
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "View")
	float FocusControlRotationInterpSpeed;

public:
	////////////////////////////
	//Team
	
	virtual FGenericTeamId GetGenericTeamId() const override;

public:
	////////////////////////////
	//Squad

	UPROPERTY(BlueprintReadWrite, Category = "Squad")
	AWRSquadAI * Squad;
	
	/**
	Spawns a squad for the AI controller because the squad does target management.
	Even if a chacracter isn't in a squad it still needs a parent squad to manage its targets.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Squad")
	void SpawnSquadForSelf(TSubclassOf<AWRSquadAI> SquadClass);

public:
	////////////////////////////
	//Perception

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Perception")
	TSubclassOf<UWRAIReportToSquadPerceptionCallback> ReportToSquadPerceptionCallbackClass;

	UPROPERTY(BlueprintReadWrite, Category = "Perception")
	UWRAIReportToSquadPerceptionCallback * ReportToSquadPerceptionCallback;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Perception")
	TSubclassOf<UWRAIFindBestPotentialTargetPerceptionCallback> FindBestPotentialTargetPerceptionCallbackClass;

	UPROPERTY(BlueprintReadWrite, Category = "Perception")
	UWRAIFindBestPotentialTargetPerceptionCallback * FindBestPotentialTargetPerceptionCallback;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Perception")
	void InitializePerceptionCallbacks();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Perception")
	UWRAIPerceptionCallback * InitializePerceptionCallback(TSubclassOf<UWRAIPerceptionCallback> CallbackClass);

	UPROPERTY(BlueprintReadWrite, Category = "Perception")
	TArray<UWRAIPerceptionCallback *> PerceptionCallbackInstances;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Perception")
	void ProcessAllPerceivedActors();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Perception")
	void ProcessSpecificPerceivedActors(const TArray<AActor *>& Actors, bool bProcessingAll = false);

public:
	////////////////////////////
	//Target		
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	AActor * GetCurrentTarget() const;
		
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	bool IsCurrentTargetPerceived() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	AActor * GetBestPotentialTarget() const;

public:
	////////////////////////////
	//Attack

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	TSubclassOf<UWRAIChooseAttackContext> ChooseAttackContextClass;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Perception")
	void InitializeChooseAttackContexts();

	UPROPERTY(BlueprintReadWrite, Category = "Attack")
	UWRAIChooseAttackContext * CurrentTargetChooseAttackContext;

	UPROPERTY(BlueprintReadWrite, Category = "Attack")
	UWRAIChooseAttackContext * BestPotentialTargetChooseAttackContext;

	UPROPERTY(BlueprintReadWrite, Category = "Attack")
	UWRAIChooseAttackContext * CheckingPerceivedTargetChooseAttackContext;

	/**
	Might return -1 for no attack.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	int32 ChooseAttackForCurrentTarget();

	/**
	Tries to attack the current target by executing the passed in attack.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	UAEState * TryAttackCurrentTarget(int32 AttackNumber);
};
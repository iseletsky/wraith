#pragma once

#include "WRAIPerceptionCallback.h"
#include "WRAIReportToSquadPerceptionCallback.generated.h"

UCLASS()
class UWRAIReportToSquadPerceptionCallback : public UWRAIPerceptionCallback
{
	GENERATED_BODY()

public:
	
	virtual void OnStartProcessingActor_Implementation(AActor * PerceivedActor) override;

	virtual void OnEndProcessingActor_Implementation(AActor * PerceivedActor) override;

	virtual void OnProcessActorStimulus_Implementation(AActor * PerceivedActor, const FAIStimulus& Stimulus) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Stimulus")
	float GetStimulusStrengthMultiplierForLocation(const FAIStimulus& Stimulus) const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Stimulus")
	float GetStimulusStrengthMultiplierForVelocity(const FAIStimulus& Stimulus) const;

	/**
	How certain is the best known location based on stimuli.
	If there's a stronger well known position from another set of stimuli, it should take precedence.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
	float BestKnownLocationCertainty;

	/**
	The best known location of the target that can be deduced from the stimuli
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
	FVector BestKnownLocation;

	/**
	How certain is the best known velocity based on stimuli.
	If there's a stronger well known velocity from another set of stimuli, it should take precedence.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
	float BestKnownVelocityCertainty;

	/**
	The best known velocity of the target that can be deduced from the stimuli
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
	FVector BestKnownVelocity;
};
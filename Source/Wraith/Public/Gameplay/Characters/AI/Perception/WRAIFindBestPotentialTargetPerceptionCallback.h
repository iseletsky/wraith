#pragma once

#include "WRAIPerceptionCallback.h"
#include "WRAIPotentialTargetInfo.h"
#include "WRAIFindBestPotentialTargetPerceptionCallback.generated.h"

UCLASS(Abstract)
class UWRAIFindBestPotentialTargetPerceptionCallback : public UWRAIPerceptionCallback
{
	GENERATED_BODY()

public:

	virtual void OnStartProcessingActors_Implementation(bool bProcessingAll) override;

	virtual void OnEndProcessingActors_Implementation() override;
	
	/**
	 * Whether or not the best potential target is currently attackable.
	 * There may be a worse potential target by other criteria that is found
	 * but if it's attackable while the current one isn't, that one should become better
	 */
	UPROPERTY(BlueprintReadWrite, Transient, Category = "Target")
	uint8 bBestPotentialTargetAttackable:1;

	UPROPERTY(BlueprintReadWrite, Category = "Target")
	FWRAIPotentialTargetInfo PotentialTargetInfo;
};
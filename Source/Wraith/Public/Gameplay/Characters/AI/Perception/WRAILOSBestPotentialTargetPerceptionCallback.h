#pragma once

#include "WRAIFindBestPotentialTargetPerceptionCallback.h"
#include "WRAILOSBestPotentialTargetPerceptionCallback.generated.h"

UCLASS()
class UWRAILOSBestPotentialTargetPerceptionCallback : public UWRAIFindBestPotentialTargetPerceptionCallback
{
	GENERATED_BODY()

public:
	
	virtual void OnStartProcessingActors_Implementation(bool bProcessingAll) override;

	virtual void OnStartProcessingActor_Implementation(AActor * PerceivedActor) override;

	virtual void OnEndProcessingActor_Implementation(AActor * PerceivedActor) override;

	virtual void OnProcessActorStimulus_Implementation(AActor * PerceivedActor, const FAIStimulus& Stimulus) override;

public:

	UPROPERTY(BlueprintReadWrite, Transient, Category = "Target")
	float ClosestAttackTargetInLOSDistanceSquared;

	UPROPERTY(BlueprintReadWrite, Transient, Category = "Target")
	FVector MyLocation;
};
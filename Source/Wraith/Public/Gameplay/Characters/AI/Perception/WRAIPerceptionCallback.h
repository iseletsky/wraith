#pragma once

#include "Runtime/AIModule/Classes/Perception/AIPerceptionTypes.h"
#include "WRAIPerceptionCallback.generated.h"

UCLASS(Abstract, BlueprintType, Blueprintable, Within = Actor)
class UWRAIPerceptionCallback : public UObject
{
	GENERATED_BODY()

public:
	virtual UWorld* GetWorld() const override
	{
		if (HasAllFlags(RF_ClassDefaultObject))
		{
			// If we are a CDO, we must return nullptr instead of calling Outer->GetWorld() to fool UObject::ImplementsGetWorld.
			return nullptr;
		}

		return GetOuterAActor()->GetWorld();
	}
	
	/**
	Called right before beginning to process a group of perceived actors.
	@param bProcessingAll If true, is processing all currently perceived actors, otherwise it's only processing a select group of actors, like when a perception status updates about an actor
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AIPerceptionCallback")
	void OnStartProcessingActors(bool bProcessingAll);

	/**
	Called right when finished processing all actors
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AIPerceptionCallback")
	void OnEndProcessingActors();

	/**
	Called right before beginning to process a specific actor and its stimuli.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AIPerceptionCallback")
	void OnStartProcessingActor(AActor * PerceivedActor);

	/**
	Called right when finished processing an actor and its stimuli.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AIPerceptionCallback")
	void OnEndProcessingActor(AActor * PerceivedActor);

	/**
	Called when processing a stimulus for an actor.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AIPerceptionCallback")
	void OnProcessActorStimulus(AActor * PerceivedActor, const FAIStimulus& Stimulus);
};
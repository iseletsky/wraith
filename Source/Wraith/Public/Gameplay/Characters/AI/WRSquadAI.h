#pragma once

#include "Runtime/AIModule/Classes/AIController.h"
#include "WRSquadAI.generated.h"

class AWRAIController;

USTRUCT(BlueprintType)
struct FWRTargetInfo
{
	GENERATED_USTRUCT_BODY()
	
	/**
	Last known location of the target, either seen or heard
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Location")
	FVector LastKnownLocation;

	/**
	Certainty of the last known target location.
	If there's a recent stimulus that has less certainty than the current known location,
	it should be ignored.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Location")
	float LastKnownLocationCertainty;
	
	/**
	Last known velocity of the target, helps AI predict where to look for the target if they lost it
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Velocity")
	FVector LastKnownVelocity;

	/**
	Certainty of the last known target velocity.
	If there's a recent stimulus that has less certainty than the current known velocity,
	it should be ignored.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Velocity")
	float LastKnownVelocityCertainty;
		
	/**
	World time known info about target was recorded
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
	float LastKnownWorldSeconds;
	
	/**
	The world time that this target info expires
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
	float ExpireWorldSeconds;
};

USTRUCT(BlueprintType)
struct FWRSquadMemberInfo
{
	GENERATED_USTRUCT_BODY()

	/**
	Whether or not the squad member currently has valid orders.
	Handles edge case where squad members that get newly added to the squad may not have had a chance to obtain valid orders yet.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Orders")
	uint8 bMemberHasValidOrders:1;

	/**
	The location that the squad decided to order this member.
	The member will move to that location if it can and isn't too busy attacking something.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Orders")
	FVector MoveToLocation;
};

UCLASS()
class AWRSquadAI : public AAIController
{
	GENERATED_BODY()

public:
	AWRSquadAI(const FObjectInitializer& ObjectInitializer);
	
	//Squad AI is only an AIController for the purposes of running Behavior Trees
	//This should never be called
	virtual void Possess(APawn* InPawn) override;

public:
	////////////////////////////
	//Target

	/**
	Default time that it takes for members of this squad to stop caring about a target after it hasn't been visible.
	Shorter times are for squads of dumber monsters.  A squad of marines will likely care about a known target for longer than a squad of mindless zombies.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
	float DefaultExpireSecondsSinceLastKnown;

	/**
	Targets the Squad knows about.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Target")
	TMap<AActor *, FWRTargetInfo> AttackTargets;

	/**
	Run often to clean up invalid targets from the AttackTargets map
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	void UpdateTargetStatuses();
		
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	void RemoveTarget(const AActor * AttackTarget);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	bool IsTargetValid(const AActor * AttackTarget) const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	bool IsTargetInfoValid(const FWRTargetInfo& TargetInfo) const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	void UpdateTargetLastKnown(AActor * AttackTarget,
		const FVector& Location, const FVector& Velocity, 
		float LocationCertainty = 1.f, float VelocityCertainty = 1.f);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	bool GetTargetLastKnown(const AActor * AttackTarget, FVector& OutLocation, FVector& OutVelocity) const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	bool IsTargetCurrentlyPerceived(const AActor * AttackTarget) const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	AActor * GetClosestPerceivedTargetToLocation(const FVector& Location, float& OutDistSquared) const;

public:
	////////////////////////////
	//Members

	/**
	If true, automatically destroys the squad if it's empty after the last RemoveSquadMember call.
	This is usually wanted if managing a squad of monsters.
	Set this to false for special squads if you want them to stick around after all members die.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Members")
	uint32 bDestroySquadIfEmpty:1;

	UPROPERTY(BlueprintReadWrite, Category = "Members")
	TMap<AWRAIController *, FWRSquadMemberInfo> SquadMembers;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Members")
	void AddSquadMember(AWRAIController * Member);

	/**
	Removes a member from the squad.
	Usually this is called when an AI character dies.
	If bDestroySquadIfEmpty is true when this is called, and the squad is empty, the squad automatically destroys itself.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Members")
	void RemoveSquadMember(AWRAIController * Member);

	/**
	Gets the location the squad decided to order the member.
	Returns false if the member doesn't have valid orders.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	bool GetSquadMemberMoveLocation(AWRAIController * Member, FVector& OutLocation) const;

	/**
	Sets the squad member move location.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	void SetSquadMemberMoveLocation(AWRAIController * Member, const FVector& Location);

	/**
	Gets the closest squad member to a location
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target")
	AWRAIController * GetClosestSquadMemberToLocation(const FVector& Location, float& OutDistSquared) const;
};
#pragma once

#include "WRInventoryPhysical.h"
#include "WRAIActionManager.generated.h"

/**
Since equippables do a lot of state management and character animation management,
it's easier to just hack in a hidden equippable for AI to do attacking and pain states and other AI actions.
The AI Controller will look for attack states to try attacking with.
It will also drive other character actions and animations they'll do.
*/
UCLASS(abstract)
class AWRAIActionManager : public AWRInventoryPhysical
{
	GENERATED_BODY()

protected:
	virtual void Added_Implementation(UWRInventoryManager * InOwningManager) override;

	virtual bool SetupAndSanityCheck_Implementation() override;

public:
	///////////////////////////////////////////////
	//State

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "State")
	TArray<TSubclassOf<UAEState>> AttackStateClass;
	
	/**
	Notifies the AIController that the action that was being performed is done
	so it can notify the Behavior tree task that it's done.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void NotifyActionDone();

public:
	///////////////////////////////////////////////
	//Attack

	/**
	Tries to perform an attack.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	UAEState * TryPerformAttack(int32 AttackIndex);
};


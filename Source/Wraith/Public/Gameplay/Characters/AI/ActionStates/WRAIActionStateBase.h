#pragma once

#include "WREquippableState.h"
#include "WRAIController.h"
#include "WRAIActionManager.h"
#include "Runtime/AIModule/Classes/BehaviorTree/Tasks/BTTask_BlueprintBase.h"
#include "WRAIActionStateBase.generated.h"

UCLASS(Abstract)
class WRAITH_API UWRAIActionStateBase : public UWREquippableState
{
	GENERATED_BODY()

protected:
	virtual void OnInterrupt_Implementation(UAEState * InterruptingState) override;

	virtual void OnEnd_Implementation(UAEState * NextState) override;

	virtual void TryGotoIdle_Implementation() override;

public:
	////////////////////////////////////
	//AI

	UFUNCTION(BlueprintCallable, Category = "AI")
	AWRAIActionManager * GetActionManager() const;

    UFUNCTION(BlueprintCallable, Category = "AI")
	AWRAIController * GetAIController() const;

public:
	////////////////////////////////////
	//Task

	/**
	Sets up the associated AI task that is running the state.
	This way the state knows to tell the AITask that it's done.
	If another state is triggered to follow this state, it automatically is set up to notify the Task.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Task")
	void SetupAITask(UBTTask_BlueprintBase * inAITask);

	/**
	Call this to pass the AITask onto another state.
	If NextState isn't of class UWRAIActionStateBase then it just calls TaskFinishExecute with success
	Calls ResetAITask when done.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Task")
	void PassTaskToState(UAEState * NextState);

	/**
	Resets the state to no longer be associated with an AI task.
	This happens automatically when the state ends after the appropriate automatic Finish calls are done if needed.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Task")
	void ResetAITask();

	/**
	Notifies the associated AITask that the state is done.
	This should be called manually when needed.
	This is called automatically if the state is going back to idle.
	Calls ResetAITask() when done.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Task")
	void TaskFinishExecute(bool bSuccess = true);

	/**
	Notifies the associated AITask that the state is done.
	This should be called manually when needed.
	This is called automatically if the state is interrupted.
	Calls ResetAITask() when done.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Task")
	void TaskFinishAbort();
		
	UPROPERTY(BlueprintReadWrite, Category = "Task")
	UBTTask_BlueprintBase * AITask;
};

FORCEINLINE_DEBUGGABLE AWRAIActionManager * UWRAIActionStateBase::GetActionManager() const
{
	return Cast<AWRAIActionManager>(GetEquippable());
}

FORCEINLINE_DEBUGGABLE AWRAIController * UWRAIActionStateBase::GetAIController() const
{
	AWRCharacter * OwningCharacter = GetOwningCharacter();

	if (OwningCharacter)
	{
		return Cast<AWRAIController>(OwningCharacter->GetController());
	}

	return NULL;
}
#pragma once

#include "WRAIChooseAttackPrerequisiteCheck.h"
#include "WRAILineOfSightAttackLocationPrerequisiteCheck.generated.h"

UCLASS()
class UWRAILineOfSightAttackLocationPrerequisiteCheck : public UWRAIChooseAttackPrerequisiteCheck
{
	GENERATED_BODY()

public:
	virtual void ProcessTarget_Implementation(AActor * Attacker, AAIController * AttackerAIController, AActor * Target) override;

	virtual bool MoveData_Implementation(UWRAIChooseAttackPrerequisiteCheck * Destination) override;

	/**
	Attack location relative to the target to attack
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Attack")
	FVector AttackRelativeToTargetLocation;
};
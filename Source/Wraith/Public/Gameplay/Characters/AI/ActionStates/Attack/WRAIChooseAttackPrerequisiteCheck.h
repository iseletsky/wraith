#pragma once

#include "WRAIChooseAttackContext.h"
#include "WRAIChooseAttackPrerequisiteCheck.generated.h"

UCLASS(Abstract, BlueprintType, Blueprintable, Within = WRAIChooseAttackContext)
class UWRAIChooseAttackPrerequisiteCheck : public UObject
{
	GENERATED_BODY()

public:
	virtual UWorld* GetWorld() const override
	{
		if (HasAllFlags(RF_ClassDefaultObject))
		{
			// If we are a CDO, we must return nullptr instead of calling Outer->GetWorld() to fool UObject::ImplementsGetWorld.
			return nullptr;
		}

		return GetOuterUWRAIChooseAttackContext()->GetWorld();
	}

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	void ProcessTarget(AActor * Attacker, AAIController * AttackerAIController, AActor * Target);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	bool MoveData(UWRAIChooseAttackPrerequisiteCheck * Destination);
};
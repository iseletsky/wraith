#pragma once

#include "WRAIAttackStateInterface.generated.h"

class UWRAIChooseAttackContext;

UINTERFACE(BlueprintType)
class UWRAIAttackStateInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class WRAITH_API IWRAIAttackStateInterface
{
	GENERATED_IINTERFACE_BODY()

	/**
	Checks if the target is attackable by this attack
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	bool IsTargetAttackable(UWRAIChooseAttackContext * ChooseAttackContext, AActor * Target) const;

	/**
	Gets the probability between 0 and 1 that this attack will be considered to be chosen each time the AI thinks about attacking.
	This happens every .5 seconds so the number should be very low.
	Return 0 if the attack shouldn't be allowed to happen at all, like if it's cooling down.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	float GetAttackProbability(UWRAIChooseAttackContext * ChooseAttackContext, AActor * Target) const;
};
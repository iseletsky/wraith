#pragma once

#include "WRAIChooseAttackContext.generated.h"

class UWRAIChooseAttackPrerequisiteCheck;

UCLASS(Abstract, BlueprintType, Blueprintable, Within = Actor)
class UWRAIChooseAttackContext : public UObject
{
	GENERATED_BODY()

public:
	virtual UWorld* GetWorld() const override
	{
		if (HasAllFlags(RF_ClassDefaultObject))
		{
			// If we are a CDO, we must return nullptr instead of calling Outer->GetWorld() to fool UObject::ImplementsGetWorld.
			return nullptr;
		}

		return GetOuterAActor()->GetWorld();
	}

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	void SetupPrerequisiteChecks();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	void RunPrerequisiteChecks(AActor * Attacker, AAIController * AttackerAIController, AActor * Target);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	TArray<TSubclassOf<UWRAIChooseAttackPrerequisiteCheck>> PrerequisiteCheckClasses;

	UPROPERTY(BlueprintReadWrite, Category = "Attack")
	TArray<UWRAIChooseAttackPrerequisiteCheck *> PrerequisiteChecks;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	void SetupPossibleAttacks(AActor * Attacker, AAIController * AttackerAIController, AActor * Target);

	UPROPERTY(BlueprintReadWrite, Category = "Attack")
	TArray<int32> PossibleAttacks;

	/**
	Called every time the AI thinks about attacking, usually every .5 seconds.
	It may return -1 for no attacks.
	Goes through list of possible attacks and considers each one with a probability returned by GetAttackProbability().
	All attacks that are returned to possibly happen are then randomly chosen from.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	int32 ChooseAttack(AActor * Attacker, AAIController * AttackerAIController, AActor * Target);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attack")
	bool MoveData(UWRAIChooseAttackContext * Destination);
};
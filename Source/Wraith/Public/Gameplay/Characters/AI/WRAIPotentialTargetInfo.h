#pragma once

#include "WRAIPotentialTargetInfo.generated.h"

USTRUCT(BlueprintType)
struct FWRAIPotentialTargetInfo
{
	GENERATED_USTRUCT_BODY()

	void DefaultUpdatePotentialTargetInfo(float CurrentWorldTime);

	/**
	The current target chosen from previous runs.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Target")
	AActor * CurrentTarget;

	/**
	Whether or not the target is currently perceived
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Target")
	uint8 bCurrentTargetPerceived:1;
		
	/**
	The best target right now.  AI shouldn't always switch to the best target right away otherwise the behavior will be very erratic.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Target")
	AActor * CurrentBestPotentialTarget;

	/**
	If target hasn't been perceived by the expire time, clear the target. 
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Target")
	float TargetNotPerceivedExpireWorldSeconds;

	/**
	If target hasn't been the best target by the expire time, set target to the current best potential target.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Target")
	float TargetNotBestExpireWorldSeconds;
};
#pragma once

#include "WRInventoryPhysical.h"
#include "WRInventoryAmmo.generated.h"

UCLASS(abstract)
class AWRInventoryAmmo : public AWRInventoryPhysical
{
	GENERATED_BODY()

public:
	AWRInventoryAmmo(const FObjectInitializer& ObjectInitializer);
};


#pragma once

#include "WRHotbarManager.generated.h"

class AWRInventoryPhysical;
class UWREquippableManager;

/**
Hotbar allows characters to manage equippable inventory items more efficiently instead of relying purely on the inventory.
*/
UCLASS(meta = (BlueprintSpawnableComponent))
class UWRHotbarManager : public UActorComponent
{
	GENERATED_BODY()

public:
	UWRHotbarManager(const FObjectInitializer& ObjectInitializer);

	virtual void OnRegister() override;
	
	UPROPERTY(BlueprintReadWrite, Category = "Equippable")
	UWREquippableManager * EquippableManager;

public:
	/////////////////////////////////////// 
	//Hotbar

	/**
	It's expected that Slot passed in here is 1 based.
	Assign NULL to the slot to unassign an item.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void AssignToHotbarSlot(AWRInventoryPhysical * Item, int32 Slot);
	
	/**
	Finds first free hotbar slot to assign an item to.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void AutoAddToHotbar(AWRInventoryPhysical * Item);

	/**
	It's expected that Slot passed in here is 1 based.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void HotbarUse(int32 Slot);

	/**
	It's expected that Slot passed in here is 1 based.
	*/
	UFUNCTION(BlueprintCallable, Category = "Hotbar")
	AWRInventoryPhysical * GetAtHotbarSlot(int32 Slot) const;

	/**
	Maximum amount of slots in the hotbar
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hotbar")
	int32 DefaultMaxHotbarSlots;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hotbar")
	void SetNumHotbarSlots(int32 Slots);

	UFUNCTION(BlueprintCallable, Category = "Hotbar")
	int32 GetNumHotbarSlots() const;

protected:	
	UPROPERTY(BlueprintReadWrite, Category = "Hotbar")
	TArray<AWRInventoryPhysical *> HotbarItems;
};

FORCEINLINE_DEBUGGABLE AWRInventoryPhysical * UWRHotbarManager::GetAtHotbarSlot(int32 Slot) const
{
	if (Slot < 1 || Slot > HotbarItems.Num())
	{
		return NULL;
	}

	return HotbarItems[Slot - 1];
}

FORCEINLINE_DEBUGGABLE int32 UWRHotbarManager::GetNumHotbarSlots() const
{
	return HotbarItems.Num();
}
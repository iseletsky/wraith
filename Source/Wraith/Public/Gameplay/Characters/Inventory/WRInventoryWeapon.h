#pragma once

#include "WRInventoryPhysical.h"
#include "WRInventoryWeapon.generated.h"

UCLASS(abstract)
class AWRInventoryWeapon : public AWRInventoryPhysical
{
	GENERATED_BODY()

public:
	AWRInventoryWeapon(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void Added_Implementation(UWRInventoryManager * InOwningManager) override;
};


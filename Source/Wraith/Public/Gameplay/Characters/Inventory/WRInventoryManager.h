#pragma once

#include "WRInventoryManager.generated.h"

class AWRInventory;
class UWRHotbarManager;

UCLASS(meta = (BlueprintSpawnableComponent))
class UWRInventoryManager : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Category = "Hotbar")
	UWRHotbarManager * HotbarManager;

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void AddInventory(AWRInventory * Inventory);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void RemoveInventory(AWRInventory * Inventory);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool IsInInventory(AWRInventory * Inventory);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void DiscardAllInventory();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	AWRInventory * K2_FindInventoryType(TSubclassOf<AWRInventory> Type, bool bExactClass = false) const;

	template<typename T>
	FORCEINLINE T* FindInventoryType(TSubclassOf<T> Type, bool bExactClass = false) const
	{
		AWRInventory* Res = K2_FindInventoryType(Type, bExactClass);
		return Cast<T>(Res);
	}

protected:
	UPROPERTY(BlueprintReadOnly, Category = "Inventory")
	TArray<AWRInventory *> InventoryItems;
};
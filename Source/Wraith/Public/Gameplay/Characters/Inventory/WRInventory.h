#pragma once
#include "WRInventory.generated.h"

class UWRInventoryManager;

UCLASS(Blueprintable, Abstract, NotPlaceable/*, meta = (ChildCanTick)*/)
class WRAITH_API AWRInventory : public AInfo
{
	GENERATED_BODY()
public:
	AWRInventory(const FObjectInitializer& ObjectInitializer);

	virtual void Destroyed() override;

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Owner")
	void Added(UWRInventoryManager * InOwningManager);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Owner")
	void Removed();

	UFUNCTION(BlueprintCallable, Category = "Owner")
	UWRInventoryManager * GetOwnerInventoryManager() const;

private:
	UPROPERTY()
	UWRInventoryManager * OwnerInventoryManager;

public:
	/** Human readable localized name for the item. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FText DisplayName;

public:
	/**
	Whether or not this inventory item is stackable.
	Things like weapons and armor aren't stackable so they appear individually.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stack")
	bool bIsStackable;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Stack")
	void SetStackCount(int32 InStackCount);

	UFUNCTION(BlueprintCallable, Category = "Stack")
	int32 GetStackCount() const;

private:
	/**
	Number of stacked items for this inventory item.
	Things like weapons and armor aren't stackable so they appear individually.
	*/
	UPROPERTY()
	int32 StackCount;
};

FORCEINLINE_DEBUGGABLE UWRInventoryManager * AWRInventory::GetOwnerInventoryManager() const
{
	return OwnerInventoryManager;
}

FORCEINLINE_DEBUGGABLE int32 AWRInventory::GetStackCount() const
{
	return StackCount;
}
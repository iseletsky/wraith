#pragma once

#include "WREquippableState.h"
#include "WREquippableStateUnloadInterface.generated.h"

UINTERFACE(BlueprintType)
class UWREquippableStateUnloadInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class WRAITH_API IWREquippableStateUnloadInterface
{
	GENERATED_IINTERFACE_BODY()

    /**
	Called by the equippable before even attempting to unload to see if an unload is possible
	Usually you just check if the mag has any ammo.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Reloading)
	bool CheckCanUnload() const;
};
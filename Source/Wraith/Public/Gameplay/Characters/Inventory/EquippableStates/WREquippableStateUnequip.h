#pragma once

#include "WREquippableState.h"
#include "WREquippableStateUnequip.generated.h"

UCLASS()
class WRAITH_API UWREquippableStateUnequip : public UWREquippableState
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Unequip")
	void OnDoneUnequip();
};
#pragma once

#include "WREquippableState.h"
#include "WREquippableStateEquip.generated.h"

UCLASS()
class WRAITH_API UWREquippableStateEquip : public UWREquippableState
{
	GENERATED_BODY()

public:
	UWREquippableStateEquip(const FObjectInitializer& ObjectInitializer);
	
    virtual void OnBegin_Implementation(UAEState * PreviousState) override;

    virtual void OnEnd_Implementation(UAEState * NextState) override;
};
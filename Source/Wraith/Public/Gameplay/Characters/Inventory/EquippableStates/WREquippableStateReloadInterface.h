#pragma once

#include "WREquippableState.h"
#include "WREquippableStateReloadInterface.generated.h"

UINTERFACE(BlueprintType)
class UWREquippableStateReloadInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class WRAITH_API IWREquippableStateReloadInterface
{
	GENERATED_IINTERFACE_BODY()

	/**
	Called by the equippable before even attempting to reload to see if a reload is possible
	Usually you just check if the mag is less than full
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Reloading")
	bool CheckCanReload() const;
};
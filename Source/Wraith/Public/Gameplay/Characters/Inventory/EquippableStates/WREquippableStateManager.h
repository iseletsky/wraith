#pragma once

#include "AEStateManager.h"
#include "WREquippableStateManager.generated.h"

UCLASS(Within = WRInventoryPhysical)
class UWREquippableStateManager : public UAEStateManager
{
	GENERATED_BODY()
};

#pragma once

#include "WREquippableState.h"
#include "WREquippableStateIdle.generated.h"

UCLASS()
class WRAITH_API UWREquippableStateIdle : public UWREquippableState
{
	GENERATED_BODY()

public:
	virtual bool AllowInterruptionByState_Implementation(UAEState * State) override;

    virtual void OnBegin_Implementation(UAEState * PreviousState) override;

    virtual void OnEnd_Implementation(UAEState * NextState) override;
};
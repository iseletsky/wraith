#pragma once

#include "WREquippableState.h"
#include "WREquippableStateUseInterface.generated.h"

UINTERFACE(BlueprintType)
class UWREquippableStateUseInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class WRAITH_API IWREquippableStateUseInterface
{
	GENERATED_IINTERFACE_BODY()

	/**
	Called when the player disengages the use mode on the equippable while this use mode is active.
	This can be used to fire a charging shot or stop firing a continuously firing beam weapon for example.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Use")
	void OnUseModeDisengaged();

	/**
	Called by the equippable before even attempting to use to see if a use is possible
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ammo")
	bool CheckEnoughAmmo() const;
	
	/**
	Called by the equippable to get the reload mode to automatically trigger if EnoughAmmo returned false.
	This will only be called if the player has Auto Reloading enabled in their game play settings.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Reloading")
	int32 GetReloadModeIndex() const;
};
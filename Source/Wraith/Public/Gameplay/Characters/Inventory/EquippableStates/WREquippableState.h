#pragma once

#include "AEPhysicalActor.h"
#include "AEState.h"
#include "WREquippableStateManager.h"
#include "WRCharacter.h"
#include "WRCharacterHandControl.h"
#include "WRPickupInventory.h"
#include "WRInventoryPhysical.h"
#include "WREquippableState.generated.h"

USTRUCT(BlueprintType, Blueprintable)
struct WRAITH_API FWREquippableStatePlayAnimationArgs
{
	GENERATED_USTRUCT_BODY()

	/*	
	Randomly chooses one from the list to play.
	If the number of montages here matches the number of montages in the other animations,
	the randomly chosen animation is synced.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    TArray<UAnimMontage *> Montage;

	/*
	Whether or not the animation duration is forced to match the effect duration
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    uint32 bMontageMatchesDuration:1;

	/*
	Has no effect if bMontageMatchesDuration is false.
    Should the animation duration that is forced account for the blend out time.
	You usually want this true if you know no more animations will play in sequence after this one and it'll go back to the idle pose.
	You want this false if you plan on playing another animation right after this.
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    uint32 bAccountForBlendoutTime:1;

	/*
    Whether or not the animation is force stopped when the state is ended
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    uint32 bMontageStopOnStateEnd:1;

	FWREquippableStatePlayAnimationArgs()
	{
		bMontageMatchesDuration = true;
		bAccountForBlendoutTime = true;
		bMontageStopOnStateEnd = true;
	}
};

/**
Passed into the play effects functions.
*/
USTRUCT(BlueprintType, Blueprintable)
struct WRAITH_API FWREquippableStatePlayEffectsArgs
{
    GENERATED_USTRUCT_BODY()

	/*
    Animation that plays on the character.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	FWREquippableStatePlayAnimationArgs CharacterAnimation;

    /*
    Animation that plays on the prop.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	FWREquippableStatePlayAnimationArgs PropAnimation;
	
	//TODO: animation that plays on attached props on the prop

	//TODO: add sound args and an array of sounds to play
	//add ways to make sounds attached to either the character or the prop and different sockets

    /**
    Sound to play
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
    USoundCue * SoundCue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	uint32 bSoundAttached:1;



    //TODO: sound, camera shake, force feedback effect

    FWREquippableStatePlayEffectsArgs()
    {

    }
};

UCLASS(Abstract, BlueprintType, Blueprintable, DefaultToInstanced, EditInlineNew)
class WRAITH_API UWREquippableState : public UAEState
{
	GENERATED_BODY()

public:
	////////////////////////////////////
	//State

	/**
	For convenience.  Is this state interruptable by the unequip state?
	You usually want this yes for most things except animations like firing or putting a magazine away.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	uint32 bAllowUnequipInterrupt:1;

	/**
	Convenience function for reenabling allowUnequipInterrupt and trying to unequip immediately
	Returns true if it did unequip successfully.
	*/
	UFUNCTION(BlueprintCallable, Category = "State")
	bool EnableAllowUnequipInterruptAndTryGotoUneqeuip();

protected:
	virtual bool AllowInterruptionByState_Implementation(UAEState * State) override;

	virtual void OnEnd_Implementation(UAEState * NextState) override;


public:
	////////////////////////////////////
	//Equippable

	UFUNCTION(BlueprintCallable, Category = "Equippable")
	AWRInventoryPhysical * GetEquippable() const;

    UFUNCTION(BlueprintCallable, Category = "Equippable")
	AWRPickupInventory * GetEquippableProp() const;
    
	UFUNCTION(BlueprintCallable, Category = "Equippable")
	AWRCharacter * GetOwningCharacter() const;

	UFUNCTION(BlueprintCallable, Category = "Equippable")
	UWRCharacterHandControl * GetHandControl() const;

	/**
	Convenience method that can be shared by most states
	that simply ends the state and tries to make the equippable go to idle
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void TryGotoIdle();

public:
	////////////////////////////////////
	//Effects

	/**
    Convenience method all in one to play various effects like animations, sounds, camera effects, etc...
    */
    UFUNCTION(BlueprintCallable, Category = "Effects")
    void PlayEffects(float Duration, const FWREquippableStatePlayEffectsArgs& InEffectArgs);

	/**
    Convenience method that calls PlayEffects and after duration does an action with a passed in timer delegate.
    Performs action immideately if duration < 0
    */
    UFUNCTION(BlueprintCallable, Category = "Effects")
    void PlayEffectsAndDoAction(float Duration, const FWREquippableStatePlayEffectsArgs& InEffectArgs, FTimerDynamicDelegate InDelegate);

    /**
    Convenience method that calls PlayEffects and after duration does an action with a passed in timer delegate.
    Performs action immideately if duration < 0
    */
    void PlayEffectsAndDoAction(float Duration, const FWREquippableStatePlayEffectsArgs& InEffectArgs, FTimerDelegate const& InDelegate);

    /**
    Convenience method that calls PlayEffects and after duration automatically goes to a next state.
    If NextStateName is None it instead calls TryGotoIdle
    */
    UFUNCTION(BlueprintCallable, Category = "Effects")
    void PlayEffectsAndGotoState(float Duration, const FWREquippableStatePlayEffectsArgs& InEffectArgs, TSubclassOf<UAEState> StateClass);

public:
	////////////////////////////////////
	//Animation

	/**
    Convenience method to play an animation montage on the character.
    Passing in NULL for the montage automatically just doesn't play.
    bStopOnStateEnd set to true makes the animation automatically stop if the state is ended.
    */
    UFUNCTION(BlueprintCallable, Category = "Animation")
    void PlayCharacterMontageWithDuration(UAnimMontage * Montage, float Duration, bool bAccountForBlendoutTime = true, bool bStopOnStateEnd = true);

    /**
    Convenience method to play an animation montage on the character.
    Passing in NULL for the montage automatically just doesn't play.
    bStopOnStateEnd set to true makes the animation automatically stop if the state is ended.
    */
    UFUNCTION(BlueprintCallable, Category = "Animation")
    void PlayCharacterMontage(UAnimMontage * Montage, float Rate = 1.f, bool bStopOnStateEnd = true);
	
    /**
    Convenience method to play an animation montage on the prop associated with the equippable.
    Passing in NULL for the montage automatically just doesn't play.
    If there is no existing prop at the moment nothing plays either.
    bStopOnStateEnd set to true makes the animation automatically stop if the state is ended.
    */
    UFUNCTION(BlueprintCallable, Category = "Animation")
    void PlayPropMontageWithDuration(UAnimMontage * Montage, float Duration, bool bAccountForBlendoutTime = true, bool bStopOnStateEnd = true);

    /**
    Convenience method to play an animation montage on the character. 
    Passing in NULL for the montage automatically just doesn't play. 
    bStopOnStateEnd set to true makes the animation automatically stop if the state is ended.
    */
    UFUNCTION(BlueprintCallable, Category = "Animation")
    void PlayPropMontage(UAnimMontage * Montage, float Rate = 1.f, bool bStopOnStateEnd = true);

	//TODO: methods to play animations on attached props

	/**
    Method to play an anim montage on some anim instance.
    Passing in NULL for the montage automatically just doesn't play.
    bStopOnStateEnd set to true makes the animation automatically stop if the state is ended.
    */
    UFUNCTION(BlueprintCallable, Category = "Animation")
    void PlayMontageWithDuration(UAnimMontage * Montage, float Duration, UAnimInstance * AnimInstance, bool bAccountForBlendoutTime = true, bool bStopOnStateEnd = true);

    /**
	Method to play an anim montage on some anim instance.
    Passing in NULL for the montage automatically just doesn't play.
    bStopOnStateEnd set to true makes the animation automatically stop if the state is ended.
    */
    UFUNCTION(BlueprintCallable, Category = "Animation")
    void PlayMontage(UAnimMontage * Montage, UAnimInstance * AnimInstance, float Rate = 1.f, bool bStopOnStateEnd = true);
	
protected:
	/**
	Keeps track of anim montages to stop on state end.
	*/
	TMultiMap<UAnimInstance *, FAnimMontageInstance *> MontagesToStopOnStateEnd;

	/**
	Sets up a montage to be tracked for stopping on state end
	*/
	void SetupAnimMontageStopOnStateEnd(UAnimInstance * AnimInstance, FAnimMontageInstance * AnimMontageInstance);

	/**
	Stops all montages tracked in MontagesToStopOnStateEnd
	*/
	UFUNCTION(BlueprintCallable, Category = "Animation")
	void StopMontages();

public:
	///////////////////////////////////////
	//Animation Event

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool EjectCasingAnimNotify();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool AttachedPropToCharacterAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool AttachedPropToPropAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool ShowAttachedPropAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool HideAttachedPropAnimNotify();
};

////////////////////////////////////
//State

FORCEINLINE_DEBUGGABLE bool UWREquippableState::EnableAllowUnequipInterruptAndTryGotoUneqeuip()
{
	bAllowUnequipInterrupt = true;

	return GetEquippable()->TryGotoUnequip();
}

////////////////////////////////////
//Equippable

FORCEINLINE_DEBUGGABLE AWRInventoryPhysical * UWREquippableState::GetEquippable() const
{
	UWREquippableStateManager * Manager = Cast<UWREquippableStateManager>(GetOuterUAEStateManager());

	if (Manager)
	{
		return Cast<AWRInventoryPhysical>(Manager->GetOuterAWRInventoryPhysical());
	}

	return NULL;
}

FORCEINLINE_DEBUGGABLE AWRPickupInventory * UWREquippableState::GetEquippableProp() const
{
	AWRInventoryPhysical * Equippable = GetEquippable();

	if (Equippable)
	{
		return Equippable->Prop;
	}

	return NULL;
}

FORCEINLINE_DEBUGGABLE AWRCharacter * UWREquippableState::GetOwningCharacter() const
{
	AWRInventoryPhysical * Equippable = GetEquippable();

	if (Equippable)
	{
		return Cast<AWRCharacter>(Equippable->GetOwner());
	}

	return NULL;
}

FORCEINLINE_DEBUGGABLE UWRCharacterHandControl * UWREquippableState::GetHandControl() const
{
	AWRInventoryPhysical * Equippable = GetEquippable();

	if (Equippable)
	{
		return Equippable->HandControl;
	}

	return NULL;
}

////////////////////////////////////
//Animation

FORCEINLINE_DEBUGGABLE void UWREquippableState::PlayCharacterMontageWithDuration(UAnimMontage * Montage, float Duration, bool bAccountForBlendoutTime, bool bStopOnStateEnd)
{
	AWRCharacter * OwningCharacter = GetOwningCharacter();

	if (OwningCharacter)
	{
		PlayMontageWithDuration(Montage, Duration, OwningCharacter->GetMesh()->GetAnimInstance(), bAccountForBlendoutTime, bStopOnStateEnd);
	}
}

FORCEINLINE_DEBUGGABLE void UWREquippableState::PlayCharacterMontage(UAnimMontage * Montage, float Rate, bool bStopOnStateEnd)
{
	AWRCharacter * OwningCharacter = GetOwningCharacter();

	if (OwningCharacter)
	{
		PlayMontage(Montage, OwningCharacter->GetMesh()->GetAnimInstance(), Rate, bStopOnStateEnd);
	}
}

FORCEINLINE_DEBUGGABLE void UWREquippableState::PlayPropMontageWithDuration(UAnimMontage * Montage, float Duration, bool bAccountForBlendoutTime, bool bStopOnStateEnd)
{
	AWRPickupInventory * EquippableProp = GetEquippableProp();

	if (EquippableProp)
	{
		PlayMontageWithDuration(Montage, Duration, EquippableProp->GetSkeletalMesh()->GetAnimInstance(), bAccountForBlendoutTime, bStopOnStateEnd);
	}
}

FORCEINLINE_DEBUGGABLE void UWREquippableState::PlayPropMontage(UAnimMontage * Montage, float Rate, bool bStopOnStateEnd)
{
	AWRPickupInventory * EquippableProp = GetEquippableProp();

	if (EquippableProp)
	{
		PlayMontage(Montage, EquippableProp->GetSkeletalMesh()->GetAnimInstance(), Rate, bStopOnStateEnd);
	}
}
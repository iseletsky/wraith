#pragma once

#include "WRInventory.h"
#include "WRDamageType.h"
#include "WRImpactType.h"
#include "WRInventoryPhysical.generated.h"

class AAEPhysicalActor;
class AWRInventoryAmmo;
class UWREquippableStateManager;
class AWRPickupInventory;
class UWRCharacterStanceData;
class UWRCharacterHandControl;
class UWREquippableManager;

namespace EquippableState
{
	extern WRAITH_API const FName Equipping;
	extern WRAITH_API const FName Unequipping;
	extern WRAITH_API const FName Idle;
	extern WRAITH_API const FName Use;
	extern WRAITH_API const FName Reload;
	extern WRAITH_API const FName Unload;
};

USTRUCT(BlueprintType)
struct FWRAmmoData
{
	GENERATED_USTRUCT_BODY()

	/**
	The ammo class used.
	Each ammo data object should have a unique ammo class since the ammo data will be looked up by ammo class.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
	TSubclassOf<AWRInventoryAmmo> AmmoClass;

	/**
	How much of this type of ammo does this weapon hold in its magazine
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
	int32 Capacity;
		
	/**
	Current ammo of this type in the magazine
	Leave at <0 at spawn to automatically set to capacity.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
	int32 Count;

	FWRAmmoData()
	{
		Count = -1;
	}
};

UENUM(BlueprintType)
namespace WRAttachedPropAttachment
{
	enum Type
	{
		/**
		Currently attached to the prop.
		*/
		ATTACHED_TO_PROP							UMETA(DisplayName = "Attached to Prop"),

		/**
		Currently attached to the character.
		*/
		ATTACHED_TO_CHARACTER						UMETA(DisplayName = "Attached to Character"),

		/**
		Currently detached and doesn't exist.
		*/
		DETACHED									UMETA(DisplayName = "Detached"),

		MAX					                        UMETA(Hidden)
	};
}

/**
Used to store info about secondary props that attach to the main prop.
Secondary props can also detach and be in the character's left hand.
Usually used for reloading animations like magazines or shell casings 
but the system is flexible enough for it to be used during other animations. 
*/
USTRUCT(BlueprintType)
struct FWRAttachedPropData
{
    GENERATED_USTRUCT_BODY()
public:
	////////////////////////////////////
	//Prop

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prop")
    TSubclassOf<AAEPhysicalActor> PropClass;

    /**
    Instance of the prop.
    */
    UPROPERTY(BlueprintReadOnly, Category = "Prop")
    AAEPhysicalActor * Prop;

public:
	////////////////////////////////////
	//Ammo

	/**
	Sometimes the prop contains ammo, like during reloading.
	This is a fallback for tracking the ammo in the prop in case the prop isn't an ammo pickup.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Ammo")
    int32 AmmoCount;

	/**
	The max ammo capacity of this prop.
	<= 0 assumes same AmmoCapacity as the ammo specified by AmmoName.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Ammo")
    int32 AmmoCapacity;

	/**
	Ammo name this prop would be associated with.
	Makes it easy to know which ammo to contribute to in the weapon itself.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	FName AmmoName;

public:
	////////////////////////////////////
	//Visibility

	/**
	Whether or not the prop is visible by default.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visibility")
	uint32 bDefaultVisible:1;

	/**
	Current override of the default visibility state.
	It could be useful to force a magazine that's normally visible by default to stay invisible if the weapon is unloaded.
	No effect if the prop is attached to the character by default.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visibility")
	TEnumAsByte<AEDefaultBoolOverrideState::Type> ForcedDefaultVisibilityState;

public:
	////////////////////////////////////
	//Collision

	/**
	Whether or not the prop has collision enabled by default.
	A magazine attached to a weapon could contribute with collision if it's attached and visible.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision")
	uint32 bDefaultCollision:1;

	/**
	Current override of the default collision state.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision")
	TEnumAsByte<AEDefaultBoolOverrideState::Type> ForcedDefaultCollisionState;
		
public:
	////////////////////////////////////
	//Attachment

	/**
	If attached to the main prop by default or attached to the character's left hand by default.
	Sometimes, things like shell casings that the character loads are by default only held in the character's hands.
	Things like defaultVisible and defaultCollision have no effect are never going to be visible if there is no character to hold the prop.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachment")
	uint32 bDefaultAttachToMainProp:1;
			  
    /**
    Socket name on the main prop that this prop attaches to
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachment")
    FName DefaultOnMainPropAttachComponentSocket;

    /**
    Socket name on this prop that attaches to the main prop
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachment")
    FName DefaultToMainPropAttachComponentSocket;

    /**
    Socket name on this prop that attaches to the character's non dominant hand
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachment")
    FName DefaultNonDominantHandAttachComponentSocket;

public:
	////////////////////////////////////
	//Optimizations

	/**
	It may be more optimal to destroy some props when the equippable is not equipped.
	Like shell casings.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Optimizations")
    uint32 bDestroyPropOnHide:1;

    FWRAttachedPropData()
    {
		bDefaultVisible = true;
		bDefaultCollision = true;
		bDefaultAttachToMainProp = true;
		DefaultOnMainPropAttachComponentSocket = FName(TEXT("magazine"));
    }
};

/**
When working with attached magazine props, use this.  
It'll create an attached prop under the hood and there are special functions for making it easier to work with the prop,
assuming it's a magazine.
*/
USTRUCT(BlueprintType)
struct FWRAttachedMagazineData
{
	GENERATED_USTRUCT_BODY()

public:
	////////////////////////////////////
	//Prop

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prop")
	TSubclassOf<AAEPhysicalActor> PropClass;

public:
	///////////////////////////////////
	//Magazine
		
	/**
	Whether or not the magazine has collision enabled when attached to the main prop.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magazine")
	uint32 bAttachedCollisionEnabled:1;
	
public:
	///////////////////////////////////
	//Ammo

	/**
	Ammo name this magazine would be associated with.
	Makes it easy to know which ammo to contribute to in the weapon itself.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	FName AmmoName;

	/**
	The max ammo capacity of this prop.
	<= 0 assumes same AmmoCapacity as the ammo specified by AmmoName.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Ammo")
    int32 AmmoCapacity;

public:
	////////////////////////////////////
	//Attachment

	/**
    Socket name on the main prop that this prop attaches to
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachment")
    FName DefaultOnMainPropAttachComponentSocket;

    /**
    Socket name on this prop that attaches to the main prop
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachment")
    FName DefaultToMainPropAttachComponentSocket;

    /**
    Socket name on this prop that attaches to the character's non dominant hand
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachment")
    FName DefaultNonDominantHandAttachComponentSocket;

	FWRAttachedMagazineData()
	{
		bAttachedCollisionEnabled = true;
		DefaultOnMainPropAttachComponentSocket = FName(TEXT("magazine"));
	}
};

USTRUCT(BlueprintType)
struct FWRReloadUnloadStatesPair
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	TSubclassOf<UAEState> ReloadStateClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	TSubclassOf<UAEState> UnloadStateClass;
};

UCLASS(Abstract)
class WRAITH_API AWRInventoryPhysical : public AWRInventory
{
	GENERATED_BODY()

public:
	AWRInventoryPhysical(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void BeginPlay() override;

	virtual void Added_Implementation(UWRInventoryManager * InOwningManager) override;

	virtual void Removed_Implementation() override;

protected:
	////////////////////////////////////
	//Sanity Check

	/**
	Sets up things for the equippable based on how it was configured and makes sure everything is configured correctly.
	If it wasn't, the function should log what went wrong and return false.
	If everything went well it should return true.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = SanityCheck)
	bool SetupAndSanityCheck();

	/**
	Whether or not this passed all sanity checks.
	Setting up an equippable can be complicated.
	The Equippable will always stay inactive if not valid.
	Any attempt to equip it will automatically log to the screen that this item is invalid and tell it to be unequipped.
	*/
	UPROPERTY(BlueprintReadWrite, Category = SanityCheck)
	bool bPassedSanityChecks;

public:
	/**
	Sometimes, while developing, it's worth running the equippable even when it fails sanity checks to test things.
	If this is true, it will be logged to the screen to make it obvious.
	*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = SanityCheck)
	bool bOverrideFailedSanityCheck;

public:
	/////////////////////////////////
	//Inventory

	/**
	Whether or not this inventory item is droppable.
	Useful for things like the player's fists as a weapon since a player should never be able to "drop" the fists.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	bool bUnDroppable;

	/**
	Weight of an individual Inventory item.
	When an item is stacked, the resulting weight is multiplied by stack count.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	float Weight;

	/**
	Computes the total weight of the object usually by multiplying StackCount by Weight.
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	float GetTotalWeight() const;

public:
	///////////////////////////////////
	//Prop

	/**
	Method to spawn the pickup for this inventory class.
	Pass NULL for World if you have an instance of the Inventory item you're calling this for already in a world.
	Otherwise you can call this on the Class Default Object to spawn in the correct world.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Prop")
	AWRPickupInventory * SpawnProp(const FTransform& SpawnLocation, bool bContainThisInventory = true, UWorld * World = NULL);

	/**
	Helper method to spawn a prop if owned prop is NULL.
	Sets up the connections.
	Returns true if it did have to spawn a prop.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Prop")
	bool SpawnPropForSelfIfNeeded(const FTransform& SpawnLocation);

	/**
	Helper method to destroy a prop if one is owned.
	Does all the correct things to ensure the Inventory object itself isn't destroyed as well.
	Returns true if it did destroy a prop.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Prop")
	bool DestroyProp();

	/**
	Sets up the prop for being visible in the world and not being held by a character.
	Handles spawning it if it's NULL
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Prop")
	void SetupPropForWorld(const FTransform& SpawnLocation);

	/**
	Sets up the prop for being visible in the world and being held by a character.
	This means it's visible but most physics and collision related things are disabled.
	Handles spawning it if it's NULL
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Prop")
	void SetupPropForCharacter();

	//TODO: implement this later for VR, similar to SetupPropForCharacter but has collision enabled
	//Figure out if this is needed or if the collision enabling will happen in SetupPropForCharacter
	//UFUNCTION(BlueprintNativeEvent, Category = "Prop")
	//void SetupPropForVRCharacter();

	/**
	Sets up the prop for being not visible in the world, like being put away into the inventory.
	Handles destroying it if it exists and bDestroyPropOnHide is true.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Prop")
	void SetupPropHidden();

	/**
	Whether or not the prop should be destroyed for this inventory item when hidden.
	This is a small optimization so things not likely to be equipped don't keep their prop around all the time.
	Things like weapons should keep theirs around since they'll be constantly spawning it.
	Things like ammo boxes should just destroy their prop most of the time.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prop")
	bool bDestroyPropOnHide;

	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Prop")
	TSubclassOf<AWRPickupInventory> PropClass;

	UPROPERTY(BlueprintReadonly, Category = "Prop")
	AWRPickupInventory * Prop;

	/**
	The socket on the prop where projectiles normally spawn from.
	This is usually the end of the weapon's muzzle, and the same place the particle effects for muzzle flash are attached.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    FName DefaultProjectileSpawnSocket;

	/**
	The default offset from where the prop is attached to the character to where prjectiles spawn from.
	Usually this is derived from the socket of the weapon's muzzle.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Prop")
	FTransform GetDefaultProjectileSpawnOffsetFromCharacterAttachment() const;

	/**
	The default offset from where the prop root to where projectiles spawn from.
	Usually this is derived from the socket of the weapon's muzzle.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Prop")
	FTransform GetDefaultProjectileSpawnOffsetFromRoot() const;

public:
	///////////////////////////////////
	//Attached Prop

	/**
    Used to store info about secondary props that attach to the main prop.
    Secondary props can also detach and be in the character's left hand.
    Usually used for reloading animations like magazines or shell casings
    but the system is flexible enough for it to be used during other animations.
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attached Prop")
    TMap<FName, FWRAttachedPropData> AttachedProps;

	/**
	Spawns the attached prop into its default state if it doesn't exist
	@param bForceSpawn If false, it'll only spawn the prop if it by default has physics or visiblity enabled
		If true it'll spawn it regardless of defaults, assuming it's about to be set visible or collision enabled
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
	void SpawnAttachedPropIfNeeded(FName AttachedPropName, bool bForceSpawn = false);
	
	/**
	Sets whether or not the prop is visible.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
	void SetAttachedPropVisibility(FName AttachedPropName, bool bVisible);

	UFUNCTION(BlueprintCallable, Category = "Attached Prop")
	void SetAttachedPropForcedDefaultVisibilityState(FName AttachedPropName, AEDefaultBoolOverrideState::Type Override);

	UFUNCTION(BlueprintCallable, Category = "Attached Prop")
	AEDefaultBoolOverrideState::Type GetAttachedPropForcedDefaultVisibilityState(FName AttachedPropName) const;

	/**
	Returns the original defaul state of the prop without taking into account the override.
	Use GetOverrideBoolValue with this value and the value of GetAttachedPropForcedDefaultVisibilityState to get the value taking override into account.
	*/
	UFUNCTION(BlueprintCallable, Category = "Attached Prop")
	bool GetAttachedPropDefaultVisibilityState(FName AttachedPropName) const;

	/**
	Sets whether or not the prop's collision is enabled.
	Has no effect if character is holding it and it's not VR.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
	void SetAttachedPropCollision(FName AttachedPropName, bool bCollisionEnabled);

	UFUNCTION(BlueprintCallable, Category = "Attached Prop")
	void SetAttachedPropForcedDefaultCollisionState(FName AttachedPropName, AEDefaultBoolOverrideState::Type Override);

	UFUNCTION(BlueprintCallable, Category = "Attached Prop")
	AEDefaultBoolOverrideState::Type GetAttachedPropForcedDefaultCollisionState(FName AttachedPropName) const;

	/**
	Returns the original defaul state of the prop without taking into account the override.
	Use GetOverrideBoolValue with this value and the value of GetAttachedPropForcedDefaultCollisionState to get the value taking override into account.
	*/
	UFUNCTION(BlueprintCallable, Category = "Attached Prop")
	bool GetAttachedPropDefaultCollisionState(FName AttachedPropName) const;

	/**
	Completely disconnects the prop from the equippable so the equippable no longer has a reference to it.
	A new one will be spawned if needed.
	The attached prop now exists in the world independently, and has physics, collision, and visibility enabled.
	This would be good if the character throws away a magazine for example.
	This would most often be called when a character dies while holding the prop and drops it to the ground.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
    AAEPhysicalActor * DisconnectAttachedProp(FName AttachedPropName);

	UFUNCTION(BlueprintCallable, Category = "Attached Prop")
	AAEPhysicalActor * GetAttachedProp(FName AttachedPropName) const;

	/**
    Attaches an attached prop to the character's left hand.
    */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
    bool AttachedPropToCharacter(FName AttachedPropName);

    /**
    Attaches an attached prop to the main prop.
    */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
    bool AttachedPropToProp(FName AttachedPropName);

	/**
	Figures out what the attached prop is currently attached to
	*/
	UFUNCTION(BlueprintCallable, Category = "Attached Prop")
	WRAttachedPropAttachment::Type GetAttachedPropState(FName AttachedPropName) const;

	/**
	Returns ammo in attached prop in case it's used during reloading animations.
	Returns -1 if prop is detached or there is no prop with this name.
	*/
	UFUNCTION(BlueprintCallable, Category = "Attached Prop")
	int32 GetAttachedPropAmmoCount(FName AttachedPropName) const;

	/**
	Sets ammo in attached prop, and handles setting ammo count in it if it's an ammo pickup.
	Returns true if successfully set ammo count.
	False if prop is detached or there is no prop with this name.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
	bool SetAttachedPropAmmoCount(FName AttachedPropName, int32 InAmmoCount);

	/**
	Moves some ammo from the weapon to the prop.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
	bool MoveAmmoFromWeaponToProp(FName AttachedPropName);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
	bool MoveAmmoFromPropToWeapon(FName AttachedPropName);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
	bool MoveAmmoFromPropToCharacter(FName AttachedPropName);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
	bool MoveAmmoFromCharacterToProp(FName AttachedPropName);

    /**
    Useful to call this when a State that modifies the attachment or visibility of a prop becomes inactive.
    This would be a safeguard against some things being accidentally left in an incorrect state when a State
    is interrupted, such as a shotgun shell remaining in a character's hand if a shell by shell reloading sequence was interrupted,
    or if a weapon was unequipped while reloading.
    */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
    void ResetAttachedPropToDefaultState(FName AttachedPropName);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
    bool AttachedPropToCharacterAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
    bool AttachedPropToPropAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
    bool ShowAttachedPropAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Prop")
    bool HideAttachedPropAnimNotify();

public:
	////////////////////////////////////
	//Attached Magazine

	/**
    Used specifically to spawn attached props that will be used as magazines for convenience.
	This will create an attached prop with the same name with 
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attached Magazine")
    TMap<FName, FWRAttachedMagazineData> AttachedMagazines;

	/**
	Attaches the magazine to the main prop.
	Transfers all the ammo from the magazine to the weapon itself.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Magazine")
	bool AttachMagazineToProp(FName AttachedPropName);

	/**
	Detaches the magazine from the main prop and puts it in the character's hand after transferring ammo from the gun to the magazine.
	If the magazine doesn't exist on the main prop it just makes the magazine visible in the character's hand.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Magazine")
	bool AttachMagazineToCharacter(FName AttachedPropName);

	/**
	Does the same thing as DisconnectAttachedProp but with all the ammo transferring that needs to happen.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attached Magazine")
	AAEPhysicalActor * DisconnectAttachedMagazine(FName AttachedPropName);
	
public:
	////////////////////////////////////
	//Ammo 

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	TMap<FName, FWRAmmoData> Ammo;
	    	
	/**
	Gets a reference to the ammo in the owner character's inventory
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ammo")
	AWRInventoryAmmo * GetOwnerReserveAmmo(FName AmmoName) const;

	/**
	Convenience method for checking if weapon has at least Amount ammo in the magazine, usually before firing
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ammo")
	bool CheckEnoughAmmo(FName AmmoName, int32 Amount = 1) const;

	/**
	Convenience method for checking if weapon is full on ammo, usually before reloading
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ammo")
	bool CheckFullAmmo(FName AmmoName) const;

	/**
	Convenience method for returning the amount of ammo
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ammo")
	int32 GetAmmoCapacity(FName AmmoName) const;

    /**
	Convenience method for returning the amount of ammo
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ammo")
	int32 GetAmmoCount(FName AmmoName) const;

	/**
	Convenience method for setting ammo in the weapon.
	Returns the remaining ammo.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ammo")
	void SetAmmoCount(FName AmmoName, int32 Amount);

    /**
    Convenience method for consuming ammo from the weapon.
    Returns the remaining ammo.
    */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ammo")
    int32 ConsumeAmmo(FName AmmoName, int32 Amount = 1);
		
public:
	///////////////////////////////////////////////
	//Animation

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation")
	TSubclassOf<UWRCharacterHandControl> HandControlClass;

	UPROPERTY(BlueprintReadWrite, Category = "Animation")
	UWREquippableManager * EquippableManager;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	UWRCharacterHandControl * CreateHandControlForEquippableManager();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation")
	UWRCharacterHandControl * HandControl;

	/**
	What stance is the character in when using this equippable.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation")
	UWRCharacterStanceData * CharacterStance;
	    
    /**
    Default pose of the character's dominant hand to override the current animation.
    The animation curve also controls the weight of this pose.
    */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	UAnimSequence * DefaultDominantHandPose;

	/**
    Default pose of the character's non-dominant hand to override the current animation.
    The animation curve also controls the weight of this pose.
    */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	UAnimSequence * DefaultNonDominantHandPose;

    /**
    Default animation playing underneath on the character when there is no anim montage playing.
    This is good for preventing the character from going back to the T pose if a WREquippableState doesn't play an animation.
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    UAnimSequence * DefaultIdleAnimation;

	/**
    Sometimes you specifically do not want the idle animation to play underneath like when the equip animation is blending in.
	This either enables or disables it on the character.
    */
    UPROPERTY(BlueprintReadWrite, Category = "Animation")
    bool bIdleAnimationEnabled;
                
    /**
    Default socket on the main prop that attaches to the character's dominant hand.
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    FName DefaultDominantHandAttachComponentSocket;

	/**
    Default socket on the main prop that the character's non dominant hand attaches to via IK.
    */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	FName DefaultNonDominantHandAttachIKComponentSocket;
        	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	UAnimSequence * GetDominantHandPose() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	UAnimSequence * GetNonDominantHandPose() const;
		
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	UAnimSequence * GetIdleAnimation() const;
    	    
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
    FName GetDominantHandAttachComponentSocket() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	FName GetNonDominantHandAttachIKComponentSocket() const;    

	/**
	This is the component on the main prop that the non dominant hand attaches to via IK.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	USceneComponent * GetNonDominantHandAttachIKComponent() const;

public:
	///////////////////////////////////////////////
	//Dominant Hand View Offset

	/**
    Offset of the character's hand from the center of the view when aiming up.
    Blends between DefaultUpDominantHandViewOffset and DefaultCenterDominantHandViewOffset when aiming between straight and up.
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dominant Hand View Offset")
    FVector DefaultUpDominantHandViewOffset;

    /**
    Offset of the character's hand from the center of the view when aiming straight.
    Blends between DefaultUpDominantHandViewOffset and DefaultCenterRightHandViewOffset when aiming between straight and up.
    Blends between DefaultDownDominantHandViewOffset and DefaultCenterRightHandViewOffset when aiming between straight and down.
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dominant Hand View Offset")
    FVector DefaultCenterDominantHandViewOffset;

    /**
    Offset of the character's hand from the center of the view when aiming down.
    Blends between DefaultDownDominantHandViewOffset and DefaultCenterDominantHandViewOffset when aiming between straight and down.
    Usually the character can't aim completely straight down or the gun would clip through the body sometimes, 
    so this offset would never have 100% influence.
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dominant Hand View Offset")
    FVector DefaultDownDominantHandViewOffset;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Dominant Hand View Offset")
    FVector GetUpDominantHandViewOffset() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Dominant Hand View Offset")
    FVector GetCenterDominantHandViewOffset() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Dominant Hand View Offset")
    FVector GetDownDominantHandViewOffset() const;

public:
	///////////////////
    //eject shell casings

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "EjectCasing")
    bool EjectCasingAnimNotify();

    /*UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = EjectCasing)
    class AWRShellCasing * EjectCasing(TSubclassOf<AWRShellCasing> CasingClass, FVector RelativeVelocity, FName CasingSpawnSocket = FName(TEXT("shell_casing_spawn")));*/

public:
	///////////////////
	//Projectile

	/**
	Gets the world transform that projectiles spawn from for the prop.
	If a character is holding the weapon, this position might be adjusted.
	It's still used for spawning effects such as beams to look like they're coming out of the barrel.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	FTransform GetProjectileSpawnLocationFromProp(const FVector& SpawnOffset = FVector::ZeroVector) const;

	/**
	Shoots a projectile of some class from the prop.
	If held by a character it'll do the appropriate spawning and position adjustments from the character.
	Otherwise if not held by a character, shoots from the prop itself.
	No effect if no character or prop.
	SpawnOffset gives you a chance to spawn a projectile somewhere away from the main muzzle of the weapon, like a secondary fire grenade launcher.
	This spawn location might be adjusted of the character is too close to a wall and the projectile might spawn inside a wall.
	RandomConeHalfAngleDegrees adds some spread.
	MinShots and MaxShots allows a random number of shots to spawn.
	It's better to let this function spawn a random number of shots than to externally call this multiple times since a bunch of work goes into finding the optimal shot spawn location.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	TArray<AWRProjectile *> ShootProjectileFromProp(TSubclassOf<AWRProjectile> ProjectileClass, 
		const FVector& SpawnOffset = FVector::ZeroVector, 
		float RandomConeHalfAngleDegrees = 0.f, 
		int32 MinShots = 1, 
		int32 MaxShots = 1);

	/**
	Shoots a single hit scan trace from the prop to deal damage.
	If held by a character it'll do the appropriate spawning and position adjustments from the character.
	Otherwise if not held by a character, shoots from the prop itself.
	No effect if no character or prop.
	SpawnOffset gives you a chance to spawn a projectile somewhere away from the main muzzle of the weapon, like a secondary fire grenade launcher.
	This spawn location might be adjusted of the character is too close to a wall and the projectile might spawn inside a wall.
	RandomConeHalfAngleDegrees adds some spread.
	MinShots and MaxShots allows a random number of shots to spawn.
	It's better to let this function spawn a random number of shots than to externally call this multiple times since a bunch of work goes into finding the optimal shot spawn location.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	TArray<FHitResult> ShootSingleHitscanTraceFromProp(TSubclassOf<UWRDamageType> DamageType,
		TSubclassOf<UWRImpactType> ImpactType,
		const FVector& SpawnOffset = FVector::ZeroVector, 
		float RandomConeHalfAngleDegrees = 0.f, 
		int32 MinShots = 1, 
		int32 MaxShots = 1,
		float DamageMultiplier = 1.f,
		float ExplosionRadiusMultiplier = 1.f,
		float MaxDistance = 50000.0f,
		bool bDebugDraw = false);

public:
	///////////////////////////////////////////////
	//State

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "State")
	UWREquippableStateManager * StateManager;
		
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "State")
	TSubclassOf<UAEState> EquipStateClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "State")
	TSubclassOf<UAEState> UnequipStateClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "State")
	TSubclassOf<UAEState> IdleStateClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "State")
	TArray<TSubclassOf<UAEState>> UseStateClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "State")
	TArray<FWRReloadUnloadStatesPair> ReloadUnloadStateClass;

	/**
	Resets all the stats and pending flags, should be done when the weapon is unequipped.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void ResetStates();

	/**
	Convenience method.
	Tries to go to a state.
	If the state is NULL just goes to idle.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	bool TryGotoStateOrIdleIfNull(UAEState * State);
	
	///////////////////
	//equip/unequip

	/**
	Sets up props, particle systems, etc...
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void SetupOnEquip();

	/**
	Tears down props, particle systems, etc...
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void TearDownOnUnequip();

	UPROPERTY(BlueprintReadWrite, Category = "State")
	bool bPendingUnequip;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void Equip();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void Unequip();
		
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void OnDoneUnequip();
		
	/**
	Checks if an Unequip is pending and performs it if needed.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	bool TryGotoUnequip();

	///////////////////
	//use
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void EngageUse(int32 UseMode);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void DisengageUse(int32 UseMode);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void OnUseModeEngaged(int32 UseMode);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void OnUseModeDisengaged(int32 UseMode);
    
	UPROPERTY(BlueprintReadWrite, Category = "State")
	TArray<bool> EngagedUseMode;
    
	/**
	Checks if any use modes are engaged and performs the first one available.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	bool TryGotoUse();

	/**
	Checks if it's possible to perform a use mode.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	bool TryUseMode(int32 UseMode);
	
	///////////////////
	//reload
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void Reload(int32 ReloadMode);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	bool TryGotoReload();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	bool TryReloadMode(int32 ReloadMode);

	UPROPERTY(BlueprintReadWrite, Category = "State")
	TArray<bool> PendingReloadMode;

    ///////////////////
    //unload
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void Unload(int32 UnloadMode);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	bool TryGotoUnload();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	bool TryUnloadMode(int32 UnloadMode);

    UPROPERTY(BlueprintReadWrite, Category = "State")
	TArray<bool> PendingUnloadMode;

	//////////////////
	//idle

	/**
	Tries to go to the idle state but first checks if there are various pending actions to do.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "State")
	void TryGotoIdle();
};

FORCEINLINE_DEBUGGABLE float AWRInventoryPhysical::GetTotalWeight() const
{
	if (bIsStackable)
	{
		return GetStackCount() * Weight;
	}

	return Weight;
}

FORCEINLINE_DEBUGGABLE void AWRInventoryPhysical::SetAttachedPropForcedDefaultVisibilityState(FName AttachedPropName, AEDefaultBoolOverrideState::Type Override)
{
	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		PropData->ForcedDefaultVisibilityState = Override;
	}
}

FORCEINLINE_DEBUGGABLE AEDefaultBoolOverrideState::Type AWRInventoryPhysical::GetAttachedPropForcedDefaultVisibilityState(FName AttachedPropName) const
{
	const FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		return PropData->ForcedDefaultVisibilityState;
	}

	return AEDefaultBoolOverrideState::NO_FORCED_OVERRIDE;
}

FORCEINLINE_DEBUGGABLE bool AWRInventoryPhysical::GetAttachedPropDefaultVisibilityState(FName AttachedPropName) const
{
	const FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		return PropData->bDefaultVisible;
	}

	return false;
}

FORCEINLINE_DEBUGGABLE void AWRInventoryPhysical::SetAttachedPropForcedDefaultCollisionState(FName AttachedPropName, AEDefaultBoolOverrideState::Type Override)
{
	FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		PropData->ForcedDefaultCollisionState = Override;
	}
}

FORCEINLINE_DEBUGGABLE AEDefaultBoolOverrideState::Type AWRInventoryPhysical::GetAttachedPropForcedDefaultCollisionState(FName AttachedPropName) const
{
	const FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		return PropData->ForcedDefaultCollisionState;
	}

	return AEDefaultBoolOverrideState::NO_FORCED_OVERRIDE;
}

FORCEINLINE_DEBUGGABLE bool AWRInventoryPhysical::GetAttachedPropDefaultCollisionState(FName AttachedPropName) const
{
	const FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		return PropData->bDefaultCollision;
	}

	return false;
}

FORCEINLINE_DEBUGGABLE AAEPhysicalActor * AWRInventoryPhysical::GetAttachedProp(FName AttachedPropName) const
{
	const FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		return PropData->Prop;
	}

	return NULL;
}

int32 FORCEINLINE_DEBUGGABLE AWRInventoryPhysical::GetAttachedPropAmmoCount(FName AttachedPropName) const
{
	if (GetAttachedPropState(AttachedPropName) == WRAttachedPropAttachment::DETACHED)
	{
		return -1;
	}

	const FWRAttachedPropData * PropData = AttachedProps.Find(AttachedPropName);

	if (PropData)
	{
		return PropData->AmmoCount;
	}

	return -1;
}
#pragma once

#include "WREquippableManager.generated.h"

class AWRInventoryPhysical;
class UWRCharacterHandControl;
class UWRInventoryManager;

/**
Handles a character equipping and unequipping an inventory object
*/
UCLASS(meta = (BlueprintSpawnableComponent))
class UWREquippableManager : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Category = "Inventory")
	UWRInventoryManager * InventoryManager;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void SwitchEquippable(AWRInventoryPhysical* NewEquippable);
	
	UPROPERTY(BlueprintReadOnly, Category = "Equippable")
	AWRInventoryPhysical* PendingEquippable;

	UPROPERTY(BlueprintReadOnly, Category = "Equippable")
	AWRInventoryPhysical* CurrentEquippable;

	/**
	Called by the current equippable when it's done unequipping to trigger equipping the pending equippable
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void EquipPendingEquippable();
	    
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void EngageUse(int32 UseMode);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
	void DisengageUse(int32 UseMode);
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
    void Reload(int32 ReloadMode);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
    void Unload(int32 UnloadMode);

	UPROPERTY(BlueprintReadWrite, Category = "Hand Control")
	UWRCharacterHandControl * DominantHandEquippableControl;

public:
	///////////////////////////////////////
	//Animation

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	UAnimSequence * GetNonDominantHandPose() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	UAnimSequence * GetDominantHandPose() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
    UAnimSequence * GetIdleAnimation() const;
    

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	USceneComponent * GetNonDominantHandAttachIKComponent() const;
    
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation")
	FName GetNonDominantHandAttachIKComponentSocket() const;

public:
	///////////////////////////////////////
	//Animation Event

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool EjectCasingAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool AttachedPropToCharacterAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool AttachedPropToPropAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool ShowAttachedPropAnimNotify();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Animation Event")
    bool HideAttachedPropAnimNotify();
};
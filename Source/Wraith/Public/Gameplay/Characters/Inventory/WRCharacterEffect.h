#pragma once

#include "WRInventory.h"
#include "WRCharacterEffect.generated.h"

class AWRCharacter;

UCLASS(Abstract)
class WRAITH_API AWRCharacterEffect : public AWRInventory
{
	GENERATED_BODY()
public:
	virtual void Added_Implementation(UWRInventoryManager * InOwningManager) override;

	virtual void Removed_Implementation() override;

	/**
	Call this on the character to change the character in some way when this effect is enabled
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Effect")
	void OnEnabledForCharacter(AWRCharacter * Character);

	/**
	Call this on the character to revert the change that happened in OnEnabledForCharacter when this effect is disabled
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Effect")
	void OnDisabledForCharacter(AWRCharacter * Character);

	/**
	The description that show up in the character screen when this effect is active
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	FText Description;

	/**
	The icon that shows up in the character screen when this effect is active.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	UMaterial * Icon;
};
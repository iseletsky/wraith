#pragma once

#include "WRPickupInventory.h"
#include "WRInventoryAmmo.h"
#include "WRPickupAmmo.generated.h"

UCLASS()
class WRAITH_API AWRPickupAmmo : public AWRPickupInventory
{
	GENERATED_BODY()

public:
	virtual AWRInventoryPhysical * SpawnContainedInventory_Implementation() override;

	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Ammo")
	TSubclassOf<AWRInventoryAmmo> AmmoClass;

	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Ammo")
	int32 AmmoCount;
};
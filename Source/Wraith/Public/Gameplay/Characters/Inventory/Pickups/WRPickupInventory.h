#pragma once

#include "WRPickup.h"
#include "WRInventoryPhysical.h"
#include "WRPickupInventory.generated.h"

UCLASS()
class WRAITH_API AWRPickupInventory : public AWRPickup
{
	GENERATED_BODY()

public:
	virtual void Destroyed() override;

	virtual void BeginPlay() override;

	virtual void OnPickedUp_Implementation() override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Inventory")
	TSubclassOf<AWRInventoryPhysical> InventoryClass;

	/**
	Map of Ammo name to starting ammo count override.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Inventory")
	TMap<FName, int32> StartingAmmoCountOverride;

	UPROPERTY(BlueprintReadWrite, Category = "Inventory")
	AWRInventoryPhysical * ContainedInventory;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
	AWRInventoryPhysical * SpawnContainedInventory();

	virtual bool TryGiveTo_Implementation(APawn * Target) override;

	virtual FText GetPickupDisplayName_Implementation() const override;
};
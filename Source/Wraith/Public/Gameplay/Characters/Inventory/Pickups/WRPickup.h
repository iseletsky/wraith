#pragma once

#include "AEPhysicalActor.h"
#include "WRUsableInterface.h"
#include "WRPickup.generated.h"

UCLASS()
class WRAITH_API AWRPickup : public AAEPhysicalActor, public IWRUsableInterface
{
	GENERATED_BODY()

public:
	/**
	Default collect collision radius of pickups the player must touch
	*/
	static const float DEFAULT_TOUCH_PICKUP_RADIUS;

	/**
	Default collect collision radius of pickups the player must "use" to pick up.
	*/
	static const float DEFAULT_USE_PICKUP_RADIUS;

public:
	AWRPickup(const FObjectInitializer& ObjectInitializer);

	virtual void PostInitializeComponents() override;

	virtual void Tick(float DeltaSeconds) override;

	virtual void LevelOrderComponentTraverse_Implementation(USceneComponent * Component) override;

	/////////////////////////////////////
	//Usable
public:
	/**
	If the pickup component is collected via use button, this is called by the pawn attempting to collect it.
	This calls TryGiveTo
	*/
	virtual void Use_Implementation(APawn * User) override;

	virtual bool RequiresAim_Implementation() const override;

	virtual FText UseActionText_Implementation() const override;

	/////////////////////////////////////
	//Pickup
public:
	/**
	called when Pickup overlaps with pawn that can collect it
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickup")
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult);

	/**
	called when Pickup no longer overlap with pawn that can collect it
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickup")
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	/**
	If the pickup component is collected by touch, this is called by the overlap call.
	This can also be called by Use_Implementation when a pawn collects this pickup using the use key
	*/
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickup")
	bool TryGiveTo(APawn * Target);

	/**
	Radius of the sphere that defines collision for allowing a character to pick up the item.
	*/
	UPROPERTY(EditAnywhere, Transient, Category = "Pickup")
	float PickupRadius;

	/**
	If usable, characters must "Use" the pickup to collect it, otherwise simply running into the pickup would collect it.
	*/
	UPROPERTY(EditAnywhere, Category = "Pickup")
	bool bUsable;

	/**
	Called when the pickup is picked up do do any needed effects
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickup")
	void OnPickedUp();

	/**
	Call to enable or disable the pickup being pick uppable.
	Usually this just sets PickupCollision to disable collisions.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickup")
	void SetPickupEnabled(bool bInPickupEnabled);

	/**
	Return localized text for the name of the pickup so the HUD shows the right message.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickup")
	FText GetPickupDisplayName() const;

	/** Human readable localized name for the item. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FText DisplayName;

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickup")
	void TryGiveToOverlappingCharacters();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickup")
	void SetupPickupCollision(USceneComponent * Parent);

	UPROPERTY(BlueprintReadOnly, Category = "Pickup")
	USphereComponent * PickupCollision;

	/**
	Characters currently overlapping with the pickup.
	Checks each game tick which one isn't behind a wall and tries to give it to them.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Pickup")
    TArray<APawn *> OverlappedCharacters;
};
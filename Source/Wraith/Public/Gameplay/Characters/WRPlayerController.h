#pragma once
#include "LGPlayerController.h"
#include "WRTeams.h"
#include "WRPlayerController.generated.h"

UCLASS()
class AWRPlayerController : public ALGPlayerController, public IGenericTeamAgentInterface
{
	GENERATED_BODY()
		
public:
	AWRPlayerController(const FObjectInitializer& ObjectInitializer);

	virtual void GetSeamlessTravelActorList(bool bToEntry, TArray <AActor *> & ActorList) override;

	virtual void UpdateRotation(float DeltaTime) override;

	virtual void Possess(APawn * InPawn) override;

public:
	///////////////////////////////////////
	//Team

	UPROPERTY(BlueprintReadWrite, Category = "Team")
	FGenericTeamId TeamID;

	virtual void SetGenericTeamId(const FGenericTeamId& NewTeamID) override {
		TeamID = NewTeamID;
	}

	virtual FGenericTeamId GetGenericTeamId() const override {
		return TeamID;
	}

	/**
	If true, the player controller takes on the Pawn's team whenever it possesses it.
	Set to false if a player controller is already on an established team and any pawns it possesses should defer to this team.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
	uint32 bAssignToPawnTeamOnPossess:1;

	//////////////////
	//Controls
		
protected:
	virtual void SetupInputComponent() override;
	
public:
	
	//Look

	/**
	YawRight should be used by inputs that are normalized between a value of -1 to 1.
	Joysticks are the primary example.
	*/
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Controls")
	void YawRight(float Val);

	/**
	YawRight should be used by inputs that are not normalized between a value of -1 to 1 and are more of a delta.
	Mice are the primary example.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Controls")
	void YawRightUnnormalized(float Val);

	/**
	PitchUp should be used by inputs that are normalized between a value of -1 to 1.
	Joysticks are the primary example.
	*/
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Controls")
	void PitchUp(float Val);

	/**
	PitchUpUnnormalized should be used by inputs that are not normalized between a value of -1 to 1 and are more of a delta.
	Mice are the primary example.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Controls")
	void PitchUpUnnormalized(float Val);
		
public:
	///////////////////////////////////////
	//Camera
    
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Camera")
    FVector GetRelativeCameraLocation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Camera")
    FRotator GetRelativeCameraRotation();

	//TODO: (ill) may need to move these camera reduction settings somewhere more global so spectating in multiplayer has effect no matter who the player controller is, unless I can still access these when spectating
	//actually now that I think about it, I should be able to

	//TODO: (ill) also add checks for whether or not currently viewing the character in first person

    /**
    Lets players tone down the camera rotation movement due to recoil from weapons.
    0 is no reduction.
    1 is full reduction, meaning fully disable.
    */
    UPROPERTY(BlueprintReadOnly, Config, Category = "Camera")
    float CameraRecoilRotationReduction;

	/**
	Lets players tone down the camera animation effects if they don't like them or it gives them motion sickness.
	0 is no reduction.
	1 is full reduction, meaning fully disable.
	*/
	UPROPERTY(BlueprintReadOnly, Config, Category = "Camera")
	float CameraAnimationReduction;

	/**
	Lets players tone down the camera shake effects if they don't like them or it gives them motion sickness.
	0 is no reduction.
	1 is full reduction, meaning fully disable.
	*/
	UPROPERTY(BlueprintReadOnly, Config, Category = "Camera")
	float CameraShakeReduction;

	/**
	Wrapper around playing a camera animation.
	Also checks if a player is force disabling camera animations in some way or applies dampening if players want to tone down effects.
	*/
	UFUNCTION(BlueprintCallable, Category = "Camera")
	UCameraAnimInst * PlayCameraAnim(
		class UCameraAnim * Anim,
		float Rate = 1.f, 
		float Scale = 1.f, 
		float BlendInTime = 0.f, 
		float BlendOutTime = 0.f, 
		bool bLoop = false,
		bool bRandomStartTime = false, 
		float Duration = 0.f, 
		ECameraAnimPlaySpace::Type PlaySpace = ECameraAnimPlaySpace::CameraLocal, 
		FRotator UserPlaySpaceRot = FRotator::ZeroRotator);

	/**
	Wrapper around playing a camera animation with a duration rather than a rate.
	Duration here is different from duration provided in the other implementation as it just scales the playrate to make the animation take the right time but play fully.
	Also checks if a player is force disabling camera animations in some way or applies dampening if players want to tone down effects.
	*/
	UFUNCTION(BlueprintCallable, Category = "Camera")
	UCameraAnimInst * PlayCameraAnimWithDuration(
		class UCameraAnim * Anim,
		float Duration,
		float Scale = 1.f,
		float BlendInTime = 0.f,
		float BlendOutTime = 1.f,
		ECameraAnimPlaySpace::Type PlaySpace = ECameraAnimPlaySpace::CameraLocal,
		FRotator UserPlaySpaceRot = FRotator::ZeroRotator,
		bool bAccountForBlendoutTime = true);

	/**
	Convenience wrapper method for stopping all camera animations.
	This is a good thing to do when an equippable state is interrupted for example...
	*/
	UFUNCTION(BlueprintCallable, Category = "Camera")
	void StopAllCameraAnims(bool bImmediate = false);

	/**
	Wrapper around playing a camera shake.
	Also checks if a player is force disabling camera shakes in some way or applies dampening if players want to tone down effects.
	*/
	UFUNCTION(BlueprintCallable, Category = "Camera")
	UCameraShake * PlayCameraShake(
		TSubclassOf< class UCameraShake > ShakeClass,
		float Scale = 1.f, 
		enum ECameraAnimPlaySpace::Type PlaySpace = ECameraAnimPlaySpace::CameraLocal, 
		FRotator UserPlaySpaceRot = FRotator::ZeroRotator);

	/**
	Returns the current minimum view pitch.
	This is intended to avoid awkward angles where the players arms will start clipping into the legs.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Camera")
	float GetMinViewPitch() const;

public:
	//////////////////
	//3rd Person Debug

	UFUNCTION(Exec, BlueprintCallable, BlueprintNativeEvent, Category = "Debug3P")
	void StartDebug3P();

	UFUNCTION(Exec, BlueprintCallable, BlueprintNativeEvent, Category = "Debug3P")
	void StopDebug3P();

	UFUNCTION(Exec, BlueprintCallable, BlueprintNativeEvent, Category = "Debug3P")
	void StartDebug3PControl();

	UFUNCTION(Exec, BlueprintCallable, BlueprintNativeEvent, Category = "Debug3P")
	void StopDebug3PControl();

	UPROPERTY(BlueprintReadWrite, Category = "Debug3P")
	bool bIsDebugging3P;

	UPROPERTY(BlueprintReadWrite, Category = "Debug3P")
	bool bIsDebugging3PControlling;

	UPROPERTY(BlueprintReadWrite, Category = "Debug3P")
	FRotator Debug3PViewRotation;

	UPROPERTY(BlueprintReadWrite, Category = "Debug3P")
	FVector Debug3PViewLocation;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Controls")
	void Debug3PMoveForward(float Val);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Controls")
	void Debug3PMoveRight(float Val);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Controls")
	void Debug3PMoveUp(float Val);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Controls")
	void CheckEnableDebug3PControl();

	FInputAxisBinding * Debug3PMoveForwardBinding;
	FInputAxisBinding * Debug3PMoveRightBinding;
	FInputAxisBinding * Debug3PMoveUpBinding;
};

#pragma once

#include "WRCharacterHandControl.h"
#include "WRCharacterHandControlProp.generated.h"

class AWRInventoryPhysical;
class AWRProjectile;

UCLASS(meta = (BlueprintSpawnableComponent))
class UWRCharacterHandControlProp : public UWRCharacterHandControl
{
	GENERATED_BODY()

public:

	UWRCharacterHandControlProp(const FObjectInitializer& ObjectInitializer);

	virtual FTransform GetHandWorldTransform_Implementation(bool bIncludeAnimationOnlyEffects = false) const override;
	
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	/**
	The equippable held by this hand.
	*/
	UPROPERTY(BlueprintReadOnly, Category = "Equippable")
	AWRInventoryPhysical* HeldEquippable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Equippable")
    FName DefaultNonDominantHandPropSocket;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Equippable")
    FName DefaultDominantHandPropSocket;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
    FName GetNonDominantHandPropSocket() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Equippable")
    FName GetDominantHandPropSocket() const;

	/**
	Returns the relative location of the hand relative to the eyes.
	Takes adjustments based on weapon pitch and all the other things that are needed.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Control")
	FVector GetBaseRelativeToEyesLocation() const;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Control")
	void UpdateBaseRelativeToEyesTransform();
	
	UFUNCTION(BlueprintCallable, Category = "Hand Control")
	FTransform GetBaseRelativeToEyesTransform() const;

	/**
	A function used early in the calculation
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Control")
	FTransform GetBaseWorldTransform() const;

protected:
	/**
	The current transform of the hand relative to the eyes.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Hand Control")
	FTransform BaseRelativeToEyesTransform;

public:
	/**
	Applies all of the needed effects that affect hands to a transform that's relative to the eyes.
	@param bIncludeAnimationOnlyEffects If true includes things that are there only for show, like walking sway and lazy weapon.
		You want this off if determining a spawn transform of projectiles.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Control")
	FTransform GetWorldTransformForHandEffectsToRelativeToEyesTransform(const FTransform& RelativeToEyesTransform, bool bIncludeAnimationOnlyEffects = false) const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Control")
	void CharacterJumped();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Control")
	void CharacterLanded(float FallingDamage = 0.f);

public:
	/////////////////////////////////////// 
	//Projectile Shooting

	/**
	Figures out the safest world transform to spawn a projectile or hitscan trace from the character's equipped prop.
	This is without any other effects applied like recoil.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	FTransform GetSafeShootingSpawnWorldTransformFromProp(const FVector& SpawnOffset = FVector::ZeroVector,
		ECollisionChannel TraceChannel = /*COLLISION_TRACE_WEAPON*/ ECC_GameTraceChannel2,
		float SpawnedActorRadius = 1.f);

	/**
	Figures out the world transform to spawn a projectile or hitscan trace from the character's equipped prop.
	This is all other effects applied like recoil. 
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	FTransform GetRecoilAffectedShootingSpawnWorldTransformFromProp(const FVector& SpawnOffset = FVector::ZeroVector,
		ECollisionChannel TraceChannel = /*COLLISION_TRACE_WEAPON*/ ECC_GameTraceChannel2,
		float SpawnedActorRadius = 1.f);

	/**
	Shoots a projectile of some class from the character's prop location.
	SpawnOffset gives you a chance to spawn a projectile somewhere away from the main muzzle of the weapon, like a secondary fire grenade launcher.
	This spawn location might be adjusted if the character is too close to a wall and the projectile might spawn inside a wall.
	RandomConeHalfAngleDegrees adds some spread.
	MinShots and MaxShots allows a random number of shots to spawn.
	It's better to let this function spawn a random number of shots than to externally call this multiple times since a bunch of work goes into finding the optimal shot spawn location.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	TArray<AWRProjectile *> ShootProjectileFromProp(TSubclassOf<AWRProjectile> ProjectileClass, 
		const FVector& SpawnOffset = FVector::ZeroVector, 
		float RandomConeHalfAngleDegrees = 0.f, 
		int32 MinShots = 1, 
		int32 MaxShots = 1);

	/**
	Shoots a single hit scan trace from the prop to deal damage.
	If held by a character it'll do the appropriate spawning and position adjustments from the character.
	Otherwise if not held by a character, shoots from the prop itself.
	No effect if no character or prop.
	SpawnOffset gives you a chance to spawn a projectile somewhere away from the main muzzle of the weapon, like a secondary fire grenade launcher.
	This spawn location might be adjusted of the character is too close to a wall and the projectile might spawn inside a wall.
	RandomConeHalfAngleDegrees adds some spread.
	MinShots and MaxShots allows a random number of shots to spawn.
	It's better to let this function spawn a random number of shots than to externally call this multiple times since a bunch of work goes into finding the optimal shot spawn location.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	TArray<FHitResult> ShootSingleHitscanTraceFromProp(TSubclassOf<UWRDamageType> DamageType,
		TSubclassOf<UWRImpactType> ImpactType,
		const FVector& SpawnOffset = FVector::ZeroVector, 
		float RandomConeHalfAngleDegrees = 0.f, 
		int32 MinShots = 1, 
		int32 MaxShots = 1,
		float DamageMultiplier = 1.f,
		float ExplosionRadiusMultiplier = 1.f,
		float MaxDistance = 50000.0f,
		bool bDebugDraw = false);

public:
	///////////////////////////////////////
	//Lazy Gun

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Lazy Gun")
	void UpdateLazyGunCharacterAim(FRotator NewControlRotation, float DeltaTime);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Lazy Gun")
    void EnableLazyGun();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Lazy Gun")
    void DisableLazyGun();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Lazy Gun")
    void UpdateLazyGunOffsetRotation(float DeltaSeconds);

	/**
	The control rotation that lags behind the current control rotation
	*/
    UPROPERTY(BlueprintReadWrite, Category = "Lazy Gun")
    FRotator LazyGunControlRotation;

	/**
	The actual offset relative to the control rotation that gives the appearance of lazy gun
	*/
    UPROPERTY(BlueprintReadWrite, Category = "Lazy Gun")
    FRotator LazyGunControlRotationOffset;

    UPROPERTY(BlueprintReadWrite, Category = "Lazy Gun")
    float LazyGunWeight;

    UPROPERTY(BlueprintReadWrite, Category = "Lazy Gun")
    float DesiredLazyGunWeight;

public:
	///////////////////////////////////////
	//Movement Hands Offset

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement Hands Offset")
    void UpdateMovementHandViewOffset(float DeltaSeconds);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement Hands Offset")
	void ImpartJumpMovementHandViewOffset();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Movement Hands Offset")
	void ImpartLandMovementHandViewOffset(float FallingDamage = 0.f);

    UPROPERTY(BlueprintReadWrite, Category = "Movement Hands Offset")
    FVector CurrentMovementHandViewPosition;

    UPROPERTY(BlueprintReadWrite, Category = "Movement Hands Offset")
    FVector DesiredMovementHandViewPosition;

    UPROPERTY(BlueprintReadWrite, Category = "Movement Hands Offset")
    FRotator CurrentMovementHandViewRotation;

    UPROPERTY(BlueprintReadWrite, Category = "Movement Hands Offset")
    FRotator DesiredMovementHandViewRotation;

public:
	///////////////////////////////////////
	//Stumble Hands Offset

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Stumble Hands Offset")
    void UpdateStumbleHandsOffset(float DeltaSeconds);

    UPROPERTY(BlueprintReadWrite, Category = "Stumble Hands Offset")
    FVector CurrentStumbleHandsPosition;

    UPROPERTY(BlueprintReadWrite, Category = "Stumble Hands Offset")
    FRotator CurrentStumbleHandsRotation;

public:
	///////////////////////////////////////
	//Walking Sway Hands Offset

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Walking Sway Hands Offset")
    void UpdateWalkingSwayHandsOffset(float DeltaSeconds);

	UPROPERTY(BlueprintReadWrite, Category = "Walking Sway Hands Offset")
    float CurrentWalkingSwayHandsOffsetWeight;

	UPROPERTY(BlueprintReadWrite, Category = "Walking Sway Hands Offset")
    float DesiredWalkingSwayHandsOffsetWeight;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Walking Sway Hands Offset")
    void EnableWalkingHandsSwayOffset();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Walking Sway Hands Offset")
    void DisableWalkingHandsSwayOffset();

	UPROPERTY(BlueprintReadWrite, Category = "Walking Sway Hands Offset")
    float CurrentWalkingSwayHandsOffsetEnableWeight;

	UPROPERTY(BlueprintReadWrite, Category = "Walking Sway Hands Offset")
    float DesiredWalkingSwayHandsOffsetEnableWeight;

public:
	///////////////////////////////////////
	//Idle Hands Animation

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Idle Hands Animation")
    void EnableIdleHandsAnimation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Idle Hands Animation")
    void DisableIdleHandsAnimation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Idle Hands Animation")
    void UpdateIdleHandsAnimation(float DeltaSeconds);

    UPROPERTY(BlueprintReadWrite, Category = "Idle Hands Animation")
    FVector StartIdleHandsPosition;

    UPROPERTY(BlueprintReadWrite, Category = "Idle Hands Animation")
    FVector DestinationIdleHandsPosition;

    UPROPERTY(BlueprintReadWrite, Category = "Idle Hands Animation")
    FRotator StartIdleHandsRotation;

    UPROPERTY(BlueprintReadWrite, Category = "Idle Hands Animation")
    FRotator DestinationIdleHandsRotation;
    
    UPROPERTY(BlueprintReadWrite, Category = "Idle Hands Animation")
    float IdleHandsAnimationWeight;

    UPROPERTY(BlueprintReadWrite, Category = "Idle Hands Animation")
    float DesiredIdleHandsAnimationWeight;

    UPROPERTY(BlueprintReadWrite, Category = "Idle Hands Animation")
    FAlphaBlend IdleHandsAnimationBlend;

    UFUNCTION(BlueprintCallable, Category = "Idle Hands Animation")
    FVector GetCurrentIdleHandsPosition() const;

    UFUNCTION(BlueprintCallable, Category = "Idle Hands Animation")
    FRotator GetCurrentIdleHandsRotation() const;
};

///////////////////////////////////////
//Idle Hands Animation

FORCEINLINE_DEBUGGABLE FVector UWRCharacterHandControlProp::GetCurrentIdleHandsPosition() const
{
	return FMath::Lerp(StartIdleHandsPosition, DestinationIdleHandsPosition, IdleHandsAnimationBlend.GetBlendedValue());
}

FORCEINLINE_DEBUGGABLE FRotator UWRCharacterHandControlProp::GetCurrentIdleHandsRotation() const
{
	return FMath::Lerp(StartIdleHandsRotation, DestinationIdleHandsRotation, IdleHandsAnimationBlend.GetBlendedValue());
}

///////////////////////////////////////
//Hand Control

FORCEINLINE_DEBUGGABLE FTransform UWRCharacterHandControlProp::GetBaseRelativeToEyesTransform() const
{
	//TODO: add a lazy load updater
	/*if (bBaseRelativeToEyesTransformNeedsUpdate)*/
	{
		//I'm doing this because of lazy loading, forgive me!
		const_cast<UWRCharacterHandControlProp *>(this)->UpdateBaseRelativeToEyesTransform();
	}

	return BaseRelativeToEyesTransform;
}
#pragma once

#include "WRCharacterComponent.h"
#include "WRCharacterHandControl.generated.h"

UCLASS(abstract)
class UWRCharacterHandControl : public UWRCharacterComponent
{
	GENERATED_BODY()

public:
	/**
	Gets the world transform of the hand.
	@param bIncludeAnimationOnlyEffects If true, add on effects that only affect visuals.
		Sometimes you don't want an effect to contribute to the transform, 
		like if firing a weapon and you want to ignore the walking bob or other effects not guranteed to replicate over the network precisely.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Control")
	FTransform GetHandWorldTransform(bool bIncludeAnimationOnlyEffects = false) const;
};
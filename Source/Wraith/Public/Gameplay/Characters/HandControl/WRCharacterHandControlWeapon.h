#pragma once

#include "WRCharacterHandControlProp.h"
#include "WRCharacterHandControlWeapon.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class UWRCharacterHandControlWeapon : public UWRCharacterHandControlProp
{
	GENERATED_BODY()

public:

	UWRCharacterHandControlWeapon(const FObjectInitializer& ObjectInitializer);

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	virtual FTransform GetHandWorldTransform_Implementation(bool bIncludeAnimationOnlyEffects) const override;

	virtual FTransform GetWorldTransformForHandEffectsToRelativeToEyesTransform_Implementation(const FTransform& RelativeToEyesTransform, bool bIncludeAnimationOnlyEffects = false) const;
	
	virtual void UpdateBaseRelativeToEyesTransform_Implementation() override;

	virtual void CharacterJumped_Implementation() override;

	virtual void CharacterLanded_Implementation(float FallingDamage = 0.f) override;

	/**
	Similar to BaseRelativeToEyesTransform.
	This only affects the visual aspect of the weapon so it doesn't appear to snap awkwardly
	as you look around at walls.  It smoothly interpolates to BaseRelativeToEyesTransform.
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Hand Control")
	FTransform AnimBaseRelativeToEyesTransform;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Control")
	void UpdateAnimBaseRelativeToEyesTransform(float DeltaSeconds);

public:
	///////////////////////////////////////
	//Hand Recoil Animation
	//(makes weapon feel powerful but is only for show)

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Recoil Animation")
    void UpdateHandRecoilAnimation(float DeltaSeconds);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hand Recoil Animation")
    void ImpartHandRecoilAnimation(float Strength, float Time, float RecoverTime);

    UPROPERTY(BlueprintReadWrite, Category = "Hand Recoil Animation")
    FAlphaBlend HandRecoilAnimationBlend;

    UPROPERTY(BlueprintReadWrite, Category = "Hand Recoil Animation")
    float HandRecoilAnimationRecoverTime;

    UPROPERTY(BlueprintReadWrite, Category = "Hand Recoil Animation")
    FVector StartHandRecoilAnimationPosition;

    UPROPERTY(BlueprintReadWrite, Category = "Hand Recoil Animation")
    FVector DestinationHandRecoilAnimationPosition;

    UPROPERTY(BlueprintReadWrite, Category = "Hand Recoil Animation")
    FRotator StartHandRecoilAnimationRotation;

    UPROPERTY(BlueprintReadWrite, Category = "Hand Recoil Animation")
    FRotator DestinationHandRecoilAnimationRotation;
	
    UFUNCTION(BlueprintCallable, Category = "Hand Recoil Animation")
    FVector GetCurrentHandRecoilAnimationPosition() const;

    UFUNCTION(BlueprintCallable, Category = "Hand Recoil Animation")
    FRotator GetCurrentHandRecoilAnimationRotation() const;

public:
	///////////////////////////////////////
	//Weapon Recoil
	//(vertical V shaped imparted by firing the weapon)

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon Recoil")
    void UpdateWeaponRecoil(float DeltaSeconds);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon Recoil")
    void ImpartWeaponRecoil(float AddYawMax, float TotalYawMax,
        float AddPitchMin, float AddPitchMax, float TotalPitchMax, 
        float Time, float RecoverTime, bool bDebugDraw);

	UPROPERTY(BlueprintReadWrite, Category = "Weapon Recoil")
	FAlphaBlend WeaponRecoilBlend;

    UPROPERTY(BlueprintReadWrite, Category = "Weapon Recoil")
    float WeaponRecoilRecoverTime;

    UPROPERTY(BlueprintReadWrite, Category = "Weapon Recoil")
    FRotator StartWeaponRecoilRotation;

    UPROPERTY(BlueprintReadWrite, Category = "Weapon Recoil")
    FRotator DestinationWeaponRecoilRotation;

    UFUNCTION(BlueprintCallable, Category = "Weapon Recoil")
    FRotator GetCurrentWeaponRecoilRotation() const;

public:
	///////////////////////////////////////
	//Radial Weapon Inaccuracy
	//(imparted by things like movement or taking hits)

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Radial Weapon Inaccuracy")
	void ImpartJumpRadialWeaponInaccuracy();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Radial Weapon Inaccuracy")
	void ImpartLandRadialWeaponInaccuracy(float FallingDamage = 0.f);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Radial Weapon Inaccuracy")
	void UpdateRadialWeaponInaccuracy(float DeltaSeconds);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Radial Weapon Inaccuracy")
    void ImpartRadialWeaponInaccuracy(float AddMin, float AddMax, float TotalMax, float Time, float RecoverTime);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Radial Weapon Inaccuracy")
	void ImpartConstantRadialWeaponInaccuracy(float Value, float RecoverTime);

	UPROPERTY(BlueprintReadWrite, Category = "Radial Weapon Inaccuracy")
    FAlphaBlend RadialWeaponInaccuracyBlend;
		
	/**
	Acts as a limit so extreme situations don't cause huge inaccuracy amounts
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Radial Weapon Inaccuracy")
    float MaxRadialWeaponInaccuracyMagnitude;

	UFUNCTION(BlueprintCallable, Category = "Radial Weapon Inaccuracy")
	float GetCurrentRadialWeaponInaccuracyMagnitude() const;

    UPROPERTY(BlueprintReadWrite, Category = "Radial Weapon Inaccuracy")
    float RadialWeaponInaccuracyRecoverTime;

	/**
	This is layered on top of RadialWeaponInaccuracyBlend and is a value that is always set each game tick based on character movement
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Radial Weapon Inaccuracy")
    FAlphaBlend ConstantRadialWeaponInaccuracyBlend;

	/**
	Updates every frame based on time passed and acts as the parameter into
	the math equation that controls the frame's radial weapon inaccuracy positon and rotation	
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Radial Weapon Inaccuracy")
    float RadialWeaponInaccuracyFuncParamTime;

	/**
	Randomly decides to start updating RadialWeaponInaccuracyFuncParamTime in the negative direction instead of positive
	during KickRadialWeaponInaccuracyFuncParamTime to avoid sometimes seeing guns moving in a predictable circular motion
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Radial Weapon Inaccuracy")
    bool bRadialWeaponInaccuracyFuncParamTimeNegativeDirection;

	/**
	Give a little kick to the RadialWeaponInaccuracyFuncParamTime during some events
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Radial Weapon Inaccuracy")
	void KickRadialWeaponInaccuracyFuncParamTime();

    UFUNCTION(BlueprintCallable, Category = "Radial Weapon Inaccuracy")
    FRotator GetCurrentRadialWeaponInaccuracyRotation() const;
};

///////////////////////////////////////
//Hand Recoil Animation

FORCEINLINE_DEBUGGABLE FVector UWRCharacterHandControlWeapon::GetCurrentHandRecoilAnimationPosition() const
{
	return FMath::Lerp(StartHandRecoilAnimationPosition, DestinationHandRecoilAnimationPosition, HandRecoilAnimationBlend.GetBlendedValue());
}

FORCEINLINE_DEBUGGABLE FRotator UWRCharacterHandControlWeapon::GetCurrentHandRecoilAnimationRotation() const
{
	return FMath::Lerp(StartHandRecoilAnimationRotation, DestinationHandRecoilAnimationRotation, HandRecoilAnimationBlend.GetBlendedValue());
}

///////////////////////////////////////
//Weapon Recoil

FORCEINLINE_DEBUGGABLE FRotator UWRCharacterHandControlWeapon::GetCurrentWeaponRecoilRotation() const
{
	return FMath::Lerp(StartWeaponRecoilRotation, DestinationWeaponRecoilRotation, WeaponRecoilBlend.GetBlendedValue());
}

///////////////////////////////////////
//Radial Weapon Inaccuracy

FORCEINLINE_DEBUGGABLE FRotator UWRCharacterHandControlWeapon::GetCurrentRadialWeaponInaccuracyRotation() const
{
	//do some kind of parameterized function here like a spirograph of some sort
	//I messed around with this website: https://graphsketch.com/parametric.php
	//And this one was a bit helpful too: http://www.geom.uiuc.edu/docs/reference/CRC-formulas/node34.html
	//x(t) = 1*cos(t) - 2 * cos(5*t)
	//y(t) = 1*sin(t) - 2 * sin(5*t)

	float X = FMath::Cos(RadialWeaponInaccuracyFuncParamTime) - 2.f * FMath::Cos(5.f * RadialWeaponInaccuracyFuncParamTime);
	float Y = FMath::Sin(RadialWeaponInaccuracyFuncParamTime) - 2.f * FMath::Sin(5.f * RadialWeaponInaccuracyFuncParamTime);

	return FRotator(Y, X, 0.f) * GetCurrentRadialWeaponInaccuracyMagnitude();
}

FORCEINLINE_DEBUGGABLE float UWRCharacterHandControlWeapon::GetCurrentRadialWeaponInaccuracyMagnitude() const
{
	return FMath::Min(MaxRadialWeaponInaccuracyMagnitude,
		RadialWeaponInaccuracyBlend.GetBlendedValue() + ConstantRadialWeaponInaccuracyBlend.GetBlendedValue());
}
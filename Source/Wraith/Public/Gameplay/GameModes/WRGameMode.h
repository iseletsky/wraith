#pragma once
#include "LGGameMode.h"
#include "WRGameMode.generated.h"

UCLASS(minimalapi)
class AWRGameMode : public ALGGameMode
{
	GENERATED_BODY()
public:
	AWRGameMode(const FObjectInitializer& ObjectInitializer);
};

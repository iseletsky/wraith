#pragma once

#include "WRDamageType.generated.h"

UCLASS()
class WRAITH_API UWRDamageType : public UDamageType
{
	GENERATED_BODY()

public:
	UWRDamageType(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Damage", meta = (WorldContext = "WorldContextObject"))
	bool ApplyDamage(const UObject* WorldContextObject,
		AActor* PointDamagedActor,
		float DamageMultiplier,
		float RadiusMultiplier,
		FVector const& PointHitFromDirection,
		FHitResult const& PointHitInfo, 
		AController* EventInstigator,
		AActor* DamageCauser,
		ECollisionChannel RadialDamagePreventionChannel = /*COLLISION_TRACE_WEAPONNOCHARACTER*/ ECC_GameTraceChannel6) const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Damage")
	void ApplyPointDamage(AActor* DamagedActor,
		float DamageMultiplier,
		FVector const& HitFromDirection,
		FHitResult const& HitInfo,
		AController* EventInstigator,
		AActor* DamageCauser) const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Damage", meta = (WorldContext = "WorldContextObject"))
	bool ApplyRadialDamage(const UObject* WorldContextObject,
		float DamageMultiplier,
		float RadiusMultiplier,
		const FVector& Origin,
		AActor* DamageCauser,
		AController* InstigatedByController,
		ECollisionChannel DamagePreventionChannel = /*COLLISION_TRACE_WEAPONNOCHARACTER*/ ECC_GameTraceChannel6) const;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage")
	bool bDamageEnabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage")
	float MinDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage")
	float MaxDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Splash Damage")
	bool bSplashDamageEnabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Splash Damage")
	float MinSplashDamageInner;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Splash Damage")
	float MaxSplashDamageInner;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Splash Damage")
	float MinSplashDamageOuter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Splash Damage")
	float MaxSplashDamageOuter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Splash Damage")
	float SplashInnerRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Splash Damage")
	float SplashOuterRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Splash Damage")
	float SplashDamageFalloff;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Stopping Power")
	float MinStoppingPower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Stopping Power")
	float MaxStoppingPower;
};
#pragma once
#include "WRImpactType.generated.h"

class AWRImpactEffect;

/**
 * Similar to DamageType.
 * Impact type contains info about what effects should happen when a projectile or hitscan trace impacts a surface.
 *
 * ImpactTypes are never instanced and should be treated as immutable data holders with static code
 * functionality.  They should never be stateful.
 */
UCLASS(const, Blueprintable, BlueprintType)
class UWRImpactType : public UObject
{
	GENERATED_BODY()

public:
	//Mapping of surface name to the effect to spawn
	//NONE name is default
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects")
	TMap<FName, TSubclassOf<AWRImpactEffect>> ImpactSurfaceToEffectClass;

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Effects")
	AWRImpactEffect * SpawnImpactEffects(UWorld* World,
		const FHitResult& Hit,
		const FVector& ImpactVelocity,
		UPrimitiveComponent* ImpactComp = NULL) const;
};




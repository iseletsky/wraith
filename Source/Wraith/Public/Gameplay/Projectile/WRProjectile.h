#pragma once

#include "AEResetInterface.h"
#include "WRProjectile.generated.h"

class UWRDamageType;
class UWRImpactType;

UCLASS(Blueprintable, Abstract, notplaceable/*, meta = (ChildCanTick)*/)
class AWRProjectile : public AActor, public IAEResetInterface
{
	GENERATED_BODY()

public:
	AWRProjectile(const FObjectInitializer& ObjectInitializer);

	virtual void Tick(float DeltaTime) override;

	virtual void Reset_Implementation() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Projectile")
	class UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Projectile")
	USphereComponent* ProjectileCollision;

	UPROPERTY(BlueprintReadWrite, Category = "Creator")
	AActor * Creator;

	UFUNCTION(BlueprintCallable, Category = "Creator")
	bool GetCanHitCreator() const;

	/**
	Enables creator being able to be hit.
	GetCanHitCreator() will still return false until next frame when this is called as a slight hack for some edge cases with spawning.
	Otherwise it's possible for the projectile to collide with creator right when being spawned without giving it a chance to properly fly.
	This will be ignored if the next game tick still has a CollidingCreatorComponent.
	*/
	UFUNCTION(BlueprintCallable, Category = "Creator")
	void EnableCanHitCreator();

protected:
	/**
	this should be false initially when the projectile spawns
	there are a few other conditions for when the creator can be hit that are checked in CheckIgnoreHitCreator()
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Creator")
	uint32 bCanHitCreator:1;

	/**
	As a slight hack for random edge cases, bCanHitCreator shouldn't be set to false immediately but in the next frame.
	*/
	uint32 enableCanHitCreatorTickCountdown;

	/**
	Set to true if the projectile has ever overlapped with the creator.
	If false, no need to do any of the logic for waiting to stop overlapping creator before no longer ignoring creator.
	*/
	uint32 bDidEverOverlapWithCreator:1;

public:	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Creator")
	bool ShouldHitCreator(const FVector& ImpactVelocity);

	/**
	The currently colliding instigator's collision component if any
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Creator")
	UPrimitiveComponent * CollidingCreatorComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float DamageMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float ExplosionRadiusMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	TSubclassOf<UWRDamageType> DamageType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	TSubclassOf<UWRImpactType> ImpactType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	TSubclassOf<UWRImpactType> BounceImpactType;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	void OnProjectileStop(const FHitResult& Hit);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	void OnProjectileBounce(const struct FHitResult& ImpactResult, const FVector& ImpactVelocity);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
	/**
	Called by bounce and stop to let the projectile process some hit actor.
	Usually OtherActor and OtherComp can be retreived from Hit,
	but sometimes the sweep result from overlap contains data about this actor instead, so OtherActor and OtherComp are passed from the overlap call.
	Returns false if the hit is ignored.
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	void OnProjectileHit(const FHitResult& Hit, 
		const FVector& ImpactVelocity, 
		bool bDidBounce = false, 
		AActor* OtherActor = NULL, 
		UPrimitiveComponent* OtherComp = NULL);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	void SpawnImpactEffects(const FHitResult& Hit, 
		const FVector& ImpactVelocity, 
		bool bDidBounce = false, 
		AActor* OtherActor = NULL, 
		UPrimitiveComponent* OtherComp = NULL);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	void DealDamage(const FHitResult& Hit, 
		const FVector& ImpactVelocity, 
		bool bDidBounce = false, 
		AActor* OtherActor = NULL, 
		UPrimitiveComponent* OtherComp = NULL);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	bool ShouldDestroyOnHit(const FHitResult& Hit, 
		const FVector& ImpactVelocity, 
		bool bDidBounce = false, 
		AActor* OtherActor = NULL, 
		UPrimitiveComponent* OtherComp = NULL);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Projectile")
	void SetSpeed(float Speed);

	/**
	Number of times the projectile bounced
	*/
	UPROPERTY(BlueprintReadWrite, Category = "Projectile")
	int32 BounceNum;

private:
	bool bOnOverlapReentrancyGuard;
};

FORCEINLINE_DEBUGGABLE bool AWRProjectile::GetCanHitCreator() const
{
	return bCanHitCreator;
}

FORCEINLINE_DEBUGGABLE void AWRProjectile::EnableCanHitCreator()
{
	enableCanHitCreatorTickCountdown = 2;
}
#pragma once

#include "AEResetInterface.h"
#include "WRImpactEffect.generated.h"

UCLASS()
class AWRImpactEffect : public ADecalActor, public IAEResetInterface
{
	GENERATED_BODY()

public:
	AWRImpactEffect(const FObjectInitializer& ObjectInitializer);

	virtual void Reset_Implementation() override;

	/**
	Spawns the effect in the passed in world.
	Usually you call this on the class default object.
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Effects")
	AWRImpactEffect * SpawnEffect(UWorld* World, 
		const FTransform& InTransform,
		const FVector& ImpactVelocity,
		UPrimitiveComponent* HitComp = NULL, 
		FName HitBone = FName()) const;
};
#pragma once

#include "WRDebrisEffect.h"
#include "WRShellCasing.generated.h"

UCLASS(Blueprintable, meta = (ChildCanTick))
class AWRShellCasing : public AWRDebrisEffect
{
    GENERATED_BODY()

public:
    AWRShellCasing(const FObjectInitializer& ObjectInitializer);    
};
#pragma once

#include "AEResetInterface.h"
#include "WRDebrisEffect.generated.h"

UCLASS(Blueprintable, Abstract, meta = (ChildCanTick))
class AWRDebrisEffect : public AActor, public IAEResetInterface
{
    GENERATED_BODY()

public:
    AWRDebrisEffect(const FObjectInitializer& ObjectInitializer);

    virtual void Reset_Implementation() override;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
    UStaticMeshComponent * Mesh;

    //TODO: I will eventually figure out how I want to handle impact sounds
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Impact)
    USoundCue * DefaultImpactSound;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Impact)
    USoundAttenuation * DefaultImpactSoundAttenuation;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Impact)
    void OnImpact(UPrimitiveComponent* OverlappedComp, AActor * OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult & Hit);
};
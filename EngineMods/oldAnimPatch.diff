From bbbae17e0138c4a0e323456d817bca52cbdc7cb0 Mon Sep 17 00:00:00 2001
From: Ilya <iseletsky@gmail.com>
Date: Wed, 16 Jul 2014 19:36:18 -0700
Subject: [PATCH] Added new animation related changes.

---
 .../Classes/AnimGraphNode_CopyBoneFromMesh.h       |  26 +++++
 .../Private/AnimGraphNode_CopyBoneFromMesh.cpp     |  41 ++++++++
 .../BoneControllers/AnimNode_CopyBoneFromMesh.h    |  55 ++++++++++
 .../Animation/BoneControllers/AnimNode_TwoBoneIK.h |  16 +++
 .../Engine/Private/Animation/AnimMontage.cpp       |  41 +++++---
 .../Animation/AnimNode_CopyBoneFromMesh.cpp        | 112 +++++++++++++++++++++
 .../Private/Animation/AnimNode_TwoBoneIK.cpp       |  58 ++++++++++-
 7 files changed, 331 insertions(+), 18 deletions(-)
 create mode 100644 Engine/Source/Editor/AnimGraph/Classes/AnimGraphNode_CopyBoneFromMesh.h
 create mode 100644 Engine/Source/Editor/AnimGraph/Private/AnimGraphNode_CopyBoneFromMesh.cpp
 create mode 100644 Engine/Source/Runtime/Engine/Classes/Animation/BoneControllers/AnimNode_CopyBoneFromMesh.h
 create mode 100644 Engine/Source/Runtime/Engine/Private/Animation/AnimNode_CopyBoneFromMesh.cpp

diff --git a/Engine/Source/Editor/AnimGraph/Classes/AnimGraphNode_CopyBoneFromMesh.h b/Engine/Source/Editor/AnimGraph/Classes/AnimGraphNode_CopyBoneFromMesh.h
new file mode 100644
index 0000000..940f5b7
--- /dev/null
+++ b/Engine/Source/Editor/AnimGraph/Classes/AnimGraphNode_CopyBoneFromMesh.h
@@ -0,0 +1,26 @@
+// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
+
+#pragma once
+#include "AnimGraphNode_SkeletalControlBase.h"
+#include "AnimGraphNode_CopyBoneFromMesh.generated.h"
+
+UCLASS(MinimalAPI)
+class UAnimGraphNode_CopyBoneFromMesh : public UAnimGraphNode_SkeletalControlBase
+{
+	GENERATED_UCLASS_BODY()
+
+	UPROPERTY(EditAnywhere, Category=Settings)
+	FAnimNode_CopyBoneFromMesh Node;
+
+public:
+	// UEdGraphNode interface
+	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const OVERRIDE;
+	virtual FString GetNodeNativeTitle(ENodeTitleType::Type TitleType) const OVERRIDE;
+	virtual FString GetTooltip() const OVERRIDE;
+	// End of UEdGraphNode interface
+
+protected:
+	// UAnimGraphNode_SkeletalControlBase interface
+	virtual FText GetControllerDescription() const OVERRIDE;
+	// End of UAnimGraphNode_SkeletalControlBase interface
+};
diff --git a/Engine/Source/Editor/AnimGraph/Private/AnimGraphNode_CopyBoneFromMesh.cpp b/Engine/Source/Editor/AnimGraph/Private/AnimGraphNode_CopyBoneFromMesh.cpp
new file mode 100644
index 0000000..ea09006
--- /dev/null
+++ b/Engine/Source/Editor/AnimGraph/Private/AnimGraphNode_CopyBoneFromMesh.cpp
@@ -0,0 +1,41 @@
+// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
+
+#include "AnimGraphPrivatePCH.h"
+#include "AnimGraphNode_CopyBoneFromMesh.h"
+
+/////////////////////////////////////////////////////
+// UAnimGraphNode_CopyBoneFromMeshSkeletalControl
+
+#define LOCTEXT_NAMESPACE "A3Nodes"
+
+UAnimGraphNode_CopyBoneFromMesh::UAnimGraphNode_CopyBoneFromMesh(const FPostConstructInitializeProperties& PCIP)
+	: Super(PCIP)
+{
+}
+
+FText UAnimGraphNode_CopyBoneFromMesh::GetControllerDescription() const
+{
+	return LOCTEXT("CopyBoneFromMesh", "Copy Bone to Another Mesh's Bone or Socket");
+}
+
+FString UAnimGraphNode_CopyBoneFromMesh::GetTooltip() const
+{
+	return LOCTEXT("AnimGraphNode_CopyBoneFromMesh_Tooltip", "The Copy Bone From Mesh control copies the Transform data or any component of it - i.e. Translation, Rotation, or Scale - from one mesh's bone or socket to another bone on the currently animating mesh.  Be sure that other mesh was animated before this one.").ToString();
+}
+
+FText UAnimGraphNode_CopyBoneFromMesh::GetNodeTitle(ENodeTitleType::Type TitleType) const
+{
+	FFormatNamedArguments Args;
+	Args.Add(TEXT("ControllerDescription"), GetControllerDescription());
+	Args.Add(TEXT("SourceSocketName"), FText::FromName(Node.SourceSocket));
+	Args.Add(TEXT("TargetBoneName"), FText::FromName(Node.TargetBone.BoneName));
+	return FText::Format(LOCTEXT("AnimGraphNode_CopyBoneFromMesh_Title", "{ControllerDescription}\nSource Socket: {SourceSocketName}\nTarget Bone: {TargetBoneName}"), Args);
+}
+
+FString UAnimGraphNode_CopyBoneFromMesh::GetNodeNativeTitle(ENodeTitleType::Type TitleType) const
+{
+	// Do not setup this function for localization, intentionally left unlocalized!
+	return FString::Printf(TEXT("%s\nSource Socket: %s\nTarget Bone: %s"), *GetControllerDescription().ToString(), *Node.SourceSocket.ToString(), *Node.TargetBone.BoneName.ToString());
+}
+
+#undef LOCTEXT_NAMESPACE
diff --git a/Engine/Source/Runtime/Engine/Classes/Animation/BoneControllers/AnimNode_CopyBoneFromMesh.h b/Engine/Source/Runtime/Engine/Classes/Animation/BoneControllers/AnimNode_CopyBoneFromMesh.h
new file mode 100644
index 0000000..60da303
--- /dev/null
+++ b/Engine/Source/Runtime/Engine/Classes/Animation/BoneControllers/AnimNode_CopyBoneFromMesh.h
@@ -0,0 +1,55 @@
+// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
+
+#pragma once
+
+#include "AnimNode_SkeletalControlBase.h"
+#include "AnimNode_CopyBoneFromMesh.generated.h"
+
+/**
+*	Simple controller to copy a bone's transform to a bone or socket in another mesh.
+*/
+
+USTRUCT()
+struct ENGINE_API FAnimNode_CopyBoneFromMesh : public FAnimNode_SkeletalControlBase
+{
+	GENERATED_USTRUCT_BODY()
+
+	/** Name of bone to control. This is the main bone chain to modify from. **/
+	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Copy)
+	FBoneReference TargetBone;
+
+	/** Source Bone or Socket Name to get transform from */
+	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Copy)
+	FName SourceSocket;
+	
+	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Copy, meta = (PinShownByDefault))
+	USkeletalMeshComponent * SourceMesh;
+
+	/** If Translation should be copied */
+	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Copy, meta = (PinShownByDefault))
+	bool bCopyTranslation;
+
+	/** If Rotation should be copied */
+	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Copy, meta = (PinShownByDefault))
+	bool bCopyRotation;
+
+	/** If Scale should be copied */
+	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Copy, meta = (PinShownByDefault))
+	bool bCopyScale;
+
+	FAnimNode_CopyBoneFromMesh();
+
+	// FAnimNode_Base interface
+	virtual void GatherDebugData(FNodeDebugData& DebugData) OVERRIDE;
+	// End of FAnimNode_Base interface
+
+	// FAnimNode_SkeletalControlBase interface
+	virtual void EvaluateBoneTransforms(USkeletalMeshComponent* SkelComp, const FBoneContainer & RequiredBones, FA2CSPose& MeshBases, TArray<FBoneTransform>& OutBoneTransforms) OVERRIDE;
+	virtual bool IsValidToEvaluate(const USkeleton * Skeleton, const FBoneContainer & RequiredBones) OVERRIDE;
+	// End of FAnimNode_SkeletalControlBase interface
+
+private:
+	// FAnimNode_SkeletalControlBase interface
+	virtual void InitializeBoneReferences(const FBoneContainer & RequiredBones) OVERRIDE;
+	// End of FAnimNode_SkeletalControlBase interface
+};
diff --git a/Engine/Source/Runtime/Engine/Classes/Animation/BoneControllers/AnimNode_TwoBoneIK.h b/Engine/Source/Runtime/Engine/Classes/Animation/BoneControllers/AnimNode_TwoBoneIK.h
index 252e6db..5cfc3a7 100644
--- a/Engine/Source/Runtime/Engine/Classes/Animation/BoneControllers/AnimNode_TwoBoneIK.h
+++ b/Engine/Source/Runtime/Engine/Classes/Animation/BoneControllers/AnimNode_TwoBoneIK.h
@@ -30,6 +30,22 @@ struct ENGINE_API FAnimNode_TwoBoneIK : public FAnimNode_SkeletalControlBase
 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=IK)
 	FVector2D StretchLimits;
 
+	/** The socket or bone name whose position and rotation relative to the end bone are used as the destination */
+	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EndEffector)
+	FName EffectorOffsetSocketName;
+
+	/** If using EffectorOffsetSocketName, this flips the relative rotation around the x axis to give correct results */
+	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EndEffector)
+	bool EffectorOffsetSocketRotationFlipX;
+
+	/** If using EffectorOffsetSocketName, this flips the relative rotation around the y axis to give correct results */
+	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EndEffector)
+	bool EffectorOffsetSocketRotationFlipY;
+
+	/** If using EffectorOffsetSocketName, this flips the relative rotation around the z axis to give correct results */
+	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EndEffector)
+	bool EffectorOffsetSocketRotationFlipZ;
+
 	/** If EffectorLocationSpace is a bone, this is the bone to use. **/
 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=EndEffector)
 	FName EffectorSpaceBoneName;
diff --git a/Engine/Source/Runtime/Engine/Private/Animation/AnimMontage.cpp b/Engine/Source/Runtime/Engine/Private/Animation/AnimMontage.cpp
index d35b8c3..8f1d6ab 100644
--- a/Engine/Source/Runtime/Engine/Private/Animation/AnimMontage.cpp
+++ b/Engine/Source/Runtime/Engine/Private/Animation/AnimMontage.cpp
@@ -679,30 +679,39 @@ void FAnimMontageInstance::Stop(float BlendOut, bool bInterrupt)
 		bInterrupted = bInterrupt;
 	}
 
-	// It is already stopped, but if BlendOut < BlendTime, that means 
-	// we need to readjust BlendTime, we don't want old animation to blend longer than
-	// new animation
-	if (DesiredWeight == 0.f)
-	{
-		// it is already stopped, but new montage blendtime is shorter than what 
-		// I'm blending out, that means this needs to readjust blendtime
-		// that way we don't accumulate old longer blendtime for newer montage to play
-		if (BlendOut < BlendTime)
+	if (BlendOut >= 0.f)
+	{
+		// It is already stopped, but if BlendOut < BlendTime, that means 
+		// we need to readjust BlendTime, we don't want old animation to blend longer than
+		// new animation
+		if (DesiredWeight == 0.f)
 		{
-			BlendTime = BlendOut;
+			// it is already stopped, but new montage blendtime is shorter than what 
+			// I'm blending out, that means this needs to readjust blendtime
+			// that way we don't accumulate old longer blendtime for newer montage to play
+			if (BlendOut < BlendTime)
+			{
+				BlendTime = BlendOut;
+			}
+
+			return;
 		}
 
-		return;
+		DesiredWeight = 0.f;
+	}
+	else
+	{
+		DesiredWeight = 1.f;
 	}
-
-	DesiredWeight = 0.f;
 
 	if( Montage )
 	{
 		// do not use default Montage->BlendOut  Time
 		// depending on situation, the BlendOut time changes
 		// check where this function gets called and see how we calculate BlendTime
-		BlendTime = BlendOut;
+		BlendTime = BlendOut >= 0.f 
+			? BlendOut
+			: 0.f;
 
 		if (OnMontageBlendingOutStarted.IsBound())
 		{
@@ -710,7 +719,7 @@ void FAnimMontageInstance::Stop(float BlendOut, bool bInterrupt)
 		}
 	}
 
-	if (BlendTime == 0.0f)
+	if (BlendTime <= 0.0f)
 	{
 		bPlaying = false;
 	}
@@ -1051,7 +1060,7 @@ void FAnimMontageInstance::Advance(float DeltaTime, FRootMotionMovementParams &
 							const float BlendOutTime = FMath::Max<float>(Montage->BlendOutTime * DefaultBlendTimeMultiplier, KINDA_SMALL_NUMBER);
 							if( DeltaTimeToEnd <= BlendOutTime )
 							{
-								Stop(DeltaTimeToEnd, false);
+								Stop(Montage->BlendOutTime < 0.f ? Montage->BlendOutTime : DeltaTimeToEnd, false);
 							}
 						}
 
diff --git a/Engine/Source/Runtime/Engine/Private/Animation/AnimNode_CopyBoneFromMesh.cpp b/Engine/Source/Runtime/Engine/Private/Animation/AnimNode_CopyBoneFromMesh.cpp
new file mode 100644
index 0000000..371ba26
--- /dev/null
+++ b/Engine/Source/Runtime/Engine/Private/Animation/AnimNode_CopyBoneFromMesh.cpp
@@ -0,0 +1,112 @@
+// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
+
+#include "EnginePrivate.h"
+
+/////////////////////////////////////////////////////
+// FAnimNode_CopyBoneFromMesh
+
+FAnimNode_CopyBoneFromMesh::FAnimNode_CopyBoneFromMesh()
+	: bCopyTranslation(false)
+	, bCopyRotation(false)
+	, bCopyScale(false)
+{
+}
+
+void FAnimNode_CopyBoneFromMesh::GatherDebugData(FNodeDebugData& DebugData)
+{
+	FString DebugLine = DebugData.GetNodeName(this);
+
+	DebugLine += "(";
+	AddDebugNodeData(DebugLine);
+	DebugLine += FString::Printf(TEXT(" Src: %s Dst: %s)"), *SourceSocket.ToString(), *TargetBone.BoneName.ToString());
+	DebugData.AddDebugItem(DebugLine);
+
+	ComponentPose.GatherDebugData(DebugData);
+}
+
+void FAnimNode_CopyBoneFromMesh::EvaluateBoneTransforms(USkeletalMeshComponent* SkelComp, const FBoneContainer & RequiredBones, FA2CSPose& MeshBases, TArray<FBoneTransform>& OutBoneTransforms)
+{
+	check(OutBoneTransforms.Num() == 0);
+
+	// Pass through if we're not doing anything.
+	if (!bCopyTranslation && !bCopyRotation && !bCopyScale)
+	{
+		return;
+	}
+
+	// Get component space transform for source and current bone.
+	FTransform CurrentBoneTM = MeshBases.GetComponentSpaceTransform(TargetBone.BoneIndex);	
+	FTransform SourceCSTM;
+
+	//if it's an attachment to the currently animating skeleton, get the transform relative to the attachment point
+	//ComponentToWorld on the source mesh will be a frame late so we must get it this way
+	//Otherwise be sure to set the other mesh to update before this one with tick prerequesites.
+	//And if you have a circular dependency, then god help you :D.  That is until there's some way to update animation poses in multiple passes, maybe?
+	if (SourceMesh->AttachParent == SkelComp)
+	{
+		//FTransform SourceMeshSocketCSTM = SourceMesh->GetSocketTransform(SourceSocket, RTS_Component);
+
+		//code is based off the get socket transform code in skeletal mesh component, only that doesn't work here since it's a frame late, I think.  We must use the FA2CSPose object.
+		//I wonder if it's possible to refactor this somehow eventually so the code can be reused
+		//get the offset socket's transform relative to the end bone
+		int32 BoneIndex = SkelComp->GetBoneIndex(SourceMesh->AttachSocketName);
+
+		//transform of the component in our component space
+		FTransform SourceMeshAttachmentCSTM;
+
+		if (BoneIndex != INDEX_NONE)
+		{
+			SourceMeshAttachmentCSTM = MeshBases.GetComponentSpaceTransform(BoneIndex);
+		}
+		else
+		{
+			USkeletalMeshSocket const* const Socket = SkelComp->GetSocketByName(SourceMesh->AttachSocketName);
+			BoneIndex = SkelComp->GetBoneIndex(Socket->BoneName);
+
+			if (Socket && BoneIndex != INDEX_NONE)
+			{
+				SourceMeshAttachmentCSTM = (Socket->GetSocketLocalTransform() * MeshBases.GetComponentSpaceTransform(BoneIndex));
+			}
+		}
+		
+		SourceCSTM =  
+			SourceMesh->GetSocketTransform(SourceSocket, RTS_Component)
+			* FTransform(SourceMesh->RelativeRotation, SourceMesh->RelativeLocation, SourceMesh->RelativeScale3D) 
+			* SourceMeshAttachmentCSTM;
+	}
+	else
+	{
+		//convert the source bone transform from its component space to this component space
+		SourceCSTM = SourceMesh->GetSocketTransform(SourceSocket, RTS_World) * SkelComp->ComponentToWorld.InverseSafe();
+	}
+	
+	// Copy individual components
+	if (bCopyTranslation)
+	{
+		CurrentBoneTM.SetTranslation(SourceCSTM.GetTranslation());
+	}
+
+	if (bCopyRotation)
+	{
+		CurrentBoneTM.SetRotation(SourceCSTM.GetRotation());
+	}
+
+	if (bCopyScale)
+	{
+		CurrentBoneTM.SetScale3D(SourceCSTM.GetScale3D());
+	}
+
+	// Output new transform for current bone.
+	OutBoneTransforms.Add(FBoneTransform(TargetBone.BoneIndex, CurrentBoneTM));
+}
+
+bool FAnimNode_CopyBoneFromMesh::IsValidToEvaluate(const USkeleton * Skeleton, const FBoneContainer & RequiredBones)
+{
+	// if both bones are valid
+	return (TargetBone.IsValid(RequiredBones) && SourceMesh && SourceMesh->DoesSocketExist(SourceSocket));
+}
+
+void FAnimNode_CopyBoneFromMesh::InitializeBoneReferences(const FBoneContainer & RequiredBones)
+{
+	TargetBone.Initialize(RequiredBones);
+}
\ No newline at end of file
diff --git a/Engine/Source/Runtime/Engine/Private/Animation/AnimNode_TwoBoneIK.cpp b/Engine/Source/Runtime/Engine/Private/Animation/AnimNode_TwoBoneIK.cpp
index e0f8e9b..06de5c5 100644
--- a/Engine/Source/Runtime/Engine/Private/Animation/AnimNode_TwoBoneIK.cpp
+++ b/Engine/Source/Runtime/Engine/Private/Animation/AnimNode_TwoBoneIK.cpp
@@ -80,10 +80,58 @@ void FAnimNode_TwoBoneIK::EvaluateBoneTransforms(USkeletalMeshComponent* SkelCom
 	const FVector InitialJointPos = LowerLimbCSTransform.GetTranslation();
 	const FVector InitialEndPos = EndBoneCSTransform.GetTranslation();
 
+	// Apply Offset Socket Transform
+	FTransform OffsetSocketRelativeTransform;
+	FVector FinalEffectorLocation = EffectorLocation;
+
+	if (EffectorOffsetSocketName != NAME_None)
+	{
+		//code is based off the get socket transform code in skeletal mesh component, only that doesn't work here since it's a frame late, I think.  We must use the FA2CSPose object.
+		//I wonder if it's possible to refactor this somehow eventually so the code can be reused
+		//get the offset socket's transform relative to the end bone
+		{
+			int32 BoneIndex = SkelComp->GetBoneIndex(EffectorOffsetSocketName);
+
+			if (BoneIndex != INDEX_NONE)
+			{
+				OffsetSocketRelativeTransform = MeshBases.GetComponentSpaceTransform(BoneIndex).GetRelativeTransform(EndBoneCSTransform);
+			}
+			else
+			{
+				USkeletalMeshSocket const* const Socket = SkelComp->GetSocketByName(EffectorOffsetSocketName);
+				BoneIndex = SkelComp->GetBoneIndex(Socket->BoneName);
+
+				if (Socket && BoneIndex != INDEX_NONE)
+				{
+					OffsetSocketRelativeTransform = (Socket->GetSocketLocalTransform() * MeshBases.GetComponentSpaceTransform(BoneIndex)).GetRelativeTransform(EndBoneCSTransform);
+				}
+			}
+		}
+		
+		//Flip the rotation
+		if (EffectorOffsetSocketRotationFlipX || EffectorOffsetSocketRotationFlipY || EffectorOffsetSocketRotationFlipZ)
+		{
+			FQuat QuatFlip = FQuat(
+				EffectorOffsetSocketRotationFlipX ? 1.0 : 0.0,
+				EffectorOffsetSocketRotationFlipY ? 1.0 : 0.0,
+				EffectorOffsetSocketRotationFlipZ ? 1.0 : 0.0,
+				0.0);
+
+			QuatFlip.Normalize();
+			OffsetSocketRelativeTransform.SetRotation(OffsetSocketRelativeTransform.GetRotation() * QuatFlip);
+		}
+		
+		//offset the position to account for the bone rotation that will be applied later
+		FVector posDir = OffsetSocketRelativeTransform.GetTranslation();
+		posDir = OffsetSocketRelativeTransform.GetRotation() * posDir;
+
+		FinalEffectorLocation -= posDir;
+	}
+
 	// Transform EffectorLocation from EffectorLocationSpace to ComponentSpace.
-	FTransform EffectorTransform(EffectorLocation);
+	FTransform EffectorTransform(FinalEffectorLocation);
 	FAnimationRuntime::ConvertBoneSpaceTransformToCS(SkelComp, MeshBases, EffectorTransform, EffectorSpaceBoneIndex, EffectorLocationSpace);
-
+	
 	// This is our reach goal.
 	FVector DesiredPos = EffectorTransform.GetTranslation();
 	FVector DesiredDelta = DesiredPos - RootPos;
@@ -265,6 +313,12 @@ void FAnimNode_TwoBoneIK::EvaluateBoneTransforms(USkeletalMeshComponent* SkelCom
 			EndBoneCSTransform = EndBoneLocalTransform * LowerLimbCSTransform;
 		}
 
+		//apply relative rotation of offset socket
+		if (EffectorOffsetSocketName != NAME_None)
+		{	
+			EndBoneCSTransform.ConcatenateRotation(OffsetSocketRelativeTransform.GetRotation());
+		}
+
 		// Set correct location for end bone.
 		EndBoneCSTransform.SetTranslation(OutEndPos);
 
-- 
1.8.3.msysgit.0

